; Skool Daze disassembly
; https://skoolkit.ca
;
; Copyright 1984 Microsphere Computer Services Ltd (Skool Daze)
; Copyright 2008-2015, 2017-2019 Richard Dymond (this disassembly)

  ORG 24573

  JP START

; Scroll the screen right one column
;
; Used by the routine at RSCROLL8.
RSCROLL:
  EXX
  LD HL,22558             ; Point HL' and DE' at the rightmost end of the top
  LD DE,22559             ; row of attribute bytes on the screen
  LD BC,31                ; We need to scroll 31 attribute bytes at a time
  EXX
  LD HL,16414             ; Point HL and DE at the rightmost end of the top row
  LD DE,16415             ; of pixels on the screen
  LD BC,31                ; We need to scroll 31 display file bytes at a time
  LD A,21                 ; There are 21 rows of character squares to scroll
RSCROLL_0:
  EX AF,AF'
RSCROLL_1:
  LD A,8                  ; Each row of character squares contains 8 rows of
                          ; pixels
RSCROLL_2:
  PUSH HL
  PUSH DE
  LDDR                    ; Scroll a row of pixels right one column
  POP DE
  POP HL
  LD C,31                 ; BC=31
  INC H                   ; Point HL and DE at the next row of pixels
  INC D                   ;
  DEC A                   ; Next row in this block of 8x32
  JR NZ,RSCROLL_2         ; Jump back until this 8x32 pixel block has been
                          ; scrolled
  EXX
  LDDR                    ; Scroll a row of attribute bytes right one column
  LD C,63                 ; Point HL' and DE' at the rightmost end of the next
  ADD HL,BC               ; row of attribute bytes on the screen
  EX DE,HL                ;
  ADD HL,BC               ;
  EX DE,HL                ;
  LD C,31                 ; BC'=31
  EXX
  LD A,32                 ; Point HL and DE at the rightmost end of the top row
  ADD A,E                 ; of the next 8x32 block of pixels
  LD E,A                  ;
  LD A,32                 ;
  ADD A,L                 ;
  LD L,A                  ;
  EX AF,AF'
  DEC A                   ; Next row of character squares
  JR Z,RSCROLL_3          ; Exit this loop if all 21 rows have been scrolled
                          ; right
  CP 13                   ; Have we reached the middle third of the screen?
  JR Z,RSCROLL_0          ; Jump if so
  CP 5                    ; Have we reached the bottom third of the screen?
  JR Z,RSCROLL_0          ; Jump if so
  EX AF,AF'
  LD A,H                  ; Point HL and DE at the rightmost end of the top row
  SUB 8                   ; of the next 8x32 block of pixels
  LD H,A                  ;
  LD D,A                  ;
  JR RSCROLL_1            ; Jump back to deal with the next row of character
                          ; squares
; The 21 rows of character squares have been scrolled one place to the right.
; Now the leftmost column needs to be filled.
RSCROLL_3:
  EXX
  LD HL,22528             ; Point HL' and DE' at the attribute file and display
  LD DE,16384             ; file addresses for the top left of the screen, and
  INC C                   ; set BC'=32; however, these values are not used
  EXX
  LD A,(LEFTCOL)          ; A=leftmost column of the skool on-screen (0-64)
  DEC A                   ; We are scrolling one column right
  LD (LEFTCOL),A          ;
; This entry point is used by the routine at LSCROLL with A holding the
; x-coordinate of the rightmost column of the skool on-screen (31-95).
RSCROLL_4:
  LD E,A                  ; E=X (x-coordinate of the far left or far right
                          ; column of the skool currently on-screen)
  LD D,152                ; D=Y (this is the y-coordinate of the top row of the
                          ; screen)
  LD B,21                 ; There are 21 rows on the screen
RSCROLL_5:
  PUSH DE
  PUSH BC
  CALL PRINTTILE          ; Print the character square at (X,Y)
  POP BC
  POP DE
  LD A,H                  ; Point HL at the attribute file address
XRSCROLLA:
  RRCA                    ; corresponding to the skool coordinates (X,Y)
  RRCA                    ;
  RRCA                    ;
  ADD A,79                ;
  LD H,A                  ;
  SET 7,E                 ; Point DE at the attribute byte for the skool
                          ; coordinates (X,Y)
  LD A,(DE)               ; Pick the attribute byte up in A
  RES 7,E                 ; Restore the skool coordinates (X,Y) to DE
  LD (HL),A               ; Poke the attribute byte onto the screen
  INC D                   ; Next row (Y) of the screen
  DJNZ RSCROLL_5          ; Jump back until all 21 rows are done
  RET

; Unused
;
; Remnants of an old version of the routine at RSCROLL.
XRSCROLL:
  SET 7,E                 ; Point DE at the attribute byte for the skool
                          ; coordinates (X,Y)
  LD A,(DE)               ; Pick the attribute byte up in A
  RES 7,E                 ; Restore the skool coordinates (X,Y) to DE
  EXX
  LD (HL),A               ; Poke the attribute byte onto the screen
  LD A,C
  ADD A,E
  LD E,A
  JR C,XRSCROLL_0
  LD A,D
  SUB 8
  LD D,A
XRSCROLL_0:
  ADD HL,BC
  EXX
  EX DE,HL
  INC H
  DEC C                   ; One more row done
  POP DE
  JR NZ,XRSCROLLA         ; Jump back until all 21 rows are done
  RET
  DEFS 7

; Scroll the screen left one column
;
; Used by the routine at LSCROLL8.
LSCROLL:
  EXX
  LD HL,22529             ; Point HL' and DE' at the leftmost end of the top
  LD DE,22528             ; row of attribute bytes on the screen
  LD BC,31                ; We need to scroll 31 attribute bytes at a time
  EXX
  LD HL,16385             ; Point HL and DE at the leftmost end of the top row
  LD DE,16384             ; of pixels on the screen
  LD BC,31                ; We need to scroll 31 display file bytes at a time
  LD A,21                 ; There are 21 rows of character squares to scroll
LSCROLL_0:
  EX AF,AF'
LSCROLL_1:
  LD A,8                  ; Each row of character squares contains 8 rows of
                          ; pixels
LSCROLL_2:
  PUSH HL
  PUSH DE
  LDIR                    ; Scroll a row of pixels left one column
  POP DE
  POP HL
  INC D                   ; Point HL and DE at the next row of pixels
  INC H                   ;
  LD C,31                 ; BC=31
  DEC A                   ; Next row in this block of 8x32
  JR NZ,LSCROLL_2         ; Jump back until this 8x32 pixel block has been
                          ; scrolled
  EXX
  LDIR                    ; Scroll a row of attribute bytes left one column
  INC HL                  ; Point HL' and DE' at the leftmost end of the next
  INC DE                  ; row of attribute bytes on the screen
  LD C,31                 ; BC'=31
  EXX
  LD A,32                 ; Point HL and DE at the leftmost end of the top row
  ADD A,E                 ; of the next 8x32 block of pixels
  LD E,A                  ;
  LD A,32                 ;
  ADD A,L                 ;
  LD L,A                  ;
  EX AF,AF'
  DEC A                   ; Next row of character squares
  JR Z,LSCROLL_3          ; Exit this loop if all 21 rows have been scrolled
                          ; left
  CP 5                    ; Have we reached the bottom third of the screen?
  JR Z,LSCROLL_0          ; Jump if so
  CP 13                   ; Have we reached the middle third of the screen?
  JR Z,LSCROLL_0          ; Jump if so
  EX AF,AF'
  LD A,H                  ; Point HL and DE at the leftmost end of the top row
  SUB 8                   ; of the next 8x32 block of pixels
  LD H,A                  ;
  LD D,A                  ;
  JR LSCROLL_1            ; Jump back to deal with the next row of character
                          ; squares
; The 21 rows of character squares have been scrolled one place to the left.
; Now the rightmost column needs to be filled.
LSCROLL_3:
  EXX
  LD HL,22559             ; Point HL' and DE' at the attribute file and display
  LD DE,16415             ; file addresses for the top right of the screen, and
  INC C                   ; set BC'=32; however, these values are not used
  EXX
  LD A,(LEFTCOL)          ; A=leftmost column of the skool on-screen (0-64)
  INC A                   ; We are scrolling one column left
  LD (LEFTCOL),A          ;
  ADD A,C                 ; A=rightmost column of the skool on-screen (31-95)
  JP RSCROLL_4            ; Fill in the rightmost column

; Unused
;
; Probably remnants of an old version of the routine at LSCROLL.
  LD A,30
  LD (LEFTCOL),A          ; Set the new leftmost column of the skool on-screen
  LD B,32                 ; There are 32 character squares in each row of the
                          ; screen

; Print a tile (unused)
;
; Used by the (unused) routine at XUPDATE.
;
; D Skool y-coordinate (152-172)
; E Skool x-coordinate (0-95)
XPRINTTILE:
  LD A,(LEFTCOL)          ; A=leftmost column of the skool on-screen (0-64)
  CP E                    ; Return if E corresponds to a column that is
  JR Z,PRINTTILE          ; currently off-screen; otherwise continue into
  RET NC                  ; PRINTTILE
  ADD A,31                ;
  CP E                    ;
  RET C                   ;

; Print a tile
;
; Used by the routines at RSCROLL and UPDATESCR. Copies a tile of the skool
; into the back buffer at BACKBUF, superimposes character sprite tiles as
; appropriate, and then copies the resultant tile to the screen.
;
; D Skool y-coordinate (152-172)
; E Skool x-coordinate (0-95)
PRINTTILE:
  LD A,E                  ; A=skool x-coordinate (0-95)
  AND 96                  ; Now H is 128 if 0<=E<32; 136 if 32<=E<64; or 144 if
  RRCA                    ; 64<=E<96
  INC A                   ;
  RRCA                    ;
  LD H,A                  ;
  LD A,(DE)               ; A=skool UDG reference for the character square at
                          ; (E,D)
  PUSH DE
  LD L,A                  ; HL=base address of the UDG bytes for the character
                          ; square at (E,D)
  LD BC,2048              ; B=8, C=0
  LD DE,BACKBUF           ; Copy the 8 bytes of the skool UDG into the back
SKOOLUDG:
  LD A,(HL)               ; buffer at BACKBUF
  LD (DE),A               ;
  INC H                   ;
  INC E                   ;
  DJNZ SKOOLUDG           ;
  NOP
  POP DE                  ; Restore the skool coordinates to DE
  LD B,21                 ; There are 21 characters to consider
  LD H,152                ; 152=little boy no. 1
; Now we enter a loop that checks which part (if any) of each character's
; sprite appears at the skool coordinates in DE.
PRINTTILE_0:
  LD L,98                 ; Byte 98 of the character's buffer holds his
                          ; x-coordinate
  LD A,E                  ; A=skool x-coordinate (0-95)
  SUB (HL)                ; Subtract the character's x-coordinate
  DEC HL                  ; Byte 97 of the character's buffer holds his
                          ; y-coordinate
  JR C,PRINTTILE_3        ; Jump if no part of the character's sprite appears
  CP 3                    ; at this x-coordinate
  JR NC,PRINTTILE_3       ;
  ADD A,A                 ; Now C is 0, 4 or 8, corresponding to the column
  ADD A,A                 ; (left, middle or right) of the character's sprite
  LD C,A                  ; that aligns with x-coordinate E
  LD A,D                  ; A=skool y-coordinate (152-172)
  SUB (HL)                ; Subtract the character's y-coordinate
  JR C,PRINTTILE_3        ; Jump if no part of the character's sprite appears
  CP 4                    ; at this y-coordinate
  JR NC,PRINTTILE_3       ;
  ADD A,C                 ; A=0-11, corresponding to the sprite tile that
                          ; appears at (E,D)
  EXX
  ADD A,173
  LD H,A                  ; H'=173-184
  EXX
  LD L,96                 ; Pick up the character's animatory state in A
  LD A,(HL)               ;
  EXX
  LD L,A                  ; L'=character's animatory state
  LD A,(HL)               ; A=sprite tile UDG reference
  AND A                   ; Is it the blank UDG?
  JR Z,PRINTTILE_2        ; Jump if so
  CALL UDGADDR            ; Get the base address of the sprite tile UDG bytes
                          ; in HL'
; The following inner loop superimposes the sprite tile onto the tile built up
; so far in the back buffer at BACKBUF (which was originally populated with the
; relevant skool UDG).
  LD B,8                  ; There are 8 bytes in the UDG
  LD DE,BACKBUF
PRINTTILE_1:
  LD A,(DE)               ; A=back buffer UDG byte
  OR (HL)                 ; OR in the sprite tile UDG byte
  INC H
  AND (HL)                ; AND in the sprite tile UDG mask
  INC H
  LD (DE),A
  INC E
  DJNZ PRINTTILE_1        ; Jump back until the UDG is done
PRINTTILE_2:
  EXX
PRINTTILE_3:
  INC H                   ; Next character
  DJNZ PRINTTILE_0        ; Jump back to consider the next character
; The display tile (consisting of the skool UDG with all relevant sprite tiles
; superimposed) has been built. Figure out where in the display file it should
; be printed.
  LD HL,LEFTCOL           ; LEFTCOL holds the leftmost column of the skool
                          ; on-screen (0-64)
  LD A,D                  ; A=skool y-coordinate (152-172)
  AND 7                   ; Set C to the LSB of the display file address
  RRCA                    ; corresponding to the screen coordinates (0,D-152)
  RRCA                    ;
  RRCA                    ;
  LD C,A                  ;
  LD A,E                  ; A=screen x-coordinate (0-31)
  SUB (HL)                ;
  ADD A,C                 ; Set L to the LSB of the display file address
  LD L,A                  ; corresponding to the skool coordinates (E,D)
  LD A,D                  ; A=screen y-coordinate (0-20)
  SUB 152                 ;
  AND 24                  ; Set H to the MSB of the display file address
  ADD A,64                ; corresponding to the skool coordinates (E,D)
  LD H,A                  ;
; Having computed the appropriate display file address in HL, copy the tile
; from the back buffer to the screen.
  LD B,8                  ; Copy the tile to the screen
  LD DE,BACKBUF           ;
PRINTTILE_4:
  LD A,(DE)               ;
  LD (HL),A               ;
  INC H                   ;
  INC E                   ;
  DJNZ PRINTTILE_4        ;
  RET

; Unused
  DEFS 9

; Collect one byte from a command list
;
; Used by the routines at MVCHARS, MVTILL, GOTO, WALKABOUT, SIGRAISE, ADDR2BUF
; and BWWRITE. Collects a byte from a character's current command list and
; returns it in A.
;
; H Character number (152-169)
GETPARAM:
  PUSH HL                 ; Save the character number temporarily
  LD L,117                ; Bytes 117 and 118 of the character's buffer hold
                          ; the address reached in the command list
  LD A,(HL)               ; Pick up the LSB of this address
  INC (HL)                ; Move along one byte in the command list
  JR NZ,GETPARAM_1        ; Jump unless the LSB flipped over to zero
  NOP
  NOP
  INC L                   ; L=118
  INC (HL)                ; Increment the MSB
  LD H,(HL)               ; Set H to the old MSB
  DEC H                   ;
GETPARAM_0:
  LD L,A                  ; HL=address of the byte to be picked up from the
                          ; command list
  LD A,(HL)               ; Pick up the byte in A
  POP HL                  ; Restore the character number to H
  RET
GETPARAM_1:
  INC L                   ; L=118
  LD H,(HL)               ; HL=address of the byte to be picked up from the
                          ; command list
  JR GETPARAM_0           ; Jump back to pick up the byte in A

; Get a random number
;
; Used by the routines at DECIDEMV, MVTILL, RANDDEST, CATTY, WRITEBRD,
; GIVELINES, PREPQA, SWOTLINES, GOTOPAGE, CLSNOERIC, TEACHERIC, GENCOMBO and
; GETLESSON. Returns with a random number in A.
GETRANDOM:
  PUSH HL
  LD HL,(RANDSEED)        ; RANDSEED holds the random number seed
  LD A,(23672)            ; Pick up the LSB of the system variable FRAMES
  INC HL                  ; Add 257 to the seed
  INC H                   ;
  XOR (HL)                ; A=random number
  LD (RANDSEED),HL        ; Store the new seed
  POP HL
  RET

; Update a character's animatory state and location and update the SRB
;
; Used by many routines. Sets the new animatory state and location of a
; character, and updates the screen refresh buffer (SRB) accordingly.
;
; A New animatory state
; D New y-coordinate (154-169)
; E New x-coordinate (0-95)
; H Character number (152-172)
UPDATEAS:
  LD L,96                 ; Point HL at byte 96 of the character's buffer
  LD (HL),A               ; Set the new animatory state
  LD B,A                  ; Store the new animatory state in B for later
  INC L                   ; L=97
  LD (HL),D               ; Set the new y-coordinate
  INC L                   ; L=98
  LD (HL),E               ; Set the new x-coordinate
; This entry point is used by the routine at UPDATESRB.
UPDATEAS_0:
  LD A,(LEFTCOL)          ; A=leftmost column of the skool on-screen (0-64)
  SUB 3                   ; Is the far left of the skool in view?
  JR C,UPDATEAS_1         ; Jump if so
  CP E                    ; Is the character off-screen to the left?
  RET NC                  ; Return if so (SRB does not need updating)
UPDATEAS_1:
  ADD A,34
  CP E                    ; Is the character off-screen to the right?
  RET C                   ; Return if so (SRB does not need updating)
  SUB 32                  ; A=character's screen x-coordinate: -2<=A<=31
  CPL                     ;
  ADD A,E                 ;
  PUSH HL                 ; Save the character number
  LD L,B                  ; L=character's new animatory state
  LD H,173                ; Pages 173-176 store UDG references for the left
                          ; column of the sprites
  BIT 7,A                 ; Is the left column of the sprite on-screen?
  JR Z,UPDATEAS_2         ; Jump if so
  LD H,177                ; Pages 177-180 store UDG references for the middle
                          ; column of the sprites
  INC A                   ; -1<=A<=0
  BIT 7,A                 ; Is the middle column of the sprite on-screen at the
                          ; left?
  JR Z,UPDATEAS_2         ; Jump if so
  INC A                   ; A=0
  LD H,181                ; Pages 181-184 store UDG references for the right
                          ; column of the sprites
; At this point, A holds the leftmost column of the screen (0-31) occupied by
; the character's sprite, and H holds the page number (173, 177, 181)
; corresponding to the leftmost column of the sprite that appears on-screen.
UPDATEAS_2:
  RLCA                    ; Set the instruction at SRBSET1 to SET n,(HL) where
  RLCA                    ; n is the appropriate bit of the SRB byte that needs
  RLCA                    ; to be set
  LD E,A                  ;
  AND 56                  ;
  ADD A,198               ;
  LD (27016),A            ;
  LD A,E                  ; A=8c (c=0-31: leftmost column of the screen
                          ; occupied by the sprite)
  RLCA
  RLCA
  AND 3                   ; A=INT(c/8)
  LD E,A                  ; E=INT(c/8)
  LD A,D                  ; A=character's y-coordinate (155-169)
  SUB 149                 ; 5<=A<=20
  ADD A,A                 ; A=4*(D-149)
  ADD A,A                 ;
  ADD A,E                 ; A=4*(D-149)+INT(c/8)
  LD E,A                  ; E=12+4*(D-152)+INT(c/8)
  LD D,127                ; Now HL points to the relevant byte of the SRB, and
  EX DE,HL                ; DE points to the base address of the sprite UDG
                          ; references
UPDATEAS_3:
  CALL SRBCOL             ; Update the SRB for one column of the character's
                          ; sprite
  JR NZ,UPDATEAS_5        ; Jump if we just updated the SRB for the right
                          ; column of the sprite
  SUB 16                  ; Point HL back to the relevant byte of the SRB
  LD L,A                  ;
  LD A,(27016)            ; Prepare to change the instruction at SRBSET1 from
  ADD A,8                 ; SET n,(HL) to SET n+1,(HL)
UPDATEAS_4:
  LD (27016),A            ; Change the instruction at SRBSET1 from SET n,(HL)
                          ; to SET n+1,(HL) or SET 0,(HL)
  JR NC,UPDATEAS_3        ; Jump back unless the instruction at SRBSET1 should
                          ; be changed to SET 0,(HL) now
  INC L                   ; Move to the next byte of the SRB
  LD A,L                  ; The zero flag will be set now if we 'wrapped
  AND 3                   ; around' to the left side of the screen, i.e. the
                          ; next column of the character sprite is off-screen
                          ; to the right
  LD A,198                ; This will change the instruction at SRBSET1 to SET
                          ; 0,(HL)
  JR NZ,UPDATEAS_4        ; Jump back to consider the remaining on-screen
                          ; columns of the sprite
UPDATEAS_5:
  POP HL                  ; Restore the character number (152-172) to H
  RET

; Unused
  DEFS 2

; Update the SRB for a character's current animatory state and location
;
; Used by many routines. Updates the screen refresh buffer (SRB) for a
; character's current animatory state and location. Returns with the
; character's current coordinates in DE and animatory state in A.
;
; H Character number (152-172)
UPDATESRB:
  LD L,96                 ; Point HL at byte 96 of the character's buffer
  LD B,(HL)               ; B=character's animatory state
  INC L                   ; L=97
  LD D,(HL)               ; D=character's y-coordinate (154-169)
  INC L                   ; L=98
  LD E,(HL)               ; E=character's x-coordinate (0-95)
  CALL UPDATEAS_0         ; Update the SRB
  LD L,96                 ; Point HL at byte 96 of the character's buffer
  LD A,(HL)               ; A=character's animatory state
  INC L                   ; L=97
  LD D,(HL)               ; D=character's y-coordinate (155-169)
  INC L                   ; L=98
  LD E,(HL)               ; E=character's x-coordinate (0-95)
  RET

; Move the characters
;
; Called from the main loop at MAINLOOP.
MVCHARS:
  LD A,(LASTCHAR)         ; LASTCHAR holds the number of the last character
                          ; moved
  CALL MVCHARS_0          ; Move a character
  LD A,(LASTCHAR)
  CALL MVCHARS_0          ; Move another character
  LD A,(LASTCHAR)
MVCHARS_0:
  CP 171                  ; Get the number of the next character to be moved
  JR NZ,MVCHARS_1         ; (152-171) in A and copy it to LASTCHAR
  LD A,151                ;
MVCHARS_1:
  INC A                   ;
  LD (LASTCHAR),A         ;
  LD H,A                  ; H=number of the character to be moved
  CALL DECIDEMV           ; Stop now unless it's time to move this character
; The character shall be moved. From this point, the following steps are taken:
;
; +------+--------------------------------------------------------------------+
; | Step | Action                                                             |
; +------+--------------------------------------------------------------------+
; | 1    | If there is an uninterruptible subcommand routine address in bytes |
; |      | 111 and 112 of the character's buffer, jump to it                  |
; | 2    | Call the continual subcommand routine address in bytes 124 and 125 |
; |      | of the character's buffer (and then return to step 3)              |
; | 3    | If there is an interruptible subcommand routine address in bytes   |
; |      | 105 and 106 of the character's buffer, jump to it                  |
; | 4    | Restart the command list if bits 1 and 0 of byte 122 of the        |
; |      | character's buffer are reset and set respectively                  |
; | 5    | If there is a primary command routine address in bytes 99 and 100  |
; |      | of the character's buffer, jump to it                              |
; | 6    | Replace any continual subcommand routine address in bytes 124 and  |
; |      | 125 of the character's buffer with DONOWT (RET)                    |
; | 7    | Collect the next primary command routine address from the command  |
; |      | list, place it into bytes 99 and 100 of the character's buffer,    |
; |      | and jump to it                                                     |
; +------+--------------------------------------------------------------------+
;
; The address of one of the following uninterruptible subcommand routines may
; be present in bytes 111 and 112 of a character's buffer:
;
; +-----------+----------------------------------------------------+
; | Address   | Description                                        |
; +-----------+----------------------------------------------------+
; | KNOCKED   | Deal with a character who has been knocked over    |
; | UNSEAT1   | Deal with a character who's been dethroned (1)     |
; | UNSEAT2   | Deal with a character who's been dethroned (2)     |
; | FINDSEAT2 | Deal with a character who is looking for a seat    |
; | PELLETH   | Control the horizontal flight of a catapult pellet |
; | PELLETV   | Control the vertical flight of a catapult pellet   |
; | PUNCH1    | Make ANGELFACE throw a punch (1)                   |
; | PUNCH2    | Make ANGELFACE throw a punch (2)                   |
; | PUNCH3    | Make ANGELFACE throw a punch (3)                   |
; | PUNCH4    | Make ANGELFACE throw a punch (4)                   |
; | FIRE2     | Make BOY WANDER fire his catapult (2)              |
; | FIRE3     | Make BOY WANDER fire his catapult (3)              |
; | FIRE4     | Make BOY WANDER fire his catapult (4)              |
; | FIRE5     | Make BOY WANDER fire his catapult (5)              |
; | FIRE6     | Make BOY WANDER fire his catapult (6)              |
; | FIRE7     | Make BOY WANDER his fire catapult (7)              |
; | LOOKING   | Make a character find ERIC                         |
; +-----------+----------------------------------------------------+
;
; The address of one of the following continual subcommand routines will be
; present in bytes 124 and 125 of a character's buffer:
;
; +----------+------------------------------------------------+
; | Address  | Description                                    |
; +----------+------------------------------------------------+
; | DONOWT   | RET (do nothing)                               |
; | TRIP     | Make a little boy trip people up               |
; | VIOLENT  | Make ANGELFACE hit now and then                |
; | CATTY    | Make BOY WANDER fire his catapult now and then |
; | RUN      | Make a character walk fast                     |
; | CHKTOUCH | Check whether ANGELFACE is touching ERIC       |
; +----------+------------------------------------------------+
;
; The address of one of the following interruptible subcommand routines (or an
; entry point thereof) may be present in bytes 105 and 106 of a character's
; buffer:
;
; +----------+---------------------------------------------------------------+
; | Address  | Description                                                   |
; +----------+---------------------------------------------------------------+
; | WALK     | Guide a character to an intermediate destination              |
; | ASCEND   | Guide a character up a staircase                              |
; | DESCEND  | Guide a character down a staircase                            |
; | WIPE1    | Make a teacher wipe a blackboard (1)                          |
; | WIPING   | Make a teacher wipe a blackboard (2)                          |
; | WRITEBRD | Make a character write on a blackboard                        |
; | SPEAK    | Make a character speak (1)                                    |
; | SPEAK2   | Make a character speak (2)                                    |
; | SEEKERIC | Make a teacher find the truant ERIC                           |
; | TOERIC   | Move a character looking for ERIC from the midstride position |
; | SWOTSPK2 | Make a teacher wait for EINSTEIN to finish speaking           |
; +----------+---------------------------------------------------------------+
  LD L,112                ; Bytes 111 and 112 of the character's buffer may
                          ; contain the address of an uninterruptible
                          ; subcommand
  LD A,(HL)               ; Pick up the MSB in A
  AND A                   ; Is there an uninterruptible subcommand routine
                          ; address here?
  JR NZ,MVCHARS_7         ; Jump to it if so
  LD L,124                ; Pick up in DE the continual subcommand routine
  LD E,(HL)               ; address from bytes 124 and 125 of the character's
  INC L                   ; buffer; this is set to DONOWT (RET) initially by
  LD D,(HL)               ; the startup routine at START
  LD BC,MVCHARS_2         ; Go to the continual subcommand routine address in
  PUSH BC                 ; bytes 124 and 125 of the character's buffer, and
  PUSH DE                 ; then return to MVCHARS_2 (below)
  RET                     ;
; This entry point is used by the routine at RM112.
MVCHARS_2:
  LD L,106                ; Bytes 105 and 106 of the character's buffer may
                          ; contain the address of an interruptible subcommand
  LD A,(HL)               ; Pick up the MSB in A
  AND A                   ; Is there an interruptible subcommand routine
                          ; address here?
  JR NZ,MVCHARS_7         ; Jump to it if so
  LD L,121                ; Byte 121 of the character buffers is unused and
  LD A,(HL)               ; always contains zero; hence this jump is always
  AND A                   ; made
  JR Z,MVCHARS_3          ;
  LD (HL),0               ; If the value of byte 121 were not zero, this would
  LD L,112                ; treat it as the MSB of a routine address, copy it
  LD (HL),A               ; to byte 112, and then jump to that routine
  JR MVCHARS_7            ;
; If we get here, that means there's no uninterruptible subcommand routine
; address in bytes 111 and 112 and no interruptible subcommand routine address
; in bytes 105 and 106 of the character's buffer at the moment. We take this
; opportunity to check whether a command list restart has been requested, by
; inspecting bit 0 of byte 122. This bit is set by the routine at NEWLESSON
; when starting a new lesson, and by the routine at CHKLINES when ERIC has
; accumulated 10000 lines or more and MR WACKER must find him and send him
; home. In addition, a command in the command list may have requested a restart
; (see CHKTIME, RESTART).
MVCHARS_3:
  LD L,122                ; Pick up byte 122 of the character's buffer in A
  LD A,(HL)               ;
  RRCA                    ; If bit 1 is reset (which it always is) and bit 0 is
  JR NC,MVCHARS_4         ; set, the command list will be restarted
  RRCA                    ;
  JR C,MVCHARS_4          ;
; The command list will be restarted.
  RES 0,(HL)              ; Reset bit 0 of byte 122
  LD L,119                ; Copy the start address of the command list from
  LD E,117                ; bytes 119 and 120 of the character's buffer into
  LD D,H                  ; bytes 117 and 118 (which hold the address currently
  LDI                     ; reached in the command list); in effect, this
  LDI                     ; restarts the command list
  JR MVCHARS_5            ; Collect the address of the first command from the
                          ; command list and jump to it
; The command list was not marked for a restart.
MVCHARS_4:
  LD L,100                ; Bytes 99 and 100 of the character's buffer hold the
  LD A,(HL)               ; address of the current primary command routine from
  AND A                   ; the command list; jump to this routine address if
  JR NZ,MVCHARS_7         ; it's present
MVCHARS_5:
  LD L,124                ; Place DONOWT (RET) into bytes 124 and 125 of the
  LD (HL),159             ; character's buffer, thus resetting the continual
  INC L                   ; subcommand
  LD (HL),98              ;
; This entry point is used by the routine at ADDR2BUF.
MVCHARS_6:
  LD L,99                 ; Get the address of the next primary command routine
  CALL GETPARAM           ; from the command list and place it into bytes 99
  LD (HL),A               ; and 100 of the character's buffer and also into BC
  LD C,A                  ;
  INC L                   ;
  CALL GETPARAM           ;
  LD (HL),A               ;
  LD B,A                  ;
  JR MVCHARS_8            ; Make an indirect jump to the routine address in BC
MVCHARS_7:
  LD B,A                  ; Pick up in BC the address of the routine to hand
  DEC L                   ; over control of this character to
  LD C,(HL)               ;
MVCHARS_8:
  LD L,122
  PUSH BC                 ; Prepare to make an indirect jump to the routine
                          ; address in BC
; This entry point is used by command lists 136, 138, 140, 142, 146, 148, 150,
; 154, 156, 158, 162, 164 and 166.
DONOWT:
  RET

; Terminate a command
;
; The main entry point is used by the routines at KNOCKED, PUNCH4 and FIRE7.
; Removes the uninterruptible subcommand routine address from bytes 111 and 112
; of the character's buffer, which has the effect of making the routine at
; MVCHARS immediately hand control of the character back to the interruptible
; subcommand or the primary command.
RM112:
  LD L,112                ; Byte 112 of the buffer contains the MSB of the
                          ; uninterruptible subcommand routine address
  JR RMADDR               ; Jump forward to replace it with 0
; This entry point is used by the routines at WALK, DESCEND, WIPE2, WRITEBRD,
; SPEAK, SPEAK2, SEEKERIC, CHKTIME and SWOTSPK2. Removes the interruptible
; subcommand routine address from bytes 105 and 106 of the character's buffer,
; which has the effect of making the routine at MVCHARS immediately hand
; control of the character back to the primary command.
RM106:
  LD L,106                ; Byte 106 of the buffer contains the MSB of the
                          ; interruptible subcommand routine address
  JR RMADDR               ; Jump forward to replace it with 0
; This entry point is used by the routines at MVTILL, SAMEFLOOR, NEXTWALK,
; SIGRAISE, REWIND, CHKTIME, TELLSIT, BWWRITECLN, BWWRITE, FINDERIC, LINES2K
; and TELLERIC. Removes the primary command routine address from bytes 99 and
; 100 of the character's buffer, which has the effect of making the routine at
; MVCHARS move immediately to the next command in the command list.
NEXTCMD:
  LD L,100                ; Byte 100 of the buffer contains the MSB of the
                          ; primary command routine address
RMADDR:
  LD A,(LASTCHAR)         ; A=number of the character being worked on (152-171)
  LD H,A                  ; Zero out the relevant routine address MSB (in byte
  LD (HL),0               ; 100, 106 or 112) in the character's buffer
  JR MVCHARS_2            ; Re-enter the character-moving routine (MVCHARS) at
                          ; the checkpoint for the interruptible subcommand

; Determine whether a character should be moved (1)
;
; Used by the routine at MVCHARS. Returns to that routine if the character
; under consideration should be moved on this pass; otherwise drops the return
; address from the stack, thus skipping ahead to consider the next character.
; Also reinitialises the walking speed change delay counter in byte 123 of the
; character's buffer when it reaches 0, and sets the new walking speed as
; appropriate: slow for teachers, fast or slow (at random) for boys.
;
; H Character number (152-171)
DECIDEMV:
  LD L,123                ; Byte 123 contains a counter that is reset to a
  DEC (HL)                ; random even number between 2 and 32; see below
  JR Z,DECIDEMV_0         ; Jump if it's time to consider a change of pace
  BIT 0,(HL)              ; Is the counter even?
  RET Z                   ; Return (to move the character) if so
  DEC L                   ; L=122
  BIT 7,(HL)              ; Is this character walking fast?
  RET Z                   ; Return (to move the character) if so
  POP HL                  ; Drop the return address from the stack so that the
                          ; character will not be moved this time round
  RET                     ; Return to consider the next character, or return to
                          ; the main loop
; It's time to consider a change of pace for the character being moved.
DECIDEMV_0:
  CALL GETRANDOM          ; A=random number
  AND 61                  ; A=even number between 2 and 32, and the carry flag
  ADD A,4                 ; holds a random bit
  RRA                     ;
  LD (HL),A               ; Place this even number into byte 123 of the
                          ; character's buffer
  DEC L                   ; L=122
  RL (HL)                 ; Bit 7 of byte 122 is now equal to the random bit
  RRC (HL)                ; that was pushed into the carry flag by the RRA
                          ; instruction above
  LD A,(HL)               ; Bit 5 of byte 122 is set for catapult pellets and
  BIT 5,A                 ; stampeding boys to ensure they always go at top
                          ; speed
  JR Z,DECIDEMV2          ; Jump if we're not dealing with such a thing
  RES 7,(HL)              ; Signal: fly or run at top speed
  JR DECIDEMV2            ; Jump over the routine at MVTILL

; Make a character walk up and down until a certain time
;
; Used by command lists 136, 140, 142, 144, 146, 148, 150, 152, 154, 156, 158,
; 160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 186, 188, 198, 200,
; 202, 206, 208, 210, 212, 214, 216, 218 and 222 to make a character walk up
; and down within 7 spaces to the left of a fixed location (the 'walkabout
; origin') until a specified time, e.g. the end of the lesson or when a teacher
; arrives at the doorway of the classroom.
;
; H Character number (152-169)
; L 122
MVTILL:
  LD A,(HL)               ; Reset bits 5 and 6 of byte 122 of the character's
  AND 159                 ; buffer, indicating that he should no longer walk
  LD (HL),A               ; fast or slow continuously (if he was doing so)
  CALL GETPARAM           ; Collect the signal byte from the command list
  LD L,101                ; Place the signal byte in byte 101 of the
  LD (HL),A               ; character's buffer
  LD L,99                 ; Change the address in bytes 99 and 100 of the
  LD (HL),229             ; character's buffer from MVTILL to MVTILL_0 (below)
; This entry point is used by the routines at NEXTWALK and WALKABOUT.
MVTILL_0:
  LD L,101                ; Collect the signal byte from byte 101 of the
  LD A,(HL)               ; character's buffer
  CALL CHECKSIG           ; Has this signal been raised?
  JR Z,MVTILL_2           ; Jump if not
; The time has come to stop moving about. However, before we move on in the
; command list, the character needs to return to the walkabout origin.
  INC L                   ; Collect the x-coordinate of the walkabout origin
  LD A,(HL)               ; from byte 102 of the character's buffer
  LD L,98                 ; Byte 98 holds the character's x-coordinate
  CP (HL)                 ; Is the character at the walkabout origin?
  JR NZ,MVTILL_1          ; Jump if not
  LD L,96                 ; Byte 96 holds the character's animatory state
  BIT 0,(HL)              ; Is the character midstride?
  JP Z,NEXTCMD            ; Move to the next command in the command list if not
MVTILL_1:
  LD L,107                ; Fill in the new walkabout destination (either the
  LD (HL),A               ; origin or some location within 7 spaces to the left
                          ; of the origin)
  INC L                   ; Initialise the counter at byte 108 to 30, which
  LD (HL),30              ; will give the character enough time to get to the
                          ; destination
  LD BC,WALK              ; Place the address of the interruptible subcommand
  LD L,105                ; routine at WALK (guide character to destination)
  LD (HL),C               ; into bytes 105 and 106 of the character's buffer
  INC L                   ;
  LD (HL),B               ;
  INC L
  PUSH BC
  RET                     ; Make an indirect jump to WALK
; The signal has not been raised yet, so it's time for another mini-walkabout.
MVTILL_2:
  INC L                   ; Point HL at the x-coordinate of the walkabout
                          ; origin
  CALL GETRANDOM          ; A=random number
  AND 7                   ; -7<=A<=0
  SUB 7                   ;
  ADD A,(HL)              ; Add the x-coordinate of the walkabout origin
  JR MVTILL_1             ; Make this the new walkabout destination

; Determine whether a character should be moved (2)
;
; Continues from DECIDEMV. So far, byte 123 and bit 7 of byte 122 of the
; character's buffer have been adjusted to designate a new period of walking or
; running. All that remains is to ensure that bit 7 of byte 122 is set if the
; character is a teacher (teachers usually walk slowly).
;
; A Contents of byte 122 of the character's buffer
; H Character number (152-171)
; L 122
DECIDEMV2:
  AND 80                  ; Check bit 6 (always reset) and bit 4 (set for
  RET Z                   ; teachers) of byte 122, and return (to move the
                          ; character) if both are reset
  SET 7,(HL)              ; Signal: walk slowly (the norm for teachers)
  RET                     ; Return to the routine at MVCHARS to move the
                          ; character

; Unused
  DEFB 0

; Check whether a signal has been raised
;
; Used by the routines at MVTILL and BWWRITE. On entry, A holds the event
; identifier, which will be one of:
;
; +----+------------------------------------------------+--------------+-----+
; | A  | Description                                    | Flags        | Bit |
; +----+------------------------------------------------+--------------+-----+
; | 0  | End of lesson/playtime                         | LFLAGS       | 0   |
; | 8  | Teacher has arrived at the Map Room            | LSIGS        | 0   |
; | 9  | Teacher has arrived at the Reading Room        | LSIGS        | 1   |
; | 10 | Teacher has arrived at the Exam Room           | LSIGS        | 2   |
; | 11 | Teacher has arrived at the White Room          | LSIGS        | 3   |
; | 16 | Little boy no. 1 is ready to stampede          | STAMPEDESIGS | 0   |
; | 17 | Little boy no. 1 is ready to stampede again    | STAMPEDESIGS | 1   |
; | 26 | EINSTEIN or BOY WANDER is ready                | SPECIALSIGS  | 2   |
; | 27 | MR WACKER is ready                             | SPECIALSIGS  | 3   |
; | 28 | ERIC has been told about EINSTEIN, BOY WANDER  | SPECIALSIGS  | 4   |
; |    | or ANGELFACE                                   |              |     |
; | 29 | EINSTEIN has told MR WACKER what ERIC's up to, | SPECIALSIGS  | 5   |
; |    | or ERIC has mumps                              |              |     |
; +----+------------------------------------------------+--------------+-----+
;
; The routine returns with the zero flag reset if the corresponding signal has
; been raised.
;
; A Event ID from a command list
CHECKSIG:
  PUSH AF                 ; Save the event identifier briefly
  AND 248                 ; Keep only bits 3-7 (the byte index) and shift them
  RRCA                    ; into bits 0-4
  RRCA                    ;
  RRCA                    ;
  ADD A,200               ; Modify the LD A,(m) instruction at SIGBYTE below so
  LD (25397),A            ; that m is LFLAGS, LSIGS, STAMPEDESIGS or
                          ; SPECIALSIGS
  POP AF                  ; Restore the event identifier to A
  AND 7                   ; Keep only bits 0-2 (the bit index)
  ADD A,A                 ; Modify the BIT n,A instruction at SIGBIT below so
  ADD A,A                 ; that n=A (0-7)
  ADD A,A                 ;
  ADD A,71                ;
  LD (25400),A            ;
SIGBYTE:
  LD A,(LFLAGS)           ; Pick up the byte holding the signal flag for this
                          ; event (the operand of this instruction is modified
                          ; earlier in this routine)
SIGBIT:
  BIT 0,A                 ; Reset the zero flag if the signal has been raised
                          ; (this instruction is modified earlier in this
                          ; routine to check the appropriate bit)
  RET

; Unused
  DEFS 2

; Guide a character to an intermediate destination
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of the character's buffer by the routines at MVTILL, GOTO, DINDUTY
; and CLSNOERIC. It is used to make a character go straight to a destination
; (or intermediate destination) that is reachable without negotiating any
; staircases.
;
; H Character number (152-169)
WALK:
  LD L,108                ; The counter at byte 108 of the character's buffer
  DEC (HL)                ; is initialised by the calling routine to limit the
WALK_0:
  JP Z,RM106              ; amount of time that this routine has control;
                          ; terminate this interruptible subcommand if the
                          ; counter is now zero
  DEC L                   ; Collect the x-coordinate of the intermediate
  LD A,(HL)               ; destination from byte 107 of the character's buffer
  LD L,98                 ; Byte 98 of the character's buffer holds his
                          ; x-coordinate
  CP (HL)                 ; Has the character reached the intermediate
                          ; destination?
  JR NZ,WALK_1            ; Jump if not
  LD L,96                 ; Byte 96 of the character's buffer holds his
                          ; animatory state
  BIT 0,(HL)              ; Is the character midstride?
  JR Z,WALK_0             ; Jump if not (nothing left to do)
WALK_1:
  CALL UPDATESRB          ; Update the SRB for the character's current location
  LD C,A                  ; Store the character's animatory state in C
  LD L,107                ; Collect the x-coordinate of the intermediate
  LD A,(HL)               ; destination from byte 107 of the character's buffer
  CP E                    ; E holds the character's x-coordinate
  LD A,C                  ; A=character's animatory state
  JR NZ,WALK_3            ; Jump unless the character has reached the
                          ; destination
; The character has reached the destination, but he is midstride. To complete
; the journey, he must finish his stride. This entry point is also used by the
; routine at FINDSEAT2.
WALK_2:
  BIT 7,A                 ; Is the character facing left?
  JR Z,WALK_4             ; Jump if so
  JR WALK_7
; The character has not yet reached the destination. Figure out his next move.
WALK_3:
  BIT 7,A                 ; Now the zero flag indicates which way the character
                          ; is facing: set=left, reset=right
  JR NC,WALK_6            ; Jump if the character is to the left of his
                          ; destination
; The character is to the right of his destination, and so must proceed
; leftwards.
  RES 7,A                 ; Make sure the character is facing left
  JP NZ,UPDATEAS          ; Update the character's animatory state and update
                          ; the SRB if he was facing right
WALK_4:
  INC A                   ; A=character's next animatory state
  BIT 0,A                 ; Will the character be midstride?
  JP NZ,UPDATEAS          ; Update the character's animatory state and update
                          ; the SRB if so
  DEC E                   ; E=character's next x-coordinate (one to the left)
WALK_5:
  AND 3                   ; Calculate the character's next animatory state in A
  LD B,A                  ;
  LD A,C                  ;
  AND 252                 ;
  ADD A,B                 ;
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB
; The character is to the left of his destination, and so must proceed
; rightwards.
WALK_6:
  SET 7,A                 ; Make sure the character is facing right
  JP Z,UPDATEAS           ; Update the character's animatory state and update
                          ; the SRB if the character was facing left
WALK_7:
  INC A                   ; A=character's next animatory state
  BIT 0,A                 ; Will the character be midstride?
  JP NZ,UPDATEAS          ; Update the character's animatory state and update
                          ; the SRB if so
  INC E                   ; E=character's next x-coordinate (one to the right)
  JR WALK_5               ; Jump back to update the character's animatory state
                          ; and location and update the SRB

; Unused
  DEFS 4

; Guide a character up a staircase
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of a character's buffer by the routine at GOTO when the character has
; reached the bottom of a staircase he needs to ascend to reach his
; destination.
ASCEND:
  LD B,255                ; Signal that the character is going up a staircase
  JR DESCEND_0

; Guide a character down a staircase
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of a character's buffer by the routine at GOTO when the character has
; reached the top of a staircase he needs to descend to reach his destination.
;
; H Character number (152-169)
DESCEND:
  LD B,1                  ; Signal that the character is going down a staircase
; This entry point is used by the routine at ASCEND with B=255.
DESCEND_0:
  LD L,107                ; Byte 107 of the character's buffer holds the number
                          ; of movements remaining
  DEC (HL)                ; Has the character finished going up or down the
                          ; stairs?
  JP Z,RM106              ; Terminate this interruptible subcommand if so
  PUSH BC                 ; Save the direction indicator (in B) briefly
  CALL UPDATESRB          ; Update the SRB for the character's current location
  POP BC                  ; Restore the direction indicator to B
  LD C,A                  ; C=character's current animatory state
  INC A                   ; A=character's next animatory state
  BIT 0,A                 ; Is the character midstride right now?
  JR Z,DESCEND_3          ; Jump if so
; The following four instructions calculate the character's next y-coordinate.
; The y-coordinate will be decremented if the character is going up the stairs
; and he's not midstride at the moment, incremented if he's going down the
; stairs and he is midstride at the moment, and otherwise unchanged.
  DEC D
DESCEND_1:
  INC B                   ; Is the character going up the stairs?
  JR Z,DESCEND_2          ; Jump if so
  INC D
; At this point A holds the character's new animatory state, and DE his new
; location.
DESCEND_2:
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB
; The character is midstride. Now we calculate his next x-coordinate.
DESCEND_3:
  RLCA                    ; Set the carry flag if the character is facing left
  CCF                     ;
  SBC A,A                 ; A=0 (facing right) or -2 (facing left)
  ADD A,A                 ;
  ADD A,E                 ; Set E equal to the character's new x-coordinate
  LD E,A                  ;
  INC E                   ;
  LD A,C                  ; A=character's current animatory state
  INC A                   ; A=character's next animatory state
  BIT 1,A                 ; Will the character be standing (phase 3)?
  JR NZ,DESCEND_1         ; Jump if so
  SUB 4                   ; Adjust the animatory state to standing (phase 1)
  JR DESCEND_1

; Unused
  DEFS 2

; Make a character go to a location
;
; Used by command lists 128, 130, 132, 134, 136, 140, 142, 144, 146, 148, 150,
; 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180,
; 184, 186, 188, 190, 192, 198, 200, 202, 208, 212, 214, 216, 218 and 222 to
; make a character go to a specified place.
;
; H Character number (152-169)
; L 122
GOTO:
  LD A,(HL)               ; Reset bits 5 and 6 of byte 122 of the character's
  AND 159                 ; buffer, indicating that he should no longer walk
  LD (HL),A               ; fast or slow continuously (if he was doing so)
  LD L,101                ; Get the destination y-coordinate from the command
  CALL GETPARAM           ; list and place it in byte 101 of the character's
  LD (HL),A               ; buffer
  INC L                   ; Get the destination x-coordinate from the command
  CALL GETPARAM           ; list and place it in byte 102 of the character's
  LD (HL),A               ; buffer
; This entry point is used by the routines at GOTORAND and FOLLOW.
GOTO_0:
  NOP
  CALL STANDUP            ; Make the character stand up if he's not already
; The address of this entry point is placed into bytes 99 and 100 of the
; character's buffer (by the routine at STANDUP, called above, and by the
; routine at GOTORAND) so that we return here after the character has stood up.
READYTOGO:
  LD L,97                 ; Collect the character's x- and y-coordinates in E
  LD D,(HL)               ; and D
  INC L                   ;
  LD E,(HL)               ;
  LD L,101                ; Bytes 101 and 102 of the character's buffer contain
                          ; the destination coordinates
  LD BC,WALK              ; Point BC at WALK (guide character to intermediate
                          ; destination)
  LD A,D                  ; A=character's y-coordinate
  CP (HL)                 ; Is the character on the same floor as his
                          ; destination?
  JP Z,SAMEFLOOR          ; Jump if so
  CP 155                  ; Is the character on the top floor?
  JP Z,TOP                ; Jump if so
  CP 169                  ; Is the character on the bottom floor?
  JP Z,BOTTOM             ; Jump if so
; The character is on the middle floor and is destined for another floor.
  LD A,(HL)               ; A=y-coordinate of destination
  CP 169                  ; Set the zero flag if the character is going to the
                          ; bottom floor
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
  JR NZ,GOTO_9            ; Jump if the character is going to the top floor
; This entry point is used by the routine at TOP with A=57 (the x-coordinate of
; the wall between the Map and Reading Rooms).
GOTO_1:
  CP E                    ; E holds the character's x-coordinate
  LD A,12                 ; This is the x-coordinate of the tops of the
                          ; staircases at the left of the skool
  JR NC,GOTO_3            ; Jump if the character is to the left of the wall
                          ; whose x-coordinate (38 or 57) was in A
; This entry point is used by the routine at SAMEFLOOR when the character is on
; the right-middle or right-top floor and needs to go down the stairs to get to
; his destination.
GOTO_2:
  LD A,77                 ; This is the x-coordinate of the tops of the
                          ; staircases at the right of the skool
; This entry point is used by the routine at SAMEFLOOR with A=12 (the
; x-coordinate of the tops of the staircases at the left of the skool) when the
; character is on the left-middle or left-top floors and needs to go down the
; stairs to get to his destination.
GOTO_3:
  CP E                    ; Is the character at the top of the stairs he needs
                          ; to descend?
  JR NZ,GOTO_8            ; Jump if not
  LD L,96                 ; Byte 96 of the character's buffer holds his
                          ; animatory state
  BIT 0,(HL)              ; Is the character midstride (about to descend)?
  JR NZ,GOTO_8            ; Jump if so
  CP 48                   ; Is the target staircase on the right of the skool?
  JR NC,GOTO_4            ; Jump if so
  BIT 7,(HL)              ; Is the character facing left?
  JR Z,GOTO_5             ; Jump if so
  JR GOTO_6
; The character must go to the top of a staircase at the right of the skool.
GOTO_4:
  BIT 7,(HL)              ; Is the character facing left?
  JR Z,GOTO_6             ; Jump if so
; This entry point is used by the routine at BOTTOM.
GOTO_5:
  CALL UPDATESRB          ; Update the SRB for the character's current location
  XOR 128                 ; Turn the character round
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB
GOTO_6:
  LD C,144                ; BC=DESCEND (guide character down stairs)
; This entry point is used by the routine at BOTTOM.
GOTO_7:
  LD A,15                 ; Going up or down a staircase entails 15 movements
; This entry point is used by the routines at BOTTOM and SAMEFLOOR.
GOTO_8:
  LD L,105                ; Place the interruptible subcommand routine address
  LD (HL),C               ; in BC (WALK, ASCEND or DESCEND) into bytes 105 and
  INC L                   ; 106 of the character's buffer
  LD (HL),B               ;
  INC L                   ; Byte 107 will contain either (a) the x-coordinate
  LD (HL),A               ; of the next intermediate destination (i.e. the
                          ; final destination itself, or the top or bottom of
                          ; the next staircase the character must negotiate
                          ; first) (BC=WALK), or (b) 15, that being the number
                          ; of movements required to ascend or descend a
                          ; staircase (BC=ASCEND or DESCEND)
  INC L                   ; Initialise byte 108 to 9; this ensures that the
  LD (HL),9               ; routine at WALK will move the character at most 9
                          ; paces towards the destination before relinquishing
                          ; control (and thus giving the routine at MVCHARS a
                          ; chance to check whether the command list should be
                          ; restarted, e.g. for a new lesson)
  PUSH BC                 ; Make an indirect jump to WALK, ASCEND or DESCEND
  RET                     ;
; The character is on the middle floor but is destined for the top floor. At
; this point A=38 (the x-coordinate of the wall between the White and Exam
; Rooms).
GOTO_9:
  INC L                   ; Point HL at the destination x-coordinate
  CP E
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Map and Reading Rooms
  JR NC,GOTO_10           ; Jump if the character is to the left of the White
                          ; Room wall
  CP (HL)                 ; (HL) holds the destination x-coordinate
  LD A,70                 ; This is the x-coordinate of the bottoms of the
                          ; staircases at the right of the skool
  JR NC,GOTO_2            ; Jump if the destination is to the left of the Map
                          ; Room wall
  CP E                    ; Is the character at the bottom of the staircase
                          ; just outside the Exam Room?
  JR NZ,GOTO_8            ; Jump if not (to send the character there)
  LD L,96                 ; Byte 96 of the character's buffer holds his
                          ; animatory state
  BIT 0,(HL)              ; Is the character midstride?
  JR NZ,GOTO_8            ; Jump if so (to finish the stride)
  BIT 7,(HL)              ; Is the character facing left?
  JR Z,GOTO_5             ; Jump if so (to turn the character round)
  LD C,140                ; BC=ASCEND (guide character up stairs)
  JR GOTO_7
; The character is on the middle floor to the left of the White Room wall, and
; is destined for the top floor. At this point A=57 (the x-coordinate of the
; wall between the Map and Reading Rooms).
GOTO_10:
  CP (HL)                 ; (HL) holds the destination x-coordinate
  LD A,12                 ; This is the x-coordinate of the tops of the
                          ; staircases at the left of the skool
  JR C,GOTO_3             ; Jump if the destination is to the right of the Map
                          ; Room wall
  LD A,19                 ; This is the x-coordinate of the bottoms of the
                          ; staircases at the left of the skool
  CP E                    ; Is the character at the bottom of the staircase
                          ; just outside the White Room?
  JR NZ,GOTO_8            ; Jump if not (to send the character there)
  LD L,96                 ; Byte 96 of the character's buffer holds his
                          ; animatory state
  BIT 0,(HL)              ; Is the character midstride?
  JR NZ,GOTO_8            ; Jump if so (to finish the stride)
  BIT 7,(HL)              ; Is the character facing right?
  JR NZ,GOTO_5            ; Jump if so (to turn the character round)
  LD C,140                ; BC=ASCEND (guide character up stairs)
  JR GOTO_7

; Unused
  DEFS 4

; Guide a character from the top floor to another floor
;
; Used by the routine at GOTO when the character under consideration is on the
; top floor but is destined for another floor.
TOP:
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Map and Reading Rooms
  JP GOTO_1               ; Jump back into the main routine

; Guide a character from the bottom floor to another floor
;
; Used by the routine at GOTO when the character under consideration is on the
; bottom floor but is destined for another floor.
;
; BC WALK
; E Character's x-coordinate
; H Character number (152-169)
; L 101
BOTTOM:
  LD A,(HL)               ; A=destination y-coordinate
  CP 162                  ; Set the zero flag if the character is going to the
                          ; middle floor
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
  JR Z,BOTTOM_0           ; Jump if the character is going to the middle floor
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Map and Reading Rooms
BOTTOM_0:
  INC L                   ; Set the carry flag if the character is destined for
  CP (HL)                 ; some place to the right of the wall referred to by
                          ; A
  LD A,70                 ; This is the x-coordinate of the bottoms of the
                          ; staircases at the right of the skool
  JR C,BOTTOM_1           ; Jump if the character must go to a staircase on the
                          ; right
  LD A,19                 ; This is the x-coordinate of the bottoms of the
                          ; staircases at the left of the skool
BOTTOM_1:
  CP E                    ; Is the character at the bottom of the staircase he
                          ; must climb?
  JP NZ,GOTO_8            ; Jump if not (to send the character there)
  LD L,96                 ; Byte 96 of the character's buffer holds his
                          ; animatory state
  BIT 0,(HL)              ; Is the character midstride?
  JP NZ,GOTO_8            ; Jump if so (to finish the stride)
  CP 48                   ; Is the character at the bottom of a staircase at
                          ; the right of the skool?
  JR NC,BOTTOM_2          ; Jump if so
  BIT 7,(HL)              ; Is the character facing right?
  JP NZ,GOTO_5            ; Turn him round if so
  JR BOTTOM_3
BOTTOM_2:
  BIT 7,(HL)              ; Is the character facing left?
  JP Z,GOTO_5             ; Turn him round if so
BOTTOM_3:
  LD C,140                ; Set BC=ASCEND (guide character up stairs) and place
  JP GOTO_7               ; this interruptible subcommand routine address into
                          ; bytes 105 and 106 of the character's buffer

; Unused
  DEFB 0

; Guide a character to a location on the same floor
;
; Used by the routine at GOTO when the character under consideration is on the
; same floor as his destination.
;
; DE Character's coordinates
; H Character number (152-169)
; L 101
SAMEFLOOR:
  INC L                   ; Point HL at the destination x-coordinate
  LD A,E                  ; A=character's current x-coordinate
  CP (HL)                 ; Is the character at the destination?
  JR NZ,SAMEFLOOR_0       ; Jump if not
  LD L,96                 ; Byte 96 holds the character's animatory state
  BIT 0,(HL)              ; Is the character midstride?
  JP Z,NEXTCMD            ; Move to the next command in the command list if not
  JP GOTO_8               ; Otherwise finish the stride
; The character hasn't reached his destination yet. Figure out his next move.
SAMEFLOOR_0:
  LD A,D                  ; A=character's y-coordinate
  CP 169                  ; Set the zero flag if the character is on the bottom
                          ; floor
  LD A,(HL)               ; A=destination x-coordinate
  JP Z,GOTO_8             ; Jump if the character is on the bottom floor
; The character is on the middle floor or the top floor. But which side of the
; wall?
  LD A,D                  ; A=character's y-coordinate
  CP 155                  ; Set the zero flag if the character is on the top
                          ; floor
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Map and Reading Rooms
  JR Z,SAMEFLOOR_1        ; Jump if the character is on the top floor
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
SAMEFLOOR_1:
  CP E                    ; Is the character to the left of the wall?
  JR NC,SAMEFLOOR_2       ; Jump if so
; The character is on the right side of the wall.
  CP (HL)                 ; Compare the wall's x-coordinate (in A) with the
                          ; destination x-coordinate
  LD A,(HL)               ; A=destination x-coordinate
  JP C,GOTO_8             ; Jump if the character doesn't need to go down the
                          ; stairs
  JP GOTO_2               ; Send the character to the top of the staircase
; The character is on the left side of the wall.
SAMEFLOOR_2:
  CP (HL)                 ; Compare the wall's x-coordinate (in A) with the
                          ; destination x-coordinate
  LD A,(HL)               ; A=destination x-coordinate
  JP NC,GOTO_8            ; Jump if the character doesn't need to go down the
                          ; stairs
  LD A,12                 ; This is the x-coordinate of the tops of the
                          ; staircases at the left of the skool
  JP GOTO_3               ; Send the character to the top of the staircase

; Unused
  DEFS 8

; Scroll the screen right 8 columns
;
; Used by the routine at MVERIC2.
RSCROLL8:
  LD B,8                  ; There are 8 columns to scroll
RSCROLL8_0:
  PUSH BC
  CALL RSCROLL            ; Scroll one column right
  POP BC
  DJNZ RSCROLL8_0         ; Jump back till all 8 columns are done
  RET

; Scroll the screen left 8 columns
;
; Used by the routines at MVERIC2 and SCRLSKOOL.
LSCROLL8:
  LD B,8                  ; There are 8 columns to scroll
LSCROLL8_0:
  PUSH BC
  CALL LSCROLL            ; Scroll one column left
  POP BC
  DJNZ LSCROLL8_0         ; Jump back till all 8 columns are done
  RET

; Update the display (unused)
;
; This routine is not used. It is probably the predecessor of the routine at
; UPDATESCR.
XUPDATE:
  LD HL,XTILES            ; At one time, XTILES would have held the number of
                          ; on-screen character squares that need refreshing
  LD A,(HL)               ; Pick up this number in A
  LD (HL),0               ; Reset the number to 0
  INC L                   ; HL=XTILELOCS
XUPDATE_0:
  AND A                   ; Do any character squares need refreshing?
  RET Z                   ; Return if not
  LD D,(HL)               ; Pick up the coordinates of the first character
  INC L                   ; square that needs refreshing in DE
  LD E,(HL)               ;
  INC L                   ; Point HL at the next set of coordinates
  PUSH HL
  PUSH AF
  CALL XPRINTTILE         ; Print the character square at the coordinates in DE
  POP AF
  POP HL
  DEC A                   ; Next character square
  JR XUPDATE_0            ; Jump back until all character squares have been
                          ; printed

; Unused
  DEFS 3

; Move ERIC from the midstride position and scroll the screen if necessary
;
; Called from the main loop at MAINLOOP when ERIC is midstride.
MVERIC2:
  LD HL,ERICTIMER2        ; Set ERIC's midstride action timer at ERICTIMER2 to
  LD A,(HL)               ; 0 to indicate that ERIC will no longer be midstride
  LD (HL),0               ; after this movement
  DEC L                   ; Set ERIC's main action timer at ERICTIMER equal to
  LD (HL),A               ; the previous contents of ERICTIMER2
  LD HL,44131             ; Point HL at byte 99 of ERIC's buffer
  CALL UPDATESRB          ; Update the SRB for ERIC's current location
  LD L,99                 ; Pick up ERIC's next animatory state in A (from byte
  LD A,(HL)               ; 99 of his buffer) and his next location in DE (from
  INC L                   ; bytes 100 and 101)
  LD D,(HL)               ;
  INC L                   ;
  LD E,(HL)               ;
  CALL UPDATEAS           ; Update ERIC's animatory state and location and
                          ; update the SRB
  CALL UPDATESCR          ; Update the display
  CALL WALKSOUND2         ; Make a walking sound effect
; Decide whether the screen should be scrolled left or right.
  LD HL,ERICCBUF          ; Point HL at byte 96 of ERIC's buffer
  LD A,(LEFTCOL)          ; A=X: leftmost column of the skool on screen (0-64)
  BIT 7,(HL)              ; Is ERIC facing left?
  LD L,98
  JR Z,MVERIC2_0          ; Jump if so
  CP 64                   ; Is the far right end of the skool on screen?
  RET Z                   ; Return if so
  SUB (HL)                ; A=X-x (x=ERIC's x-coordinate)
  CP 234                  ; Is ERIC's screen x-coordinate 22?
  RET NZ                  ; Return if not
  JR LSCROLL8             ; Otherwise scroll the screen left 8 columns
MVERIC2_0:
  AND A                   ; Is the far left end of the skool on screen (X=0)?
  RET Z                   ; Return if so
  SUB (HL)                ; A=X-x (x=ERIC's x-coordinate)
  CP 247                  ; Is ERIC's screen x-coordinate 9?
  RET NZ                  ; Return if not
  JP RSCROLL8             ; Otherwise scroll the screen right 8 columns

; Put ERIC midstride, or raise his arm while writing on a blackboard
;
; Used by the routines at DOWN, UP, LEFT and WRITE.
;
; A ERIC's next animatory state (midstride or arm up)
; B ERIC's post-midstride animatory state
; DE ERIC's post-midstride coordinates
; HL ERIC's midstride coordinates
MVERIC1:
  PUSH HL                 ; Store ERIC's midstride/arm-up animatory state and
  PUSH AF                 ; coordinates briefly
  LD HL,44131             ; Point HL at byte 99 of ERIC's buffer
  LD (HL),B               ; Store ERIC's post-midstride animatory state and
  INC L                   ; coordinates in bytes 99, 100 and 101 of his buffer
  LD (HL),D               ;
  INC L                   ;
  LD (HL),E               ;
  CALL UPDATESRB          ; Update the SRB for ERIC's current location
  POP AF                  ; Restore ERIC's midstride/arm-up animatory state and
  POP DE                  ; location to A and DE
  CALL UPDATEAS           ; Update ERIC's animatory state and location and
                          ; update the SRB
  LD L,4
  LD A,(KEYCODE)          ; KEYCODE holds the ASCII code of the last keypress
  BIT 5,A                 ; Was the last keypress upper case (fast)?
  JR Z,MVERIC1_0          ; Jump (with L=4: fast) if so
  LD L,9                  ; Otherwise set L=9 (slow)
MVERIC1_0:
  LD H,L                  ; Set appropriate values for ERIC's main action timer
  LD (ERICTIMER),HL       ; (ERICTIMER) and midstride action timer (ERICTIMER2)
  JP WALKSOUND2           ; Make a walking (or writing-on-board) sound effect

; Unused
  DEFS 4

; Check whether ERIC is sitting or lying down
;
; Used by the routines at DOWN, UP, LEFT and RIGHT. Returns with ERIC's status
; flags in A, and the zero flag reset if ERIC is lying on his back or sitting
; on a chair, the stairs, or the floor.
STANDING:
  LD A,(STATUS)           ; Collect ERIC's status flags from STATUS
  BIT 7,A                 ; Reset the zero flag if ERIC is sitting or lying
                          ; down
  RET

; Turn ERIC round
;
; Used by the routines at DOWN, UP, LEFT and RIGHT.
TURNERIC:
  LD H,172                ; 172=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current location
  XOR 128                 ; A=animatory state of ERIC facing the other way
  CALL UPDATEAS           ; Update ERIC's animatory state and update the SRB
  CALL WALKSOUND2         ; Make a sound effect
  LD HL,ERICTIMER         ; Set ERIC's action timer at ERICTIMER to 5
  LD (HL),5               ;
  RET

; Unused
;
; Probably remnants of an old version of the routine at TURNERIC.
  DEFB 9                  ; Probably the operand of LD (HL),n (which would have
                          ; set ERIC's action timer at ERICTIMER to 9)
  RET

; 'A' pressed - down
;
; The address of this routine is found in the table of keypress handling
; routines at K_NONE. It is called from the main loop at MAINLOOP when 'A' or
; '6' is pressed, and is also used by the routines at LEFT and RIGHT.
;
; B ERIC's animatory state
; DE ERIC's coordinates
DOWN:
  CALL STANDING           ; Is ERIC sitting or lying down?
  RET NZ                  ; Return if so
  BIT 6,A                 ; Bit 6 of ERIC's status flags at STATUS is always
  JR NZ,26068             ; reset, so this jump (which should be to DOWN_6) is
                          ; never made
  LD A,D                  ; A=ERIC's y-coordinate
  CP 169                  ; Is ERIC on the bottom floor?
  JR Z,DOWN_6             ; Jump forward to move him left or right if so
  CP 162                  ; Is ERIC on the middle floor?
  JR Z,DOWN_4             ; Jump if so
  CP 155                  ; Is ERIC on the top floor?
  JR Z,DOWN_4             ; Jump if so
; ERIC's not on the top, middle or bottom floor, so he must be on a staircase.
; Should he go down a step, or turn round first?
  LD A,E                  ; A=ERIC's x-coordinate
  CP 48                   ; Set the carry flag if ERIC's on a staircase at the
                          ; left of the skool
  BIT 7,B                 ; Set the zero flag if ERIC is facing left
  JR C,DOWN_0             ; Jump if ERIC's on a staircase at the left of the
                          ; skool
  JP NZ,TURNERIC          ; Turn ERIC round if he's facing up a staircase at
                          ; the right of the skool
  JR DOWN_1
DOWN_0:
  JP Z,TURNERIC           ; Turn ERIC round if he's facing up a staircase at
                          ; the left of the skool
; ERIC's at the top of or on a staircase, facing the bottom of it.
DOWN_1:
  LD H,D                  ; Copy ERIC's coordinates to HL
  LD L,E                  ;
  NOP
  INC D                   ; Down a step
  DEC E                   ; One pace to the left
  LD B,2                  ; 2=ERIC standing, facing left
  CP 48                   ; Is ERIC on a staircase on the right side of the
                          ; skool?
  JR NC,DOWN_2            ; Jump if so
  LD B,130                ; 130=ERIC standing, facing right
  INC E                   ; One pace to the right (taking into the account the
  INC E                   ; one pace to the left made above)
DOWN_2:
  LD A,B                  ; A=2/130
  BIT 0,L                 ; A=1/129, B=2/130 if ERIC's current x-coordinate is
  JR Z,DOWN_3             ; even; A=3/131, B=0/128 if ERIC's current
  DEC B                   ; x-coordinate is odd
  DEC B                   ;
  ADD A,2                 ;
DOWN_3:
  DEC A                   ;
  JP MVERIC1              ; Update ERIC's animatory state and location
; ERIC is on the middle floor or the top floor.
DOWN_4:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 12                   ; This is the x-coordinate of the tops of the
                          ; staircases at the left of the skool
  JR NZ,DOWN_5            ; Jump unless ERIC is there
  BIT 7,B                 ; Is ERIC facing left?
  JP Z,LEFT               ; Move ERIC left if so
  JR DOWN_1               ; Otherwise send ERIC down a step
DOWN_5:
  CP 77                   ; This is the x-coordinate of the tops of the
                          ; staircases at the right of the skool
  JR NZ,DOWN_6            ; Jump unless ERIC is there
  BIT 7,B                 ; Is ERIC facing left?
  JR Z,DOWN_1             ; Send ERIC down a step if so
; This entry point is used by the routine at UP.
DOWN_6:
  BIT 7,B                 ; Is ERIC facing left?
  JP Z,LEFT               ; Move ERIC left if so
  JP RIGHT                ; Otherwise move ERIC right

; Unused
  DEFS 9

; 'Q' pressed - up
;
; The address of this routine is found in the table of keypress handling
; routines at K_NONE. It is called from the main loop at MAINLOOP when 'Q' or
; '7' is pressed, and is also used by the routines at LEFT and RIGHT.
;
; B ERIC's animatory state
; DE ERIC's coordinates
UP:
  CALL STANDING           ; Is ERIC sitting or lying down?
  RET NZ                  ; Return if so
  BIT 6,A                 ; Bit 6 of ERIC's status flags at STATUS is always
  JR NZ,DOWN_6            ; reset, so this jump is never made
  LD A,D                  ; A=ERIC's y-coordinate
  CP 155                  ; Is ERIC on the top floor?
  JR Z,DOWN_6             ; Jump if so
  CP 162                  ; Is ERIC on the middle floor?
  JR Z,UP_4               ; Jump if so
  CP 169                  ; Is ERIC on the bottom floor?
  JR Z,UP_4               ; Jump if so
; ERIC's not on the top, middle or bottom floor, so he must be on a staircase.
; Should he go up a step, or turn round first?
  LD A,E                  ; A=ERIC's x-coordinate
  CP 48                   ; Set the carry flag if ERIC's on a staircase at the
                          ; left of the skool
  BIT 7,B                 ; Set the zero flag if ERIC is facing left
  JR C,UP_0               ; Jump if ERIC's on a staircase at the left of the
                          ; skool
  JP Z,TURNERIC           ; Turn ERIC round if he's facing down a staircase at
                          ; the right of the skool
  JR UP_1
UP_0:
  JP NZ,TURNERIC          ; Turn ERIC round if he's facing down a staircase at
                          ; the left of the skool
; ERIC's at the bottom of or on a staircase, facing the top of it.
UP_1:
  DEC D                   ; Up a step
  LD H,D                  ; Copy ERIC's next coordinates to HL
  LD L,E                  ;
  DEC E                   ; One pace to the left
  LD B,2                  ; 2=ERIC standing, facing left
  CP 48                   ; Is ERIC on a staircase on the left side of the
                          ; skool?
  JR C,UP_2               ; Jump if so
  LD B,130                ; 130=ERIC standing, facing right
  INC E                   ; One pace to the right (taking into the account the
  INC E                   ; one pace to the left made above)
UP_2:
  LD A,B                  ; A=2/130
  BIT 0,L                 ; A=3/131, B=2/130 if ERIC's current x-coordinate is
  JR Z,UP_3               ; even (which is a bug); A=3/131, B=0/128 if ERIC's
  DEC B                   ; current x-coordinate is odd
  DEC B                   ;
UP_3:
  ADD A,2                 ;
  DEC A                   ;
  JP MVERIC1              ; Update ERIC's animatory state and location
; ERIC is on the middle floor or the top floor.
UP_4:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 19                   ; This is the x-coordinate of the bottoms of the
                          ; staircases at the left of the skool
  JR NZ,UP_5              ; Jump unless ERIC is here
  BIT 7,B                 ; Is ERIC facing right?
  JP NZ,RIGHT             ; Move ERIC right if so
  JR UP_1                 ; Otherwise send ERIC up a step
UP_5:
  CP 70                   ; This is the x-coordinate of the bottoms of the
                          ; staircases at the right of the skool
  JR NZ,UP_6              ; Jump unless ERIC is here
  BIT 7,B                 ; Is ERIC facing right?
  JR NZ,UP_1              ; Send ERIC up a step if so
UP_6:
  BIT 7,B                 ; Is ERIC facing left?
  JP Z,LEFT               ; Move ERIC left if so
  JP RIGHT                ; Otherwise move ERIC right

; Unused
  DEFS 9

; Check whether a character is on a staircase
;
; Used by the routines at LEFT, RIGHT, SITSTAND, FLOOR, JUMP and WRITE. Returns
; with the zero flag set if the character is not on a staircase.
;
; D Character's y-coordinate
ONSTAIRS:
  LD A,D                  ; A=character's y-coordinate
; This entry point is used by the routines at TRIP, PELLETH, VIOLENT, CATTY and
; CPERICLOC with A holding the character's y-coordinate.
ONSTAIRS_0:
  CP 155                  ; Is the character on the top floor?
  RET Z                   ; Return with the zero flag set if so
  CP 162                  ; Is the character on the middle floor?
  RET Z                   ; Return with the zero flag set if so
  CP 169                  ; Is the character on the top floor?
  RET                     ; Return with the zero flag set if so

; 'O' pressed - left
;
; The address of this routine is found in the table of keypress handling
; routines at K_NONE. It is called from the main loop at MAINLOOP when 'O' or
; '5' is pressed, and is also used by the routines at DOWN and UP.
;
; B ERIC's animatory state
; DE ERIC's coordinates
LEFT:
  CALL STANDING           ; Is ERIC sitting or lying down?
  RET NZ                  ; Return if so
  CALL ONSTAIRS           ; Check whether ERIC is on a staircase
  LD A,E                  ; A=ERIC's x-coordinate
  JR Z,LEFT_0             ; Jump if ERIC's not on a staircase
  CP 48                   ; Is ERIC on a staircase at the right of the skool?
  JP NC,DOWN              ; Move ERIC down a step if so
  JP UP                   ; Otherwise move ERIC up a step
LEFT_0:
  BIT 7,B                 ; Is ERIC facing right?
  JP NZ,TURNERIC          ; Turn ERIC round if so
; ERIC is facing left, and is not on a staircase.
  AND A                   ; Is ERIC standing at the far left side of the skool?
  RET Z                   ; Return if so
  LD A,D                  ; A=ERIC's y-coordinate
  CP 169                  ; Is ERIC on the bottom floor?
  JR Z,LEFT_2             ; Jump if so
  CP 155                  ; Set the zero flag if ERIC is on the top floor
  LD A,58                 ; This is the x-coordinate of the wall between the
                          ; Map and Reading Rooms
  JR Z,LEFT_1             ; Jump if ERIC's on the top floor
  LD A,39                 ; This is the x-coordinate of the wall between the
                          ; Exam and White Rooms
LEFT_1:
  CP E                    ; Is ERIC facing this wall (from the right side)?
  RET Z                   ; Return if so (ERIC cannot walk through walls)
; There is no wall obstructing ERIC, so it's time to put him midstride.
LEFT_2:
  LD A,1                  ; A=1 (ERIC midstride, facing left)
  LD L,E                  ; L=ERIC's current x-coordinate
  DEC E                   ; E=ERIC's post-midstride x-coordinate
; This entry point is used by the routine at RIGHT with A=129 (ERIC facing
; right, midstride).
LEFT_3:
  LD C,A                  ; C=1/129 (ERIC midstride)
  LD A,(KEYCODE)          ; KEYCODE holds the ASCII code of the last keypress
  BIT 5,A                 ; Was the last keypress upper case (fast)?
  JR Z,LEFT_4             ; Jump if so
  LD A,C                  ; Clearly these instructions have no effect, but one
  ADD A,0                 ; wonders what they might once have been and done
  LD C,A                  ;
LEFT_4:
  LD A,(STATUS)           ; Collect ERIC's status flags from STATUS
  BIT 6,A                 ; Bit 6 is always reset, so this jump is always made
  JR Z,LEFT_5             ;
  LD A,C                  ; Clearly these instructions have no effect, but one
  ADD A,0                 ; wonders what they might once have been and done
  LD C,A                  ;
LEFT_5:
  LD A,C                  ; A=1/129 (ERIC midstride)
  LD H,D                  ; H=ERIC's current y-coordinate
  LD B,A                  ; B=2/130 (ERIC standing, phase 3)
  INC B                   ;
  BIT 0,L                 ; Is ERIC's current x-coordinate even?
  JR Z,LEFT_6             ; Jump if so
  DEC B                   ; Adjust ERIC's midstride animatory state (in A) to
  DEC B                   ; 3/131 and post-midstride animatory state (in B) to
  ADD A,2                 ; 0/128 if ERIC's current x-coordinate is odd
LEFT_6:
  JP MVERIC1              ; Put ERIC midstride

; Unused
  DEFS 6

; 'P' pressed - right
;
; The address of this routine is found in the table of keypress handling
; routines at K_NONE. It is called from the main loop at MAINLOOP when 'P' or
; '8' is pressed, and is also used by the routines at DOWN and UP.
;
; B ERIC's animatory state
; DE ERIC's coordinates
RIGHT:
  CALL STANDING           ; Is ERIC sitting or lying down?
  RET NZ                  ; Return if so
  CALL ONSTAIRS           ; Check whether ERIC is on a staircase
  LD A,E                  ; A=ERIC's x-coordinate
  JR Z,RIGHT_0            ; Jump if ERIC's not on a staircase
  CP 48                   ; Is ERIC on a staircase at the right of the skool?
  JP NC,UP                ; Jump if so
  JP DOWN
RIGHT_0:
  BIT 7,B                 ; Is ERIC facing left?
  JP Z,TURNERIC           ; Turn ERIC round if so
; ERIC is facing right, and is not on a staircase.
  CP 93                   ; Is ERIC at the far right of the skool?
  RET Z                   ; Return if so
  LD A,D                  ; A=ERIC's y-coordinate
  CP 169                  ; Is ERIC on the bottom floor?
  JR Z,RIGHT_3            ; Jump if so
  CP 162                  ; Is ERIC on the middle floor?
  JR NZ,RIGHT_1           ; Jump if not
  LD A,E                  ; A=ERIC's x-coordinate
  CP 78                   ; This is the x-coordinate of the far right wall on
                          ; the middle floor
  RET Z                   ; Return if ERIC is facing this wall
  LD A,37                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
  JR RIGHT_2
RIGHT_1:
  LD A,56                 ; This is the x-coordinate of the wall between the
                          ; Reading and Map Rooms
RIGHT_2:
  CP E                    ; Is ERIC facing this wall (from the left side)?
  RET Z                   ; Return if so (ERIC cannot walk through walls)
; There is no wall obstructing ERIC, so it's time to put him midstride.
RIGHT_3:
  LD A,129                ; 129=ERIC midstride, facing right
  LD L,E                  ; L=ERIC's current x-coordinate
  INC E                   ; E=ERIC's post-midstride x-coordinate
  JP LEFT_3               ; Put ERIC midstride

; Unused
  DEFS 6

; Change the lesson
;
; This routine is called from the main loop at MAINLOOP when the lesson clock
; has counted down to zero. It sets each character up with the appropriate
; command list for the next lesson.
NEWLESSON:
  LD HL,5376              ; Reset the lesson clock at CLOCK
  LD (CLOCK),HL           ;
  LD HL,LESSONNO          ; Advance the lesson number at LESSONNO by one and
  INC (HL)                ; get the next lesson number (0-63) in A
  LD A,(HL)               ;
  CP 64                   ;
  JR NZ,NEWLESSON_0       ;
  XOR A                   ;
  LD (HL),A               ;
NEWLESSON_0:
  LD E,A                  ; E=lesson number (0-63)
  CALL GETLESSON          ; Get the identifier for this lesson in A
  CP D                    ; D=254; ring the bell unless this is the second
  PUSH AF                 ; PLAYTIME in a row (only PLAYTIMEs 254 and 255
  CALL C,RINGBELL         ; follow another PLAYTIME)
  LD HL,LFLAGS            ; Clear the flags at LFLAGS, LSIGS, STAMPEDESIGS and
  LD B,20                 ; SPECIALSIGS, the unused bytes at UGSB9, MR CREAK's
NEWLESSON_1:
  LD (HL),0               ; birth year question indicator at ASKED, and the
  INC L                   ; unused byte at UGSB10
  DJNZ NEWLESSON_1        ;
  POP AF
  LD E,A                  ; E=lesson identifier (224-255) from the main
                          ; timetable
  LD D,172                ; Pick up the lesson descriptor (which determines who
  LD A,(DE)               ; is teaching ERIC and in what room) and place it in
  LD (LESSONDESC),A       ; LESSONDESC
  PUSH DE
  CALL PRINTBOX           ; Print the teacher and room names in the lesson box
  POP DE
  LD B,20                 ; There are 20 non-player characters to prepare for
  LD H,152                ; the new lesson, starting with little boy no. 1
                          ; (152)
; This entry point is used by the routine at RESTART with B=1 to restart the
; command list for a single character.
NEWLESSON_2:
  EXX
  LD H,254                ; The addresses of the command lists are in page 254
                          ; at CLISTADDR
  EXX
NEWLESSON_3:
  LD D,H                  ; D=character number (152-171)
  LD L,122                ; Set bit 0 of byte 122 of the character's buffer;
  SET 0,(HL)              ; this will trigger a restart of the command list
                          ; (see MVCHARS)
  LD L,119
  LD A,(DE)               ; Pick up the command list number for this lesson
                          ; from the character's personal timetable
  EXX
  LD L,A                  ; A=LSB of the address of the command list
  LD A,(HL)               ;
  INC L                   ; Point HL' at the MSB of the address of the command
                          ; list
  EXX
  LD (HL),A               ; Copy the LSB to byte 119 of the character's buffer
  INC L                   ; L=120
  EXX
  LD A,(HL)               ; A=MSB of the address of the command list
  EXX
  LD (HL),A               ; Copy the MSB to byte 120 of the character's buffer
  INC H                   ; Next character
  DJNZ NEWLESSON_3        ; Jump back until all characters have been done
  RET

; Get the ASCII code of the last key pressed (unused)
;
; This routine is not used.
XREADKEY:
  SET 3,(IY+48)           ; Put CAPS LOCK on, and continue into READKEY

; Get the ASCII code of the last key pressed
;
; Used by the routines at CPROMPT, GETKEY, JUMPING, GETINPUT and LISTEN.
; Returns with the zero flag set if no key of interest was pressed. Otherwise
; returns with A holding the ASCII code of the last key pressed.
READKEY:
  CALL READJS             ; Get input from the Kempston joystick if it's in use
; This entry point is used by the routines at INPUTDEV and ASKNAMES.
READKEY_0:
  BIT 5,(HL)              ; Check bit 5 of 23611, which will be set if a key
  RES 5,(HL)              ; was pressed; reset bit 5 afterwards
  RET Z                   ; Return with the zero flag set if no key was pressed
  LD A,(23560)            ; Get the code of the last key pressed
  CP 13                   ; Was it ENTER?
  JR Z,READKEY_1          ; Jump if so
  CP 32                   ; Return with the zero flag reset if the last key
  JR C,READKEY_2          ; pressed had an ASCII code between 32 and 127
READKEY_1:
  CP 128                  ; inclusive (or it was ENTER)
  RET C                   ;
READKEY_2:
  XOR A                   ; Set the zero flag to indicate that no key of
                          ; interest was pressed
; The address of this entry point appears in the table of keypress handling
; routines at K_NONE. It is called from the main loop at MAINLOOP whenever a
; non-game key is pressed.
NOACTION:
  RET

; Ring the bell
;
; Used by the routine at NEWLESSON.
;
; L 249
RINGBELL:
  DI                      ; Disable interrupts
  LD H,17                 ; HL=4601 (duration parameter)
  LD D,H                  ; D=17 (blue border)
RINGBELL_0:
  LD A,D                  ; Make the bell sound
  OUT (254),A             ;
  XOR 16                  ;
  LD D,A                  ;
  LD B,128                ;
RINGBELL_1:
  DJNZ RINGBELL_1         ;
  DEC HL                  ;
  LD A,H                  ;
  OR L                    ;
  JR NZ,RINGBELL_0        ;
  EI                      ; Re-enable interrupts before returning
  RET

; Main loop
;
; Entered from the routine at SCRLSKOOL.
MAINLOOP:
  LD HL,(CLOCK)           ; CLOCK holds the lesson clock
  DEC HL                  ; Decrease it by 1 and set the zero flag if it's time
  LD A,H                  ; to ring the bell
  OR L                    ;
  LD (CLOCK),HL           ;
  CALL Z,NEWLESSON        ; Change the lesson if the lesson clock reached zero
  CALL MVCHARS            ; Move the characters
  CALL MOVEERIC           ; Move ERIC
  JR C,MAINLOOP_2         ; Jump if ERIC has already been dealt with
  LD HL,ERICTIMER         ; Decrement ERIC's main action timer at ERICTIMER
  DEC (HL)                ;
  JR NZ,MAINLOOP_2        ; Jump unless it's time to deal with ERIC
  INC L                   ; Pick up the midstride indicator at ERICTIMER2
  LD A,(HL)               ;
  AND A                   ; Is ERIC midstride?
  JR Z,MAINLOOP_0         ; Jump if not
  CALL MVERIC2            ; Move ERIC from the midstride position, then scroll
                          ; the screen left or right if necessary
  JR MAINLOOP_3
; Time to check the keyboard (or simulate a keypress in demo mode) to see how
; ERIC should move next.
MAINLOOP_0:
  DEC L                   ; Reset ERIC's main action timer at ERICTIMER to 9
  LD (HL),9               ;
  CALL GETINPUT           ; Collect or simulate a keypress
  JR Z,MAINLOOP_2         ; Jump if no relevant keypress was collected or
                          ; simulated
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  BIT 5,(HL)              ; Is ERIC writing on a blackboard?
  JR Z,MAINLOOP_1         ; Jump if not
  CALL WRITING            ; Deal with keypresses while ERIC is writing on the
                          ; board
  JR MAINLOOP_2
MAINLOOP_1:
  INC L                   ; DE=KEYCODE (which holds the ASCII code of the last
  EX DE,HL                ; keypress)
  CP 32                   ; Was the keypress ASCII code >= 32?
  JR C,MAINLOOP_2         ; Jump if not
  LD HL,MAINLOOP_2        ; Push the address of the entry point at MAINLOOP_2
  PUSH HL                 ; (see below) onto the stack
  LD L,A                  ; Point HL at the appropriate entry in the keypress
  LD H,104                ; offset table
  LD L,(HL)               ;
  LDI                     ; Copy the ASCII code of the keypress into KEYCODE
  LD E,(HL)               ; Pick up in DE the address of the appropriate
  INC L                   ; routine for dealing with the keypress
  LD D,(HL)               ;
  PUSH DE                 ; Push this address onto the stack
  LD HL,ERICCBUF          ; Point HL at byte 96 of ERIC's buffer
  LD B,(HL)               ; Pick up ERIC's animatory state in B and his
  INC L                   ; coordinates in DE
  LD D,(HL)               ;
  INC L                   ;
  LD E,(HL)               ;
  RET                     ; Make an indirect jump to the appropriate routine
                          ; for dealing with ERIC, then return to MAINLOOP_2
                          ; below
; Now that ERIC has been moved or otherwise dealt with, update the display.
MAINLOOP_2:
  CALL UPDATESCR          ; Update the display
; The next section of code ensures that we don't pass through the main loop
; more than once every 20 milliseconds.
MAINLOOP_3:
  LD HL,LFRAMES           ; Wait until the system variable FRAMES at 23672 has
MAINLOOP_4:
  LD A,(23672)            ; been incremented since the last time it was checked
  CP (HL)                 ; (FRAMES is incremented every 20ms)
  JR Z,MAINLOOP_4         ;
MAINLOOP_5:
  LD (HL),A               ;
  JP MAINLOOP             ; Jump back to the beginning of the main loop

; Unused
  JR Z,MAINLOOP_5         ; Remains of the main loop at MAINLOOP
  JP MAINLOOP             ;
  LD H,L
  ADD HL,BC
  PUSH DE
  EXX
  EX (SP),HL
  LD B,D
  LD C,E

; Get input from the Kempston joystick if it's in use
;
; Used by the routine at READKEY. Returns with the zero flag set if there was
; no joystick input. Otherwise returns to the caller of READKEY with the zero
; flag reset and the ASCII code of the equivalent keypress in A.
READJS:
  LD HL,23611             ; Prepare HL in case we return to READKEY
  LD A,(KEMPSTON)         ; A=107 ('k') if the Kempston joystick is being used,
                          ; 0 otherwise
  AND A                   ; Is the Kempston joystick being used?
  RET Z                   ; Return if not
  IN A,(31)               ; Collect input from the joystick
  AND 31                  ; Check bits 0-4
  RET Z                   ; Return with the zero flag set if there was no input
  POP HL                  ; Drop the return address from the stack so we return
                          ; straight to the caller of READKEY
  LD L,A                  ; L=input from the joystick
  BIT 4,L
  LD A,102                ; 102='f' (fire catapult)
  RET NZ                  ; Return if the fire button was pressed
  LD A,81                 ; 81='Q' (up)
  BIT 3,L
  RET NZ                  ; Return if the joystick was moved up
  DEC A                   ; A=80: 'P' (right)
  BIT 0,L
  RET NZ                  ; Return if the joystick was moved right
  DEC A                   ; A=79: 'O' (left)
  BIT 1,L
  RET NZ                  ; Return if the joystick was moved left
  LD A,65                 ; 65='A' (down)
  AND A                   ; Reset the zero flag
  RET

; Unused
;
; Probably remnants of an old version of the routine at READJS.
  DEFB 81                 ; 81='Q' (up)
  RET
  DEFB 0

; ASCII codes and routine addresses for the keypress offset table
;
; Used by the routine at MAINLOOP. The entries in this table are pointed to by
; the the entries in the keypress offset table at KEYTABLE. Another table of
; ASCII codes and routine addresses can be found at K_WRITE. For more details,
; see the keypress table.
K_NONE:
  DEFB 32                 ; ' ' (SPACE)
  DEFW NOACTION           ; Do nothing
K_UPF:
  DEFB 81                 ; 'Q'
  DEFW UP                 ; Up (fast)
K_DOWNF:
  DEFB 65                 ; 'A'
  DEFW DOWN               ; Down (fast)
K_LEFTF:
  DEFB 79                 ; 'O'
  DEFW LEFT               ; Left (fast)
K_RIGHTF:
  DEFB 80                 ; 'P'
  DEFW RIGHT              ; Right (fast)
K_UP:
  DEFB 113                ; 'q'
  DEFW UP                 ; Up (slow)
K_DOWN:
  DEFB 97                 ; 'a'
  DEFW DOWN               ; Down (slow)
K_LEFT:
  DEFB 111                ; 'o'
  DEFW LEFT               ; Left (slow)
K_RIGHT:
  DEFB 112                ; 'p'
  DEFW RIGHT              ; Right (slow)
K_SITSTAND:
  DEFB 115                ; 's'
  DEFW SITSTAND           ; Sit/stand

; Unused
  DEFB 0

; Input device indicator
;
; Set by the routine at INPUTDEV, and checked by the routine at READJS. Holds
; 107 ('k') if the Kempston joystick is being used, 0 otherwise.
KEMPSTON:
  DEFB 0

; Keypress offset table
;
; Used by the routine at MAINLOOP. Each non-zero entry in this table
; corresponds to a game key, and is the LSB of an entry in the table at K_NONE
; or the table at K_WRITE. For more details, see the keypress table.
KEYTABLE:
  DEFB 0                  ; ' ': Unused
  DEFB 0                  ; '!': Unused
  DEFB 0                  ; '"': Unused
  DEFB 0                  ; '#': Unused
  DEFB 0                  ; '$': Unused
  DEFB 0                  ; '%': Unused
  DEFB 0                  ; '&': Unused
  DEFB 0                  ; ''': Unused
  DEFB 0                  ; '(': Unused
  DEFB 0                  ; ')': Unused
  DEFB 0                  ; '*': Unused
  DEFB 0                  ; '+': Unused
  DEFB 0                  ; ',': Unused
  DEFB 0                  ; '-': Unused
  DEFB 0                  ; '.': Unused
  DEFB 0                  ; '/': Unused
K_NUMERIC:
  DEFB 137                ; '0': Fire catapult
  DEFB 0                  ; '1': Unused
  DEFB 0                  ; '2': Unused
  DEFB 0                  ; '3': Unused
  DEFB 0                  ; '4': Unused
  DEFB 9                  ; '5': Move left (fast)
  DEFB 6                  ; '6': Move down (fast)
  DEFB 3                  ; '7': Move up (fast)
  DEFB 12                 ; '8': Move right (fast)
  DEFB 0                  ; '9': Unused
  DEFB 0                  ; ':': Unused
  DEFB 0                  ; ';': Unused
  DEFB 0                  ; '<': Unused
  DEFB 0                  ; '=': Unused
  DEFB 0                  ; '>': Unused
  DEFB 0                  ; '?': Unused
  DEFB 0                  ; '@': Unused
  DEFB 6                  ; 'A': Move down (fast)
  DEFB 0                  ; 'B': Unused
  DEFB 137                ; 'C': Fire catapult
  DEFB 0                  ; 'D': Unused
  DEFB 0                  ; 'E': Unused
  DEFB 137                ; 'F': Fire catapult
  DEFB 0                  ; 'G': Unused
  DEFB 131                ; 'H': Hit
  DEFB 0                  ; 'I': Unused
  DEFB 134                ; 'J': Jump
  DEFB 0                  ; 'K': Unused
  DEFB 134                ; 'L': Jump
  DEFB 0                  ; 'M': Unused
  DEFB 0                  ; 'N': Unused
  DEFB 9                  ; 'O': Move left (fast)
  DEFB 12                 ; 'P': Move right (fast)
  DEFB 3                  ; 'Q': Move up (fast)
  DEFB 0                  ; 'R': Unused
  DEFB 27                 ; 'S': Sit/stand
  DEFB 0                  ; 'T': Unused
  DEFB 0                  ; 'U': Unused
  DEFB 0                  ; 'V': Unused
  DEFB 128                ; 'W': Write on board
  DEFB 0                  ; 'X': Unused
  DEFB 0                  ; 'Y': Unused
  DEFB 0                  ; 'Z': Unused
  DEFB 0                  ; '[': Unused
  DEFB 0                  ; '\': Unused
  DEFB 0                  ; ']': Unused
  DEFB 0                  ; '↑': Unused
  DEFB 0                  ; '_': Unused
  DEFB 0                  ; '£': Unused
  DEFB 18                 ; 'a': Move down (slow)
  DEFB 0                  ; 'b': Unused
  DEFB 137                ; 'c': Fire catapult
  DEFB 0                  ; 'd': Unused
  DEFB 0                  ; 'e': Unused
  DEFB 137                ; 'f': Fire catapult
  DEFB 0                  ; 'g': Unused
  DEFB 131                ; 'h': Hit
  DEFB 0                  ; 'i': Unused
  DEFB 134                ; 'j': Jump
  DEFB 0                  ; 'k': Unused
  DEFB 134                ; 'l': Jump
  DEFB 0                  ; 'm': Unused
  DEFB 0                  ; 'n': Unused
  DEFB 21                 ; 'o': Move left (slow)
  DEFB 24                 ; 'p': Move right (slow)
  DEFB 15                 ; 'q': Move up (slow)
  DEFB 0                  ; 'r': Unused
  DEFB 27                 ; 's': Sit/stand
  DEFB 0                  ; 't': Unused
  DEFB 0                  ; 'u': Unused
  DEFB 0                  ; 'v': Unused
  DEFB 128                ; 'w': Write on board
  DEFB 0                  ; 'x': Unused
  DEFB 0                  ; 'y': Unused
  DEFB 0                  ; 'z': Unused
  DEFB 0                  ; '{': Unused
  DEFB 0                  ; '|': Unused
  DEFB 0                  ; '}': Unused
  DEFB 0                  ; '~': Unused
  DEFB 0                  ; '©': Unused

; ASCII codes and routine addresses for the keypress offset table
;
; Used by the routine at MAINLOOP. The entries in this table are pointed to by
; the entries in the keypress offset table at KEYTABLE. Another table of ASCII
; codes and routine addresses can be found at K_NONE. For more details, see the
; keypress table.
K_WRITE:
  DEFB 119                ; 'w'
  DEFW WRITE              ; Write
K_HIT:
  DEFB 104                ; 'h'
  DEFW HIT                ; Hit
K_JUMP:
  DEFB 106                ; 'j'
  DEFW JUMP               ; Jump
K_FIRE:
  DEFB 102                ; 'f'
  DEFW FIRE               ; Fire catapult

; Keypress vector table for keyboard input
;
; Used by the routine at INPUTDEV. These 10 bytes are copied to K_NUMERIC in
; the keypress offset table (the entries for keys 0-9) if Int2 is not selected.
NOINT2:
  DEFB 137                ; 0: Fire catapult
  DEFB 0                  ; 1: Unused
  DEFB 0                  ; 2: Unused
  DEFB 0                  ; 3: Unused
  DEFB 0                  ; 4: Unused
  DEFB 9                  ; 5: Move left (fast)
  DEFB 6                  ; 6: Move down (fast)
  DEFB 3                  ; 7: Move up (fast)
  DEFB 12                 ; 8: Move right (fast)
  DEFB 0                  ; 9: Unused

; Keypress vector table for Int2 input
;
; Used by the routine at INPUTDEV. These 10 bytes are copied to K_NUMERIC in
; the keypress offset table (the entries for keys 0-9) if Int2 is selected.
INT2:
  DEFB 137                ; 0: Fire catapult
  DEFB 9                  ; 1: Move left (fast)
  DEFB 12                 ; 2: Move right (fast)
  DEFB 6                  ; 3: Move down (fast)
  DEFB 3                  ; 4: Move up (fast)
  DEFB 137                ; 5: Fire catapult
  DEFB 9                  ; 6: Move left (fast)
  DEFB 12                 ; 7: Move right (fast)
  DEFB 6                  ; 8: Move down (fast)
  DEFB 3                  ; 9: Move up (fast)

; Request and set the input method (keyboard, joystick, Int2)
;
; Used by the routine at ASKNAMES.
INPUTDEV:
  LD HL,M_INPUT           ; M_INPUT: 'CONTROL KEYS -
                          ; Normal/Kempston/Cursor/Int2?'
  CALL PRINTMSG           ; Print this message
INPUTDEV_0:
  LD HL,23611             ; This is the system variable FLAGS
  RES 5,(HL)              ; Signal: no key pressed yet
INPUTDEV_1:
  CALL READKEY_0          ; Wait for a keypress and collect its ASCII code in A
  JR Z,INPUTDEV_1         ;
  LD HL,NOINT2            ; Point HL at the keypress table for keys 0-9 when
                          ; Int2 is not selected
  LD BC,10                ; There are 10 entries in this table
  CP 110                  ; Was 'n' pressed?
  JR Z,INPUTDEV_2         ; Jump if so
  CP 99                   ; Was 'c' pressed?
  JR Z,INPUTDEV_2         ; Jump if so
  CP 107                  ; Was 'k' pressed?
  JR Z,INPUTDEV_3         ; Jump if so
  CP 105                  ; Was 'i' pressed?
  JR NZ,INPUTDEV_0        ; Jump back to collect another keypress if not
  ADD HL,BC               ; HL=INT2 if 'i' was pressed
INPUTDEV_2:
  XOR A                   ; Set A to 0 unless 'k' was pressed
INPUTDEV_3:
  LD DE,K_NUMERIC         ; Copy 10 bytes from NOINT2 or INT2 to K_NUMERIC (the
  LDIR                    ; keypress table entries for keys 0-9)
  LD L,31                 ; HL=KEMPSTON
  LD (HL),A               ; Store 107 ('k' for Kempston) or 0 here as
                          ; appropriate
  LD HL,20480             ; Prepare HL for drawing the catwalk
  RET

; 'CONTROL KEYS - Normal/Kempston/Cursor/Int2?'
;
; Used by the routine at INPUTDEV.
M_INPUT:
  DEFM "CONTROL KEYS - Normal/Kempston/Cursor/Int2?"
  DEFB 0                  ; End marker

; Start a new game or enter demo mode
;
; This section of code is entered at various points. The main entry point below
; is used by the startup routine at 24288.
START:
  LD SP,23806             ; Put the stack pointer somewhere safe
  CALL PLAYTHEME0         ; Play the theme tune
  JR START_1              ; Enter demo mode
; This entry point is used by the routine at SENDHOME2 when ERIC has been sent
; home (either for having mumps or for having over 10000 lines).
START_0:
  CALL RMSPEECH           ; Remove the speech bubble
  LD DE,(SCORE)           ; DE=score
  AND A                   ; Reset the carry flag ready for a subtraction
  SBC HL,DE               ; Subtract the score from the high score...
  ADD HL,DE               ; ...and add it back again
  JR NC,START_1           ; Jump unless we have a new high score
  EX DE,HL                ; HL=new high score
START_1:
  LD A,255                ; 255: demo mode
  JR START_3
; This entry point is used by the routine at EXITDEMO when exiting demo mode.
START_2:
  CALL PREPGAME           ; Ask for the input method, and change the character
                          ; names (if desired)
AFTERNAMES:
  LD HL,(HISCORE)         ; HL=high score
  XOR A                   ; 0: game mode (as opposed to demo mode)
START_3:
  LD SP,23806             ; Put the stack pointer somewhere safe
  PUSH HL                 ; Save the high score
  LD E,A                  ; E=255 (demo mode) or 0
  LD A,(LESSONNO)         ; Collect the current lesson number (0-63) from
                          ; LESSONNO
  AND 48                  ; Set A to 15, 31, 47 or 63 (lessons 0, 16, 32, and
  ADD A,15                ; 48 are PLAYTIME)
  LD D,A                  ; D=new lesson number minus one
  PUSH DE                 ; Save this for now
  CALL UNFLASH            ; Unflash the safe and all shields
  LD HL,LASTCHAR          ; LASTCHAR holds the number (152-171) of the last
                          ; character moved
  LD (HL),151             ; Reset this number so 152 (little boy no. 1) is
                          ; moved next
START_4:
  DEC L                   ; Blank out the rest of the game status buffer
  LD (HL),0               ; (except for the random number seed at RANDSEED)
  JR NZ,START_4           ;
  LD L,232                ; Initialise ERIC's main action timer at ERICTIMER
  LD (HL),17              ;
  LD L,249                ; HL=LESSONNO (which holds the lesson number, 0-63)
  POP DE                  ; Restore the lesson number to D and game mode
                          ; indicator (0 or 255) to E
  LD (HL),D               ; Set the lesson number
  LD L,247                ; Set the lesson clock (at CLOCK) to 1
  INC (HL)                ;
  INC E                   ; If a new game is starting, prepare the shields and
  CALL NZ,NEXTPHASE       ; safe, and play the theme tune
  POP HL                  ; Restore the high score to HL, and save it in
  LD (HISCORE),HL         ; HISCORE
; The following loop sets the initial animatory state, location and character
; flags (byte 122 of the buffer) for each character (including ERIC).
  LD H,152                ; 152=little boy no. 1
  LD DE,INITIALAS         ; The data table at INITIALAS holds the initial
                          ; animatory states of the characters
START_5:
  LD L,96                 ; Point HL at byte 96 of the character buffer
  LD A,(DE)               ; Set the initial animatory state of the character
  LD (HL),A               ;
  INC L                   ; L=97
  INC D                   ; The data table at INITIALX holds the initial
                          ; x-coordinates of the characters
  LD (HL),169             ; All characters start on the bottom floor (y=169)
  INC L                   ; L=98
  LD A,(DE)               ; Pick up the character's initial x-coordinate
  INC D                   ; The data table at INITIAL122 holds the character
                          ; flags
  LD (HL),A               ; Set the character's initial x-coordinate
  LD A,(DE)               ; Pick up the character flags (0=kid; 16=adult;
                          ; 32=pellet)
  DEC D                   ; Point DE back at the table of initial animatory
  DEC D                   ; states
  INC E                   ; Next character
  LD B,29                 ; Blank out the remaining 29 bytes of the character's
START_6:
  INC L                   ; buffer
  LD (HL),0               ;
  DJNZ START_6            ;
  LD L,122                ; Place the character flags into byte 122 of the
  LD (HL),A               ; character's buffer
  INC L
  INC L                   ; L=124
  LD (HL),159             ; Place the address of the continual subcommand
  INC L                   ; routine at DONOWT (RET) into bytes 124 and 125 of
  LD (HL),98              ; the character's buffer
  INC H                   ; Next character
  INC A                   ; Have we done all the non-player characters?
  JR NZ,START_5           ; Jump back to do the next one if not
; All that remains now is to print the logo, lesson box and score box, and
; bring the skool into view.
  CALL PRINTB3L           ; Print the bottom 3 lines of the screen
  JP SCRLSKOOL            ; Scroll the skool into view and enter the main loop

; Unused
  DEFB 101

; Update the SRB for one column of a sprite
;
; Used by the routine at UPDATEAS. Sets the bits in the screen refresh buffer
; (SRB) that correspond to the four tiles in the left, middle or right column
; of a sprite.
;
; DE Base address of the sprite UDG references
; HL SRB address corresponding to the top row of the sprite
SRBCOL:
  LD B,4                  ; There are 4 rows (and 3 columns) in a sprite
SRBCOL_0:
  LD A,(DE)               ; Pick up a sprite UDG reference
  INC D                   ; Next row of the sprite
  AND A                   ; Is this the blank UDG?
  JR Z,SRBCOL_1           ; Jump if so (no need to update this SRB byte)
SRBSET1:
  SET 0,(HL)              ; Set the appropriate bit in the SRB byte; this
                          ; instruction is modified by the routine at UPDATEAS
                          ; before this routine is called
SRBCOL_1:
  LD A,4                  ; Point HL at the byte of the SRB corresponding to
  ADD A,L                 ; the next row of the screen
  LD L,A                  ;
  DJNZ SRBCOL_0           ; Jump back until all 4 squares in the column are
                          ; done
  BIT 3,D                 ; Return with the zero flag reset if we have just
  RET                     ; updated the SRB for the rightmost column of the
                          ; sprite

; Update the display
;
; Used by the routines at MVERIC2, MAINLOOP, RMSPEECH and CATWALK. Goes through
; the screen refresh buffer (SRB) and for every set bit found, updates the
; corresponding character square on-screen.
UPDATESCR:
  LD HL,SRB               ; Point HL at the first byte of the SRB
  LD B,84                 ; B will count the bytes in the screen refresh buffer
UPDATESCR_0:
  LD A,(HL)               ; Pick up an SRB byte
  AND A                   ; Any character squares in this segment need
                          ; refreshing?
  JR Z,UPDATESCR_5        ; Jump if not
  PUSH BC                 ; Save the SRB byte counter
  LD A,L                  ; D=y-coordinate of the segment of 8 character
  AND 124                 ; squares to which this SRB byte corresponds
  RRCA                    ;
  RRCA                    ;
  ADD A,149               ;
  LD D,A                  ;
  LD A,L                  ; E=x-coordinate of the leftmost character square in
  AND 3                   ; the segment
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  LD E,A                  ;
  LD A,(LEFTCOL)          ;
  ADD A,E                 ;
  LD E,A                  ;
  DEC E
; Here we enter a loop to check each SRB byte for set bits.
UPDATESCR_1:
  PUSH HL                 ; Save the pointer to the SRB byte
UPDATESCR_2:
  INC E                   ; Set DE to the coordinates of the next character
                          ; square in the current segment
  RR (HL)                 ; Push an SRB bit into the carry flag, and 0 into bit
                          ; 7 of the SRB byte
  JR C,UPDATESCR_3        ; Refresh the corresponding character square if the
                          ; bit was set
  JR NZ,UPDATESCR_2       ; Jump back until all set bits in the SRB byte have
                          ; been checked
  JR UPDATESCR_4          ; Move to the next SRB byte
; We have found a set bit in the current SRB byte. Refresh the corresponding
; tile on screen.
UPDATESCR_3:
  PUSH DE
  CALL PRINTTILE          ; Print the character square at (E,D)
  POP DE
  POP HL                  ; Restore the pointer to the SRB byte
  AND A                   ; Clear the carry flag so that bit 7 of the SRB byte
  JR UPDATESCR_1          ; will be reset, and jump back to check the next bit
; There are no set bits remaining in the current SRB byte. Move on to the next.
UPDATESCR_4:
  POP HL                  ; Restore the pointer to the SRB byte
  POP BC                  ; Restore the SRB byte counter to B
UPDATESCR_5:
  INC L                   ; Move to the next byte in the screen refresh buffer
  DJNZ UPDATESCR_0        ; Jump back until every byte in the SRB has been
                          ; checked
  RET

; Four skool locations
;
; Used by the routine at RANDDEST.
RANDLOCS:
  DEFB 155,36             ; Revision Library
  DEFB 155,94             ; Fire escape
  DEFB 169,94             ; Gym
  DEFB 169,8              ; Big window

; Set a random destination for a character
;
; Used by the routine at GOTORAND. Selects a destination at random from the
; table of locations at RANDLOCS and places its coordinates into bytes 101 and
; 102 of the character's buffer.
;
; H Character number (152-169)
RANDDEST:
  CALL GETRANDOM          ; A=random number
  AND 6                   ; A=202, 204, 206 or 208
  ADD A,202               ;
  LD E,A                  ; Point DE at one of the four skool locations in the
  LD D,105                ; table at RANDLOCS
  EX DE,HL                ; Pick up the coordinates of the skool location in A
  LD A,(HL)               ; and D
  INC L                   ;
  LD H,(HL)               ;
  EX DE,HL                ;
  LD L,101                ; Place the coordinates of the skool location into
  LD (HL),A               ; bytes 101 and 102 of the character's buffer to make
  INC L                   ; it his next destination
  LD (HL),D               ;
  RET

; Make a character go to a random location
;
; Used by command lists 182, 190, 192, 194, 196, 208, 210, 212, 216 and 220 to
; make a character go to a place at random (selected from the table at
; RANDLOCS).
;
; H Character number (152-169)
; L 122
GOTORAND:
  LD A,(HL)               ; Reset bits 5 and 6 of byte 122 of the character's
  AND 159                 ; buffer, indicating that he should no longer walk
  LD (HL),A               ; fast or slow continuously (if he was doing so)
; This entry point is used by the routine at RANDTRIP.
GOTORAND_0:
  CALL RANDDEST           ; Set a random destination for the character
  LD L,100                ; Prepare the MSB of the primary command routine
  LD (HL),99              ; address in bytes 99 and 100 of the character's
                          ; buffer; the LSB will be set to 209 soon (making the
                          ; routine address READYTOGO)
  JP GOTO_0               ; Set the character on his way

; Unused
  DEFB 0

; Make a little boy trip people up
;
; The address of this continual subcommand routine is placed into bytes 124 and
; 125 of little boy no. 1's character buffer by the routine at RANDTRIP, and
; into bytes 124 and 125 of the character buffers of little boys 2-11 by the
; routine at FOLLOW.
;
; H Little boy's character number (152-162)
TRIP:
  LD L,97                 ; Pick up the little boy's y-coordinate in A
  LD A,(HL)               ;
  CALL ONSTAIRS_0         ; Is the boy on a staircase?
  RET NZ                  ; Return if so
  LD B,10                 ; We will check characters 163-172 (teachers and main
                          ; kids)
  LD DE,41826             ; D=163 (MR WACKER), E=98
  INC L                   ; L=98
  EX DE,HL
TRIP_0:
  LD A,(DE)               ; A=little boy's x-coordinate
  CP (HL)                 ; Does this teacher's or main kid's x-coordinate
                          ; match?
  JR Z,TRIP_2             ; Jump if so
TRIP_1:
  INC H                   ; Next teacher or main kid
  DJNZ TRIP_0             ; Jump back until all teachers and main kids have
                          ; been checked
  EX DE,HL
  RET
; A teacher or main kid is at the same x-coordinate as the little boy. Now
; check the y-coordinates.
TRIP_2:
  DEC E                   ; E=L=97
  DEC L                   ;
  LD A,(DE)               ; A=little boy's y-coordinate
  INC E                   ; E=98
  CP (HL)                 ; Does this teacher's or main kid's y-coordinate
                          ; match?
  INC HL                  ; L=98
  JR NZ,TRIP_1            ; Consider the next teacher or main kid if not
; A teacher or main kid is at the same location as the little boy.
  LD L,112                ; Bytes 111 and 112 of the character's buffer may
  LD A,(HL)               ; hold a routine address; pick up the MSB in A
  LD L,98
  AND A                   ; Is there an uninterruptible subcommand routine
                          ; address in bytes 111 and 112?
  JR NZ,TRIP_1            ; Consider the next teacher or main kid if so
  LD A,H                  ; A=number of the character in the same location as
                          ; the little boy
  CP 172                  ; Is it ERIC?
  JR Z,TRIP_3             ; Jump if so
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),70              ; routine at KNOCKED into bytes 111 and 112 of the
  INC L                   ; teacher's or main kid's buffer
  LD (HL),106             ;
  INC L                   ; Initialise the timing parameter in byte 113 of the
  LD (HL),20              ; character's buffer (which determines how long the
                          ; character will stay down)
  LD L,98
  JR TRIP_1               ; Consider the next teacher or main kid
; ERIC is in the same location as the little boy. Knock ERIC out.
TRIP_3:
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  SET 4,(HL)              ; Signal: ERIC has been knocked out
  EX DE,HL
  RET

; Unused
  DEFS 5

; Make a stricken character give lines if he's a teacher
;
; Used by the routine at LINESLTR.
;
; H Character number (152-169)
DONT:
  LD A,H                  ; Return now unless we are dealing with a teacher
  CP 163                  ;
  RET C                   ;
  CP 167                  ;
  RET NC                  ;
  JP CHKKIDS              ; Give lines to the nearest main kid (if any)

; Deal with a character who has been knocked over
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of a character's buffer by the routines at TRIP and CHECKHIT. It
; knocks the character to the floor, makes him give lines to any nearby kids or
; reveal his safe combination letter (if he's a teacher), and then makes him
; get up.
;
; H Character number (152-169)
KNOCKED:
  LD L,113                ; Byte 113 of the character's buffer holds the
                          ; knockout delay counter
  DEC (HL)                ; Has the character already got up?
  JP Z,RM112              ; Terminate this uninterruptible subcommand if so
  LD A,(HL)               ; Pick up the delay counter in A
  CP 19                   ; Has the character only just been hit?
  JR Z,KNOCKED_0          ; Jump if so
  CALL LINESLTR           ; If the character is a teacher, make him reveal his
                          ; safe combination letter or give lines (as
                          ; appropriate) and exit, or else return here
  NOP
  DEC A                   ; Is it time for the character to get up?
  RET NZ                  ; Return if not
  INC L                   ; Copy the character's pre-decked animatory state
  LD A,(HL)               ; (stored in byte 114 of the character's buffer) to A
  PUSH AF
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  POP AF
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB
; The character has only just been hit, so adjust his animatory state to that
; of 'decked'.
KNOCKED_0:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  LD L,114                ; Store the character's pre-decked animatory state in
  LD (HL),A               ; byte 114 of the buffer
  AND 248                 ; Set A equal to the animatory state of the character
  ADD A,7                 ; lying on the floor
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Make little boy no. 1 go to a place at random and trip people up on the way
;
; Used by command list 204 (which is used only by little boy no. 1).
;
; H 152 (little boy no. 1)
; L 122
RANDTRIP:
  LD A,(HL)               ; Reset bit 6 (do not walk slowly continuously) and
  AND 31                  ; bit 7 (walk fast) and set bit 5 (walk fast
  ADD A,32                ; continuously) of byte 122 of little boy no. 1's
  LD (HL),A               ; buffer
  LD L,124                ; Place the address of the continual subcommand
  LD (HL),246             ; routine at TRIP into bytes 124 and 125 of little
  INC L                   ; boy no. 1's buffer
  LD (HL),105             ;
  JP GOTORAND_0           ; Make little boy no. 1 go to a random location

; Unused
  DEFS 4

; Make a little boy find and follow little boy no. 1
;
; Used by command list 206 (which is used by little boys 2-11).
;
; H Little boy's character number (153-162)
; L 122
FOLLOW:
  LD A,(HL)               ; Reset bit 6 (do not walk slowly continuously) and
  AND 31                  ; bit 7 (walk fast) and set bit 5 (walk fast
  ADD A,32                ; continuously) of byte 122 of the boy's buffer
  LD (HL),A               ;
  LD DE,39037             ; Collect byte 125 of little boy no. 1's buffer,
  LD A,(DE)               ; which holds the MSB of a continual subcommand
                          ; routine address
  CP 105                  ; Return unless little boy no. 1's continual
  RET NZ                  ; subcommand is the routine at TRIP, i.e. he's
                          ; tripping people up as he proceeds to his
                          ; destination
  LD L,E                  ; Place the address of the continual subcommand
  LD (HL),A               ; routine at TRIP into bytes 124 and 125 of this
  DEC L                   ; boy's character buffer (making him trip people up
  LD (HL),246             ; as he follows little boy no. 1)
  LD E,99                 ; Copy bytes 99-102 of little boy no. 1's character
  LD L,E                  ; buffer into bytes 99-102 of this little boy's
  LD BC,4                 ; character buffer; bytes 99 and 100 will then
  EX DE,HL                ; contain READYTOGO, and bytes 101 and 102 will
  LDIR                    ; contain little boy no. 1's destination
  EX DE,HL
  JP GOTO_0               ; Make this little boy follow little boy no. 1

; Unused
  DEFS 6

; Deal with ERIC when he's been knocked over
;
; Used by the routine at MOVEERIC when bit 4 of ERIC's status flags at STATUS
; is set (by the routine at CHECKHIT, PELLETH or DETHRONE).
ERICHIT:
  LD HL,KODELAY           ; KODELAY holds the delay counter used when ERIC has
  LD A,(HL)               ; been knocked over; pick up its value (0-40) in A
  AND A                   ; Has ERIC just been struck (A=0)?
  JR Z,ERICHIT_0          ; Adjust his animatory state if so (and initialise
                          ; the delay counter to 40)
  DEC (HL)                ; Decrement the knockout delay counter
  JP NZ,SNDEFFECT         ; Make a knocked-out sound effect if ERIC's just been
                          ; struck
; The knockout delay counter at KODELAY has reached zero, so ERIC can get up
; any time now.
  LD L,251                ; HL=STATUS (ERIC's status flags)
  LD (HL),128             ; Set bit 7 (indicating that ERIC is sitting or lying
                          ; down) and reset bit 4 (so that we don't visit this
                          ; routine again)
  RET
; ERIC has just been struck; decide whether he should be sitting on the floor
; or lying on his back, and adjust his animatory state accordingly.
ERICHIT_0:
  LD (HL),40              ; Initialise the knockout delay counter at KODELAY
  LD L,233                ; Set ERICTIMER2 to 0, indicating that ERIC is not
  LD (HL),A               ; midstride
  LD L,251                ; HL=STATUS (ERIC's status flags)
  LD (HL),144             ; Set bit 7 (indicating that ERIC is sitting or lying
                          ; down) and keep bit 4 set (so that we visit this
                          ; routine again)
  LD H,172                ; 172=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current location
  CP 5                    ; 5=animatory state of ERIC sitting on a chair
  JR Z,ERICHIT_1          ; Jump if ERIC's sitting on a chair
  AND 128
  ADD A,6
ERICHIT_1:
  INC A                   ; A=6 (ERIC sitting on the floor) or 7/135 (ERIC
                          ; lying on his back)
  LD B,A                  ; Store ERIC's new animatory state in B temporarily
  LD A,D                  ; A=ERIC's y-coordinate
  CALL ERICLOC_0          ; Get the y-coordinate of the floor ERIC's on in D
  LD A,B                  ; Restore ERIC's new animatory state to A
  JP UPDATEAS             ; Update ERIC's animatory state and update the SRB

; Return to the main loop having already dealt with ERIC
;
; Used indirectly by the routine at MOVEERIC when ERIC is engaged in an
; activity that precludes responding to keypresses.
ERICDONE:
  SCF                     ; Return to the main loop with the carry flag set to
  RET                     ; indicate that ERIC has already been dealt with

; Deal with  ERIC
;
; Called from the main loop at MAINLOOP. Deals with ERIC when any of bits 0-4
; at STATUS (ERIC's status flags) are set. Returns with the carry flag set if
; and only if ERIC was dealt with, indicating that there is no need to check
; keypresses in the main loop.
MOVEERIC:
  CALL CHKERIC            ; Make any nearby teachers give ERIC lines if he's
                          ; not where he should be, or standing or sitting when
                          ; or where he shouldn't be
  LD A,(STATUS)           ; Check ERIC's status flags at STATUS and return with
  AND 31                  ; the carry flag reset unless ERIC has been knocked
  RET Z                   ; over, or he is firing, hitting, jumping, or being
                          ; spoken to
  LD HL,ERICDONE          ; After dealing with ERIC, return to the main loop
  PUSH HL                 ; via the routine at ERICDONE with the carry flag set
                          ; to indicate that ERIC was dealt with
  BIT 3,A                 ; Is ERIC being spoken to by a little boy?
  JP NZ,LISTEN            ; Jump if so
  CP 16                   ; Has ERIC been knocked over?
  JP NC,ERICHIT           ; Jump if so
  DEC A                   ; Is ERIC firing a catapult?
  JP Z,FIRING             ; Jump if so
  DEC A                   ; Is ERIC hitting?
  JP Z,HITTING            ; Jump if so
  JP JUMPING              ; ERIC is jumping; deal with him accordingly

; Add to the score and print it
;
; Used by the routines at PELLETH, CHKSHIELD and HITTING.
ADD10:
  LD A,1                  ; This will add 10 to the score
; This entry point is used by the routines at CHKSHIELD and HITTING with A
; holding the amount (divided by 10) to add to the score.
ADDPTS:
  LD HL,(SCORE)           ; Pick up the current score in HL
  ADD A,L                 ; Set HL equal to the new score
  LD L,A                  ;
  ADC A,H                 ;
  SUB L                   ;
  LD H,A                  ;
  LD (SCORE),HL           ; Store the new score
  LD DE,20924             ; Set DE to the display file address at which to
                          ; print the score
  JP PRINTNUM             ; Print the new score

; Print the bottom three lines of the screen
;
; Used by the routine at START. Sets the attribute bytes for the bottom three
; lines of the screen, and then prints the Skool Daze logo and the score box.
PRINTB3L:
  LD HL,23200             ; Set HL and DE to the attribute file address for the
  LD D,H                  ; leftmost character square of the third line from
  LD E,L                  ; the the bottom of the screen
PRINTB3L_0:
  LD (HL),18              ; INK 2: PAPER 2 for the bottom three lines of the
  INC L                   ; screen
  JR NZ,PRINTB3L_0        ;
  LD H,239                ; HL=LOGO: Skool Daze logo
  CALL BUF2SCR            ; Print the logo at the bottom left of the screen
  LD DE,23224             ; This is the attribute file address for the top-left
                          ; corner of the score box
  LD HL,SCOREBOX          ; SCOREBOX: Score/Lines/Hi-Score box graphic
  CALL BUF2SCR            ; Print the Score/Lines/Hi-Score box
  LD HL,(HISCORE)         ; HL=high score
  LD A,H                  ; Is the high score zero?
  OR L                    ;
  RET Z                   ; Return if so
  LD DE,20988             ; DE=display file address for the high score
  JP PRINTNUM             ; Print the high score

; Send a character on a mini-walkabout
;
; The address of this primary command routine is placed into bytes 99 and 100
; of a character's buffer by the routine at WALKABOUT. It either sends the
; character to his next walkabout destination (the walkabout origin or some
; spot within 7 spaces to the left of it), or moves to the next command in the
; command list if the designated number of walkabouts has been completed.
;
; H Character number (152-169)
NEXTWALK:
  LD L,103                ; Decrement the walkabouts counter in byte 103 of the
  DEC (HL)                ; character's buffer
  JP Z,NEXTCMD            ; Move to the next command in the command list if all
                          ; the walkabouts have been done
  JP MVTILL_0             ; Otherwise go for another mini-walkabout

; Unused
  DEFS 2

; Make a character walk up and down a few times or until a certain time
;
; Used by command lists 182, 192, 194, 196 and 204 to make a character walk
; about a fixed location until a specified time, or until a certain number of
; walkabouts have been performed.
;
; H Character number (152-169)
; L 122
WALKABOUT:
  LD A,(HL)               ; Reset bits 5 and 6 of byte 122 of the character's
  AND 159                 ; buffer, indicating that he should no longer walk
  LD (HL),A               ; fast or slow continuously (if he was doing so)
  LD L,99                 ; Replace the address of this routine in bytes 99 and
  LD (HL),50              ; 100 of the character's buffer with that of NEXTWALK
  LD L,103                ; Collect the next byte (the number of walkabouts)
  CALL GETPARAM           ; from the command list and place it in byte 103 of
  LD (HL),A               ; the character's buffer
  LD L,101                ; Collect the next byte (the signal byte - always 0)
  CALL GETPARAM           ; from the command list and place it in byte 101 of
  LD (HL),A               ; the character's buffer
  JP MVTILL_0             ; Send the character on his first mini-walkabout

; Lower the signal for a certain event
;
; Used by command lists 204 and 216.
SIGLOWER:
  LD B,134                ; 134 is the code for 'RES 0,(HL)'
  JR SIGRAISE_0           ; Jump into the signal-setting routine

; Raise the signal for a certain event
;
; Used by command lists 128, 130, 132, 134, 204, 208, 210, 212, 214, 216 and
; 218.
;
; H Character number (152-169)
SIGRAISE:
  LD B,198                ; 198 is the code for 'SET 0,(HL)'
; This entry point is used by the routine at SIGLOWER with B=134 (the code for
; 'RES 0,(HL)').
SIGRAISE_0:
  CALL GETPARAM           ; Get the event identifier (0-29) from the command
                          ; list
  RRCA                    ; Point HL at the appropriate signal flag holder
  RRCA                    ; (LFLAGS, LSIGS, STAMPEDESIGS or SPECIALSIGS)
  RRCA                    ;
  LD C,A                  ;
  AND 31                  ;
  ADD A,200               ;
  LD E,A                  ;
  LD D,127                ;
  EX DE,HL                ;
  LD A,C                  ; Modify the instruction at SIGNAL below to 'SET
  RRCA                    ; n,(HL)' or 'RES n,(HL)' as appropriate
  RRCA                    ;
  AND 56                  ;
  ADD A,B                 ;
  LD (27507),A            ;
SIGNAL:
  SET 0,(HL)              ; Lower or raise the signal for the event; this
                          ; instruction is modified immediately before
                          ; execution so that it sets or resets the appropriate
                          ; bit
  EX DE,HL                ; Restore the character number to H
  JP NEXTCMD              ; Move to the next command in the command list

; Unused
  DEFS 5

; Knock the current occupant (if any) out of a chair
;
; Used by the routines at FINDSEAT2 and SITSTAND. Knocks the current occupant
; (if any) out of the chair next to the character looking for a seat.
;
; H Number of the character looking for a seat
CLEARSEAT:
  LD B,11                 ; There are 11 little boys
  LD D,152                ; 152=little boy no. 1
  CALL DETHRONE           ; Knock any occupant out of the chair
  RET C                   ; Return if a little boy was knocked out of the chair
  LD B,3                  ; There are 3 main kids besides ERIC
  LD D,167                ; 167=BOY WANDER
  CALL DETHRONE           ; Knock any occupant out of the chair
  LD D,172                ; 172=ERIC
  RET C                   ; Return if a main kid was knocked out of the chair
  INC B                   ; B=1 (just ERIC)
  CALL DETHRONE           ; Knock ERIC out of the chair if he's there
  JR NC,DETHRONE_3        ; Jump unless ERIC was knocked out of the chair
  RET

; Unused
  DEFB 0

; Check whether a chair is occupied and unseat any occupant
;
; Used by the routine at CLEARSEAT. Returns with the carry flag reset if the
; chair next to the character looking for a seat is not occupied by any of the
; potential occupants being checked. Otherwise knocks the occupant out of the
; chair and returns with the carry flag set.
;
; B Number of potential occupants to check (11, 3, or 1)
; D Number of the first potential occupant to check (152, 167, or 172)
; H Number of the character looking for a seat (152-169)
DETHRONE:
  LD L,98                 ; Byte 98 of a character's buffer holds his
  LD E,L                  ; x-coordinate
  LD A,(DE)               ; A=x-coordinate of the potential dethronee
  CP (HL)                 ; Do the x-coordinates match?
  JR NZ,DETHRONE_0        ; Jump ahead to consider the next character if not
  DEC E                   ; E=L=97 (which byte holds the character's
  DEC L                   ; y-coordinate)
  LD A,(DE)               ; A=y-coordinate of the potential dethronee
  CP (HL)                 ; Do the y-coordinates match?
  JR NZ,DETHRONE_0        ; Jump ahead to consider the next character if not
  DEC E                   ; E=96
  LD A,(DE)               ; A=animatory state of the potential dethronee
  AND 15
  CP 5                    ; Is this character sitting in the chair?
  JR Z,DETHRONE_1         ; Dethrone him if so
DETHRONE_0:
  INC D                   ; Next character to check
  DJNZ DETHRONE           ; Jump back until all potential dethronees have been
                          ; checked
  AND A                   ; Return with the carry flag reset to indicate that
  RET                     ; no one was knocked out of the chair
DETHRONE_1:
  LD A,D                  ; A=number of the character sitting in the chair
  CP 172                  ; Is ERIC sitting here?
  JR NZ,DETHRONE_5        ; Jump if not
  LD DE,STATUS            ; STATUS holds ERIC's status flags
  EX DE,HL
  SET 4,(HL)              ; Signal: ERIC has been knocked out of his chair
DETHRONE_2:
  EX DE,HL
; This entry point is used by the routine at CLEARSEAT to make the
; chair-seeking character sit down after having found a vacant seat.
DETHRONE_3:
  LD L,112                ; Remove the uninterruptible subcommand routine
  LD (HL),0               ; address from bytes 111 and 112 of the buffer of the
                          ; character who is going to sit down
  LD L,96                 ; A=this character's current animatory state
  LD A,(HL)               ;
  AND 248                 ; Set A to the animatory state of this character
  ADD A,5                 ; sitting in a chair
DETHRONE_4:
  INC L                   ; Pick up the character's coordinates in DE
  LD D,(HL)               ;
  INC L                   ;
  LD E,(HL)               ;
  CALL UPDATEAS           ; Update the character's animatory state and update
                          ; the SRB
  SCF                     ; Return with the carry flag set to indicate that a
  RET                     ; character was pushed out of his chair
; Someone is sitting in the chair, but it isn't ERIC.
DETHRONE_5:
  LD E,112                ; Is there an uninterruptible subcommand routine
  LD A,(DE)               ; address in bytes 111 and 112 of the seated
  AND A                   ; character's buffer?
  JR Z,DETHRONE_6         ; Jump if not
; The current occupant of the chair must be in the process of being dethroned
; by someone else at the moment (i.e. bytes 111 and 112 of his buffer hold the
; address of the uninterruptible subcommand routine at UNSEAT1). In that case,
; the chair-seeking character will give way to the dethroner and look for
; another chair.
  LD L,96                 ; A=current animatory state of the character who
  LD A,(HL)               ; wants to sit down
  INC A                   ; Put this character midstride; this has the effect
  JR DETHRONE_4           ; of making him walk to the next chair
; The current occupant of the chair can be dethroned. Make it so.
DETHRONE_6:
  EX DE,HL
  LD (HL),108             ; Place the address of the uninterruptible subcommand
  DEC L                   ; routine at UNSEAT1 into bytes 111 and 112 of the
  LD (HL),85              ; seated character's buffer
  JR DETHRONE_2

; Unused
  DEFS 11

; Check whether a character is beside a chair
;
; Used by the routine at FINDSEAT2. Returns with the zero flag set if the
; character is standing beside a chair. Returns with the carry flag set if the
; character should turn round.
;
; A Character's animatory state
; DE Character's coordinates
; H Character number (152-169)
BYSEAT:
  BIT 7,A                 ; Set the zero flag if the character is facing left
; This entry point is used by the routine at SITSTAND with H=172 (ERIC).
BYSEAT_0:
  LD A,D                  ; A=character's y-coordinate
  JR NZ,BYSEAT_7          ; Jump if the character is facing right
  CP 155                  ; Set the zero flag if the character is on the top
                          ; floor
  LD A,E                  ; A=character's x-coordinate
  JR NZ,BYSEAT_4          ; Jump if the character's on the middle floor
  CP 58                   ; Is the character to the right of the Map Room wall?
  JR NC,BYSEAT_3          ; Jump if so
  CP 48                   ; This is the x-coordinate of the leftmost chair in
                          ; the Reading Room
  RET C                   ; Return if the character is to the left of this
  NOP
  CP 56                   ; This is the x-coordinate of the rightmost chair in
                          ; the Reading Room
BYSEAT_1:
  JR Z,BYSEAT_2           ; Jump if the character is beside the rightmost chair
                          ; in the room
  RET NC                  ; Return if the character is to the right of this
                          ; chair
BYSEAT_2:
  AND A
  BIT 0,A                 ; Return with the zero flag set if the character is
  RET                     ; standing beside a chair
; The character is on the top floor to the right of the Map Room wall.
BYSEAT_3:
  CP 64                   ; This is the x-coordinate of the leftmost chair in
                          ; the Map Room
  RET C                   ; Return if the character is to the left of this
  CP 72                   ; This is the x-coordinate of the rightmost chair in
                          ; the Map Room
  JR BYSEAT_1
; The character is somewhere on the middle floor.
BYSEAT_4:
  CP 39                   ; Is the character to the right of the Exam Room
                          ; wall?
  JR NC,BYSEAT_5          ; Jump if so
  CP 29                   ; This is the x-coordinate of the leftmost chair in
                          ; the White Room
  RET C                   ; Return if the character is to the left of this
  INC A                   ; This is the x-coordinate (+1) of the rightmost
  CP 38                   ; chair in the White Room
  JR BYSEAT_1
; The character is on the middle floor to the right of the Exam Room wall.
BYSEAT_5:
  CP 43                   ; This is the x-coordinate of the leftmost chair in
                          ; the Exam Room
  RET C                   ; Return if the character is to the left of this
  CP 60                   ; This is the x-coordinate of the left end of the
                          ; first set of chairs in the Exam Room
  JR C,BYSEAT_6           ; Jump if the character is to the left of this
  CP 68                   ; This is the x-coordinate of the rightmost chair in
                          ; the Exam Room
  JR BYSEAT_1
; The character is in the Exam Room, to the left of the first set of chairs.
BYSEAT_6:
  INC A                   ; This is the x-coordinate (+1) of the right end of
  CP 54                   ; the second set of chairs in the Exam Room
  JR BYSEAT_1
; The character is facing right.
BYSEAT_7:
  CP 155                  ; Set the zero flag if the character is on the top
                          ; floor
  LD A,E                  ; A=character's x-coordinate
  JR NZ,BYSEAT_10         ; Jump if the character is on the middle floor
  CP 58                   ; This is the x-coordinate of the wall between the
                          ; Reading and Map Rooms
  JR NC,BYSEAT_9          ; Jump if the character is to the right of this
  CP 55                   ; This is the x-coordinate of the spot between the
                          ; two chairs at the right in the Reading Room; if
                          ; this were 'CP 56' instead, kids would be able to
                          ; find the rightmost seat in the room
BYSEAT_8:
  CCF                     ; Clear the carry flag if the character is to the
                          ; left of the rightmost chair in the room (i.e. he's
                          ; still on his way there)
  RET NZ                  ; Return unless the character is beside the rightmost
                          ; chair in the room
  AND A                   ; Reset the zero flag, and set the carry flag to
  CCF                     ; indicate that the character should turn round
  RET
BYSEAT_9:
  CP 72                   ; This is the x-coordinate of the rightmost seat in
                          ; the Map Room
  JR BYSEAT_8
BYSEAT_10:
  CP 39                   ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
  JR NC,BYSEAT_11         ; Jump if the character is to the right of this
  CP 37                   ; This is the x-coordinate of the rightmost chair in
                          ; the White Room
  JR BYSEAT_8
BYSEAT_11:
  CP 68                   ; This is the x-coordinate of the rightmost chair in
                          ; the Exam Room
  JR BYSEAT_8

; Unused
  DEFS 7

; Deal with a character who's been dethroned (1)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of a character's buffer by the routine at DETHRONE when he's been
; knocked out of a chair.
;
; H Dethroned character's number (152-169)
UNSEAT1:
  LD L,113                ; Set the delay counter determining how long the
  LD (HL),4               ; character will sit on the floor before rising
  LD L,111                ; Replace the address of this uninterruptible
  LD (HL),100             ; subcommand routine in bytes 111 and 112 of the
                          ; character's buffer with that of UNSEAT2
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  INC A                   ; A=animatory state of the character sitting on the
                          ; floor
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Deal with a character who's been dethroned (2)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of a character's buffer by the routine at UNSEAT1.
;
; H Dethroned character's number (152-169)
UNSEAT2:
  LD L,113                ; Byte 113 holds the delay counter
  DEC (HL)                ; Is it time to stand up yet?
  RET NZ                  ; Return if not
  LD L,111                ; Replace the address of this routine in bytes 111
  LD (HL),124             ; and 112 of the character's buffer with that of
                          ; FINDSEAT2
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  AND 248                 ; C=animatory state of the character standing up
  LD C,A                  ;
  LD A,H                  ; A=number of the dethroned character
  CP 169                  ; Was EINSTEIN knocked out of his chair?
  JR Z,UNSEAT2_0          ; Jump if so
  DEC E                   ; Anyone but EINSTEIN will look for another chair to
                          ; the left
UNSEAT2_0:
  LD A,C                  ; A=animatory state of the character standing up
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB

; Deal with a character who is looking for a seat
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of a character's buffer by the routine at UNSEAT2 (after he's
; been knocked out of his chair and just got up to look for another one - or
; the same one, in EINSTEIN's case), or by the routine at FINDSEAT.
;
; H Character number (152-169)
FINDSEAT2:
  CALL UPDATESRB          ; Update the SRB for the character's current location
  LD C,A                  ; Store the character's animatory state in C
                          ; temporarily
  BIT 0,A                 ; Is the character midstride?
  JP NZ,WALK_2            ; Finish the stride if so
  CALL BYSEAT             ; Is the character standing beside a chair?
  JP Z,CLEARSEAT          ; If so, knock the current occupant (if any) to the
                          ; floor, make this character sit down, and remove the
                          ; address of this routine from bytes 111 and 112 of
                          ; his buffer
  LD A,C                  ; Restore the character's animatory state to A
  NOP
  JR NC,FINDSEAT2_0       ; Jump unless the character should turn round
  XOR 128                 ; Flip bit 7 of the animatory state, thus turning him
                          ; round
  DEC A                   ; Compensate for the 'INC A' below
FINDSEAT2_0:
  INC A                   ; A=character's new animatory state (midstride, or
                          ; just turned round)
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Unused
  DEFS 10

; Make a character find a seat
;
; Used by command lists 136, 140, 142, 144, 146, 148, 150, 152, 154, 156, 158,
; 160, 162, 164, 166 and 168. Makes a character start looking for a seat.
;
; H Character number (152-169)
FINDSEAT:
  LD L,100                ; Remove the address of this primary command routine
  LD (HL),0               ; from bytes 99 and 100 of the character's buffer,
                          ; triggering a move to the next command in the
                          ; command list (which will be GRASSETC if the
                          ; character is EINSTEIN, or DONOWT=RET otherwise) as
                          ; soon as the character has found a seat
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),124             ; routine at FINDSEAT2 into bytes 111 and 112 of the
  INC L                   ; character's buffer
  LD (HL),108             ;
  JP FINDSEAT2            ; Set the character off in search of a seat

; Unused
  DEFB 0

; 'S' pressed - sit/stand
;
; The address of this routine is found in the table of keypress handling
; routines at K_NONE. It is called from the main loop at MAINLOOP when 'S' is
; pressed.
SITSTAND:
  LD HL,STATUS            ; Flip bit 7 of ERIC's status flags at STATUS
  LD A,(HL)               ;
  XOR 128                 ;
  LD (HL),A               ;
  LD H,172                ; 172=ERIC
  RLCA                    ; Is ERIC sitting now?
  JR C,SITSTAND_0         ; Jump if so
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
  AND 128                 ; A=animatory state of ERIC standing up
  JP UPDATEAS             ; Update ERIC's animatory state and update the SRB
; ERIC is sitting now - on the floor, the stairs, or a chair?
SITSTAND_0:
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
  LD B,A                  ; B=ERIC's animatory state
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  JR Z,SITSTAND_2         ; Jump if not
; ERIC is on a staircase.
  LD A,E                  ; A=ERIC's x-coordinate
  CP 48                   ; Set the carry flag if ERIC's on the left side of
                          ; the skool
  LD A,133                ; 133=ERIC sitting down facing right
  JR C,SITSTAND_1         ; Jump if ERIC is on a staircase on the left side of
                          ; the skool
  LD A,5                  ; 5=ERIC sitting down facing left
SITSTAND_1:
  JP UPDATEAS             ; Update ERIC's animatory state and update the SRB
; ERIC is not on a staircase.
SITSTAND_2:
  CP 169                  ; Is ERIC on the bottom floor?
  JR Z,SITSTAND_3         ; Jump if so
  BIT 7,B                 ; Is ERIC facing right?
  JR NZ,SITSTAND_3        ; Jump if so
  CALL 27633              ; This should be CALL BYSEAT_0 - is ERIC beside a
                          ; chair?
  JP Z,CLEARSEAT          ; Seat him if so
SITSTAND_3:
  LD A,B                  ; A=6 or 134 (ERIC sitting on the floor)
  AND 128                 ;
  ADD A,6                 ;
  JP UPDATEAS             ; Update ERIC's animatory state and update the SRB

; Unused
  DEFS 2

; Make a character stand up if he's not already standing
;
; Used by the routine at GOTO to make sure a character is standing up before
; setting off for his destination.
;
; H Character number (152-169)
STANDUP:
  LD L,99                 ; Change the primary command routine address in bytes
  LD (HL),209             ; 99 and 100 of the character's buffer to READYTOGO
  LD L,96                 ; Point HL at the character's animatory state
  LD A,(HL)               ; Pick this up in A
  AND 7                   ; Keep only bits 0-2
  CP 4                    ; Is the character standing?
  RET C                   ; Return if so
  POP DE                  ; Drop the return address (READYTOGO) from the stack
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  AND 248                 ; A=animatory state of the character standing up
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Unused
  DEFS 5

; Get the base address of the graphic data for a sprite tile
;
; Used by the routine at PRINTTILE. Returns with the base address of the
; graphic data in HL.
;
; A Sprite UDG reference
; H' Character number (152-172)
UDGADDR:
  LD L,A                  ; L=sprite UDG reference
  EXX
  LD A,H                  ; A=character number (152-172)
  CP 163                  ; Is it a little boy?
  JR C,UDGADDR_0          ; Jump if so
  CP 167                  ; Is it a teacher?
  LD A,201
  JR C,UDGADDR_1          ; Jump if so
UDGADDR_0:
  LD A,185
UDGADDR_1:
  EXX
  LD H,A                  ; H=201 if the character is a teacher, 185 otherwise
  RET

; Control the horizontal flight of a catapult pellet
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of a catapult pellet's buffer by the routine at FIRE4. It
; controls the pellet from the beginning of its horizontal flight to the end,
; handing over to the routine at PELLETV if the pellet bounces upwards off some
; obstacle.
;
; H 170 (BOY WANDER's pellet) or 171 (ERIC's pellet)
PELLETH:
  CALL UPDATESRB          ; Update the SRB for the pellet's current location
  LD L,113                ; Byte 113 of the pellet's buffer holds the distance
                          ; left to travel
  DEC (HL)                ; Has the pellet finished travelling?
  JR NZ,PELLETH_1         ; Jump if not
; This entry point is used by the routine at PELLETV. It places the catapult
; pellet off-screen and terminates its flight.
PELLETH_0:
  LD L,98                 ; Set the pellet's x-coordinate to 150 (i.e.
  LD (HL),150             ; off-screen)
  LD L,112                ; Remove the address of this routine from bytes 111
  LD (HL),0               ; and 112 of the pellet's buffer
  RET
; The pellet has not finished travelling. Where to next?
PELLETH_1:
  INC E                   ; Set E equal to the next x-coordinate for the pellet
  RLCA                    ;
  JR C,PELLETH_2          ;
  DEC E                   ;
  DEC E                   ;
PELLETH_2:
  LD A,E                  ; A=pellet's next x-coordinate
  CP 96                   ; Will it still be on-screen?
  JR NC,PELLETH_0         ; Terminate the pellet if not
  LD A,D                  ; A=pellet's y-coordinate
  CALL ONSTAIRS_0         ; Was the pellet fired on the top, middle or bottom
                          ; floor?
  JR NZ,PELLETH_4         ; Jump if not
  CP 169                  ; Is the pellet on the bottom floor?
  JR Z,PELLETH_4          ; Jump if so
  CP 155                  ; Set the zero flag if the pellet is on the top floor
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
  JR NZ,PELLETH_3         ; Jump if the pellet is on the middle floor
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Reading and Map Rooms
PELLETH_3:
  CP E                    ; Has the pellet hit a wall?
  JR Z,PELLETH_0          ; Terminate the pellet if so
PELLETH_4:
  LD L,96                 ; Byte 96 of the pellet's buffer holds its animatory
                          ; state
  LD A,(HL)               ; Pick this up in A
  CALL UPDATEAS           ; Update the pellet's location and update the SRB
  LD L,97                 ; Byte 97 of the pellet's buffer holds its
                          ; y-coordinate
  LD A,(HL)               ; Pick this up in A
  CALL ONSTAIRS_0         ; Was the pellet fired on the top, middle or bottom
                          ; floor?
  JP NZ,PELLETV_0         ; Jump if not
; The pellet was fired on the top, middle or bottom floor. Let's see if it's
; hit anybody.
  LD L,113                ; Byte 113 of the pellet's buffer holds the distance
                          ; left to travel
  LD A,(HL)               ; Pick this up in A
  CP 8                    ; Is the pellet ready to hit something?
  RET NC                  ; Return if not
  LD DE,44130             ; Point DE at ERIC's x-coordinate
  LD L,E                  ; L=98
  LD A,(DE)               ; Pick up ERIC's x-coordinate in A
  CP (HL)                 ; Does it match the pellet's x-coordinate?
  JR NZ,PELLETH_6         ; Jump if not
  DEC E                   ; E=97
  DEC L                   ; L=97
  LD A,(DE)               ; Pick up ERIC's y-coordinate in A
  INC E                   ; E=98
  CP (HL)                 ; Compare ERIC's y-coordinate with that of the pellet
  LD L,E                  ; L=98
  JR NZ,PELLETH_6         ; Jump unless ERIC's location matches that of the
                          ; pellet
; ERIC has been hit by the pellet. Knock him over.
  LD DE,STATUS            ; Signal that ERIC has been knocked over by setting
  EX DE,HL                ; bit 4 of ERIC's status flags at STATUS
  SET 4,(HL)              ;
PELLETH_5:
  EX DE,HL
  LD L,113                ; The pellet has hit something; set its remaining
  LD (HL),1               ; distance to travel to 1, so it's terminated next
                          ; time
  LD A,D                  ; A=number of the character hit by the pellet
  CP 168                  ; Was ANGELFACE hit?
  JP Z,ADD10              ; Add to the score and print it if so
  RET
; ERIC wasn't hit by the pellet. What about the main kids and teachers?
PELLETH_6:
  LD D,169                ; 169=EINSTEIN
  LD B,7                  ; There are 3 main kids and 4 teachers
PELLETH_7:
  LD A,(DE)               ; Pick up the potential target's x-coordinate in A
  CP (HL)                 ; Does it match that of the pellet?
  JR Z,PELLETH_9          ; Jump if so
PELLETH_8:
  DEC D                   ; Next main kid or teacher
  DJNZ PELLETH_7          ; Jump back until all the main kids and teachers have
                          ; been checked
  RET
PELLETH_9:
  DEC E                   ; E=97
  DEC L                   ; L=97
  LD A,(DE)               ; Pick up the potential target's y-coordinate in A
  INC E                   ; E=98
  CP (HL)                 ; Compare the potential target's y-coordinate
  LD L,E                  ; L=98
  JR NZ,PELLETH_8         ; Jump unless the potential target's coordinates
                          ; match
; We have found a potential target at the pellet's current coordinates. Can we
; knock him over?
  LD E,112                ; Byte 112 of the character's buffer may hold the MSB
  LD A,(DE)               ; of an uninterruptible subcommand routine address;
                          ; pick this up in A
  LD E,L                  ; E=98
  AND A                   ; Is there an uninterruptible subcommand routine
                          ; address in bytes 111 and 112?
  JR NZ,PELLETH_10        ; Jump if so
  EX DE,HL
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),70              ; routine at KNOCKED into bytes 111 and 112 of the
  INC L                   ; character's buffer, which will knock the character
  LD (HL),106             ; over
  INC L                   ; Initialise the parameter determining how long the
  LD (HL),20              ; character will stay down in byte 113 of his buffer
  JR PELLETH_5
; The potential target may already have been knocked over. Let's check.
PELLETH_10:
  LD E,96                 ; Byte 96 of the buffer holds the potential target's
                          ; animatory state
  LD A,(DE)               ; Pick this up in A
  LD E,L                  ; E=98
  AND 127                 ; Remove the direction bit (bit 7)
  CP 72                   ; Is the potential target a kid?
  JR C,PELLETH_8          ; Jump back to consider the next main kid or teacher
                          ; if so
  AND 7                   ; Retain only bits 0-2 of the teacher's animatory
                          ; state
  CP 7                    ; Has the teacher already been knocked down?
  JR NZ,PELLETH_8         ; Jump back to consider the next teacher if not
; The pellet has hit a teacher who's already been knocked over. Prepare for
; vertical flight.
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),198             ; routine at PELLETV into bytes 111 and 112 of the
                          ; pellet's character buffer, which will make the
                          ; pellet travel upwards from now on
  LD L,113                ; Set the pellet's remaining distance to travel to 5
  LD (HL),5               ; spaces
  RET

; Unused
  DEFB 0

; Control the vertical flight of a catapult pellet
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of a catapult pellet's buffer by the routine at PELLETH. It
; controls the pellet from the beginning of its vertical flight to the end,
; including any collision with a shield.
;
; H 170 (BOY WANDER's pellet) or 171 (ERIC's pellet)
PELLETV:
  CALL UPDATESRB          ; Update the SRB for the pellet's current location
  LD B,A                  ; B=pellet's animatory state
  LD A,D                  ; A=pellet's y-coordinate
  CP 152                  ; Has the pellet hit the ceiling of the top floor?
  JP Z,PELLETH_0          ; Terminate the pellet if so
  DEC D                   ; Up a level
  LD L,113                ; Byte 113 of the pellet's buffer holds the distance
                          ; left to travel
  DEC (HL)                ; Has the pellet finished travelling?
  JP Z,PELLETH_0          ; Jump if so
  LD A,B                  ; A=pellet's animatory state
  CALL UPDATEAS           ; Update the pellet's location and update the SRB
; This entry point is used by the routine at PELLETH to check whether a pellet
; fired from a staircase has hit a shield.
PELLETV_0:
  LD L,97                 ; Byte 97 holds the pellet's y-coordinate
  LD D,(HL)               ; Pick this up in D
  INC D                   ; D=y-coordinate of the pellet's potential target
  INC L                   ; Byte 98 holds the pellet's x-coordinate
  LD E,(HL)               ; Pick this up in E
  INC E                   ; E=x-coordinate of the pellet's potential target
; This entry point is used by the routine at CHECKJP2 with DE holding the
; coordinates of ERIC's hand (and H=172).
PELLETV_1:
  SET 7,E                 ; E=128 + x-coordinate of the potential target
  PUSH HL
  CALL CHKSHIELD          ; Has the pellet (or ERIC's hand) hit a shield?
  POP HL
  RET NC                  ; Return if not
  LD L,113                ; If it was a pellet that hit the shield, this sets
  LD (HL),1               ; its remaining distance to travel to 1, so it will
                          ; be terminated the next time this routine is called
  RET

; Check whether a shield has been hit
;
; Used by the routine at PELLETV. Returns with the carry flag set if a shield
; was hit (either by a catapult pellet or by ERIC while jumping).
;
; D Target y-coordinate
; E 128 + target x-coordinate
CHKSHIELD:
  LD A,(GAMEMODE)         ; GAMEMODE holds the game mode indicator
  RRCA                    ; Are the shields hittable at the moment?
  RET NC                  ; Return if not (it's either demo mode, or ERIC's
                          ; looking for the safe combination)
  RRCA                    ; Now bit 7 of C is reset if we're in 'flash the
  LD C,A                  ; shields' mode, or set if in 'unflash the shields'
                          ; mode
  LD B,10                 ; 100 points for hitting a shield on the top floor
  LD A,D                  ; A=y-coordinate of the potential target
  CP 154                  ; This is the y-coordinate of the shields on the top
                          ; floor
  JR Z,CHKSHIELD_0        ; Jump if the pellet or ERIC's hand is at
                          ; shield-height on the top floor
  CP 160                  ; This is the y-coordinate of the shields on the
                          ; middle floor
  LD B,20                 ; 200 points for hitting a shield on the middle floor
  JR Z,CHKSHIELD_0        ; Jump if the pellet or ERIC's hand is at
                          ; shield-height on the middle floor
  LD B,40                 ; 400 points for hitting a shield on the bottom floor
  CP 166                  ; This is the y-coordinate of the shields on the
                          ; bottom floor
  JR Z,CHKSHIELD_0        ; Jump if the pellet or ERIC's hand is at
                          ; shield-height on the bottom floor
  AND A                   ; Reset the carry flag to indicate that no shield was
                          ; hit
  RET
; The pellet or ERIC's hand is at the right height to hit a shield. Is it at
; the right x-coordinate?
CHKSHIELD_0:
  CALL GETATTR            ; Is the potential target on-screen?
  CCF
  RET NC                  ; Return with the carry flag reset if not
  LD A,(DE)               ; A=attribute of the potential target
  AND 56                  ; Return if the potential target is drawn in black
  RET Z                   ; paper or black ink (which is normally true only if
  LD A,(DE)               ; it's not a shield, but is also true if a speech
  AND 7                   ; bubble has been drawn over a shield; in this case,
  RET Z                   ; the routine exits here, which is a bug)
  LD A,(DE)               ; A=attribute of the shield
  XOR C                   ; Check whether we should make the shields flash or
  RLCA                    ; not flash, and return unless the flash status of
  RET C                   ; the shield should be toggled
  LD A,(DE)               ; A=attribute of the shield
  XOR 128                 ; Toggle the FLASH status of the shield
  LD (DE),A               ; Insert the new attribute byte into the skool
                          ; graphic data
  LD (HL),A               ; Apply the new attribute on-screen
  LD A,B                  ; A=10 (top floor), 20 (middle) or 40 (bottom)
  CALL ADDPTS             ; Add 100, 200 or 400 to the score and print it
  CALL SHIELDSND          ; Play the 'hit a shield' sound effect
  LD HL,SHIELDCNT         ; SHIELDCNT holds the shield counter
  DEC (HL)                ; One more shield hit
  SCF                     ; Set the carry flag to indicate that a shield was
                          ; hit
  RET NZ                  ; Return unless the last shield was hit
  CALL NEXTPHASE          ; All the shields have been hit; take appropriate
                          ; action
  SCF                     ; Set the carry flag to indicate that a shield was
                          ; hit
  RET

; Select a special PLAYTIME occasionally
;
; Used by the routine at GETLESSON. Three times out of eight, this routine
; returns with the identifier of a 'special' playtime (243, 244 or 245) in A;
; the rest of the time it returns with A holding the PLAYTIME identifier from
; the main timetable.
;
; A Random number
; DE Address of the main timetable entry
PLAYTIME:
  AND 7                   ; A=random number between 238 and 245
  ADD A,238               ;
  CP 243                  ; Return if A is 243, 244, or 245 (one of the
  RET NC                  ; 'special' playtimes)
  LD A,(DE)               ; Otherwise return with A holding the PLAYTIME
  RET                     ; identifier from the main timetable

; Check whether a boy can be seen by a teacher
;
; Used by the routines at VIOLENT, CATTY and FIRING. Returns with the carry
; flag set if the boy can be seen by a teacher, and the teacher's character
; number in H'.
;
; H 167 (BOY WANDER), 168 (ANGELFACE) or 172 (ERIC)
VISIBLE:
  CALL FLOOR              ; Get the floor (155, 162, 169) nearest to the
                          ; character in D
; This entry point is used by the routine at CHKERIC.
VISIBLE_0:
  CALL VISRANGE           ; Get the teacher's visibility range in [C,B]
  EXX
  LD B,4                  ; There are 4 teachers
  LD H,163                ; And the first is 163=MR WACKER
VISIBLE_1:
  LD L,98                 ; Collect the teacher's x-coordinate from byte 98 of
  LD A,(HL)               ; his buffer
  EXX
  CP B                    ; Is the teacher at or to the left of the upper bound
                          ; of the visibility range?
  JR Z,VISIBLE_4          ; Jump if so
  JR C,VISIBLE_4          ;
VISIBLE_2:
  EXX
VISIBLE_3:
  INC H                   ; Next teacher
  DJNZ VISIBLE_1          ; Jump back until all four teachers are done
  AND A                   ; Clear the carry flag to indicate that the boy
                          ; wasn't seen by a teacher
  EXX
  RET
VISIBLE_4:
  CP C                    ; Is the teacher to the left of the lower bound of
                          ; the visibility range?
  JR C,VISIBLE_2          ; Jump back to consider the next teacher if so
; The teacher's x-coordinate is within the visibility range [C,B]. Now check
; his y-coordinate.
  EXX
  LD L,112                ; Is there an uninterruptible subcommand routine
  LD A,(HL)               ; address in bytes 111 and 112 of the teacher's
  AND A                   ; buffer?
  JR NZ,VISIBLE_3         ; Jump back to consider the next teacher if so (this
                          ; one is occupied)
  LD L,97                 ; Collect the teacher's y-coordinate from byte 97 of
  LD A,(HL)               ; his buffer
  INC L                   ; L=98
  EXX
  CP 158                  ; Is the teacher near or on the top floor?
  JR NC,VISIBLE_5         ; Jump if not
  LD A,155                ; This is the y-coordinate of the top floor
  JR VISIBLE_6
VISIBLE_5:
  CP 165
  LD A,162                ; This is the y-coordinate of the middle floor
  JR C,VISIBLE_6          ; Jump if the teacher is near or on the middle floor
  LD A,169                ; This is the y-coordinate of the bottom floor
VISIBLE_6:
  CP D                    ; Is the teacher within the y-coordinate visibility
                          ; range?
  JR NZ,VISIBLE_2         ; Jump back to consider the next teacher if not
; The teacher is close enough to the boy to see him. But is the teacher facing
; the right way?
  EXX
  LD A,(HL)               ; A=teacher's x-coordinate
  EXX
  CP E                    ; Is the teacher standing right where the boy is?
  SCF                     ; Return with the carry flag set if so
  RET Z                   ;
  CP E                    ; A=128 if the boy is to the right of the teacher, 0
  LD A,0                  ; if the boy is to the left
  RRA                     ;
  EXX
  LD L,96                 ; Byte 96 of the teacher's buffer holds his animatory
                          ; state
  XOR (HL)                ; Reset bit 7 of A if the teacher is facing the boy
  LD L,98
  EXX
  RLCA                    ; Push bit 7 of A into the carry flag
  CCF                     ; Return with the carry flag set if the teacher is
  RET C                   ; facing the boy
  JR VISIBLE_2            ; Otherwise consider the next teacher

; Unused
  DEFB 0

; Determine which floor is nearest to a character
;
; Used by the routines at VISIBLE and CHKKIDS. Returns with D holding 155
; (top), 162 (middle), or 169 (bottom), corresponding to the floor nearest to
; the character.
;
; H 167 (BOY WANDER), 168 (ANGELFACE), 169 (EINSTEIN) or 172 (ERIC)
FLOOR:
  LD L,98                 ; Byte 98 of a character's buffer holds his
                          ; x-coordinate
  LD E,(HL)               ; Pick this up in E
  DEC L                   ; L=97
  LD D,(HL)               ; D=character's y-coordinate
  DEC L                   ; L=96
; This entry point is used by the routines at CHKERIC and CHKKIDS.
FLOOR_0:
  CALL ONSTAIRS           ; Is the character on a staircase?
  RET Z                   ; Return if not
; The character is on a staircase. Which floor is he nearest?
  LD D,155                ; This is the y-coordinate of the top floor
  CP 158                  ; Is the character nearest the top floor?
  RET C                   ; Return if so
  LD D,162                ; This is the y-coordinate of the middle floor
  CP 165                  ; Is the character nearest the middle floor?
  RET C                   ; Return if so
  LD D,169                ; This is the y-coordinate of the bottom floor
  RET

; Get the x-coordinate range within which a character can see or be seen
;
; Used by the routines at VISIBLE and CHKKIDS. Returns with C and B holding the
; lower and upper x-coordinates of the range within which the target character
; can see or be seen. This routine is called with DE holding either (a) the
; coordinates of a teacher (to determine where kids must be in order to get
; lines), or (b) the coordinates of a kid (to determine where a teacher must be
; in order to give lines).
;
; D Target character's floor: 155 (top), 162 (middle) or 169 (bottom)
; E Target character's x-coordinate
VISRANGE:
  LD A,E                  ; A=x (character's x-coordinate)
  LD BC,24576             ; B=96, C=0 (maximum and minimum bounds)
  SUB 10                  ; Is the character's x-coordinate < 10?
  JR C,VISRANGE_1         ; Jump if so
  LD C,A                  ; C=x-10
  LD A,D                  ; A=floor nearest to the character
  CP 169                  ; Is the character nearest the bottom floor?
  JR Z,VISRANGE_1         ; Jump if so
  CP 162                  ; Set the zero flag if the character is nearest the
                          ; middle floor
  LD A,38                 ; This is the x-coordinate of the Exam Room wall
  JR Z,VISRANGE_0         ; Jump if the character is nearest the middle floor
  LD A,57                 ; This is the x-coordinate of the Map Room wall
VISRANGE_0:
  CP C                    ; Is the character more than 10 paces to the right of
                          ; a wall?
  JR C,VISRANGE_1         ; Jump if so
  CP E                    ; Is the character to the left of a wall?
  JR NC,VISRANGE_1        ; Jump if so
  LD C,A                  ; C=x-coordinate of the wall that the character is
                          ; within 10 paces to the right of
; Now C holds the lower x-coordinate of the visibility range. Time to calculate
; the upper x-coordinate.
VISRANGE_1:
  LD A,E                  ; A=x (character's x-coordinate)
  ADD A,10                ; A=x+10
  CP B                    ; Is the character within 10 paces of the far right
                          ; wall?
  RET NC                  ; Return if so
  LD B,A                  ; B=x+10
  LD A,D                  ; A=floor nearest to the character
  CP 169                  ; Is the character nearest the bottom floor?
  RET Z                   ; Return if so
  CP 162                  ; Set the zero flag if the character is nearest the
                          ; middle floor
  LD A,37                 ; This is the x-coordinate of the White Room wall
  JR Z,VISRANGE_2         ; Jump if the character is nearest the middle floor
  LD A,56                 ; This is the x-coordinate of the Reading Room wall
VISRANGE_2:
  CP B                    ; Is the character more than 10 paces to the left of
                          ; a wall?
  RET NC                  ; Return if so
  CP E                    ; Is the character to the right of a wall?
  RET C                   ; Return if so
  LD B,A                  ; B=x-coordinate of the wall
  RET

; Unused
  DEFS 36

; Check whether a character was or can be punched by ERIC or ANGELFACE
;
; Used by the routines at VIOLENT and CHECKHIT. On entry, E holds the
; x-coordinate of the spot three spaces in front of ANGELFACE to check whether
; it's worth raising his fist (VIOLENT), or the x-coordinate of the spot two
; spaces in front of ERIC or ANGELFACE when he has already raised his fist
; (CHECKHIT). Returns with the carry flag set if the potential victim is at the
; target coordinates and is facing the right way.
;
; C Punching character's animatory state
; D Punching character's y-coordinate
; E Target x-coordinate (2 or 3 spaces in front of the punching character)
; H Character number of the potential victim (152-172)
VICTIM:
  LD L,98                 ; Byte 98 of a character's buffer holds his
                          ; x-coordinate
  LD A,(HL)               ; Pick this up in A
  CP E                    ; Is this character at the target x-coordinate?
  JR Z,VICTIM_1           ; Jump if so
VICTIM_0:
  AND A                   ; Reset the carry flag to indicate absence of
                          ; character
  RET
VICTIM_1:
  DEC L                   ; Byte 97 holds the character's y-coordinate
  LD A,(HL)               ; Pick this up in A
  CP D                    ; Is this character at the target y-coordinate?
  JR NZ,VICTIM_0          ; Jump if not
; The character being checked has been found at the target coordinates. But is
; he facing the right way?
  DEC L                   ; Byte 96 holds the character's animatory state
  LD A,(HL)               ; Pick this up in A
  XOR C                   ; Now A=128 if the character and puncher are facing
  AND 128                 ; each other, and 0 otherwise
  RET Z                   ; Return if the character's not facing the puncher
  LD L,112                ; Is there an uninterruptible subcommand routine
  LD A,(HL)               ; address in bytes 111 and 112 of the potential
  AND A                   ; victim's buffer (i.e. is he not in a punchable
                          ; state at the moment)?
  RET NZ                  ; Return with the carry flag reset if so
  SCF                     ; Set the carry flag to indicate the presence of a
                          ; ripe target
  RET

; Unused
  DEFS 4

; Make ANGELFACE hit now and then
;
; The address of this continual subcommand routine is placed into bytes 124 and
; 125 of ANGELFACE's buffer by command lists 142, 150, 158, 166, 174, 180 and
; 182. It decides whether ANGELFACE should throw a punch, and gets the punching
; action under way if so.
;
; H 168 (ANGELFACE)
VIOLENT:
  LD L,112                ; Return now if there is an uninterruptible
  LD A,(HL)               ; subcommand routine address in bytes 111 and 112 of
  AND A                   ; ANGELFACE's buffer (meaning he is currently
  RET NZ                  ; occupied)
  LD L,96                 ; Byte 96 holds ANGELFACE's animatory state
  BIT 0,(HL)              ; Is ANGELFACE midstride?
  RET NZ                  ; Return if so
  INC L                   ; Byte 97 holds ANGELFACE's y-coordinate
  LD A,(HL)               ; Pick this up in A
  CALL ONSTAIRS_0         ; Is ANGELFACE on a staircase?
  RET NZ                  ; Return if so
  CALL VISIBLE            ; Can any teachers see ANGELFACE?
  RET C                   ; Return if so
; ANGELFACE is not on a staircase, and there are no teachers nearby. Are there
; any potential victims nearby?
  LD L,98                 ; Byte 98 holds ANGELFACE's x-coordinate
  LD A,(HL)               ; Pick this up in A
  DEC L                   ; Pick up ANGELFACE's y-coordinate in D
  LD D,(HL)               ;
  DEC L                   ; L=96 (which byte holds ANGELFACE's animatory state)
  SUB 3                   ; Set A equal to the 'target' x-coordinate (i.e.
  BIT 7,(HL)              ; where a character would have to be in order to
  JR Z,VIOLENT_0          ; receive ANGELFACE's punch)
  ADD A,6                 ;
VIOLENT_0:
  LD E,A                  ; Place the target x-coordinate in E
  LD C,(HL)               ; C=ANGELFACE's animatory state
  LD H,172                ; 172=ERIC
  CALL VICTIM             ; Is ERIC a potential target?
  JR NC,VIOLENT_2         ; Jump if not
  NOP
; If we get here, there is a potential victim in front of ANGELFACE for him to
; take a swing at.
VIOLENT_1:
  POP HL                  ; Drop the return address (MVCHARS_2) from the stack;
                          ; this makes the character-moving routine at MVCHARS
                          ; skip any further consideration of ANGELFACE on this
                          ; pass, thus preventing him from moving midstride
                          ; before throwing the punch
  LD HL,43121             ; Point HL at byte 113 of ANGELFACE's buffer
  LD (HL),C               ; Store his current animatory state there
  DEC L                   ; Place the address of the uninterruptible subcommand
  LD (HL),111             ; routine at PUNCH1 (throw a punch) into bytes 111
  DEC L                   ; and 112 of ANGELFACE's buffer
  LD (HL),128             ;
  RET
; ERIC is not a potential target for ANGELFACE's fist. What about the other
; main kids?
VIOLENT_2:
  LD H,169                ; 169=EINSTEIN
  LD B,3                  ; There are three main kids
VIOLENT_3:
  CALL VICTIM             ; Is this main kid a potential target?
  JR NC,VIOLENT_4         ; Jump if not
  JR VIOLENT_1            ; Otherwise throw a punch
VIOLENT_4:
  DEC H                   ; Next main kid
  DJNZ VIOLENT_3          ; Jump back until the three main kids have been
                          ; checked
; EINSTEIN and BOY WANDER weren't potential targets either. What about the
; little boys?
  LD H,152                ; 152=little boy no. 1
  LD B,7                  ; We check only the first seven little boys -
                          ; interesting!
VIOLENT_5:
  CALL VICTIM             ; Is this little boy a potential target?
  JR NC,VIOLENT_6         ; Jump if not
  JR VIOLENT_1            ; Otherwise throw a punch
VIOLENT_6:
  INC H                   ; Next little boy
  DJNZ VIOLENT_5          ; Jump back until the seven little boys have been
                          ; checked
  LD H,168                ; Restore ANGELFACE's character number to H
  RET

; Unused
  DEFS 11

; Make ANGELFACE throw a punch (1)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of ANGELFACE's buffer by the routine at VIOLENT.
;
; H 168 (ANGELFACE)
PUNCH1:
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),142             ; routine at PUNCH2 into bytes 111 and 112 of
                          ; ANGELFACE's buffer
  CALL UPDATESRB          ; Update the SRB for ANGELFACE's current animatory
                          ; state
  AND 128                 ; Retain the direction bit of ANGELFACE's animatory
                          ; state
  ADD A,42                ; 42=animatory state of ANGELFACE hitting (phase 1)
  JP UPDATEAS             ; Update ANGELFACE's animatory state and update the
                          ; SRB

; Make ANGELFACE throw a punch (2)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of ANGELFACE's buffer by the routine at PUNCH1.
;
; H 168 (ANGELFACE)
PUNCH2:
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),226             ; routine at PUNCH3 into bytes 111 and 112 of
                          ; ANGELFACE's buffer
  CALL UPDATESRB          ; Update the SRB for ANGELFACE's current animatory
                          ; state
  INC A                   ; A=43/171: ANGELFACE hitting (phase 2)
  CALL UPDATEAS           ; Update ANGELFACE's animatory state and update the
                          ; SRB, then continue into the routine at CHECKHIT

; Check whether anyone was punched by ERIC or ANGELFACE
;
; Continues from PUNCH2. Also used by the routine at HITTING. Checks whether
; anyone was punched by ERIC or ANGELFACE, and knocks them out if so.
;
; H 168 (ANGELFACE) or 172 (ERIC)
CHECKHIT:
  LD L,98                 ; Byte 98 of the puncher's buffer holds his
                          ; x-coordinate
  LD A,(HL)               ; Pick this up in A
  DEC L                   ; L=97
  LD D,(HL)               ; Pick up the puncher's y-coordinate in D
  DEC L                   ; L=96
  LD C,(HL)               ; Pick up the puncher's animatory state in C
  SUB 2                   ; A=x-coordinate of the location two spaces in front
  BIT 7,(HL)              ; of the puncher
  JR Z,CHECKHIT_0         ;
  ADD A,4                 ;
CHECKHIT_0:
  LD E,A                  ; Store the potential victim's x-coordinate in E
  LD A,(STATUS)           ; Collect ERIC's status flags from STATUS
  AND A                   ; Is ERIC doing the punching?
  JR NZ,CHECKHIT_1        ; Jump if so
  LD H,172                ; 172=ERIC
  CALL VICTIM             ; Was ERIC punched?
  JR NC,CHECKHIT_1        ; Jump if not
  LD DE,STATUS            ; STATUS holds ERIC's status flags
  EX DE,HL
  SET 4,(HL)              ; Signal: ERIC was hit
  EX DE,HL
  RET
; Now we check whether anyone was punched. We start with the three main kids.
CHECKHIT_1:
  LD B,3                  ; There are three main kids
  LD H,169                ; 169=EINSTEIN
CHECKHIT_2:
  CALL VICTIM             ; Was this main kid punched?
  JR NC,CHECKHIT_4        ; Jump if not
CHECKHIT_3:
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),70              ; routine at KNOCKED into bytes 111 and 112 of the
  INC L                   ; victim's character buffer
  LD (HL),106             ;
  INC L                   ; Initialise the parameter determining how long the
  LD (HL),20              ; character will stay down (in byte 113)
  RET
CHECKHIT_4:
  DEC H                   ; Next main kid
  DJNZ CHECKHIT_2         ; Jump back until three main kids have been
                          ; considered
; None of the main kids was hit. What about the little boys?
  LD H,152                ; 152=character number of little boy no. 1
  LD B,11                 ; There are 11 little boys
CHECKHIT_5:
  CALL VICTIM             ; Was this little boy hit?
  JR C,CHECKHIT_3         ; Knock him over if so
  INC H                   ; Next little boy
  DJNZ CHECKHIT_5         ; Jump back until all 11 little boys have been
                          ; considered
  RET

; Make ANGELFACE throw a punch (3)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of ANGELFACE's buffer by the routine at PUNCH2.
;
; H 168 (ANGELFACE)
PUNCH3:
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),239             ; routine at PUNCH4 into bytes 111 and 112 of
                          ; ANGELFACE's buffer
  CALL UPDATESRB          ; Update the SRB for ANGELFACE's current animatory
                          ; state
  LD L,113                ; Collect the animatory state of ANGELFACE before he
  LD A,(HL)               ; threw the punch (from byte 113 of his buffer)
  JP UPDATEAS             ; Restore ANGELFACE's pre-punch animatory state and
                          ; update the SRB

; Make ANGELFACE throw a punch (4)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of ANGELFACE's buffer by the routine at PUNCH3.
;
; H 168 (ANGELFACE)
PUNCH4:
  JP RM112                ; Terminate this uninterruptible subcommand
                          ; (ANGELFACE has finished the punch)

; Unused
  DEFS 7

; Make BOY WANDER fire his catapult (1)
;
; Continues from the routine at CATTY.
;
; H 167 (BOY WANDER)
; L 111
FIRE1:
  LD (HL),44              ; Place the address of the uninterruptible subcommand
  INC L                   ; routine at FIRE2 into bytes 111 and 112 of BOY
  LD (HL),112             ; WANDER's buffer
  POP HL                  ; Drop the return address (MVCHARS_2) from the stack
  RET                     ; and go on to consider moving the next character

; Make BOY WANDER fire his catapult now and then
;
; The address of this continual subcommand routine is placed into bytes 124 and
; 125 of BOY WANDER's buffer by command lists 140, 148, 156, 164, 172, 178,
; 192, 194 and 216. It decides whether BOY WANDER should let rip with a pellet,
; and gets the catapult-firing action under way if so.
;
; H 167 (BOY WANDER)
CATTY:
  LD L,98                 ; Byte 98 of BOY WANDER's buffer holds his
                          ; x-coordinate
  LD A,(HL)               ; Pick this up in A
  AND 3                   ; Is BOY WANDER's x-coordinate divisible by 4?
  RET NZ                  ; Return if not
  LD L,96                 ; Byte 96 holds BOY WANDER's animatory state
  BIT 0,(HL)              ; Is BOY WANDER midstride?
  RET NZ                  ; Return if so
  LD L,112                ; Return if there is already an uninterruptible
  LD A,(HL)               ; subcommand routine address in bytes 111 and 112 of
  AND A                   ; BOY WANDER's buffer (he's otherwise occupied)
  RET NZ                  ;
  CALL GETRANDOM          ; A=random number
  RRCA                    ; Return half the time
  RET NC                  ;
  LD L,97                 ; Byte 97 holds BOY WANDER's y-coordinate
  LD A,(HL)               ; Pick this up in A
  CALL ONSTAIRS_0         ; Is BOY WANDER on a staircase?
  RET NZ                  ; Return if so
  CALL VISIBLE            ; Are there any teachers around who can see BOY
                          ; WANDER?
  RET C                   ; Return if so
  LD A,(43618)            ; Pick up the x-coordinate of BOY WANDER's pellet in
                          ; A
  RLCA                    ; Is BOY WANDER's catapult pellet airborne at the
                          ; moment?
  RET NC                  ; Return if so
  LD L,111
  JR FIRE1                ; Make BOY WANDER fire his catapult

; Unused
  DEFS 3

; Make BOY WANDER fire his catapult (2)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of BOY WANDER's buffer by the routine at FIRE1.
;
; H 167 (BOY WANDER)
FIRE2:
  LD L,111                ; Replace the address of this uninterruptible
  LD (HL),61              ; subcommand routine in bytes 111 and 112 of BOY
                          ; WANDER's buffer with FIRE3
  CALL UPDATESRB          ; Update the SRB for BOY WANDER's current animatory
                          ; state
  LD L,113                ; Store BOY WANDER's pre-fire animatory state in byte
  LD (HL),A               ; 113 of his buffer
  AND 128                 ; A=56/184: BOY WANDER firing (phase 1)
  ADD A,56                ;
  JP UPDATEAS             ; Update BOY WANDER's animatory state and update the
                          ; SRB

; Make BOY WANDER fire his catapult (3)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of BOY WANDER's buffer by the routine at FIRE2.
;
; H 167 (BOY WANDER)
FIRE3:
  LD L,111                ; Replace the address of this uninterruptible
  LD (HL),72              ; subcommand routine in bytes 111 and 112 of BOY
                          ; WANDER's buffer with FIRE4
; This entry point is used by the routines at FIRING and HITTING with H=172
; (ERIC).
FIRE3_0:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  INC A                   ; A=animatory state of the character firing (phase 2)
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Make BOY WANDER fire his catapult (4)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of BOY WANDER's buffer by the routine at FIRE3. At this point BOY
; WANDER's catapult is fully raised, and so a pellet is launched.
;
; H 167 (BOY WANDER)
FIRE4:
  LD L,111                ; Replace the address of this uninterruptible
  LD (HL),103             ; subcommand routine in bytes 111 and 112 of BOY
                          ; WANDER's buffer with FIRE5
  LD L,96                 ; Byte 96 of BOY WANDER's buffer holds his animatory
                          ; state (57/185)
  LD A,(HL)               ; Pick this up in A
  ADD A,70                ; Now A=127 or 255 (catapult pellet)
  INC L                   ; Pick up BOY WANDER's coordinates in DE
  LD D,(HL)               ;
  INC L                   ;
  LD E,(HL)               ;
  LD H,170                ; 170=character number of BOY WANDER's catapult
                          ; pellet
; This entry point is used by the routine at FIRING with H=171 (ERIC's catapult
; pellet).
FIRE4_0:
  LD (HL),E               ; Fill in the initial coordinates and animatory state
  DEC L                   ; of the catapult pellet
  LD (HL),D               ;
  DEC L                   ;
  LD (HL),A               ;
  LD L,111                ; Place the address of the uninterruptible subcommand
  LD (HL),28              ; routine at PELLETH into bytes 111 and 112 of the
  INC L                   ; catapult pellet's buffer
  LD (HL),109             ;
  INC L                   ; Initialise the pellet's remaining distance to
  LD (HL),13              ; travel (13 spaces)
  RET

; Make BOY WANDER fire his catapult (5)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of BOY WANDER's buffer by the routine at FIRE4.
;
; H 167 (BOY WANDER)
FIRE5:
  LD L,111                ; Replace the address of this uninterruptible
  LD (HL),114             ; subcommand routine in bytes 111 and 112 of BOY
                          ; WANDER's buffer with FIRE6
; This entry point is used by the routines at FIRING and JUMPING with H=172
; (ERIC).
FIRE5_0:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  DEC A                   ; A=character's next animatory state (lowering
                          ; catapult)
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Make BOY WANDER fire his catapult (6)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of BOY WANDER's buffer by the routine at FIRE5.
;
; H 167 (BOY WANDER)
FIRE6:
  CALL UPDATESRB          ; Update the SRB for BOY WANDER's current animatory
                          ; state
  LD L,113                ; Byte 113 of BOY WANDER's buffer holds his pre-fire
  LD A,(HL)               ; animatory state; pick this up in A
  LD L,111                ; Replace the address of this uninterruptible
  LD (HL),127             ; subcommand routine in bytes 111 and 112 of BOY
                          ; WANDER's buffer with FIRE7
  JP UPDATEAS             ; Restore BOY WANDER's pre-fire animatory state and
                          ; update the SRB

; Make BOY WANDER his fire catapult (7)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 111 and 112 of BOY WANDER's buffer by the routine at FIRE6.
;
; H 167 (BOY WANDER)
FIRE7:
  JP RM112                ; Terminate this uninterruptible subcommand (BOY
                          ; WANDER has finished firing)

; Unused
  DEFS 5

; Update the SRB for a blackboard or the speech bubble lip
;
; Used by the routines at WRITECHR, WIPE2, SPKWAIT and RMBUBBLE. Updates the
; screen refresh buffer (SRB) so that the character squares at (E,D) and
; (E+1,D) are marked dirty. Two character squares in a row are marked dirty to
; ensure that blackboard contents are properly displayed (characters written on
; a blackboard may cross character square boundaries).
;
; D Skool y-coordinate
; E Skool x-coordinate
SRBXY:
  LD A,(LEFTCOL)          ; A=X: leftmost column of the skool on screen (0-64)
  SUB 2                   ; Return unless X-1<=E<=X+31 (i.e. unless the
  JR C,SRBXY_0            ; coordinates in DE correspond to a skool location
  CP E                    ; that is currently on-screen or just off to the
  RET NC                  ; left)
SRBXY_0:
  ADD A,33                ;
  CP E                    ;
  RET C                   ;
  PUSH DE                 ; Store the skool coordinates
  SUB 32                  ; A=E-X (-1, 0-31)
  CPL                     ;
  ADD A,E                 ;
  BIT 7,A                 ; Are the coordinates in DE on-screen?
  JR Z,SRBXY_1            ; Jump if so
  INC A                   ; A=0
SRBXY_1:
  RLCA                    ; A=8x (x=0-31, the screen x-coordinate)
  RLCA                    ;
  RLCA                    ;
  LD E,A                  ; Save this value in E briefly
  AND 56                  ; Modify the SET m,(HL) instruction at SRBSET2 below
  ADD A,198               ;
  LD (28858),A            ;
  LD A,E                  ; Point HL at the appropriate byte in the screen
  RLCA                    ; refresh buffer
  RLCA                    ;
  AND 3                   ;
  LD E,A                  ;
  LD A,D                  ;
  SUB 149                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,E                 ;
  LD E,A                  ;
  LD D,127                ;
  EX DE,HL                ;
SRBSET2:
  SET 0,(HL)              ; Set the appropriate bit in the SRB byte; this
                          ; instruction is modified earlier in this routine to
                          ; set the required bit
  LD A,(28858)
  ADD A,8                 ; Was m=7?
  JR NC,SRBXY_2           ; Jump if not
  INC L                   ; Move to the next SRB byte
  LD A,L                  ; A will be 0 if we wrapped round to the next row of
  AND 3                   ; the screen
  LD A,198                ; 198 will set n=0
  JR Z,SRBXY_3            ; Jump if we wrapped round to the next row
SRBXY_2:
  LD (28878),A            ; Modify the SET n,(HL) instruction below so that
                          ; n=m+1 mod 8
SRBSET3:
  SET 0,(HL)              ; Set the appropriate bit in the SRB byte; this
                          ; instruction is modified immediately before
                          ; execution to set the required bit
SRBXY_3:
  EX DE,HL
  POP DE                  ; Restore the skool coordinates to DE
  RET

; Get the next character of a message being spoken or written
;
; Used by the routines at WRITECHR and SLIDECHR1. Returns with the next
; character in A.
;
; H Number of the character speaking or writing (152-169)
NEXTCHR:
  LD L,110                ; Bytes 109 and 110 of the character's buffer hold
  LD A,(HL)               ; the address of the next character in the submessage
  AND A                   ; Are we in a submessage at the moment?
  JR Z,NEXTCHR_2          ; Jump if not
  LD D,A                  ; DE=address of the next character in the submessage
  DEC L                   ;
  LD E,(HL)               ;
  LD A,(DE)               ; Pick up this character in A
  AND A                   ; Has the submessage finished?
  JR Z,NEXTCHR_1          ; Jump if so
  NOP
NEXTCHR_0:
  INC DE                  ; Move to the next character in the submessage, and
  LD (HL),E               ; store its address in bytes 109 and 110 of the
  INC L                   ; character's buffer
  LD (HL),D               ;
  RET
; The submessage has finished.
NEXTCHR_1:
  LD (HL),A               ; Zero out bytes 109 and 110 of the character's
  INC L                   ; buffer to indicate that the submessage is finished
  LD (HL),A               ;
; We are dealing with the main message.
NEXTCHR_2:
  LD L,108                ; DE=address of the next character in the main
  LD D,(HL)               ; message (collected from bytes 107 and 108 of the
  DEC L                   ; character's buffer)
  LD E,(HL)               ;
  LD A,(DE)               ; Pick up the next character of the message
  INC DE                  ; Move to the next character in the main message, and
  LD (HL),E               ; store its address in bytes 107 and 108 of the
  INC L                   ; character's buffer
  LD (HL),D               ;
  INC L                   ; L=109
  BIT 7,A                 ; Is the ASCII code of the next character < 128?
  JR Z,NEXTCHR_4          ; Jump if so
; The ASCII code of the next character is >= 128, which refers to an entry in
; one of the symbol/element, capital city/country and date/battle
; question-and-answer pairs.
  AND 127                 ; Set DE to M_SYMBOL00+12*(A-128)
  LD E,A                  ;
  EX DE,HL                ;
  ADD A,A                 ;
  ADD A,L                 ;
  LD L,A                  ;
  LD H,56                 ;
  JR NC,NEXTCHR_3         ;
  INC H                   ;
NEXTCHR_3:
  ADD HL,HL               ;
  ADD HL,HL               ;
  DEC H                   ;
  EX DE,HL                ;
  LD A,(DE)               ; Pick up the first character of this submessage in A
  JR NEXTCHR_0            ; and store the next address in bytes 109 and 110 of
                          ; the character's buffer
; The ASCII code of the next character in the message is < 128.
NEXTCHR_4:
  CP 32                   ; Return if the next character is a normal, printable
  RET NC                  ; character (ASCII code >= 32)
  CP 16                   ; Return if the ASCII code of the next character in
  RET C                   ; the message is < 16
; The ASCII code of the next character in the message is 16-24. We have a
; submessage to deal with, which will be one of the following:
;
; +------+-------------+-----------------------------------------+
; | Code | Address     | Submessage                              |
; +------+-------------+-----------------------------------------+
; | 16   | M_ERIC      | 'ERIC'                                  |
; | 17   | M_BOYWANDER | 'BOY WANDER'                            |
; | 18   | M_ANGELFACE | 'ANGELFACE'                             |
; | 19   | M_EINSTEIN  | 'EINSTEIN'                              |
; | 20   | M_WACKER    | 'MR WACKER'                             |
; | 21   | M_ROCKITT   | 'MR ROCKITT'                            |
; | 22   | M_WITHIT    | 'MR WITHIT'                             |
; | 23   | M_CREAK     | 'MR CREAK'                              |
; | 24   | M_PLEASESIR | 'Please Sir - I cannot tell a lie . . ' |
; +------+-------------+-----------------------------------------+
  SUB 16                  ; Point DE at the appropriate submessage from the
  LD E,A                  ; table above
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  SUB E                   ;
  ADD A,A                 ;
  ADD A,128               ;
  LD E,A                  ;
  LD D,217                ;
  LD A,(DE)               ; Pick up the first character of the submessage in A
  JR NEXTCHR_0            ; Store the address of the next character in the
                          ; submessage

; Unused
  DEFS 8

; Get the identifier and coordinates of the blackboard closest to a character
;
; Used by the routines at WRITECHR, WIPE1, WIPE2, WRITEBRD, CLSNOERIC,
; TEACHERIC, WRITE, WRITING and NRBOARD. Returns with A holding the blackboard
; identifier (the LSB of RRBRDPXL, WRBRDPXL or ERBRDPXL) and DE holding the
; coordinates:
;
; +-----+----------+-------------------------+
; | A   | (D,E)    | Description             |
; +-----+----------+-------------------------+
; | 236 | (155,42) | Reading Room blackboard |
; | 238 | (161,26) | White Room blackboard   |
; | 240 | (161,41) | Exam Room blackboard    |
; +-----+----------+-------------------------+
;
; H Character number (163-167)
BOARDID:
  LD L,97                 ; Byte 97 of the character's buffer holds his
                          ; y-coordinate
  LD A,(HL)               ; Pick this up in A
  LD DE,39722             ; D=155 (y-coordinate), E=42 (x-coordinate)
  CP 160                  ; Set the carry flag if the character's on the top
                          ; floor
  LD A,236                ; 236=Reading Room blackboard
  RET C                   ; Return if the character's on the top floor
  INC L                   ; L=98
  LD DE,41242             ; D=161 (y-coordinate), E=26 (x-coordinate)
  LD A,(HL)               ; A=character's x-coordinate
  CP 37                   ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
  LD A,238                ; 238=White Room blackboard
  RET C                   ; Return if the character's to the left of the wall
                          ; between the White and Exam Rooms
  LD E,41                 ; D=161 (y-coordinate), E=41 (x-coordinate)
  LD A,240                ; 240=Exam Room blackboard
  RET

; Write a single character on a blackboard
;
; Used by the routine at WRITEBRD. Returns with the zero flag set if the end of
; the message has been reached.
;
; H Number of the character writing on the board (163-167)
WRITECHR:
  CALL NEXTCHR            ; A=next character in the message being written
  LD B,A                  ; Save this in B briefly
  CALL BOARDID            ; A=identifier of the blackboard being written on
  LD C,A                  ; Transfer this to C
  LD A,B                  ; A=next character in the message being written
  LD B,127                ; Point BC at the appropriate blackboard buffer
  AND A                   ; Is the message finished?
  JR NZ,WRITECHR_1        ; Jump if not
  DEC A                   ; A=255
WRITECHR_0:
  LD (BC),A               ; Store the pixel coordinates of the next character
                          ; to be written on the board
  INC A                   ; Set the zero flag if the message is finished
  RET
; The character has not finished writing on the blackboard. A holds the ASCII
; code of the next letter to be written.
WRITECHR_1:
  CP 2                    ; Is it newline?
  JR NZ,WRITECHR_2        ; Jump if not
  LD A,(BC)               ; Pick up the contents of RRBRDPXL (Reading Room
  AND 64                  ; blackboard), WRBRDPXL (White Room blackboard) or
  XOR 64                  ; ERBRDPXL (Exam Room blackboard) and set it to 0
                          ; (start of top line) or 64 (start of bottom line)
  JR WRITECHR_0
; This entry point is used by the routine at WRITING when ERIC is writing on a
; blackboard with BC=RRBRDPOS (Reading Room blackboard), WRBRDPOS (White Room
; blackboard), or ERBRDPOS (Exam Room blackboard).
WRITECHR_2:
  PUSH HL
  LD L,A                  ; Point HL at the address holding the pixel width of
  LD H,217                ; the character to be written on the board
  LD A,(BC)               ; A=pixel coordinates (0-127) of the location at
                          ; which the character will be written
  AND 63                  ; Discard bit 6 (the line indicator bit)
  ADD A,192               ; Set the carry flag if there is not enough space at
  SCF                     ; the end of the current line to print the character
  ADC A,(HL)              ;
  LD A,(BC)               ; A=pixel coordinates (0-127)
  JR NC,WRITECHR_3        ; Jump if there is enough space to print the
                          ; character on the current line
  AND 64                  ; Otherwise toggle which line (top or bottom) to
  XOR 64                  ; write on and reset the pixel x-coordinate to 0
  LD (BC),A               ; (start of the line)
WRITECHR_3:
  RRCA                    ; Compute the skool coordinates of the location of
  RRCA                    ; the character to be written and store them in DE
  RRCA                    ;
  AND 15                  ;
  CP 8                    ;
  JR C,WRITECHR_4         ;
  INC D                   ;
  AND 7                   ;
WRITECHR_4:
  ADD A,E                 ;
  LD E,A                  ;
  CALL SRBXY              ; Update the screen refresh buffer for these
                          ; coordinates
  LD A,(BC)               ; Modify the RES n,(HL) instruction at CHALK below,
  AND 7                   ; which in effect draws a white pixel (of chalk) at
  ADD A,A                 ; the appropriate spot
  ADD A,A                 ;
  ADD A,A                 ;
  CPL                     ;
  SUB 65                  ;
  LD (29103),A            ;
  LD A,(BC)               ; Compute the pixel x-coordinate (0-63) of the
  ADD A,(HL)              ; rightmost pixel column of the character to be
  INC A                   ; written on the board, and store it in the
  LD (BC),A               ; blackboard buffer
  LD C,(HL)               ; C=pixel width of the character to be written on the
                          ; board
  EX DE,HL                ; Point HL at the skool UDG reference of the
  LD A,(HL)               ; character square of the blackboard that will be
  LD L,A                  ; written on and pick it up in L
; Now we enter the loop that draws the pixel columns of the font character
; bitmap onto the blackboard.
WRITECHR_5:
  LD H,128                ; Point HL at the graphic data for the skool UDG
  CP 233                  ; reference in A; the skool UDG references are
  JR NC,WRITECHR_7        ; 206-191 for the Reading Room board, 238-223 for the
  CP 231                  ; White Room board, and 222-207 for the Exam Room
  JR NC,WRITECHR_6        ; board, decreasing from left to right
  CP 225                  ;
  JR NC,WRITECHR_7        ;
WRITECHR_6:
  LD H,136                ;
WRITECHR_7:
  INC D                   ; A=next pixel column of the character to be written
  LD A,(DE)               ; on the board
  LD B,8                  ; There are 8 pixels in a pixel column
WRITECHR_8:
  RLCA                    ; Draw the pixel column on the blackboard by directly
  JR NC,WRITECHR_9        ; resetting bits in the skool graphic data; any bits
CHALK:
  RES 7,(HL)              ; already reset by previous writing remain reset (the
WRITECHR_9:
  INC H                   ; 'RES n,(HL)' instruction is modified earlier in
  DJNZ WRITECHR_8         ; this routine to reset the required bit)
  DEC C                   ; Next pixel column
  JR Z,WRITECHR_10        ; Jump if we've finished drawing the character on the
                          ; board
  LD A,H                  ; Point HL back at the top pixel row of the skool UDG
  SUB 8                   ; data
  LD H,A                  ;
  LD A,(29103)            ; Modify the RES n,(HL) instruction at CHALK above to
  OR 64                   ; RES m,(HL) where m=n-1 (mod 8), effectively moving
  SUB 8                   ; one pixel column to the right
  AND 191                 ;
  LD (29103),A            ;
  CP 190                  ; Did we wrap round from bit 0 to bit 7?
  JR NZ,WRITECHR_7        ; Continue drawing pixel columns in this character
                          ; square if not
  DEC L                   ; A=skool UDG reference of the next character square
  LD A,L                  ; to the right on the blackboard
  JR WRITECHR_5           ; Draw the next pixel column in the next character
                          ; square
WRITECHR_10:
  INC C                   ; Reset the zero flag to indicate that the message
                          ; isn't finished
  POP HL                  ; Restore the character number to H
  RET

; Unused
  DEFB 120,120,120,120,120,120,120,120,120,120,120

; Make a teacher wipe a blackboard (1)
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of a teacher's buffer by the routine at CLSNOERIC.
;
; H Teacher's character number (163-166)
WIPE1:
  CALL BOARDID            ; A=identifier of the blackboard the teacher is next
                          ; to
  JR WIPE2                ; Jump over the unused code remnants at WIPESKIP
                          ; (which apparently would have made the teacher skip
                          ; wiping the board if it was already clean)

; Unused
;
; Code remnants in the routine at WIPE1. If this code were still in use, it
; would make the teacher skip wiping the board if it was already clean.
WIPESKIP:
  DEFB 127                ; This was probably the operand of 'LD B,127'
  INC C                   ; C=1 + blackboard identifier
  LD A,(BC)               ; A=number of the character who last wrote on the
                          ; board
  AND A                   ; Is the board clean?
  JP Z,RM106              ; Terminate this interruptible subcommand if so
                          ; (there is nothing to wipe)

; Make a teacher wipe a blackboard (2)
;
; Continues from WIPE1.
;
; A Blackboard identifier (236, 238 or 240)
; DE Coordinates of the top-left corner of the blackboard
; H Teacher's character number (163-166)
WIPE2:
  LD L,105                ; Replace the address of the interruptible subcommand
  LD (HL),247             ; routine at WIPE1 in bytes 105 and 106 of the
                          ; teacher's buffer with WIPING (below)
  LD L,107                ; Wiping a blackboard requires 32 distinct actions
  LD (HL),32              ; (in 8 groups of 4: two paces forward, arm up, arm
                          ; down)
  INC L                   ; Store the x-coordinate of the rightmost column of
  LD A,E                  ; the blackboard (which will be wiped first) in byte
  ADD A,7                 ; 108 of the teacher's buffer
  LD (HL),A               ;
  INC L                   ; Store the y-coordinate of the top row of the
  LD (HL),D               ; blackboard in byte 109 of the teacher's buffer
; After the initial call to this routine, each subsequent call enters here.
WIPING:
  LD L,107                ; Decrement the number of board-wiping actions
  DEC (HL)                ; remaining
  BIT 7,(HL)              ; Has the teacher finished wiping the board?
  JR NZ,WIPE2_7           ; Jump if so
  CALL UPDATESRB          ; Update the SRB for the teacher's current location
  LD L,107                ; Is the teacher midstride or is his arm raised?
  BIT 0,(HL)              ;
  JR Z,WIPE2_4            ; Jump if so
  BIT 1,(HL)              ; Is the teacher ready to raise his arm (and wipe)?
  JR Z,WIPE2_0            ; Jump if so
  INC A                   ; A=animatory state of the teacher midstride
  JP UPDATEAS             ; Update the teacher's animatory state and update the
                          ; SRB
; The teacher is ready to raise his arm and wipe a column of the board.
WIPE2_0:
  ADD A,5                 ; A=animatory state of the teacher with his arm
                          ; raised
  CP 72                   ; Are we actually dealing with a teacher?
  JR NC,WIPE2_1           ; Jump if so
  ADD A,8                 ; If ANGELFACE or BOY WANDER were wiping the board, A
                          ; would now hold the animatory state of the boy with
                          ; his arm raised
WIPE2_1:
  CALL UPDATEAS           ; Update the teacher's animatory state and update the
                          ; SRB
  LD L,108                ; E=x-coordinate of the blackboard column to wipe
  LD E,(HL)               ;
  DEC (HL)                ; Decrement the blackboard column x-coordinate
  INC L                   ; D=y-coordinate of the top row of the blackboard
  LD D,(HL)               ; column to wipe
  CALL SRBXY              ; Update the SRB for the upper and lower character
  INC D                   ; squares of the blackboard column that will be wiped
  CALL SRBXY              ;
  LD A,(DE)               ; L=skool UDG reference for the lower character
  LD L,A                  ; square of the blackboard column
  ADD A,8                 ; E=skool UDG reference for the upper character
  LD E,A                  ; square of the blackboard column
  LD H,128                ; Point HL at the base address of the UDG data for
  CP 233                  ; the lower character square of the blackboard
  JR NC,WIPE2_2           ; column, and DE at the base address of the UDG data
  LD H,136                ; for the upper character square of the blackboard
WIPE2_2:
  LD D,H                  ; column
  LD B,8                  ; There are 8 bytes in a UDG
  LD A,255                ; All bits set means clean (no chalk)
WIPE2_3:
  LD (HL),A               ; Wipe the blackboard column clean by altering the
  LD (DE),A               ; skool UDG data directly
  INC H                   ;
  INC D                   ;
  DJNZ WIPE2_3            ;
  RET
; The teacher is midstride or his arm is raised.
WIPE2_4:
  AND 248                 ; A=base animatory state of the teacher
  CP 72                   ; Are we actually dealing with a teacher?
  JR NC,WIPE2_5           ; Jump if so
  AND 240                 ; If ANGELFACE or BOY WANDER were wiping the board, A
                          ; would now hold the base animatory state of the boy
WIPE2_5:
  BIT 1,(HL)              ; Is the teacher's arm raised?
  JR Z,WIPE2_6            ; Jump if so
  DEC E                   ; The teacher was midstride, so move him forward to
                          ; the next blackboard column
WIPE2_6:
  JP UPDATEAS             ; Update the teacher's animatory state and location
                          ; and update the SRB
; The teacher has finished wiping the board.
WIPE2_7:
  CALL BOARDID            ; A=identifier of the blackboard the teacher is next
                          ; to
  LD L,A                  ; HL=RRBRDPXL (Reading Room blackboard), WRBRDPXL
  LD H,127                ; (White Room blackboard), or ERBRDPXL (Exam Room
                          ; blackboard)
  LD (HL),1               ; The first clean pixel column is now column no. 1
  INC L                   ; Signal: no one has written on this blackboard
  LD (HL),0               ;
  JP RM106                ; Terminate this interruptible subcommand

; Unused
  DEFS 6

; Make a character write on a blackboard
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of a character's buffer by the routines at CLSNOERIC, BWWRITECLN and
; TEACHERIC.
;
; H Character number (163-167)
WRITEBRD:
  CALL BOARDID            ; Collect the identifier of the nearest blackboard in
                          ; A
  LD C,A                  ; BC=RRBRDAUTH (Reading Room blackboard), WRBRDAUTH
  LD B,127                ; (White Room blackboard), or ERBRDAUTH (Exam Room
  INC C                   ; blackboard)
  LD A,(BC)               ; A=number of the character who last wrote on the
                          ; board
  AND A                   ; Is the board dirty?
  JP NZ,RM106             ; Terminate this interruptible subcommand if so
  NOP
  NOP
  NOP
; The board is clean, so the character can go ahead and write on it.
  LD A,H                  ; Store this character's number in the blackboard
  LD (BC),A               ; buffer, indicating that he wrote on it
  LD L,110                ; Zero out bytes 109 and 110 of the character's
  LD (HL),0               ; buffer (which hold the submessage address)
  DEC L                   ;
  LD (HL),0               ;
  ADD A,67                ; A=234 (BOY WANDER) or 230-233 (a teacher)
  CP 234                  ; Are we dealing with a teacher?
  JR C,WRITEBRD_0         ; Jump if so
  LD A,229                ; A=229 if we're dealing with BOY WANDER
WRITEBRD_0:
  DEC L                   ; A holds the MSB of the blackboard message address;
  LD (HL),A               ; store this in byte 108 of the character's buffer
  DEC L                   ; Get a random multiple of 32 in A; this will be the
  CALL GETRANDOM          ; LSB of the blackboard message address, which is
  AND 224                 ; stored in byte 107 of the character's buffer
  LD (HL),A               ;
  LD L,105                ; Replace the address of this routine in bytes 105
  LD (HL),145             ; and 106 of the character's buffer with that of the
                          ; entry point at WRITELTR (below)
; This entry point is used on subsequent calls.
WRITELTR:
  CALL WRITECHR           ; Write a single letter on the blackboard
  JR NZ,WRITEBRD_2        ; Jump unless the character has finished writing
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  AND 248                 ; A=base animatory state of the teacher writing on
                          ; the board
  BIT 6,A                 ; Is it actually a teacher writing on the board?
  JR NZ,WRITEBRD_1        ; Jump if so
  AND 240                 ; A=base animatory state of BOY WANDER
WRITEBRD_1:
  CALL UPDATEAS           ; Update the character's animatory state and update
                          ; the SRB
  JP RM106                ; Terminate this interruptible subcommand
; The character hasn't finished writing on the board yet.
WRITEBRD_2:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  BIT 6,A                 ; Set the zero flag if BOY WANDER is writing on the
                          ; board
  LD B,A                  ; B=character's current animatory state
  JR NZ,WRITEBRD_4        ; Jump if a teacher is writing on the board
; BOY WANDER is writing on the board. Determine his next animatory state:
; 60/188 or 61/189.
  AND 127                 ; Drop the direction bit (bit 7) for now
  CP 60                   ; 60=BOY WANDER writing (phase 1)
  LD A,122                ; A/2=61=BOY WANDER writing (phase 2)
  JR Z,WRITEBRD_3         ; Jump if BOY WANDER is in writing phase 1
  LD A,120                ; A/2=60=BOY WANDER writing (phase 1)
WRITEBRD_3:
  RL B                    ; Slide the direction bit into bit 7 of A, which will
  RRA                     ; then hold BOY WANDER's next animatory state
  JP UPDATEAS             ; Update BOY WANDER's animatory state and update the
                          ; SRB
; A teacher is writing on the board. Toggle the status of his arm (up/down).
WRITEBRD_4:
  AND 248                 ; A=base animatory state of the teacher
  CP B                    ; Is the teacher's arm raised?
  JR NZ,WRITEBRD_5        ; Jump if not
  ADD A,5                 ; A=animatory state of the teacher with his arm
                          ; raised
WRITEBRD_5:
  JP UPDATEAS             ; Update the teacher's animatory state and update the
                          ; SRB

; Unused
  DEFS 9

; Update the SRB for the middle six columns of the speech bubble
;
; Used by the routines at RMBUBBLE and OPENLIP. Returns with the carry flag set
; if the speech bubble is off-screen.
SPEECHSRB:
  LD DE,(LIPCOORDS)       ; Copy the speech bubble lip coordinates from
                          ; LIPCOORDS to DE
  LD A,E                  ; Now E holds the x-coordinate of the left edge of
  AND 248                 ; the speech bubble
  LD E,A                  ;
  LD A,(LEFTCOL)          ; A=leftmost column of the skool on screen (0-64)
  SUB E                   ; Is the speech bubble at the far left of the screen?
  JR Z,SPEECHSRB_0        ; Jump if so
  CCF                     ; Return with the carry flag set if the speech bubble
  RET C                   ; is off-screen to the left
  CP 225                  ; Return with the carry flag set if the speech bubble
  RET C                   ; is off-screen to the right
  NEG                     ; A=1, 2 or 3 (corresponding to the quadrant of the
  RRCA                    ; screen containing the speech bubble)
  RRCA                    ;
  RRCA                    ;
SPEECHSRB_0:
  LD E,A                  ; E=0-3 (quadrant of the screen containing the speech
                          ; bubble)
  LD A,D                  ; Point DE at the byte of the screen refresh buffer
  SUB 151                 ; corresponding to the top row of the speech bubble
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,E                 ;
  LD E,A                  ;
  LD D,127                ;
  LD A,(DE)               ; Set bits 1-6 of this screen refresh buffer byte (so
  OR 126                  ; that the part of the screen hidden by the middle 6
  LD (DE),A               ; UDGs of the top row of the speech bubble will be
                          ; refreshed)
  LD A,E                  ; Point DE at the byte of the screen refresh buffer
  ADD A,4                 ; corresponding to the bottom row of the speech
  LD E,A                  ; bubble
  LD A,(DE)               ; Set bits 1-6 of this screen refresh buffer byte (so
  OR 126                  ; that the part of the screen hidden by the middle 6
  LD (DE),A               ; UDGS of the bottom row of the speech bubble will be
                          ; refreshed)
  RET                     ; Return with the carry flag reset

; Unused
  DEFB 0

; Slide a message character into the speech bubble text window (2)
;
; Continues from the routine at SLIDECHR1. Returns with the carry flag set if
; the speech bubble is off-screen.
;
; A ASCII code of the character
SLIDECHR2:
  LD E,A                  ; E=ASCII code of the character from the message
                          ; being spoken
  LD A,(LIPCOORDS)        ; Copy the speech bubble lip x-coordinate from
                          ; LIPCOORDS to A
  AND 120                 ; A=x-coordinate of the right edge of the text window
  ADD A,6                 ; within the speech bubble
  LD HL,LEFTCOL           ; LEFTCOL holds the leftmost column of the skool on
                          ; screen (0-64)
  SUB (HL)                ; Return with the carry flag set if the speech bubble
  RET C                   ; is off-screen to the left
  CP 32                   ; Return with the carry flag set if the speech bubble
  CCF                     ; is off-screen to the right
  RET C                   ;
; The speech bubble is on screen.
  LD C,A                  ; C=screen x-coordinate of the right edge of the text
                          ; window within the speech bubble (6, 14, 22 or 30)
  LD A,(32613)            ; A=y-coordinate of the speech bubble lip
  SUB 154                 ; D=screen y-coordinate of the top row of the speech
  LD D,A                  ; bubble (0-17)
  RRCA                    ; Set HL to the display file address corresponding to
  RRCA                    ; the top right edge of the text window within the
  RRCA                    ; speech bubble
  AND 224                 ;
  ADD A,C                 ;
  LD L,A                  ;
  LD A,D                  ;
  AND 24                  ;
  ADD A,68                ;
  LD H,A                  ;
  LD D,217                ; C=width (in pixels) of the message character bitmap
  LD A,(DE)               ;
  LD C,A                  ;
; Now we enter a loop to slide the pixel columns of the message character
; bitmap into the text window of the speech bubble.
SLIDECHR2_0:
  INC D                   ; A=next pixel column of the message character bitmap
  LD A,(DE)               ;
  CALL SLIDECHR2_1        ; Slide this into the speech bubble text window
  DEC C                   ; Next pixel column
  JR NZ,SLIDECHR2_0       ; Jump back until all pixel columns are done
SLIDECHR2_1:
  PUSH HL                 ; Store the display file address temporarily
  CALL SLIDETXT           ; Slide the top four pixel rows of the text window
                          ; one pixel to the left
  LD B,A                  ; Store the message character pixel column in B
                          ; temporarily
  LD A,L                  ; Set HL to the display file address corresponding to
  ADD A,32                ; the fifth pixel row down from the top right edge of
  LD L,A                  ; the text window within the speech bubble
  JR C,SLIDECHR2_2        ;
  LD A,H                  ;
  SUB 8                   ;
  LD H,A                  ;
SLIDECHR2_2:
  LD A,B                  ; Restore the message character pixel column to A
  CALL SLIDETXT           ; Slide the bottom four pixel rows of the text window
                          ; one pixel to the left
  POP HL                  ; Restore the display file address to HL
  XOR A                   ; Reset the carry flag
  RET

; Unused
;
; Remnants of an old version of the routine at SLIDECHR2.
  CALL SLIDETXT           ; Slide the bottom four pixel rows of the text window
                          ; one pixel to the left
  POP HL                  ; Restore the display file address to HL
  RET

; Print the speech bubble
;
; Used by the routine at SPEAK. Returns with the carry flag set if the
; character about to speak is off-screen. Returns with the zero flag reset if
; somebody else is speaking at the moment.
;
; H Number of the character about to speak (152-169)
SPKWAIT:
  LD A,(LEFTCOL)          ; A=leftmost column of the skool on screen (0-64)
  LD B,A
  LD L,98                 ; Byte 98 of a character's buffer holds his
                          ; x-coordinate
  LD A,(HL)               ; Pick this up in A
  INC A                   ; Return with the carry flag set if the character is
  LD E,A                  ; off-screen to the left
  SUB B                   ;
  RET C                   ;
  CP 32                   ; Return with the carry flag set if the character is
  CCF                     ; off-screen to the right
  RET C                   ;
  LD C,A                  ; C=screen x-coordinate (0-31) of the spot above the
                          ; character's head (where the speech bubble lip will
                          ; be)
  LD A,(32613)            ; A=current speech bubble lip x-coordinate (if any)
  AND A                   ; Is somebody else speaking at the moment?
  RET NZ                  ; Return with the zero flag reset if so
; The character is on-screen and no one else is speaking at the moment.
  LD L,96
  LD A,H                  ; A=character number (152-169)
  INC L                   ; L=97
  CP 167                  ; Set the carry flag unless EINSTEIN is the speaker
  SBC A,A                 ; A=0 if EINSTEIN is speaking, 255 otherwise
  ADD A,(HL)              ; D=y if EINSTEIN is speaking, y-1 otherwise (where y
  LD D,A                  ; is the speaker's y-coordinate)
  LD (LIPCOORDS),DE       ; Store the speech bubble lip coordinates at
                          ; LIPCOORDS
  SUB 152                 ; Get the attribute file address of the speech bubble
  RRCA                    ; lip in BC
  RRCA                    ;
  RRCA                    ;
  LD B,A                  ;
  AND 224                 ;
  ADD A,C                 ;
  LD C,A                  ;
  LD A,B                  ;
  AND 3                   ;
  ADD A,88                ;
  LD B,A                  ;
  LD A,(DE)               ; A=UDG reference of the character square under the
                          ; speech bubble lip
  LD (ULIPUDG),A          ; Store this in ULIPUDG
  LD A,239                ; 239=UDG reference for the speech bubble lip
  LD (DE),A               ; Replace the skool UDG with the speech bubble lip
                          ; UDG
  SET 7,E                 ; Pick up the skool attribute byte of the character
  LD A,(DE)               ; square under the speech bubble lip and store it in
  LD (ULIPATTR),A         ; ULIPATTR
  LD A,56                 ; 56=PAPER 7: INK 0
  LD (DE),A               ; Replace the skool attribute byte
  LD (BC),A               ; Set the new attribute on-screen
; The skool UDG reference and attribute byte for the location of the speech
; bubble lip have been adjusted. Now for the speech bubble itself.
  LD A,E                  ; Get the x-coordinate of the left edge of the speech
  AND 120                 ; bubble in E
  LD E,A                  ;
  DEC D                   ; The speech bubble is two character squares high
  DEC D                   ;
  PUSH HL
  LD HL,65472             ; Set HL to the attribute file address for the
  ADD HL,BC               ; top-left corner of the speech bubble
  LD A,L                  ;
  AND 248                 ;
  LD L,A                  ;
  LD B,8                  ; Fill in the top row of 8 character squares with the
SPKWAIT_0:
  LD (HL),56              ; speech bubble attribute (56=PAPER 7: INK 0)
  INC HL                  ;
  DJNZ SPKWAIT_0          ;
  LD C,24                 ; Set HL to the attribute file address for the
  ADD HL,BC               ; bottom-left corner of the speech bubble
  LD B,8                  ; Fill in the bottom row of 8 character squares with
SPKWAIT_1:
  LD (HL),56              ; the speech bubble attribute (56=PAPER 7: INK 0)
  INC L                   ;
  DJNZ SPKWAIT_1          ;
  LD HL,UTOPUDGS          ; Store the UDG references for the part of the skool
  EX DE,HL                ; that will be overwritten by the top row of the
  LD A,L                  ; speech bubble at UTOPUDGS
  LD C,8                  ;
  LDIR                    ;
  LD L,A                  ; Replace the 8 UDG references for the part of the
  LD BC,2288              ; skool that will be overwritten by the top row of
SPKWAIT_2:
  LD (HL),C               ; the speech bubble with the appropriate speech
  INC L                   ; bubble UDG references (240-247)
  INC C                   ;
  DJNZ SPKWAIT_2          ;
  LD L,A                  ; Store the UDG references for the part of the skool
  INC H                   ; that will be overwritten by the bottom row of the
  LD C,8                  ; speech bubble at UBOTUDGS
  LDIR                    ;
  LD L,A                  ; Replace the 8 UDG references for the part of the
  LD BC,2296              ; skool that will be overwritten by the bottom row of
SPKWAIT_3:
  LD (HL),C               ; the speech bubble with the appropriate speech
  INC L                   ; bubble UDG references (248-255)
  INC C                   ;
  DJNZ SPKWAIT_3          ;
  ADD A,128               ; Store the 8 attribute bytes for the part of the
  LD L,A                  ; skool that will be overwritten by the top row of
  LD C,8                  ; the speech bubble at UTOPATTR
  DEC H                   ;
  LDIR                    ;
  LD L,A                  ; Replace the 8 attribute bytes for the part of the
  LD BC,2104              ; skool that will be overwritten by the top row of
SPKWAIT_4:
  LD (HL),C               ; the speech bubble with the appropriate speech
  INC L                   ; bubble attribute byte (C=56=PAPER 7: INK 0)
  DJNZ SPKWAIT_4          ;
  LD L,A                  ; Store the 8 attribute bytes for the part of the
  LD C,8                  ; skool that will be overwritten by the bottom row of
  INC H                   ; the speech bubble at UBOTATTR
  LDIR                    ;
  LD L,A                  ; Replace the 8 attribute bytes for the part of the
  LD BC,2104              ; skool that will be overwritten by the bottom row of
SPKWAIT_5:
  LD (HL),C               ; the speech bubble with the appropriate speech
  INC L                   ; bubble attribute byte (C=56=PAPER 7: INK 0)
  DJNZ SPKWAIT_5          ;
  LD DE,(LIPCOORDS)       ; Copy the speech bubble lip coordinates from
                          ; LIPCOORDS to DE
  CALL SRBXY              ; Update the SRB for the lip of speech bubble
  CALL OPENLIP            ; Open the lip of the speech bubble
  CALL BUBBLESRB          ; Update the SRB for the speech bubble
  POP HL
  INC A                   ; Set the zero flag
  RET                     ; Return with the zero flag set to indicate success

; Unused
;
; Code remnants, perhaps from an old version of the routine at SPKWAIT.
XSPKWAIT:
  LD B,6                  ; The text window in the speech bubble is 6 character
                          ; squares wide
  LD L,249                ; UDG 249 corresponds to the second tile on the
                          ; bottom row of the speech bubble
XSPKWAIT_0:
  LD (HL),0               ; Blank out a pixel row in the bottom half of the
  INC L                   ; text window in the speech bubble UDGs
  DJNZ XSPKWAIT_0         ;
  INC H                   ; Next pixel row down

; Slide a message character into the speech bubble text window (1)
;
; Used by the routine at SPEAK2.
;
; H Number of the character who is speaking (152-169)
SLIDECHR1:
  CALL NEXTCHR            ; Get the next character from the message being
                          ; spoken
  AND A                   ; Return with the carry flag set if the message has
  SCF                     ; finished
  RET Z                   ;
  CP 2                    ; Replace an ASCII code of 2 with 32 (space)
  JR Z,SLIDECHR1_0        ;
  CP 3                    ; Jump unless the ASCII code is 3, which stands for 8
  JR NZ,SLIDECHR1_1       ; spaces
  LD L,108                ; M_SPACE8='<8 spaces>'; place the address of this
  LD (HL),218             ; message into bytes 107 and 108 of the speaking
  DEC L                   ; character's buffer
  LD (HL),22              ;
SLIDECHR1_0:
  LD A,32                 ; 32=' ' (space)
SLIDECHR1_1:
  JP SLIDECHR2

; Unused
  RET
  DEFS 2

; Update the SRB for the speech bubble
;
; Used by the routines at SPKWAIT (when printing the speech bubble) and
; RMBUBBLE (when removing the speech bubble). Sets the bits in the screen
; refresh buffer (SRB) that correspond to the speech bubble, so that they are
; refreshed when the display is updated.
;
; DE SRB byte address
BUBBLESRB:
  LD A,255                ; Set all bits in this SRB byte, indicating that 8
  LD (DE),A               ; squares in a row (the bottom half of the bubble)
                          ; need refreshing
  LD A,E                  ; Point DE at the SRB byte corresponding to the next
  SUB 4                   ; row up
  LD E,A                  ;
  LD A,255                ; Set all bits in this SRB byte, indicating that 8
  LD (DE),A               ; squares in a row (the top half of the bubble) need
                          ; refreshing
  RET

; Remove the speech bubble
;
; Used by the routines at SPEAK2 and RMSPEECH.
;
; H Number of the character who is speaking (152-169)
RMBUBBLE:
  PUSH HL                 ; Save the character number
  CALL SPEECHSRB          ; Is the speech bubble off-screen?
  JR C,RMBUBBLE_0         ; Jump if so
  CALL BUBBLESRB          ; Update the SRB for the part of the screen hidden by
                          ; the speech bubble
; First, restore the original attribute bytes for the area of the screen
; occupied by the speech bubble.
  LD DE,(LIPCOORDS)       ; Copy the speech bubble lip coordinates from
                          ; LIPCOORDS to DE
  CALL SRBXY              ; Update the SRB for the part of the screen hidden by
                          ; the lip
  LD HL,LEFTCOL           ; LEFTCOL holds the leftmost column of the skool on
                          ; screen (0-64)
  LD A,E                  ; E=screen x-coordinate of the speech bubble lip
  SUB (HL)                ; (0-31)
  LD E,A                  ;
  LD A,D                  ; A=screen y-coordinate of the speech bubble lip
  SUB 152                 ; (2-17)
  RRCA                    ; Set DE to the attribute file address corresponding
  RRCA                    ; to the lip of the speech bubble
  RRCA                    ;
  LD D,A                  ;
  AND 224                 ;
  ADD A,E                 ;
  LD E,A                  ;
  LD A,D                  ;
  AND 3                   ;
  ADD A,88                ;
  LD D,A                  ;
  LD L,103                ; Pick up the attribute byte for the part of screen
  LD A,(HL)               ; hidden by the speech bubble lip from ULIPATTR and
  LD (DE),A               ; restore it to the attribute file
  EX DE,HL                ; Point DE at the attribute file address
  LD BC,65472             ; corresponding to the top left corner of the speech
  ADD HL,BC               ; bubble
  LD A,L                  ;
  AND 248                 ;
  LD L,A                  ;
  EX DE,HL                ;
  LD BC,8                 ; Restore the attribute bytes of the part of the
  LD L,120                ; screen overwritten by the top row of the speech
  LDIR                    ; bubble (stored at UTOPATTR by the routine at
                          ; SPKWAIT)
  LD C,24                 ; Point DE at the attribute file address
  EX DE,HL                ; corresponding to the bottom left corner of the
  ADD HL,BC               ; speech bubble
  EX DE,HL                ;
  LD C,8                  ; Restore the attribute bytes of the part of the
  LDIR                    ; screen overwritten by the bottom row of the speech
                          ; bubble (stored at UBOTATTR by the routine at
                          ; SPKWAIT)
; Next, restore the skool UDG references and attribute bytes for the area of
; the skool occupied by the speech bubble.
RMBUBBLE_0:
  LD DE,(LIPCOORDS)       ; Copy the speech bubble lip coordinates from
                          ; LIPCOORDS to DE
  LD HL,ULIPUDG           ; Pick up the UDG reference for the part of the skool
  LD A,(HL)               ; occupied by the speech bubble lip and restore it
  INC L                   ;
  LD (DE),A               ;
  SET 7,E                 ; Pick up the attribute byte for the part of the
  LD A,(HL)               ; skool occupied by the speech bubble lip and restore
  INC L                   ; it
  LD (DE),A               ;
  LD A,E                  ; DE=coordinates of the top left corner of the speech
  AND 120                 ; bubble
  DEC D                   ;
  DEC D                   ;
  LD E,A                  ;
  LD BC,8                 ; Restore the UDG references of the part of the skool
  LDIR                    ; overwritten by the top row of the speech bubble
                          ; (stored at UTOPUDGS by the routine at SPKWAIT)
  INC D                   ; Restore the UDG references of the part of the skool
  LD C,8                  ; overwritten by the bottom row of the speech bubble
  LD E,A                  ; (stored at UBOTUDGS by the routine at SPKWAIT)
  LDIR                    ;
  ADD A,128               ; Point DE at the skool graphic data attribute byte
  LD E,A                  ; corresponding to the top-left corner of the speech
  DEC D                   ; bubble
  LD C,8                  ; Restore the attribute bytes of the part of the
  LDIR                    ; skool overwritten by the top row of the speech
                          ; bubble (stored at UTOPATTR by the routine at
                          ; SPKWAIT)
  INC D                   ; Restore the attribute bytes of the part of the
  LD C,8                  ; skool overwritten by the bottom row of the speech
  LD E,A                  ; bubble (stored at UBOTATTR by the routine at
  LDIR                    ; SPKWAIT)
; Finally, clear the speech bubble lip coordinates (at LIPCOORDS) to indicate
; that no one is speaking.
  LD (LIPCOORDS),BC       ; Set the speech bubble lip coordinates at to (0,0)
  POP HL                  ; Restore the speaker's character number to H
  XOR A
  RET

; Unused
  DEFB 120,120,120

; Save the area of the screen that will be overwritten by a lines bubble
;
; Used by the routines at GIVELINES and LINESLTR. Returns with the carry flag
; set if the teacher whose coordinates are in DE is off-screen. Otherwise
; calculates the position of the lines bubble and copies the area of the screen
; that would be overwritten by the bubble to the buffer at OVERBUF, and returns
; with DE holding the attribute file address for the top-left corner of the
; lines bubble.
;
; DE Teacher's coordinates
SCR2BUF:
  LD A,(LEFTCOL)          ; A=leftmost column of the skool on screen (0-64)
  LD B,A                  ; Copy this to B
  LD A,E                  ; A=teacher's x-coordinate
  AND 248
  SUB B                   ; Is the teacher off-screen to the left?
  RET C                   ; Return with the carry flag set if so
  CP 32                   ; Is the teacher off-screen to the right?
  CCF
  RET C                   ; Return with the carry flag set if so
; The teacher is on screen. First, save the attribute bytes of the area of the
; screen that will be overwritten by the lines bubble.
  LD E,A                  ; E=teacher's screen x-coordinate (0-31)
  LD A,D                  ; A=teacher's y-coordinate (152-169)
  SUB 155                 ; A=teacher's screen y-coordinate (0-14)
  JR NC,SCR2BUF_0         ;
  XOR A                   ;
SCR2BUF_0:
  RRCA                    ; Set DE to the attribute file address of the top
  RRCA                    ; left corner of the lines bubble
  RRCA                    ;
  LD D,A                  ;
  AND 224                 ;
  ADD A,E                 ;
  LD E,A                  ;
  LD A,D                  ;
  AND 3                   ;
  ADD A,88                ;
  LD D,A                  ;
  PUSH HL
  PUSH DE
  LD HL,OVERBUF           ; The buffer at OVERBUF will store the area of the
                          ; screen overwritten by the lines bubble
  EX DE,HL                ; DE=OVERBUF, HL=attribute file address
  LD BC,8                 ; Copy the 24 attribute bytes that will be
  LDIR                    ; overwritten by the lines bubble from the screen to
  LD C,24                 ; OVERBUF
  ADD HL,BC               ;
  LD C,8                  ;
  LDIR                    ;
  LD C,24                 ;
  ADD HL,BC               ;
  LD C,8                  ;
  LDIR                    ;
; Next, save the display file bytes of the area of the screen that will be
; overwritten by the lines bubble.
  POP HL                  ; Restore the attribute file address of the top-left
  PUSH HL                 ; corner of the lines bubble to HL
  LD A,H                  ; Get the display file address of the top-left corner
  SUB 80                  ; of the lines bubble in HL
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  LD H,A                  ;
  LD BC,3                 ; A lines bubble has 3 rows...
SCR2BUF_1:
  LD A,8                  ; ...and 8 columns; copy the 192 display file bytes
  PUSH BC                 ; that will be overwritten by the lines bubble from
SCR2BUF_2:
  PUSH HL                 ; the screen to OVERBUFG
  LD C,8                  ;
  LDIR                    ;
  POP HL                  ;
  INC H                   ;
  DEC A                   ;
  JR NZ,SCR2BUF_2         ;
  LD A,L                  ;
  ADD A,32                ;
  LD L,A                  ;
  JR C,SCR2BUF_3          ;
  LD A,H                  ;
  SUB 8                   ;
  LD H,A                  ;
SCR2BUF_3:
  POP BC                  ;
  DEC C                   ;
  JR NZ,SCR2BUF_1         ;
  POP DE                  ; Restore the attribute file address of the top-left
                          ; corner of the lines bubble to DE
  POP HL
  RET                     ; Return with the carry flag reset

; Unused
  DEFS 5

; Copy a graphic buffer to the screen
;
; Used by the routines at PRINTB3L (to print the Skool Daze logo and the score
; box), GIVELINES (to print a lines bubble), PRTLESSON (to print the lesson
; box) and LINESLTR (to print a safe combination letter in a bubble).
;
; DE Attribute file address
; HL Graphic buffer address
BUF2SCR:
  PUSH DE                 ; Save the attribute file address
  LD BC,8                 ; Copy the first row of attribute bytes from the
  LDIR                    ; graphic buffer to the screen
  LD C,24                 ; Copy the second row of attribute bytes from the
  EX DE,HL                ; graphic buffer to the screen
  ADD HL,BC               ;
  EX DE,HL                ;
  LD C,8                  ;
  LDIR                    ;
  LD C,24                 ; Copy the third row of attribute bytes from the
  EX DE,HL                ; graphic buffer to the screen
  ADD HL,BC               ;
  EX DE,HL                ;
  LD C,8                  ;
  LDIR                    ;
; The attribute bytes have been copied to the screen. Now for the graphic
; bytes. This entry point is used by the routine at LINESMSG.
BUF2SCR_0:
  POP DE                  ; Restore the attribute file address to DE
  LD A,D                  ; Set DE to the corresponding display file address
  SUB 80                  ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  LD D,A                  ;
  LD C,3                  ; There are three rows of UDGs to copy to the screen
BUF2SCR_1:
  LD A,8                  ; There are 8 pixel lines per row of UDGs
  PUSH BC                 ; Save C (the row counter)
BUF2SCR_2:
  PUSH DE                 ; Copy a row of 8 UDGs from the graphic buffer to the
  LD C,8                  ; screen
  LDIR                    ;
  POP DE                  ;
  INC D                   ;
  DEC A                   ;
  JR NZ,BUF2SCR_2         ;
  LD A,E                  ; Set DE to the display file address for the next row
  ADD A,32                ;
  LD E,A                  ;
  JR C,BUF2SCR_3          ;
  LD A,D                  ;
  SUB 8                   ;
  LD D,A                  ;
BUF2SCR_3:
  POP BC                  ; Restore the row counter to C
  DEC C                   ; Next row
  JR NZ,BUF2SCR_1         ; Jump back until all three rows of UDGs have been
                          ; copied
  RET

; Unused
  DEFS 3

; Write a line of text into a graphic buffer
;
; Used by the routines at LINESMSG, NAME2BUF, PRTLESSON and PRINTBOX. Writes a
; line of text into a graphic buffer (at BUBBLE1, BUBBLE2 or LESSONBOX) and
; centres it.
;
; DE Buffer address
; HL Message address
TXT2BUF:
  XOR A                   ; Clear the buffer ready for writing
  LD B,64                 ;
TXT2BUF_0:
  LD (DE),A               ;
  INC DE                  ;
  DJNZ TXT2BUF_0          ;
  DEC DE                  ; Point DE at the last byte of the buffer
  LD C,62                 ; There are 62 pixel columns available for writing in
TXT2BUF_1:
  LD A,(HL)               ; Pick up a character from the message in A
  INC HL                  ; Move to the next character in the message
  AND A                   ; Have we reached the end of the message?
  JR Z,TXT2BUF_3          ; Jump if so
  PUSH HL                 ; Save the message pointer
  LD L,A                  ; Point HL at the pixel width of the bitmap for the
  LD H,217                ; character in A
  LD A,(HL)               ; Pick up the width in A
  LD B,A                  ; Transfer the width to B
TXT2BUF_2:
  INC H                   ; Point HL at the next font character bitmap byte
  LD A,(HL)               ; Pick this up in A
  CALL PCOL2BUF           ; Insert the font character bitmap byte into the
                          ; buffer
  DEC C                   ; Decrease the pixel column count
  DJNZ TXT2BUF_2          ; Jump back until all pixel columns for this letter
                          ; are done
  XOR A                   ; A=0 (empty vertical pixel column, i.e. space)
  DEC C                   ; Decrease the pixel column count
  CALL PCOL2BUF           ; Insert the space into the buffer
  POP HL                  ; Restore the message pointer to HL
  JR TXT2BUF_1            ; Jump back to write the next letter from the message
; We've finished writing the message. Now centre it.
TXT2BUF_3:
  BIT 7,C                 ; Did we use more than 62 pixel columns?
  RET NZ                  ; Return if so
  RR C                    ; C holds the number of unused pixel columns; halve
  INC C                   ; this and add one to get the padding
TXT2BUF_4:
  XOR A                   ; Insert an empty pixel column (space)
  CALL PCOL2BUF           ;
  DEC C                   ; Next column of padding
  JR NZ,TXT2BUF_4         ; Jump back until the text has been centred
  RET

; Unused
  DEFS 4

; Insert a pixel column into a graphic buffer
;
; Used by the routine at TXT2BUF. Slides the current contents of a row of the
; graphic buffer (at BUBBLE1, BUBBLE2 or LESSONBOX) one pixel to the left, and
; places the new pixel column into the slot freed up on the right.
;
; A Pixel column byte
; DE Graphic buffer address
PCOL2BUF:
  PUSH DE
  EXX
  POP HL                  ; HL'=graphic buffer address
  LD C,8                  ; There are 8 pixels to insert into the column
PCOL2BUF_0:
  LD B,8                  ; There are 8 bytes per row in the buffer
  RRCA                    ; Push a pixel into the carry flag
PCOL2BUF_1:
  RL (HL)                 ; Drag the pixel from the carry flag into the buffer,
  DEC HL                  ; and slide all the other pixels in the row one to
  DJNZ PCOL2BUF_1         ; the left
  DEC C                   ; Next pixel
  JR NZ,PCOL2BUF_0        ; Jump back until all 8 pixels have been inserted
  EXX
  RET

; Lines bubble edge graphics
;
; Used by the routine at DRAWEDGES. The graphic bytes are arranged in eight
; pairs. The first byte (after 1 is added to the value) of each pair
; corresponds to the left edge of the lines bubble, and the second byte
; corresponds to the right edge.
BUBEDGES:
  DEFB 127,1
  DEFB 191,3
  DEFB 223,7
  DEFB 239,15
  DEFB 239,15
  DEFB 223,7
  DEFB 191,3
  DEFB 127,1
  DEFB 255                ; End marker

; Draw the left and right edges of a lines bubble
;
; Used by routines at LINESMSG and NAME2BUF. Draws the left and right edges of
; the lines bubble against the lines of text just written into the graphic
; buffer (at BUBBLE1 or BUBBLE2).
;
; DE Graphic buffer address
DRAWEDGES:
  LD HL,65473             ; Point HL at the top left corner of the text that
  ADD HL,DE               ; was just written into the graphic buffer
  LD DE,7                 ; This is the 'distance' (in bytes) between the left
                          ; and right edges of the graphic buffer
  LD BC,BUBEDGES          ; Point BC at the graphic data table at BUBEDGES
DRAWEDGES_0:
  LD A,(BC)               ; Pick up a bubble left-edge graphic byte in A
  INC C                   ; Move BC along in the graphic data table
  INC A                   ; Have we finished drawing the edges?
  RET Z                   ; Return if so
  OR (HL)                 ; Superimpose the bubble left-edge graphic byte
  LD (HL),A               ;
  ADD HL,DE               ; Move over to the right edge
  LD A,(BC)               ; Pick up a bubble right-edge graphic byte in A
  INC C                   ; Move BC along in the graphic data table
  OR (HL)                 ; Superimpose the bubble right-edge graphic byte
  LD (HL),A               ;
  INC HL                  ; Move back to the left edge, one pixel row down
  JR DRAWEDGES_0          ; Jump back to superimpose another edge graphic byte

; Print a reprimand message
;
; Used by the routine at GIVELINES. Generates the graphic data for the text of
; a reprimand message (e.g. 'NOW FIND A SEAT') in the buffer at BUBBLE2, and
; then copies the buffer to the screen.
;
; A Reprimand message identifier (0-15)
; DE Attribute file address
LINESMSG:
  PUSH DE                 ; Save the attribute file address
  RRCA                    ; Point HL at the appropriate message
  RRCA                    ;
  LD H,A                  ;
  AND 192                 ;
  SCF                     ;
  RRA                     ;
  LD L,A                  ;
  LD A,H                  ;
  AND 3                   ;
  ADD A,219               ;
  LD H,A                  ;
; Now HL points at the appropriate message.
;
; +----+-----------+----------------------------+
; | A  | HL        | Message                    |
; +----+-----------+----------------------------+
; | 0  | M_LINES0  | DON'T SIT ON THE STAIRS    |
; | 1  | M_LINES1  | THE ROOM IS PRIVATE        |
; | 2  | M_LINES2  | GET TO WHERE YOU SHOULD BE |
; | 3  | M_LINES3  | NOW FIND A SEAT            |
; | 4  | M_LINES4  | GET OFF THE FLOOR          |
; | 5  | M_LINES5  | COME ALONG WITH ME BOY     |
; | 6  | M_LINES6  | HURRY UP YOU HORROR        |
; | 7  | M_LINES7  | DON'T TRY MY PATIENCE BOY  |
; | 8  | M_LINES8  | NOW DON'T DO IT AGAIN      |
; | 9  | M_LINES9  | DON'T TELL TALES           |
; | 10 | M_LINES10 | NEVER BE LATE AGAIN        |
; | 11 | M_LINES11 | AND STAY THIS TIME         |
; | 12 | M_LINES12 | DON'T TOUCH BLACKBOARDS    |
; | 13 | M_LINES13 | CATAPULTS ARE FORBIDDEN    |
; | 14 | M_LINES14 | DON'T HIT YOUR MATES       |
; | 15 | M_LINES15 | YOU ARE NOT A KANGAROO     |
; +----+-----------+----------------------------+
  PUSH HL                 ; Save the message address
  LD DE,BUBBLE2L1         ; BUBBLE2L1=appropriate lines bubble graphic buffer
                          ; address for the first line of the message
  CALL TXT2BUF            ; Write the first line of the message
  CALL DRAWEDGES          ; Draw the left and right edges of the bubble
  POP HL                  ; Restore the message address to HL
  LD A,L                  ; Point HL at the second half of the message
  ADD A,16                ;
  LD L,A                  ;
  LD DE,BUBBLE2L2         ; BUBBLE2L2=appropriate lines bubble graphic buffer
                          ; address for the second line of the message
  CALL TXT2BUF            ; Write the second line of the message
  CALL DRAWEDGES          ; Draw the left and right edges of the bubble
  LD B,0                  ; Prepare B for the routine at BUF2SCR_0
  LD HL,BUBBLE2GR         ; BUBBLE2GR=first byte of the graphic data in the
                          ; lines bubble graphic buffer
  JP BUF2SCR_0            ; Transfer the lines bubble graphic data to the
                          ; screen (the lines bubble attribute bytes are
                          ; already on-screen)

; Unused
  DEFS 5

; Write a character's name into the lines bubble graphic buffer
;
; Used by the routine at GIVELINES. Generates the graphic data for the text of
; the lines recipient's name (e.g. 'ERIC') and stores it in the buffer at
; BUBBLE1.
;
; A Lines recipient's character number (167-169, 172)
NAME2BUF:
  LD BC,6167              ; B=24; C=23 (INK 7: PAPER 2)
  LD HL,BUBBLE1           ; The lines bubble graphic is stored at BUBBLE1
  LD DE,M_ERIC            ; M_ERIC: 'ERIC'
  CP 172                  ; Is ERIC being given lines?
  JR Z,NAME2BUF_0         ; Jump if so
  LD C,31                 ; 31=INK 7: PAPER 3
  LD E,142                ; DE=M_BOYWANDER: 'BOY WANDER'
  SUB 167                 ; Is BOY WANDER being given lines?
  JR Z,NAME2BUF_0         ; Jump if so
  LD C,39                 ; 39=INK 7: PAPER 4
  LD E,156                ; DE=M_ANGELFACE: 'ANGELFACE'
  DEC A                   ; Is ANGELFACE being given lines?
  JR Z,NAME2BUF_0         ; Jump if so
  LD E,170                ; DE=M_EINSTEIN: 'EINSTEIN'
NAME2BUF_0:
  LD (HL),C               ; Fill in the appropriate attribute bytes for the
  INC L                   ; lines bubble
  DJNZ NAME2BUF_0         ;
  LD L,120                ; HL=BUBBLE1L2
  EX DE,HL                ; DE=BUBBLE1L2, HL=address of character name
; This entry point is used by the routine at LINESLTR.
NAME2BUF_1:
  CALL TXT2BUF            ; Write the message into the graphic buffer
  JP DRAWEDGES            ; Draw the left and right edges of the bubble

; Unused
  DEFS 7

; Insert a pixel column into the number graphic buffer
;
; Used by the routine at NUMGRAPH. Slides the current contents of the number
; graphic buffer (at NBUFG) one pixel to the left, and places the new pixel
; column into the slot freed up on the right.
;
; A Pixel column byte
PCOL2NBUF:
  LD HL,55583             ; Point HL at the end of the buffer at NBUFG
  RRCA                    ; Discard the baseline bit
  LD C,7                  ; We will consider the remaining 7 bits
PCOL2NBUF_0:
  LD B,3                  ; The buffer is 3 character squares wide
  RRCA                    ; Drop a bit into the carry flag
PCOL2NBUF_1:
  RL (HL)                 ; Pick it up in the buffer and shift the other pixels
  DEC HL                  ; in this row one to the left
  DJNZ PCOL2NBUF_1        ;
  DEC C                   ; Next bit
  JR NZ,PCOL2NBUF_0       ; Jump back until all 7 bits in the column are done
  RET

; Unused
  DEFS 2

; Generate graphic data for a number
;
; Used by the routines at PRINTNUM and NUM2BUF. Generates a 7x24-bit graphic
; for the digits of the number in DE, and stores it in the buffer at NBUFG.
;
; DE Number
NUMGRAPH:
  PUSH HL
  LD HL,55583             ; Blank out the 32-byte buffer at NBUF
  XOR A                   ;
  LD B,31                 ;
NUMGRAPH_0:
  LD (HL),A               ;
  DEC L                   ;
  DJNZ NUMGRAPH_0         ;
  LD A,48                 ; 48 is the ASCII code for '0' (zero)
  LD BC,55536             ; BC=-10000
  EX DE,HL                ; HL=the number, DE=NBUF
  ADD HL,BC               ; Subtract 10000
  JR C,NUMGRAPH_1         ; Jump if the number is > 10000
  SBC HL,BC               ; Add the 10000 back on
  LD BC,64536             ; BC=-1000
  ADD HL,BC               ; Subtract 1000
  JR C,NUMGRAPH_2         ; Jump if the number is > 1000
  SBC HL,BC               ; Add the 1000 back on
  LD BC,65436             ; BC=-100
  ADD HL,BC               ; Subtract 100
  JR C,NUMGRAPH_3         ; Jump if the number is > 100
  SBC HL,BC               ; Add the 100 back on
  LD C,246                ; BC=-10
  ADD HL,BC               ; Subtract 10
  JR C,NUMGRAPH_4         ; Jump if the number is > 10
  SBC HL,BC               ; Add the 10 back on
  JR NUMGRAPH_5
; We get here if the number is >= 10000.
NUMGRAPH_1:
  INC A                   ; Perform trial subtractions of 10000 and get the
  ADD HL,BC               ; ASCII code for the 10000s digit in A
  JR C,NUMGRAPH_1         ;
  SBC HL,BC               ; Undo the last trial subtraction
  LD BC,64536             ; BC=-1000
  LD (DE),A               ; Store the ASCII code for the 10000s digit
  LD A,47
  INC E                   ; Move along in the buffer
NUMGRAPH_2:
  INC A                   ; Perform trial subtractions of 1000 and get the
  ADD HL,BC               ; ASCII code for the 1000s digit in A
  JR C,NUMGRAPH_2         ;
  SBC HL,BC               ; Undo the last trial subtraction
  LD BC,65436             ; BC=-100
  LD (DE),A               ; Store the ASCII code for the 1000s digit
  LD A,47
  INC E                   ; Move along in the buffer
NUMGRAPH_3:
  INC A                   ; Perform trial subtractions of 100 and get the ASCII
  ADD HL,BC               ; code for the 100s digit in A
  JR C,NUMGRAPH_3         ;
  SBC HL,BC               ; Undo the last trial subtraction
  LD (DE),A               ; Store the ASCII code for the 100s digit
  INC E                   ; Move along in the buffer
  LD C,246                ; BC=-10
  LD A,47
NUMGRAPH_4:
  INC A                   ; Perform trial subtractions of 10 and get the ASCII
  ADD HL,BC               ; code for the 10s digit in A
  JR C,NUMGRAPH_4         ;
  SBC HL,BC               ; Undo the last trial subtraction
  LD (DE),A               ; Store the ASCII code for the 10s digit
  INC E                   ; Move along in the buffer
NUMGRAPH_5:
  LD A,L                  ; Get the ASCII code for the units digit in A
  ADD A,48                ;
  LD (DE),A               ; Store the ASCII code for the units digit
  XOR A
  INC E                   ; Move along in the buffer
  LD (DE),A               ; Mark the end of the number string with a 0 byte
; Now that the ASCII codes for the digits of the number have been calculated
; and stored at NBUF, generate the corresponding graphic data.
  LD E,0                  ; DE=NBUF (first character in the number string)
NUMGRAPH_6:
  LD A,(DE)               ; Pick up a character from the number string
  AND A                   ; Have we reached the end of the string?
  JR NZ,NUMGRAPH_7        ; Jump if not
  POP HL
  RET
NUMGRAPH_7:
  INC E                   ; Move to the next character in the number string
  EXX
  LD L,A                  ; L'=ASCII code for a digit of the number (48-57)
  LD H,217                ; Point HL' at the start of the font data for this
                          ; digit
  LD B,(HL)               ; Pick up the pixel width in B'
  INC B                   ; Add 1 for left padding
  XOR A                   ; An empty byte for left padding
NUMGRAPH_8:
  INC H                   ; Point HL' at the next bitmap byte
  EXX
  CALL PCOL2NBUF          ; Slide the bitmap byte into the buffer
  EXX
  LD A,(HL)               ; Pick up the next bitmap byte
  DJNZ NUMGRAPH_8         ; Jump back until all bitmap bytes are done
  EXX
  JR NUMGRAPH_6           ; Jump back to collect the next digit

; Unused
  DEFS 2

; Print the score, lines total or hi-score (1)
;
; Used by the routines at ADD10, PRINTB3L, GIVELINES, SETSCORE and LINES2K.
; Prepares the buffer at NBUFG with the graphic data for the digits of the
; number in HL, then copies the graphic data to the screen at the location
; specified by DE.
;
; DE Display file address: 20924 (score), 20956 (lines) or 20988 (hi-score)
; HL Score, lines total or hi-score
PRINTNUM:
  EX DE,HL                ; Transfer the score, lines total or hi-score to DE
  CALL NUMGRAPH           ; Generate a 7x24-bit graphic for the number stored
                          ; in DE
  EX DE,HL                ; Transfer the display file address back to DE
  LD HL,NBUFG             ; The graphic data for the number is stored in the
                          ; buffer at NBUFG
  LD BC,2047              ; B=7 (number of pixel rows), C=255
PRINTNUM_0:
  PUSH DE                 ; Save the display file address temporarily
  LDI                     ; Transfer a 24-bit wide row of pixels to the screen
  LDI                     ;
  LDI                     ;
  POP DE                  ; Restore the display file address to DE
  INC D                   ; Next pixel row on the screen
  DJNZ PRINTNUM_0         ; Jump back until all 7 pixel rows are done
  JR PRINTNUM2            ; Jump over the routine at NUM2BUF

; Write the number of lines into the lines bubble graphic buffer
;
; Used by the routine at GIVELINES. Writes the number of lines being given by a
; teacher into the lines bubble graphic buffer at BUBBLE1.
;
; DE Number of lines
NUM2BUF:
  CALL NUMGRAPH           ; Generate the graphic data for the number of lines
  LD HL,55564             ; Point HL at the buffer containing the graphic data
                          ; for the number (at NBUFG)
  LD DE,60217             ; Point DE at the appropriate spot in the lines
                          ; bubble graphic buffer (at BUBBLE1) for writing the
                          ; number of lines
  LD BC,2047              ; B=7, C=255
NUM2BUF_0:
  LDI                     ; Copy the graphic data (a 7x16 block of pixels) for
  LDI                     ; the number of lines into the lines bubble graphic
  INC HL                  ; buffer
  INC DE                  ;
  LD A,E                  ;
  ADD A,5                 ;
  LD E,A                  ;
  DJNZ NUM2BUF_0          ;
  RET

; Print the score, lines total or hi-score (2)
;
; Continues from PRINTNUM. Checks whether ERIC is over the lines limit (if the
; number of lines was just printed).
;
; DE Display file address: 20924 (score), 20956 (lines) or 20988 (hi-score)
PRINTNUM2:
  LD A,E                  ; Copy the LSB of the display file address to A
  CP 220                  ; Was it the lines total that was just printed?
  RET NZ                  ; Return if not
  JP CHKLINES             ; Otherwise check whether ERIC is over the lines
                          ; limit

; Make a teacher give lines
;
; Used by the routines at CHKERIC, CHKKIDS, CLSLINES and FIRING. Prints the
; lines message (e.g. '100 LINES ERIC') in a bubble above the teacher's head,
; and shortly afterwards prints the reprimand message (e.g. 'NOW FIND A SEAT')
; in the same bubble.
;
; A Lines recipient's character number (167-169, 172)
; B Reprimand message identifier (0-15)
; DE Teacher's coordinates
GIVELINES:
  PUSH BC
  PUSH AF
  CALL SCR2BUF            ; Is the lines-giving teacher fully on-screen?
  JR NC,GIVELINES_0       ; Jump if so
  POP AF
  POP BC
  RET                     ; Otherwise return without having given lines
; The teacher is on-screen, so it's time to prepare the lines bubble for
; display. First, write the lines recipient's name into the lines bubble
; graphic buffer.
GIVELINES_0:
  POP AF
  PUSH DE                 ; Save the attribute file address of the lines bubble
  PUSH AF                 ; Save the character number of the lines recipient
  CALL NAME2BUF           ; Write the recipient's name into the lines bubble
                          ; graphic buffer
; Next, determine how many lines should be given.
  CALL GETRANDOM          ; A=random number
  AND 14                  ; Set DE equal to the number of lines (divided by
  ADD A,2                 ; 10): 10, 20, 30, 40, 50, 60, 70 or 80
  LD E,A                  ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,E                 ;
  LD E,A                  ;
  LD D,0                  ;
; Add to the lines total or the score depending on who's getting lines.
  POP AF                  ; Restore the lines recipient's character number to A
  CP 167                  ; Is BOY WANDER the lines recipient?
  JR Z,GIVELINES_3        ; Jump if so
  PUSH DE                 ; Store the number of lines
  CP 172                  ; Is ERIC the lines recipient?
  JR Z,GIVELINES_1        ; Jump if so
  LD HL,(SCORE)           ; Add to the score if ANGELFACE or EINSTEIN was the
  ADD HL,DE               ; lines recipient
  LD (SCORE),HL           ;
  LD DE,20924
  JR GIVELINES_2
GIVELINES_1:
  LD HL,(LINES)           ; HL=number of lines ERIC has
  ADD HL,DE               ; Add the lines being given
  LD (LINES),HL           ; Store the new lines total
  LD DE,20956             ; Set DE to the appropriate display file address for
                          ; the lines total
GIVELINES_2:
  CALL PRINTNUM           ; Print the new lines total
  POP DE                  ; DE=number of lines being given
; Now write the number of lines being given into the lines bubble graphic
; buffer, and copy the lines bubble graphic to the screen.
GIVELINES_3:
  CALL NUM2BUF            ; Write the number of lines into the lines bubble
                          ; graphic buffer
  POP DE                  ; Restore the attribute file address to DE and save
  PUSH DE                 ; it again
  LD HL,BUBBLE1           ; Point HL at the lines bubble graphic buffer at
                          ; BUBBLE1
  CALL BUF2SCR            ; Copy the lines bubble graphic to the screen
; Time for the first lines-giving sound effect.
  LD A,(BUBBLE1)          ; A=lines bubble graphic attribute byte
  LD DE,10240             ; DE=sound effect duration parameter
  RRCA                    ; The border colour will be the same as the PAPER
  RRCA                    ; colour of the lines bubble
  RRCA                    ;
  AND 7                   ;
GIVELINES_4:
  OUT (254),A             ; Play the first lines-giving sound effect
  XOR 16                  ;
  LD B,20                 ;
GIVELINES_5:
  DJNZ GIVELINES_5        ;
  DEC E                   ;
  JR NZ,GIVELINES_4       ;
  DEC D                   ;
  JR NZ,GIVELINES_4       ;
; Now that the lines have been dished out, the teacher should explain why.
  POP DE                  ; Restore the attribute file address to DE
  POP BC                  ; Restore the reprimand message identifier (0-15) to
                          ; B
  PUSH DE                 ; Save the attribute file address
  PUSH AF                 ; Save the border colour
  LD A,B                  ; A=reprimand message identifier (0-15)
  CALL LINESMSG           ; Print the reprimand message
  POP AF                  ; Restore the border colour to A
; This entry point is used by the routine at LINESLTR.
GIVELINES_6:
  LD DE,5120              ; DE=sound effect duration parameter
GIVELINES_7:
  OUT (254),A             ; Play the second lines-giving sound effect
  XOR 16                  ;
  LD B,50                 ;
GIVELINES_8:
  DJNZ GIVELINES_8        ;
  DEC E                   ;
  JR NZ,GIVELINES_7       ;
  DEC D                   ;
  JR NZ,GIVELINES_7       ;
  LD A,1                  ; Set the border colour back to blue
  OUT (254),A             ;
  POP DE                  ; Restore the attribute file address to DE
  LD HL,OVERBUF           ; Point HL at the buffer containing the area of the
                          ; screen overwritten by the lines bubble
  JP BUF2SCR              ; Restore the area of the screen that was overwritten

; Unused
  DEFB 120,120,120,120,120

; Skool region data table for the top floor
;
; Used by the routine at CHKERIC.
TOPFLOOR:
  DEFB 8,0                ; Head's study (x<8; always off-limits)
  DEFB 16,7               ; Head's study door to the Revision Library (8<=x<16)
  DEFB 40,5               ; Revision Library (16<=x<40)
  DEFB 57,1               ; Reading Room (40<=x<57)
  DEFB 74,2               ; Map Room (57<=x<74)
  DEFB 130,7              ; Map Room door to the fire escape (x>=74)

; Skool region data table for the middle floor
;
; Used by the routine at CHKERIC.
MIDFLOOR:
  DEFB 10,0               ; Staff room (x<10; always off-limits)
  DEFB 22,7               ; Between the staff room and the White Room
                          ; (10<=x<22)
  DEFB 38,3               ; White Room (22<=x<38)
  DEFB 70,4               ; Exam Room (38<=x<70)
  DEFB 130,7              ; Outside the Exam Room door (x>=70)

; Skool region data table for the bottom floor
;
; Used by the routine at CHKERIC.
BOTFLOOR:
  DEFB 34,7               ; Left of the dinner hall (x<34)
  DEFB 51,6               ; Dinner hall (34<=x<51)
  DEFB 130,7              ; Right of the dinner hall (x>=51)

; Make any nearby teacher give ERIC lines if necessary
;
; Used by the routine at MOVEERIC. Checks whether ERIC is somewhere he
; shouldn't be, or sitting or standing when or where he shouldn't be, or lying
; down, and makes any nearby teacher give lines accordingly.
CHKERIC:
  LD HL,LINESDELAY        ; Check (and decrement) the lines-giving delay
  LD A,(HL)               ; counter at LINESDELAY, and proceed only if it was
  AND A                   ; <75 (the counter starts off at 0 for a new game,
  JR Z,CHKERIC_0          ; and is set to 150 by this routine after ERIC has
  DEC (HL)                ; been given lines)
  CP 75                   ;
  RET NC                  ;
CHKERIC_0:
  LD BC,(44129)           ; Collect ERIC's coordinates in BC
  LD L,251                ; HL=STATUS (ERIC's status flags)
  BIT 2,(HL)              ; Is ERIC jumping?
  JR Z,CHKERIC_1          ; Jump if not
  LD A,C                  ; A=ERIC's y-coordinate
  LD C,155                ; This is the y-coordinate of the top floor
  CP 156                  ; Is ERIC on the top floor?
  JR C,CHKERIC_1          ; Jump if so
  LD C,162                ; This is the y-coordinate of the middle floor
  CP 163                  ; Is ERIC on the middle floor?
  JR C,CHKERIC_1          ; Jump if so
  LD C,169                ; This is the y-coordinate of the bottom floor
CHKERIC_1:
  LD A,C                  ; A=ERIC's y-coordinate (adjusted if he's jumping)
  LD DE,TOPFLOOR          ; Point HL at the skool region data table for the top
  EX DE,HL                ; floor at TOPFLOOR
  CP 155                  ; Is ERIC on the top floor?
  JR Z,CHKERIC_2          ; Jump if so
  LD L,156                ; HL=MIDFLOOR (skool region data table for the middle
                          ; floor)
  CP 162                  ; Is ERIC on the middle floor?
  JR Z,CHKERIC_2          ; Jump if so
  LD L,166                ; HL=BOTFLOOR (skool region data table for the bottom
                          ; floor)
  CP 169                  ; Is ERIC on the bottom floor?
  JR Z,CHKERIC_2          ; Jump if so
; ERIC is on a staircase. Is he sitting on the stairs?
  EX DE,HL                ; HL=STATUS (ERIC's status flags)
  BIT 7,(HL)              ; Reset the zero flag if ERIC is sitting
  LD A,7                  ; This value is ignored
  JR Z,CHKERIC_4          ; Jump unless ERIC is sitting on the stairs
  LD A,0                  ; Lines reprimand 0: DON'T SIT ON THE STAIRS
  JR CHKERIC_9            ; Make any nearby teacher give lines
; At this point, HL holds the base address of a data table describing the
; regions in the skool (TOPFLOOR for the top floor, MIDFLOOR for the middle
; floor, or BOTFLOOR for the bottom floor).
CHKERIC_2:
  LD A,B                  ; B=ERIC's x-coordinate
CHKERIC_3:
  CP (HL)                 ; Compare it with the x-coordinate from the region
                          ; table
  INC HL
  INC HL
  JR NC,CHKERIC_3         ; Jump back until we've determined where ERIC is
  DEC HL                  ; Point DE at the ID byte of the region table entry
  EX DE,HL                ;
; The ID bytes in the region tables have the following meanings:
;
; +----+--------------------------------------------------------+
; | ID | Meaning                                                |
; +----+--------------------------------------------------------+
; | 0  | ERIC should never be here (head's study or staff room) |
; | 1  | Reading Room                                           |
; | 2  | Map Room                                               |
; | 3  | White Room                                             |
; | 4  | Exam Room                                              |
; | 5  | Revision Library                                       |
; | 6  | Dinner hall                                            |
; | 7  | None of the above                                      |
; +----+--------------------------------------------------------+
CHKERIC_4:
  LD A,(DE)               ; A=ID of the region ERIC's in (7 if he's on a
                          ; staircase)
  AND A                   ; Set the zero flag if ERIC's in a forbidden zone
  LD A,1                  ; Lines reprimand 1: THE ROOM IS PRIVATE
  JR Z,CHKERIC_9          ; Jump if ERIC's in the head's study or the staff
                          ; room
  LD A,(DE)               ; A=ID of the region ERIC's in
  LD L,200                ; HL=LFLAGS (lesson status flags)
  BIT 7,(HL)              ; Jump if EINSTEIN has not had a chance to grass on
  JR Z,CHKERIC_8          ; ERIC for being absent yet, or dinner has not
                          ; started yet, or this is PLAYTIME or REVISION
                          ; LIBRARY
CHKERIC_5:
  LD A,(LESSONDESC)       ; LESSONDESC holds the current lesson descriptor
  AND 7                   ; Keep only the 'room' bits (0-2)
  EX DE,HL
  CP (HL)                 ; Compare this with ERIC's region ID
  EX DE,HL
  LD A,2                  ; Lines reprimand 2: GET TO WHERE YOU SHOULD BE
  JR NZ,CHKERIC_9         ; Jump if ERIC is not where he should be
  LD A,(HL)               ; Pick up the lesson status flags from LFLAGS
  RLCA                    ; Set the carry flag if ERIC's lesson has started
  RLCA                    ; with ERIC present
CHKERIC_6:
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  JR NC,CHKERIC_7         ; Jump if ERIC's lesson has not started
  AND 15                  ; ERIC's lesson has started; is ERIC sitting in a
  CP 5                    ; chair?
  RET Z                   ; Return if so
  LD A,3                  ; Lines reprimand 3: NOW FIND A SEAT
  JR CHKERIC_9            ; Make any nearby teacher give lines
; ERIC's lesson has not started yet.
CHKERIC_7:
  AND 7                   ; Is ERIC standing, or midstride, or sitting on a
  CP 6                    ; chair?
  RET C                   ; Return if so
  LD A,4                  ; Lines reprimand 4: GET OFF THE FLOOR
  JR CHKERIC_9            ; Make any nearby teacher give lines
; Bit 7 of LFLAGS is reset, which means EINSTEIN has not yet had a chance to
; grass on ERIC for being absent at the start of the lesson, or dinner has not
; started yet, or this is PLAYTIME or REVISION LIBRARY.
CHKERIC_8:
  CP 5                    ; Is ERIC in a room?
  JR NC,CHKERIC_6         ; Jump if not
  LD A,(32760)            ; Pick up the MSB of the lesson clock in A
  CP 19                   ; Should ERIC have left the room he was in last
                          ; period?
  JR NC,CHKERIC_6         ; Jump if not
  JR CHKERIC_5            ; Otherwise check ERIC's location
; ERIC is not where he should be, or is not sitting down when he should be, or
; is sitting on the floor or on the stairs. In short, he should be given lines
; by any adult witness. At this point A holds one of the following values:
;
; +---+-----------------------------------------------+
; | A | Meaning                                       |
; +---+-----------------------------------------------+
; | 0 | ERIC is sitting on the stairs                 |
; | 1 | ERIC is in the head's study or the staff room |
; | 2 | ERIC is not in the room he should be in       |
; | 3 | ERIC is not sitting down when he should be    |
; | 4 | ERIC is sitting on the floor                  |
; +---+-----------------------------------------------+
CHKERIC_9:
  LD D,A                  ; D=0, 1, 2, 3 or 4
  LD L,242                ; HL=LINESDELAY: lines-giving delay counter (0-73)
  LD A,(HL)               ; Is it too soon for the same teacher to give ERIC
  AND A                   ; lines twice in a row?
  JR Z,CHKERIC_10         ; Jump if not
  INC HL                  ; Otherwise, collect from LASTLINES into H the
  LD H,(HL)               ; character number of the teacher who last gave ERIC
                          ; lines
  LD L,98                 ; Store this teacher's x-coordinate in E, and set his
  LD E,(HL)               ; x-coordinate to 255 temporarily, so he's out of
  LD (HL),255             ; range and will not be regarded as a lines-giving
                          ; candidate
CHKERIC_10:
  PUSH DE
  PUSH AF
  LD D,C                  ; DE=ERIC's coordinates
  LD E,B                  ;
  LD HL,ERICCBUF          ; Point HL at ERIC's character buffer
  CALL FLOOR_0            ; Get the floor closest to ERIC (155, 162 or 169) in
                          ; D
  CALL VISIBLE_0          ; Can ERIC be seen by a teacher?
  JR C,CHKERIC_11         ; Jump if so
  POP AF
  POP DE
  RET Z                   ; Return unless we need to restore the x-coordinate
                          ; of the last teacher who gave ERIC lines
  LD A,(LASTLINES)        ; Restore the x-coordinate of the teacher who was
  LD H,A                  ; temporarily placed out of lines-giving range
  LD L,98                 ;
  LD (HL),E               ;
  RET
; Time to dish out some lines to ERIC.
CHKERIC_11:
  POP AF
  POP DE
  JR Z,CHKERIC_12         ; Jump unless we need to restore the x-coordinate of
                          ; the last teacher who gave ERIC lines
  LD A,(LASTLINES)        ; Restore the x-coordinate of the teacher who was
  LD H,A                  ; temporarily placed out of lines-giving range
  LD L,98                 ;
  LD (HL),E               ;
CHKERIC_12:
  LD B,D                  ; B=0, 1, 2, 3 or 4 (lines reprimand ID)
  EXX                     ; H=character number of the teacher who saw ERIC
  PUSH HL                 ;
  EXX                     ;
  POP HL                  ;
  LD L,150                ; Reset the lines-giving delay counter at LINESDELAY
  LD (LINESDELAY),HL      ; to 150, and set LASTLINES to the character number
                          ; of the teacher who saw ERIC
  LD L,98                 ; DE=coordinates of the teacher who saw ERIC
  LD E,(HL)               ;
  DEC L                   ;
  LD D,(HL)               ;
  LD A,B                  ; B=0, 1, 2, 3 or 4 (lines reprimand ID)
  CP 2                    ; Was ERIC spotted somewhere other than where he
                          ; should be?
  JR NZ,CHKERIC_14        ; Jump if not
; ERIC was spotted somewhere other than where he should be. The appropriate
; lines reprimand depends on who saw him: his teacher for this period, or
; another teacher.
  LD A,(LESSONDESC)       ; Pick up the current lesson descriptor from
                          ; LESSONDESC
  AND 240                 ; Keep only the teacher bits (4-7)
  RRCA                    ; A=character number of ERIC's teacher for this
  RRCA                    ; period
  RRCA                    ;
  RRCA                    ;
  ADD A,163               ;
  CP H                    ; Was it ERIC's teacher who saw him?
  JR NZ,CHKERIC_14        ; Jump if not (with B=2: GET TO WHERE YOU SHOULD BE)
; The truant ERIC has been spotted by his teacher for this period.
  LD B,5                  ; Lines reprimand 5: COME ALONG WITH ME BOY
  LD A,(LFLAGS)           ; Bits 4 and 5 of LFLAGS hold the flags that
                          ; determine ERIC's teacher's next lines reprimand
  BIT 5,A                 ; Set the zero flag if ERIC's teacher hasn't said
                          ; 'COME ALONG WITH ME BOY' yet
  SET 5,A                 ; Indicate that he has said this now
  JR Z,CHKERIC_13         ; Jump if ERIC's teacher has spotted him for the
                          ; first time
  XOR 16                  ; Flip bit 4
  INC B                   ; B=6: HURRY UP YOU HORROR
  BIT 4,A                 ; Should ERIC's teacher say this next?
  JR Z,CHKERIC_13         ; Jump if so
  INC B                   ; B=7: DON'T TRY MY PATIENCE BOY
CHKERIC_13:
  LD (LFLAGS),A           ; Update the truancy lines reprimand flags in LFLAGS
; Now B holds the appropriate lines reprimand ID. Finally, give lines to ERIC.
CHKERIC_14:
  LD A,172                ; 172=ERIC
  JP GIVELINES            ; Give lines to ERIC

; Unused
  DEFS 2

; Make a teacher give lines to the closest main kid
;
; Used by the routine at DONT. Makes a teacher who has been knocked over give
; lines to the closest main kid (if any).
;
; H Teacher's character number (163-166)
CHKKIDS:
  PUSH HL
  LD A,(STATUS)           ; Copy ERIC's status flags from STATUS to A
  LD HL,44130             ; Pick up ERIC's coordinates in DE
  LD E,(HL)               ;
  DEC L                   ;
  LD D,(HL)               ;
  LD BC,910               ; B=3 (number of main kids besides ERIC), C=LSB of
                          ; KIDDISTBW
  BIT 2,A                 ; Is ERIC jumping?
  JR Z,CHKKIDS_0          ; Jump if not
  LD A,D                  ; A=ERIC's y-coordinate
  LD D,155                ; This is the y-coordinate of the top floor
  CP 156                  ; Is ERIC jumping on the top floor?
  JR C,CHKKIDS_1          ; Jump if so
  LD D,162                ; This is the y-coordinate of the middle floor
  CP 163                  ; Is ERIC jumping on the middle floor?
  JR C,CHKKIDS_1          ; Jump if so
  LD D,169                ; This is the y-coordinate of the bottom floor
  JR CHKKIDS_1
CHKKIDS_0:
  CALL FLOOR_0            ; Get the floor nearest ERIC (155, 162, 169) in D
CHKKIDS_1:
  LD (KIDDIST),DE         ; Store ERIC's normalised coordinates in KIDDIST
; Here we enter a loop that calculates BOY WANDER's, ANGELFACE's and EINSTEIN's
; normalised coordinates and stores them in KIDDISTBW, KIDDISTA and KIDDISTE
; respectively. (If a character's coordinates are (x, y), then his normalised
; coordinates are (x, Y), where Y=155, 162, 169, i.e. the y-coordinate of the
; floor he is closest to.)
  LD H,167                ; 167=BOY WANDER
CHKKIDS_2:
  CALL FLOOR              ; D=floor nearest the kid (155, 162, 169)
  LD L,C                  ; L=142, 144 or 146
  LD A,H                  ; Store the kid's character number in A temporarily
  LD H,127                ; HL=KIDDISTBW, KIDDISTA or KIDDISTE
  LD (HL),E               ; Store the kid's normalised coordinates here
  INC L                   ;
  LD (HL),D               ;
  INC L                   ; Point HL at the next normalised coordinates slot
  LD C,L                  ; Save L in C temporarily
  LD H,A                  ; Restore the kid's character number (167-169) to H
  INC H                   ; Next kid
  DJNZ CHKKIDS_2          ; Jump back until the three main kids have been done
  POP HL                  ; Restore the teacher's character number (163-166) to
  PUSH HL                 ; H
  LD L,98                 ; Pick up the teacher's coordinates in DE
  LD E,(HL)               ;
  DEC L                   ;
  LD D,(HL)               ;
  CALL VISRANGE           ; Get the lower and upper limits of the teacher's
                          ; visibility range in C and B
; Now we enter a loop to calculate the distance of each main kid from the
; teacher.
  LD HL,32659             ; Point HL at EINSTEIN's normalised y-coordinate
  LD A,4                  ; There are four main kids to check
CHKKIDS_3:
  PUSH AF
  LD A,(HL)               ; Pick up a kid's normalised y-coordinate
  DEC HL                  ; Point HL at the kid's x-coordinate
  CP D                    ; Compare the normalised y-coordinate with the
                          ; teacher's y-coordinate
  LD A,(HL)               ; A=kid's x-coordinate
  LD (HL),255             ; Default assumption: kid and teacher are on
                          ; different floors
  JR NZ,CHKKIDS_6         ; Jump unless the kid's normalised y-coordinate
                          ; matches the teacher's y-coordinate
  CP B                    ; Jump forward if the teacher cannot see the kid
  JR Z,CHKKIDS_4          ;
  JR NC,CHKKIDS_6         ;
CHKKIDS_4:
  CP C                    ;
  JR C,CHKKIDS_6          ;
  SUB E                   ; A=distance between the teacher and the kid
                          ; (possibly < 0)
  JR NC,CHKKIDS_5         ; Jump if the kid is to the right of the teacher
                          ; (A>=0)
  NEG
CHKKIDS_5:
  LD (HL),A               ; Store the absolute distance between the teacher and
                          ; the kid (if they are close to each other)
CHKKIDS_6:
  DEC HL                  ; Point HL at the normalised y-coordinate of the next
  POP AF                  ; main kid and jump back until all four have been
  DEC A                   ; checked
  JR NZ,CHKKIDS_3         ;
; Finally, we enter a loop to determine which main kid (if any) is closest to
; the teacher.
  INC HL                  ; HL=KIDDIST (holding ERIC's distance from the
                          ; teacher)
  LD B,4                  ; There are four main kids to consider
  LD A,255                ; 255 was used to signal that the kid was not on the
                          ; same floor as the teacher
CHKKIDS_7:
  CP (HL)                 ; When this loop is finished, A will hold the
  JR C,CHKKIDS_8          ; distance between the teacher and the closest main
  LD A,(HL)               ; kid (or 255 if none of the kids were nearby)
CHKKIDS_8:
  INC HL                  ;
  INC HL                  ;
  DJNZ CHKKIDS_7          ;
  INC A                   ; Were any of the main kids close to the teacher?
  JR NZ,CHKKIDS_9         ; Jump if so
  POP HL                  ; Restore the teacher's character number to H
  RET
; So at least one of the main kids is within range. Which one will get lines?
CHKKIDS_9:
  DEC A                   ; A=distance between the teacher and the nearest kid
  LD L,140                ; HL=KIDDIST (holding ERIC's distance from the
                          ; teacher)
  INC D                   ; D=teacher's y-coordinate + 1
  LD BC,2220              ; B=8 (NOW DON'T DO IT AGAIN), C=172 (ERIC)
  CP (HL)                 ; Is ERIC closest to the teacher?
  JR Z,CHKKIDS_11         ; Jump if so
  LD C,166                ; On exit from this loop, C will hold 167 (BOY
CHKKIDS_10:
  INC C                   ; WANDER), 168 (ANGELFACE), or 169 (EINSTEIN)
  INC L                   ;
  INC L                   ;
  CP (HL)                 ;
  JR NZ,CHKKIDS_10        ;
CHKKIDS_11:
  LD A,C                  ; A=character number of the kid closest to the
                          ; teacher
  CALL GIVELINES          ; Give lines to the kid who is closest to the teacher
  POP HL                  ; Restore the teacher's character number to H
  RET

; Unused
  DEFS 6

; Print the lesson
;
; Used by the routine at PRINTBOX. Prints the lesson (or DINNER or PLAYTIME) in
; the lesson box.
PRTLESSON:
  LD A,(LESSONDESC)       ; Pick up the lesson descriptor from LESSONDESC
  AND 7                   ; Keep only the 'room' bits (bits 0-2)
  CP 5                    ; Set the zero flag if this period is REVISION
                          ; LIBRARY, and set the carry flag if this is a
                          ; classroom period
  LD DE,60720             ; Point DE at the appropriate spot in the lesson box
                          ; graphic buffer (at LESSONBOX) for the first line of
                          ; text
  LD HL,M_SPACE_2         ; M_SPACE_2=' ' (single space)
  JR NZ,PRTLESSON_0       ; Jump if this period is not REVISION LIBRARY
  LD L,128                ; HL=M_REVISION: 'REVISION'
PRTLESSON_0:
  JR NC,PRTLESSON_1       ; Jump if this is REVISION LIBRARY, DINNER or
                          ; PLAYTIME
; This period takes place in a classroom. With which teacher?
  LD A,(LESSONDESC)       ; Pick up the lesson descriptor from LESSONDESC
  AND 240                 ; Keep only the 'teacher' bits (bits 4-7)
  LD L,A                  ; Point HL at the appropriate teacher's name:
  RRCA                    ; M_WACKER (MR WACKER), M_ROCKITT (MR ROCKITT),
  RRCA                    ; M_WITHIT (MR WITHIT), or M_CREAK (MR CREAK)
  RRCA                    ;
  LD H,A                  ;
  LD A,L                  ;
  SUB H                   ;
  ADD A,184               ;
  LD L,A                  ;
  LD H,217                ;
PRTLESSON_1:
  CALL TXT2BUF            ; Write the first line of text (' ', 'REVISION' or
                          ; the teacher's name) into the lesson box graphic
                          ; buffer
  LD A,(LESSONDESC)       ; Pick up the lesson descriptor from LESSONDESC
  AND 7                   ; Keep only the 'room' bits (bits 0-2)
  ADD A,A                 ; Point HL at the appropriate room name: M_READINGRM
  ADD A,A                 ; (READING ROOM), M_MAPROOM (MAP ROOM), M_WHITEROOM
  ADD A,A                 ; (WHITE ROOM), M_EXAMROOM (EXAM ROOM), M_DINNER
  ADD A,A                 ; (DINNER), or M_PLAYTIME (PLAYTIME)
  ADD A,128               ;
  LD L,A                  ;
  LD H,218                ;
; This entry point is also used by the routine at PRINTBOX.
PRTLESSON_2:
  LD DE,60800             ; Point DE at the appropriate spot in the lesson box
                          ; graphic buffer (at LESSONBOX) for the second line
                          ; of text
  CALL TXT2BUF            ; Write the second line of text into the buffer
  LD HL,LESSONBOX         ; Point HL at the start of the lesson box graphic
                          ; buffer
  LD DE,23212             ; Set DE to the attribute file address for the lesson
                          ; box
  JP BUF2SCR              ; Copy the lesson box graphic buffer to the screen

; Unused
  DEFB 120,120

; Make a character speak (1)
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of a character's buffer by the routines at PREPQA, GOTOPAGE and
; QALESSON. The subcommand terminates if the character is off-screen; otherwise
; it waits for anybody else to stop speaking before printing the speech bubble
; above the character's head and handing over control to the routine at SPEAK2.
;
; H Character number (152-169)
SPEAK:
  LD L,110                ; Zero out bytes 109 and 110 of the character's
  XOR A                   ; buffer, which normally hold the address of the next
  LD (HL),A               ; character in the submessage being spoken (when a
  DEC L                   ; character starts speaking, there is no submessage)
  LD (HL),A               ;
; This entry point is used while the character is waiting for someone else to
; stop speaking.
WAITSPK:
  CALL SPKWAIT            ; Make the character start speaking if possible
  JP C,RM106              ; Terminate this interruptible subcommand if the
                          ; character is off-screen
  LD L,105                ; Set the interruptible subcommand routine address in
  LD (HL),140             ; bytes 105 and 106 of the character's buffer to the
                          ; entry point at WAITSPK
  RET NZ                  ; Return if somebody else is speaking at the moment
; Now it's this character's turn to speak.
  LD (HL),154             ; Set the interruptible subcommand routine address in
                          ; bytes 105 and 106 of the character's buffer to
                          ; SPEAK2
  RET

; Make a character speak (2)
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of a character's buffer by the routine at SPEAK just after the speech
; bubble has been printed above the character's head. It controls the character
; until he has finished speaking, and then removes the speech bubble.
;
; H Character number (152-169)
SPEAK2:
  CALL SLOW               ; Make sure the character speaks slowly
  PUSH HL
  CALL SLIDECHR1          ; Slide one letter into the text window of the speech
                          ; bubble
  POP HL
  JR NC,SPEAK2_1          ; Jump unless the character has finished speaking
SPEAK2_0:
  CALL RMBUBBLE           ; Remove the speech bubble
  JP RM106                ; Terminate this interruptible subcommand
SPEAK2_1:
  CALL SLIDECHR1          ; Slide another letter into the text window of the
                          ; speech bubble
  JR C,SPEAK2_0           ; Remove the speech bubble if the character has
                          ; finished speaking
  RET

; Remnants of an old version of the previous routine
;
; This unused code appears to be the remnants of an early version of the
; routine at SPEAK2; the job previously done by this code is now done by the
; routine at SLIDECHR1.
  LD (HL),218             ; (22,218)=M_SPACE8='<8 spaces>'
  DEC L                   ;
  LD (HL),22              ;
  LD A,32                 ; 32=' ' (space)
  JP SLIDECHR2

; Control EINSTEIN during class (1)
;
; Used by command lists 144, 152, 160 and 168. Makes EINSTEIN wait for his turn
; to speak, and then hands over control to the interruptible subcommand routine
; at WAITSPK; when EINSTEIN has finished speaking, control returns to the
; primary command routine at GRASS2.
;
; H 169 (EINSTEIN)
GRASSETC:
  LD A,(LSIGS)            ; Pick up the lesson signal flags from LSIGS
  RLCA                    ; Is it EINSTEIN's turn to speak yet?
  RET NC                  ; Return if not
  LD L,99                 ; Replace the address of this primary command routine
  LD (HL),202             ; in bytes 99 and 100 of EINSTEIN's buffer with that
                          ; of GRASS2 (so that we go there after EINSTEIN has
                          ; finished speaking)
  LD L,106                ; Set the routine address MSB in byte 106 of
  LD (HL),121             ; EINSTEIN's buffer to that of WAITSPK; the LSB will
                          ; be set later
  JP SPEAK                ; Make EINSTEIN join the queue to speak

; Control EINSTEIN during class (2)
;
; The address of this primary command routine is placed into bytes 99 and 100
; of EINSTEIN's buffer by the routine at GRASSETC. After EINSTEIN has finished
; speaking, control resumes here.
;
; H 169 (EINSTEIN)
GRASS2:
  LD L,99                 ; Replace the address of this primary command routine
  LD (HL),186             ; in bytes 99 and 100 of EINSTEIN's buffer with that
                          ; of GRASSETC
  LD HL,LSIGS             ; LSIGS holds the lesson signal flags
  RES 7,(HL)              ; Signal: EINSTEIN has just spoken (so it's now the
                          ; teacher's turn to speak)
  RET

; Check whether ERIC is where he should be
;
; Used by the routines at SEEKERIC, DINDUTY, DDUTY2, SWOTWAIT and TEACHERIC.
; Returns with the zero flag set if and only if ERIC is where he should be
; during dinner or class.
PRESENT:
  CALL ERICLOC            ; Get ERIC's coordinates in DE
  LD A,D                  ; A=ERIC's y-coordinate
  PUSH HL
  LD HL,TOPFLOOR          ; Point HL at the skool region table for the top
                          ; floor (see CHKERIC)
  CP 155                  ; Is ERIC on the top floor?
  JR Z,PRESENT_0          ; Jump if so
  CP 162                  ; Set the zero flag if ERIC's on the middle floor
  LD L,156                ; HL=MIDFLOOR (skool region data table for the middle
                          ; floor; see CHKERIC)
  JR Z,PRESENT_0          ; Jump if ERIC's on the middle floor
  LD L,166                ; HL=BOTFLOOR (skool region data table for the bottom
                          ; floor; see CHKERIC)
  CP 169                  ; Is ERIC on the bottom floor?
  JR Z,PRESENT_0          ; Jump if so
  POP HL
  RET                     ; Return with the zero flag reset
; ERIC is on the top, middle or bottom floor (i.e. not on a staircase).
PRESENT_0:
  LD A,E                  ; A=ERIC's x-coordinate
PRESENT_1:
  CP (HL)                 ; Find the region identifier (0-7) for ERIC's current
  INC HL                  ; location
  INC HL                  ;
  JR NC,PRESENT_1         ;
  DEC HL                  ; HL now points at the region identifier
  LD A,(LESSONDESC)       ; Pick up the current lesson descriptor from
                          ; LESSONDESC
  AND 7                   ; Keep only the room bits (bits 0-2)
  CP (HL)                 ; Is ERIC where he should be?
  POP HL
  RET                     ; Return with the zero flag set if so

; Get ERIC's coordinates
;
; Used by the routines at PRESENT, SEEKERIC and FINDERIC. Returns with ERIC's
; coordinates (adjusted appropriately if he's jumping) in DE.
ERICLOC:
  LD DE,(44129)           ; Pick up ERIC's coordinates in DE
  LD A,(STATUS)           ; STATUS holds ERIC's status flags
  BIT 2,A                 ; Bit 2 will be set if ERIC is jumping
  LD A,E                  ; Get ERIC's y-coordinate in D and x-coordinate in E
  LD E,D                  ;
  LD D,A                  ;
  RET Z                   ; Return unless ERIC is jumping
; This entry point is used by the routine at ERICHIT with ERIC's y-coordinate
; in A.
ERICLOC_0:
  LD D,155                ; This is the y-coordinate of the top floor
  CP 156                  ; Is ERIC on the top floor?
  RET C                   ; Return if so
  LD D,162                ; This is the y-coordinate of the middle floor
  CP 163                  ; Is ERIC on the middle floor?
  RET C                   ; Return if so
  LD D,169                ; This is the y-coordinate of the bottom floor
  RET

; Unused
  DEFB 0

; Data table for MR WACKER's questions and answers
;
; Used by the routine at PREPQA.
WACKERQA:
  DEFB 17                 ; LSB of M_WACKERA: '{Please Sir...}It's 1234'
  DEFB 0                  ; LSB of M_WACKERQ: 'WHAT IS 12 x 34?{8 spaces}'

; Data table for MR ROCKITT's questions and answers
;
; Used by the routine at PREPQA.
ROCKITTQA:
  DEFB 56                 ; LSB of M_ROCKITTQ1Q: 'Q' in 'WHAT ELEMENT HAS THE
                          ; SYMBOL Q?'
  DEFB 66                 ; LSB of M_ANSWERQ: 'Q' in '{Please Sir...}It is Q'
  DEFB 59                 ; LSB of M_ANSWER: '{Please Sir...}It is Q'
  DEFB 28                 ; LSB of M_ROCKITTQ1: 'WHAT ELEMENT HAS THE SYMBOL
                          ; Q?'
  DEFB 100                ; LSB of M_ROCKITTQ2Q: 'Q' in 'WHAT IS THE CHEMICAL
                          ; SYMBOL FOR Q?'
  DEFB 66                 ; LSB of M_ANSWERQ: 'Q' in '{Please Sir...}It is Q'
  DEFB 59                 ; LSB of M_ANSWER: '{Please Sir...}It is Q'
  DEFB 68                 ; LSB of M_ROCKITTQ2: 'WHAT IS THE CHEMICAL SYMBOL
                          ; FOR Q?'

; Data table for MR WITHIT's questions and answers
;
; Used by the routine at PREPQA.
WITHITQA:
  DEFB 155                ; LSB of M_WITHITQ2Q: 'Q' in 'WHICH COUNTRY'S CAPITAL
                          ; IS Q?'
  DEFB 66                 ; LSB of M_ANSWERQ: 'Q' in '{Please Sir...}It is Q'
  DEFB 59                 ; LSB of M_ANSWER: '{Please Sir...}It is Q'
  DEFB 128                ; LSB of M_WITHITQ2: 'WHICH COUNTRY'S CAPITAL IS Q?'
  DEFB 125                ; LSB of M_WITHITQ1Q: 'Q' in 'WHAT'S THE CAPITAL OF
                          ; Q?'
  DEFB 66                 ; LSB of M_ANSWERQ: 'Q' in '{Please Sir...}It is Q'
  DEFB 59                 ; LSB of M_ANSWER: '{Please Sir...}It is Q'
  DEFB 103                ; LSB of M_WITHITQ1: 'WHAT'S THE CAPITAL OF Q?'

; Data table for MR CREAK's questions and answers
;
; Used by the routine at PREPQA.
CREAKQA:
  DEFB 209                ; LSB of M_CREAKQ2Q: 'Q' in 'WHICH BATTLE OCCURRED IN
                          ; Q?'
  DEFB 247                ; LSB of M_CREAKA2Q: 'Q' in '{Please Sir...}It was
                          ; the BATTLE OF Q'
  DEFB 225                ; LSB of M_CREAKA2: '{Please Sir...}It was the BATTLE
                          ; OF Q'
  DEFB 184                ; LSB of M_CREAKQ2: 'WHICH BATTLE OCCURRED IN Q?'
  DEFB 181                ; LSB of M_CREAKQ1Q: 'Q' in 'WHEN WAS THE BATTLE OF
                          ; Q?'
  DEFB 223                ; LSB of M_CREAKA1Q: 'Q' in '{Please Sir...}It was in
                          ; Q'
  DEFB 212                ; LSB of M_CREAKA1: '{Please Sir...}It was in Q'
  DEFB 158                ; LSB of M_CREAKQ1: 'WHEN WAS THE BATTLE OF Q?'

; Prepare a question and answer
;
; Used by the routine at QALESSON. Prepares a question message and stores its
; address in bytes 107 and 108 of the teacher's buffer; also prepares the
; answer message and stores its address in bytes 107 and 108 of EINSTEIN's
; buffer. Places the address of the interruptible subcommand routine at SPEAK
; (make character speak) into bytes 105 and 106 of the teacher's buffer before
; returning.
;
; H Teacher's character number (163-166)
PREPQA:
  PUSH HL                 ; Save the teacher's character number
  LD A,H                  ; A=teacher's character number
  CP 163                  ; Is it MR WACKER?
  JR NZ,PREPQA_7          ; Jump if not
; MR WACKER is conducting this lesson, so we have to prepare a multiplication
; question and answer. First, generate two random numbers and store the ASCII
; codes of their digits in the question message.
  LD HL,M_WACKERQN1       ; HL=M_WACKERQN1: the '12' in 'WHAT IS 12 x 34?'
  LD B,2                  ; There are two numbers to generate
PREPQA_0:
  LD C,A                  ; Store the second (the multiplier) in C on pass 2
PREPQA_1:
  CALL GETRANDOM          ; Collect a random number between 32 and 99 in A
  AND 127                 ;
  CP 100                  ;
  JR NC,PREPQA_1          ;
  CP 32                   ;
  JR C,PREPQA_1           ;
  PUSH AF                 ; Store this number temporarily
  LD (HL),47              ; Insert the ASCII codes for the digits of the number
PREPQA_2:
  INC (HL)                ; into the 'WHAT IS 12 x 34?' message
  SUB 10                  ;
  JR NC,PREPQA_2          ;
  ADD A,58                ;
  INC L                   ;
  LD (HL),A               ;
  POP AF                  ; Restore the number to A
  LD L,13                 ; HL=M_WACKERQN2: the '34' in 'WHAT IS 12 x 34?'
  DJNZ PREPQA_0           ; Jump back to insert the multiplier into the message
; Now multiply the two numbers, and store the ASCII codes of the digits of the
; product in the answer message.
  LD DE,0                 ; HL=0 (ready for multiplication), and DE=M_WACKERAN
  LD L,23                 ; (the '1234' in EINSTEIN's response message)
  EX DE,HL                ;
PREPQA_3:
  ADD HL,BC               ; HL=the product of the two numbers
  DEC A                   ;
  JR NZ,PREPQA_3          ;
  LD BC,64536             ; Calculate the 1000s digit and store its ASCII code
PREPQA_4:
  INC A                   ; in the appropriate spot
  ADD HL,BC               ;
  JR C,PREPQA_4           ;
  SBC HL,BC               ;
  ADD A,47                ;
  LD (DE),A               ;
  LD BC,65436             ; Calculate the 100s digit and store its ASCII code
  LD A,47                 ; in the appropriate spot
PREPQA_5:
  INC A                   ;
  ADD HL,BC               ;
  JR C,PREPQA_5           ;
  SBC HL,BC               ;
  INC E                   ;
  LD (DE),A               ;
  LD A,L                  ; Calculate the 10s digit and store its ASCII code in
  EX DE,HL                ; the appropriate spot
  INC L                   ;
  LD (HL),47              ;
PREPQA_6:
  INC (HL)                ;
  SUB 10                  ;
  JR NC,PREPQA_6          ;
  ADD A,58                ; Store the ASCII code of the units digit in the
  INC L                   ; appropriate spot
  LD (HL),A               ;
  LD HL,WACKERQA          ; This is the base address of the data table used for
                          ; MR WACKER's questions and answers
  JR PREPQA_10
; Some teacher other than MR WACKER is conducting this lesson.
PREPQA_7:
  SUB 164                 ; A=0 (MR ROCKITT), 1 (MR WITHIT), or 2 (MR CREAK)
  CALL BIRTHQ             ; Make MR CREAK ask the birth year question if
                          ; appropriate
  LD B,128
  JR Z,PREPQA_8           ; Jump if we're dealing with MR ROCKITT
  LD B,170
  LD L,34                 ; HL=WITHITQA
  DEC A                   ; Are we dealing with MR WITHIT?
  JR Z,PREPQA_8           ; Jump if so
  LD L,42                 ; HL=CREAKQA
  LD B,212
; Now HL points at one of the data tables at ROCKITTQA (MR ROCKITT), WITHITQA
; (MR WITHIT) and CREAKQA (MR CREAK). Each data table contains eight LSBs: four
; LSBs for each type of question. The LSBs correspond to (1) the identifier
; slot in the teacher's question; (2) the identifier slot in EINSTEIN's answer;
; (3) the base address of EINSTEIN's answer; and (4) the base address of the
; teacher's question.
PREPQA_8:
  CALL GETRANDOM          ; Get a random number between 0 and 41 in A
  CP 42                   ;
  JR NC,PREPQA_8          ;
  ADD A,B                 ; A=128-169 (MR ROCKITT), 170-211 (MR WITHIT), or
                          ; 212-253 (MR CREAK)
  BIT 0,A                 ; There are two types of question the teacher can
  JR Z,PREPQA_9           ; ask; here we decide which one
  INC L                   ; Move forward four spaces in the data table to the
  INC L                   ; LSBs for the second type of question
  INC L                   ;
  INC L                   ;
PREPQA_9:
  LD D,240                ; Insert the random identifier into the teacher's
  LD E,(HL)               ; question
  LD (DE),A               ;
  INC L                   ; Insert the corresponding random identifier into
  LD E,(HL)               ; EINSTEIN's answer
  XOR 1                   ;
  LD (DE),A               ;
  INC L
PREPQA_10:
  LD E,(HL)               ; Store the address of the answer message in bytes
  LD D,240                ; 107 and 108 of EINSTEIN's buffer
  LD (43371),DE           ;
  INC L
  LD E,(HL)               ; DE=address of the question message
; This entry point is used by the routine at BIRTHQ.
PREPQA_11:
  POP HL                  ; Restore the teacher's character number to H
  LD L,107                ; Store the address of the question message in bytes
  LD (HL),E               ; 107 and 108 of the teacher's buffer
  INC L                   ;
  LD (HL),D               ;
  LD L,106                ; Place the address of the interruptible subcommand
  LD (HL),121             ; routine at SPEAK (make character speak) into bytes
  DEC L                   ; 105 and 106 of the teacher's buffer
  LD (HL),134             ;
  RET

; Unused
  DEFS 5

; Determine the next move of a character following another character
;
; Used by the routines at SEEKERIC, GETINPUT and FINDERIC. Returns with one of
; the following values in A depending on the relative locations of the follower
; and his target:
;
; +---+---------------------------------------------------+
; | A | Meaning                                           |
; +---+---------------------------------------------------+
; | 0 | Follower is at the same coordinates as the target |
; | 1 | Follower should go (or continue going) upstairs   |
; | 2 | Follower should go (or continue going) downstairs |
; | 3 | Follower should go left                           |
; | 4 | Follower should go right                          |
; +---+---------------------------------------------------+
;
; DE Target character's coordinates
; H Follower's character number (152-172)
NEXTMOVE:
  LD L,97                 ; Byte 97 of the follower's buffer holds his
                          ; y-coordinate
  LD A,(HL)               ; Pick this up in A
  CP 155                  ; Is the follower on the top floor?
  JR Z,NEXTMOVE_0         ; Jump if so
  CP 162                  ; Is the follower on the middle floor?
  JR Z,NEXTMOVE_0         ; Jump if so
  CP 169                  ; Is the follower on the bottom floor?
  JR Z,NEXTMOVE_0         ; Jump if so
; The follower is on a staircase. Find out whether he's going up or down and
; set A accordingly to make him continue.
  LD L,98                 ; Byte 98 of the follower's buffer holds his
                          ; x-coordinate
  LD A,(HL)               ; Pick this up in A
  LD L,96                 ; Byte 96 holds the follower's animatory state
  ADD A,80                ; Set bit 7 of A if the follower is on a staircase at
                          ; the right of the skool
  XOR (HL)                ; A=1 if the follower is going upstairs, 2 if he's
  AND 128                 ; going downstairs
  RLCA                    ;
  INC A                   ;
  RET
; The follower is not on a staircase. Which way should he go?
NEXTMOVE_0:
  CP D                    ; Is the follower on the same floor as the target?
  JR NZ,NEXTMOVE_8        ; Jump if not
; The follower is on the same floor as the target. However, there may be a wall
; between them.
  INC L                   ; Pick up the follower's x-coordinate in A
  LD A,(HL)               ;
  SUB E                   ; Is the follower in the same spot as his target?
  RET Z                   ; Return if so
  DEC L                   ; Pick up the follower's y-coordinate in A
  LD A,(HL)               ;
  CP 169                  ; Is the follower on the bottom floor?
  JR NZ,NEXTMOVE_3        ; Jump if not
; The follower and target are both on the bottom floor, so the follower should
; simply move either left or right.
  INC HL                  ; L=98
NEXTMOVE_1:
  LD A,(HL)               ; A=follower's x-coordinate
  CP E                    ; Set the carry flag if the follower should go right
NEXTMOVE_2:
  LD A,3                  ; A=3 if the follower should go left, 4 if he should
  ADC A,0                 ; go right
  RET
; The follower and target are both on the middle floor or both on the top
; floor.
NEXTMOVE_3:
  CP 162                  ; Set the zero flag if the follower and target are on
                          ; the middle floor
  INC HL                  ; L=98
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Reading and Map Rooms
  JR NZ,NEXTMOVE_4        ; Jump if the follower and target are on the top
                          ; floor
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
NEXTMOVE_4:
  CP (HL)                 ; Compare the follower's x-coordinate with that of
                          ; the wall
  JR C,NEXTMOVE_6         ; Jump if the follower is to the right of the wall
  CP E                    ; Is the target character to the left of the wall?
  JR NC,NEXTMOVE_1        ; Jump if so
; The follower is on the left side of the skool and must go downstairs to get
; to the target on the other side of the skool.
NEXTMOVE_5:
  LD A,(HL)               ; A=follower's x-coordinate
  CP 12                   ; This is the x-coordinate of the tops of the
                          ; staircases at the left of the skool
  JR NZ,NEXTMOVE_2        ; Jump unless the follower is at the top of a
                          ; staircase
  LD L,96                 ; Byte 96 of the follower's buffer holds his
                          ; animatory state
  LD A,4                  ; A=4 will make the follower turn round if facing
                          ; left
  BIT 7,(HL)              ; Is the follower facing left?
  RET Z                   ; Return if so
  LD A,2                  ; A=2 will make the follower go down the stairs
  RET
; The follower is on the right side of the skool. Is the target there too?
NEXTMOVE_6:
  CP E                    ; Is the target character to the right of the wall?
  JR C,NEXTMOVE_1         ; Jump if so
NEXTMOVE_7:
  LD A,(HL)               ; A=follower's x-coordinate
  CP 77                   ; This is the x-coordinate of the tops of the
                          ; staircases at the right of the skool
  JR NZ,NEXTMOVE_2        ; Jump unless the follower is at the top of a
                          ; staircase
  LD L,96                 ; Byte 96 of the follower's buffer holds his
                          ; animatory state
  LD A,3                  ; A=3 will make the follower turn round if facing
                          ; right
  BIT 7,(HL)              ; Is the follower facing right?
  RET NZ                  ; Return if so
  LD A,2                  ; A=2 will make the follower go down the stairs
  RET
; The follower and target are on different floors.
NEXTMOVE_8:
  JR NC,NEXTMOVE_11       ; Jump if the target is on a floor above the follower
  CP 162                  ; Set the zero flag if the follower is on the middle
                          ; floor
  INC HL                  ; L=98
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Reading and Map Rooms
  JR NZ,NEXTMOVE_9        ; Jump if the follower is on the top floor
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
NEXTMOVE_9:
  CP (HL)                 ; Compare the follower's x-coordinate with that of
                          ; the wall
NEXTMOVE_10:
  JR NC,NEXTMOVE_5        ; Jump if the follower is to the left of the wall
                          ; (and so needs to go to the top of a staircase on
                          ; the left)
  JR C,NEXTMOVE_7         ; Jump if the follower is to the right of the wall
                          ; (and so needs to go to the top of a staircase on
                          ; the right)
; The target is on a floor above the follower. Which staircase should the
; follower use?
NEXTMOVE_11:
  LD A,D                  ; A=target's y-coordinate
  LD D,155                ; This is the y-coordinate of the top floor
  CP 162                  ; This is the y-coordinate of the middle floor
  LD A,57                 ; This is the x-coordinate of the wall between the
                          ; Reading and Map Rooms
  JR C,NEXTMOVE_12        ; Jump if the target is above the middle floor
  LD D,162                ; This is the y-coordinate of the middle floor
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
NEXTMOVE_12:
  CP E                    ; Compare the target's x-coordinate with that of the
                          ; wall
  LD E,97                 ; Signal: target is to the right of the wall
  JR C,NEXTMOVE_13        ; Jump if the target is to the right of the wall
  LD E,1                  ; Signal: target is to the left of the wall
NEXTMOVE_13:
  LD A,(HL)               ; A=follower's y-coordinate
  CP 169                  ; Set the zero flag if the follower is on the bottom
                          ; floor
  INC HL                  ; L=98
  JR NZ,NEXTMOVE_17       ; Jump if the follower is on the middle floor
NEXTMOVE_14:
  LD A,19                 ; This is the x-coordinate of the bottoms of the
                          ; staircases at the left of the skool
  DEC E                   ; Is the target to the left of the wall?
  JR Z,NEXTMOVE_15        ; Jump if so
  LD A,70                 ; This is the x-coordinate of the bottoms of the
                          ; staircases at the right of the skool
NEXTMOVE_15:
  CP (HL)                 ; Compare this with the follower's x-coordinate
  CCF                     ; Set the carry flag if the follower should go right
  JR NZ,NEXTMOVE_2        ; Jump if the follower is not at the bottom of a
                          ; staircase
  LD L,96                 ; Byte 96 of the follower's buffer holds his
                          ; animatory state
  CP 19                   ; This is the x-coordinate of the bottoms of the
                          ; staircases at the left of the skool
  LD A,1                  ; A=1 will make the follower go upstairs
  JR Z,NEXTMOVE_16        ; Jump if the follower should go up a staircase at
                          ; the left of the skool
  BIT 7,(HL)              ; Is the follower facing right?
  RET NZ                  ; Jump if so
  LD A,4                  ; A=3 will make the follower turn round and face
                          ; right
  RET
NEXTMOVE_16:
  BIT 7,(HL)              ; Is the follower facing left?
  RET Z                   ; Return (with A=1) if so
  LD A,3                  ; A=3 will make the follower turn round and face left
  RET
; The follower is on the middle floor, and the target is on the top floor.
NEXTMOVE_17:
  LD A,38                 ; This is the x-coordinate of the wall between the
                          ; White and Exam Rooms
  CP (HL)                 ; Compare this with the follower's x-coordinate
  LD A,97                 ; Signal: follower is to the right of the wall
  JR C,NEXTMOVE_18        ; Jump if the follower is to the right of the wall
  LD A,1                  ; Signal: follower is to the left of the wall
NEXTMOVE_18:
  CP E                    ; Is the target on the same side of the skool?
  JR Z,NEXTMOVE_14        ; Jump if so (follower must go up the nearest
                          ; staircase)
  CCF                     ; Set the carry flag if the follower must go down a
                          ; staircase at the right of the skool, and reset it
                          ; if he must go down a staircase at the left of the
                          ; skool
  JR NEXTMOVE_10

; Unused
  DEFB 0

; Rewind to a specific point in a command list
;
; Used by the routine at SEEKERIC. Partially rewinds a teacher's command list
; after he has fetched the truant ERIC, thus making him resume normal classroom
; duties.
;
; DE Command list entry address
; H Teacher's character number (163-166)
; L 118
REWIND:
  LD (HL),D               ; Place the command list entry address into bytes 117
  DEC L                   ; and 118 of the teacher's buffer, which will make
  LD (HL),E               ; the teacher restart the command list from that
                          ; point
  LD L,106                ; Remove the interruptible subcommand routine address
  LD (HL),0               ; from bytes 105 and 106 of the teacher's buffer
  JP NEXTCMD              ; Move to the next command in the command list

; Make a teacher find the truant ERIC
;
; Used by the routine at DINDUTY, which also places the address of this
; interruptible subcommand routine into bytes 105 and 106 of the teacher's
; buffer when ERIC is absent during dinner or class. It makes the teacher run
; after and stalk ERIC until he goes to wherever he should be (the dinner hall
; or the classroom).
;
; H Teacher's character number (163-166)
SEEKERIC:
  CALL ERICLOC            ; Get ERIC's coordinates in DE
  CALL RUN                ; Make this teacher walk fast
  CALL NEXTMOVE           ; Determine which way the teacher needs to go to find
                          ; ERIC
  AND A                   ; Are ERIC and this teacher in the same location?
  JR Z,SEEKERIC_0         ; Jump if so
  CP 3                    ; Is this teacher going up or down a staircase?
  JR C,SEEKERIC_2         ; Jump if so
SEEKERIC_0:
  LD L,122                ; If this teacher's command list has been marked for
  BIT 0,(HL)              ; a restart (by the routine at NEWLESSON), terminate
  JP NZ,RM106             ; this interruptible subcommand and restart now
  LD B,A
  CALL PRESENT            ; Check whether ERIC is where he should be
  LD A,B
  JR NZ,SEEKERIC_2        ; Jump if ERIC is not where he should be
; ERIC is back where he should be, so it's time for the teacher to resume his
; classroom duties.
  LD L,117                ; Pick up in DE the address the teacher has reached
  LD E,(HL)               ; in his command list
  INC L                   ;
  LD D,(HL)               ;
  LD B,2                  ; 190 is the LSB of the address of the routine at
SEEKERIC_1:
  DEC DE                  ; GOTO; point DE at the second-from-last occurrence
  LD A,(DE)               ; of this routine address in the teacher's command
  CP 190                  ; list (which will take him to the side of the
  JR NZ,SEEKERIC_1        ; blackboard where he waits for EINSTEIN to grass, or
  DJNZ SEEKERIC_1         ; back to the dinner hall)
  JR REWIND               ; Restart the teacher's command list from this point
; ERIC is not where he should be, or the teacher is on a staircase.
SEEKERIC_2:
  AND A                   ; Are ERIC and the teacher in exactly the same
                          ; location?
  JR NZ,SEEKERIC_4        ; Jump if not
  LD A,(23672)            ; 23672=LSB of FRAMES system variable
  AND A                   ; This LSB will be 0 once every 5.12 seconds
  RET NZ                  ; Return if it's not zero now
SEEKERIC_3:
  CALL UPDATESRB          ; Update the SRB for the teacher's current animatory
                          ; state
  XOR 128                 ; Make the teacher turn round
  JP UPDATEAS             ; Update the teacher's animatory state and update the
                          ; SRB
; This entry point is used by the routine at FINDERIC with H holding the number
; of a character looking for ERIC (which will be little boy no. 10 or a
; teacher), and A holding 1, 2, 3 or 4 (indicating the character's next move).
SEEKERIC_4:
  CP 3                    ; Set the carry flag if the chaser is on a staircase
  LD BC,0                 ; DE will hold the appropriate x- and y-coordinate
  LD DE,0                 ; increments for the chaser's next move (to the
                          ; midstride position), and BC will hold the
                          ; appropriate x- and y-coordinate increments for the
                          ; chaser's move after that (from the midstride
                          ; position); initialise these increments to 0
  LD L,96                 ; Byte 96 of the buffer holds the animatory state of
                          ; ERIC's chaser
  JR C,FINDERIC2          ; Jump if the chaser is currently going up or down
                          ; stairs
  JR NZ,SEEKERIC_5        ; Jump if the chaser must go right (A=4)
  BIT 7,(HL)              ; Is the chaser facing right?
  JR NZ,SEEKERIC_3        ; Turn him round if so
  DEC C                   ; C=-1
  JR FINDERIC2_1
SEEKERIC_5:
  BIT 7,(HL)              ; Is the chaser facing left?
  JR Z,SEEKERIC_3         ; Turn him round if so
  INC C                   ; C=1
  JR FINDERIC2_1

; Move a character looking for ERIC from the midstride position
;
; Used by the routine at FINDERIC. The address of this interruptible subcommand
; routine is also placed into bytes 105 and 106 of a character's buffer by the
; routine at FINDERIC2. It moves ERIC's chaser from the midstride position.
;
; H Number of the character looking for ERIC (161, 163-166)
TOERIC:
  LD L,105                ; Replace the address of this routine in bytes 105
  LD (HL),160             ; and 106 of the character's buffer (if present) with
                          ; that of SEEKERIC
  CALL UPDATESRB          ; Update the SRB for the character's current location
  LD L,108                ; D=character's new y-coordinate (adjusted by the
  LD B,A                  ; value in byte 108 of his buffer, placed there by
  LD A,D                  ; the routine at FINDERIC2)
  ADD A,(HL)              ;
  LD D,A                  ;
  DEC L                   ; E=character's new x-coordinate (adjusted by the
  LD A,E                  ; value in byte 107 of his buffer, placed there by
  ADD A,(HL)              ; the routine at FINDERIC2)
  LD E,A                  ;
; The character's post-midstride coordinates have been calculated; now we
; consider his post-midstride animatory state. Usually, a character's animatory
; state is incremented as he moves to or from the midstride position, which
; makes his walking animation cycle through phases 1, 2, 3 and 4. However, the
; next section of code - unlike its counterpart in the routine at FINDERIC2,
; which moves ERIC's chaser to the midstride position - decrements the
; animatory state if the character is moving leftwards, and increments it if
; the character is moving rightwards. So when a character is moving leftwards
; while chasing ERIC, his walking animation will cycle through phases 1 and 2
; only, or 3 and 4 only. This is a bug.
  LD A,(HL)               ; A=x-coordinate increment (-1 or +1)
  ADD A,B                 ; Add this to the character's current animatory state
  AND 3                   ; Now C holds the new walking phase bits
  LD C,A                  ;
  LD A,B                  ; A=character's current animatory state
  AND 252                 ; Clear the walking phase bits (bits 0 and 1)
  ADD A,C                 ; Set the walking phase to 1 (C=0) or 3 (C=2)
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB

; Move a character looking for ERIC to the midstride position
;
; Continues from SEEKERIC. Moves the character looking for ERIC to the
; midstride position in the appropriate direction.
;
; A 1 if the character's going upstairs, 2 if going down
; BC 0
; DE 0
; H Number of the character looking for ERIC (161, 163-166)
; L 96
;
; The main entry point is used when the character is on a staircase.
FINDERIC2:
  INC B                   ; B=1
  DEC A                   ; Is the chaser going downstairs?
  JR NZ,FINDERIC2_0       ; Jump if so
  DEC B                   ; B=0
  DEC D                   ; D=-1
FINDERIC2_0:
  INC C                   ; C=1
  BIT 7,(HL)              ; Is the chaser facing right?
  JR NZ,FINDERIC2_1       ; Jump if so
  DEC C                   ; C=-1
  DEC C                   ;
; This entry point is used by the routine at SEEKERIC when the character
; looking for ERIC is not on a staircase; in this case B=D=E=0, and C=-1 if the
; chaser is to ERIC's left, or 1 if he is to ERIC's right.
FINDERIC2_1:
  LD L,107                ; C=-1 if the chaser is to ERIC's left, or 1 if to
  LD (HL),C               ; his right; B=1 if the chaser is going downstairs, 0
  INC L                   ; otherwise; place these values into bytes 107 and
  LD (HL),B               ; 108 of the chaser's buffer
  LD L,105                ; Change the interruptible subcommand routine address
  LD (HL),251             ; in bytes 105 and 106 of the chaser's buffer from
                          ; SEEKERIC to TOERIC
  PUSH DE                 ; Save the x- and y-coordinate increments for the
                          ; chaser's next move
  CALL UPDATESRB          ; Update the SRB for the chaser's current location
  INC A                   ; A=chaser's next animatory state (midstride)
  LD L,A                  ; Store this in L briefly
  POP BC                  ; Get the appropriate y- and x-coordinate increments
                          ; for the chaser's move to the midstride position in
                          ; BC
  LD A,B                  ; Get the chaser's new y-coordinate in D (which is
  ADD A,D                 ; the same as his current y-coordinate unless he's
  LD D,A                  ; going up a staircase)
  LD A,C                  ; Get the chaser's new x-coordinate in E (which is
  ADD A,E                 ; the same as his current x-coordinate, because C is
  LD E,A                  ; always 0)
  LD A,L                  ; Restore the chaser's new animatory state to A
  JP UPDATEAS             ; Update the chaser's animatory state and location
                          ; and update the SRB

; Restart the command list unless it's time to start the lesson
;
; Used by the teachers' command lists 128, 130, 132, 134 and 184. Restarts the
; teacher's command list if it's not time to start the class (or the dinner
; period) yet; otherwise moves to the next command in the command list.
;
; H Teacher's character number (163-166)
CHKTIME:
  LD A,(32760)            ; Pick up the MSB of the lesson clock (0-21)
  CP 15                   ; Is it time to start dinner or the lesson?
  JP C,NEXTCMD            ; Move to the next command in the command list if so;
                          ; otherwise continue into RESTART (restart command
                          ; list)

; Restart the command list
;
; Used by command lists 182, 190, 192, 194, 196, 204 and 206.
;
; H Character number (152-169)
RESTART:
  LD A,(LESSONNO)         ; Pick up the current lesson number (0-63) from
                          ; LESSONNO
  LD E,A                  ; Copy this to E
  LD B,1                  ; We want to restart the command list for this
                          ; character only
  LD D,254                ; Set E to the lesson identifier (224-255) from the
  LD A,(DE)               ; main timetable; note that this will not be the
  LD E,A                  ; identifier of the current lesson if the current
                          ; lesson is a 'special' playtime - 243, 244 or 245 -
                          ; which is a bug
  CALL NEWLESSON_2        ; Set this character's command list up for a restart
  JP RM106                ; Move to the first command in the command list

; Unused
  DEFS 3

; Place a continual subcommand routine address into a character's buffer
;
; Used by command lists 140, 142, 148, 150, 156, 158, 164, 166, 172, 174, 178,
; 180, 182, 192, 194, 214, 216, 218 and 220. Collects a continual subcommand
; routine address from a command list and places it into bytes 124 and 125 of
; the character's buffer. The routine address will be one of the following:
;
; +----------+------------------------------------------------+
; | Address  | Description                                    |
; +----------+------------------------------------------------+
; | VIOLENT  | Make ANGELFACE hit now and then                |
; | CATTY    | Make BOY WANDER fire his catapult now and then |
; | RUN      | Make a character walk fast                     |
; | CHKTOUCH | Check whether ANGELFACE is touching ERIC       |
; +----------+------------------------------------------------+
;
; H Character number (152-169)
ADDR2BUF:
  NOP
  NOP
  NOP
  NOP
ADDR2CBUF:
  LD L,124                ; Collect the address of the continual subcommand
  CALL GETPARAM           ; routine from the command list and place it into
  LD (HL),A               ; bytes 124 and 125 of the character's buffer
  INC L                   ;
  CALL GETPARAM           ;
  LD (HL),A               ;
  JP MVCHARS_6            ; Move to the next command in the command list

; Unused
  DEFS 2

; Make a teacher perform dinner duty (1)
;
; Used by command list 184. Makes the teacher on dinner duty pace up and down
; the dinner hall, checking whether ERIC is present. If ERIC is absent, the
; teacher is sent to find him, after which the command list is restarted.
;
; H 163 (MR WACKER) or 165 (MR WITHIT)
DINDUTY:
  LD DE,LFLAGS            ; Set bit 7 at LFLAGS, indicating that dinner has
  EX DE,HL                ; started (and so ERIC should be in the dinner hall)
  SET 7,(HL)              ;
  EX DE,HL                ;
; This entry point is used on subsequent calls when the teacher on dinner duty
; has reached the right end of the dinner hall.
ONDUTY:
  CALL PRESENT            ; Set the zero flag if ERIC is where he should be
  LD DE,38689             ; D=151 (LSB of DDUTY2), E=33 (x-coordinate of the
                          ; left end of the teacher's dinner duty orbit)
; This entry point is used by the routine at DDUTY2 with D=117 (LSB of the
; entry point at ONDUTY, above) and E=62 (x-coordinate of the right end of the
; teacher's dinner duty orbit), and also by the routine at TEACHERIC with the
; zero flag reset.
DINDUTY_0:
  LD L,105
  JR Z,DINDUTY_1          ; Jump if ERIC is where he should be
  LD (HL),160             ; Place the address of the interruptible subcommand
  INC L                   ; routine at SEEKERIC into bytes 105 and 106 of the
  LD (HL),123             ; character's buffer
  JP SEEKERIC             ; And jump to that routine now
; ERIC is where he should be, so there's no need to chase him.
DINDUTY_1:
  LD (HL),60              ; Place the address of the interruptible subcommand
  INC L                   ; routine at WALK into bytes 105 and 106 of the
  LD (HL),99              ; teacher's buffer
  INC L                   ; Place 33 or 62 into byte 107 of the teacher's
  LD (HL),E               ; buffer (to make him go to that x-coordinate next)
  INC L                   ; Place 0 into byte 108 of the teacher's buffer,
  LD (HL),0               ; giving him more than enough time to reach the other
                          ; end of the dinner hall without interruption
  LD L,99                 ; Replace the address of this primary command routine
  LD (HL),D               ; in bytes 99 and 100 of the teacher's buffer with
                          ; that of DDUTY2 or ONDUTY
  JP WALK                 ; Proceed to the other end of the dinner hall

; Make a teacher perform dinner duty (2)
;
; The address of this primary command routine is placed into bytes 99 and 100
; of the teacher's buffer by the routine at DINDUTY. The teacher has walked to
; the left end of the dinner hall, so it's time to check whether ERIC is
; present.
DDUTY2:
  CALL PRESENT            ; Set the zero flag if ERIC is in the dinner hall
  LD DE,30014             ; D=117 (LSB of ONDUTY), E=62 (x-coordinate of the
                          ; right end of the teacher's dinner duty orbit)
  JR DINDUTY_0            ; Go and look for ERIC, or go to the right end of the
                          ; dinner hall

; Unused
  DEFB 0

; Make a teacher give lines to EINSTEIN for telling tales
;
; Continues from the routine at SWOTLINES.
;
; H Teacher's character number (163-166)
CLSLINES:
  LD A,169                ; 169=EINSTEIN
  LD B,9                  ; Lines reprimand 9: DON'T TELL TALES
; This entry point is used by the routine at TEACHERIC with A=172 (ERIC) and
; B=8 (NOW DON'T DO IT AGAIN), 10 (NEVER BE LATE AGAIN), 11 (AND STAY THIS
; TIME) or 12 (DON'T TOUCH BLACKBOARDS).
CLSLINES_0:
  LD L,97                 ; Bytes 97 and 98 of the teacher's buffer hold his
                          ; coordinates
  PUSH HL                 ; Save the teacher's character number briefly
  LD D,(HL)               ; Collect the teacher's coordinates in DE
  INC L                   ;
  LD E,(HL)               ;
  CALL GIVELINES          ; Make the teacher give lines
  POP HL                  ; Restore the teacher's character number to H
  RET

; Make EINSTEIN speak
;
; Used by the routine at TEACHERIC. Copies the message address into bytes 107
; and 108 of EINSTEIN's buffer, and then signals that it's his turn to speak.
;
; BC Message address
; H Teacher's character number (163-166)
SWOTSPK:
  LD DE,43371             ; Point HL at byte 107 of EINSTEIN's buffer
  EX DE,HL                ;
  LD (HL),C               ; Copy the message address in BC into bytes 107 and
  INC L                   ; 108 of EINSTEIN's buffer
  LD (HL),B               ;
; This entry point is used by the routine at QALESSON.
SWOTSPK_0:
  LD HL,LSIGS             ; LSIGS holds the lesson signal flags
  SET 7,(HL)              ; Signal (to the routine at GRASSETC) that it's
                          ; EINSTEIN's turn to speak
  EX DE,HL
  LD L,106                ; Place the address of the interruptible subcommand
  LD (HL),124             ; routine at SWOTSPK2 into bytes 105 and 106 of the
  DEC L                   ; teacher's buffer
  LD (HL),200             ;
  POP DE                  ; Collect the return address in DE and copy it to
  LD L,99                 ; bytes 99 and 100 of the teacher's buffer (so that
  LD (HL),E               ; control will resume there when SWOTSPK2
                          ; relinquishes it)
  RET                     ; Return to the main loop

; Make a teacher wait for EINSTEIN to finish speaking
;
; The address of this interruptible subcommand routine is placed into bytes 105
; and 106 of a teacher's buffer by the routine at SWOTSPK.
;
; H Teacher's character number (163-166)
SWOTSPK2:
  LD A,(LSIGS)            ; LSIGS holds the lesson signal flags
  RLCA                    ; Is EINSTEIN still speaking?
  RET C                   ; Return if so
  JP RM106                ; Terminate this interruptible subcommand

; Check whether ERIC and EINSTEIN are in class
;
; Used by the routines at TEACHERIC and QALESSON. If EINSTEIN is in class, this
; routine returns to the caller with the zero flag set if and only if ERIC is
; present too. If EINSTEIN is not yet sitting down in class, it makes the
; teacher wait until he shows up.
;
; H Teacher's character number (163-166)
SWOTWAIT:
  LD A,(SWOTCBUF)         ; A=EINSTEIN's animatory state
  CP 21                   ; 21=EINSTEIN sitting in a chair
  JP Z,PRESENT            ; Jump if EINSTEIN is sitting in a chair
  POP DE                  ; Place the address of the instruction that called
  LD L,99                 ; this routine into bytes 99 and 100 of the teacher's
  LD A,E                  ; buffer, so that control of the teacher remains in
  SUB 3                   ; this routine until EINSTEIN is seated
  LD (HL),A               ;
  RET

; Make a teacher decide whether to give EINSTEIN lines for telling tales
;
; Used by the routine at TEACHERIC.
;
; H Teacher's character number (163-166)
SWOTLINES:
  LD A,H                  ; L=0 (MR WACKER), 16 (MR ROCKITT), 32 (MR WITHIT),
  SUB 163                 ; or 48 (MR CREAK)
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  LD L,A                  ;
; The number in L determines how often the teacher will give lines to EINSTEIN
; for telling tales:
;
; +------------+-----------+
; | Teacher    | Frequency |
; +------------+-----------+
; | MR WACKER  | Never     |
; | MR ROCKITT | 1 in 16   |
; | MR WITHIT  | 1 in 8    |
; | MR CREAK   | 3 in 16   |
; +------------+-----------+
  CALL GETRANDOM          ; A=random number
  CP L                    ; Set the carry flag if the teacher is going to give
                          ; lines to EINSTEIN
  LD L,255                ; Return with L=255 if the teacher's not in the mood
  RET NC                  ; to give lines to EINSTEIN
  JR CLSLINES             ; Make the teacher give EINSTEIN lines for telling
                          ; tales

; Call an interruptible subcommand
;
; Used by the routines at GOTOPAGE, CLSNOERIC, BWWRITECLN, TEACHERIC and
; QALESSON. Drops the return address from the stack and copies its LSB into
; byte 99 of the character's buffer (where the LSB of the address of the
; primary command routine is held), copies the address of the interruptible
; subcommand routine from BC into bytes 106 and 107 of the character's buffer,
; and then jumps to that routine.
;
; BC Interruptible subcommand routine address
; H Character number (152-169)
CALLSUBCMD:
  LD L,106                ; Copy the routine address from BC into bytes 105 and
  LD (HL),B               ; 106 of the character's buffer
  DEC L                   ;
  LD (HL),C               ;
  POP DE                  ; Drop the return address off the stack and place its
  LD L,99                 ; LSB into byte 99 of the character's buffer; this
  LD (HL),E               ; ensures that when the interruptible subcommand has
                          ; finished, control resumes in the caller of this
                          ; routine
  PUSH BC                 ; Make an indirect jump to the routine address in BC
  RET                     ;

; Make a teacher tell the kids to go to a certain page in their books
;
; Used by the routines at CLSNOERIC and TEACHERIC. Makes the teacher tell the
; kids to turn to or answer the questions on a randomly chosen page of their
; books.
;
; H Teacher's character number (163-166)
GOTOPAGE:
  LD DE,M_CLASSDO1P       ; BC=M_CLASSDO1 (TURN TO PAGE 123 OF YOUR BOOKS...);
  LD C,98                 ; DE=M_CLASSDO1P (the '123' in this message)
  LD B,D                  ;
  CALL GETRANDOM          ; A=random number
  RLCA                    ; This jump (which decides between the message at
  JR C,GOTOPAGE_0         ; M_CLASSDO1 and the message at M_CLASSDO2) is
                          ; performed half the time
  LD E,186                ; BC=M_CLASSDO2 (ANSWER THE QUESTIONS ON PAGE 123 OF
  LD C,157                ; YOUR LOVELY TEXTBOOK); DE=M_CLASSDO2P (the '123' in
                          ; this message)
GOTOPAGE_0:
  SCF                     ; A=128-255 (random page number to insert into the
  RRA                     ; message)
  NOP
  EX DE,HL                ; Point HL at the slot for the 100s digit of the page
                          ; number in the message
  LD (HL),47              ; Compute and insert the ASCII code for the 100s
GOTOPAGE_1:
  INC (HL)                ; digit of the page number
  SUB 100                 ;
  JR NC,GOTOPAGE_1        ;
  INC L                   ; Point HL at the slot for the 10s digit of the page
                          ; number
  ADD A,100               ; Compensate for the trial subtraction of 100 above
  LD (HL),47              ; Compute and insert the ASCII code for the 10s digit
GOTOPAGE_2:
  INC (HL)                ; of the page number
  SUB 10                  ;
  JR NC,GOTOPAGE_2        ;
  INC L                   ; Point HL at the slot for the units digit of the
                          ; page number
  ADD A,58                ; Compensate for the trial subtraction of ten above,
                          ; and add an extra 48 (the ASCII code for '0')
  LD (HL),A               ; Insert the ASCII code for the units digit of the
                          ; page number
  EX DE,HL                ; Restore the teacher's character number to H
; This entry point is used by the routines at CLSNOERIC, TELLSIT, TEACHERIC,
; LINES2K, TELLERIC and SENDHOME2.
GOTOPAGE_3:
  LD L,108                ; Place the address of the message into bytes 107 and
  LD (HL),B               ; 108 of the character's buffer
  DEC L                   ;
  LD (HL),C               ;
  LD BC,SPEAK             ; Place the address of the interruptible subcommand
  JR CALLSUBCMD           ; routine at SPEAK (make character speak) into bytes
                          ; 105 and 106 of the character's buffer and jump to
                          ; it

; Make a teacher conduct a class without ERIC
;
; Used by the routine at DOCLASS. Makes the teacher wipe the board (if there is
; one), tell the kids what to do, and then walk up and down until the end of
; the lesson.
;
; H Teacher's character number (163-166)
CLSNOERIC:
  CALL BOARDID            ; A=identifier of the blackboard the teacher is next
                          ; to
  LD C,A                  ; Copy this to C
  CP 236                  ; Is the teacher on the top floor?
  JR NZ,CLSNOERIC_0       ; Jump if not
  INC L                   ; L=98
  LD A,(HL)               ; A=teacher's x-coordinate
  CP 58                   ; Is the teacher in the Map Room?
  JR NC,CLSNOERIC_1       ; Jump if so (there's no blackboard in the Map Room)
CLSNOERIC_0:
  LD BC,WIPE1             ; Hand over control to the routine at WIPE1, making
  CALL CALLSUBCMD         ; the teacher wipe the blackboard
; Control returns here when the teacher has finished wiping the blackboard.
  LD L,98                 ; A=teacher's x-coordinate (equal to that of the left
  LD A,(HL)               ; edge of the blackboard)
  ADD A,3                 ; Place the x-coordinate of the middle of the
  LD L,107                ; blackboard into byte 107 of the teacher's buffer,
  LD (HL),A               ; and the same value into byte 108 (which ensures
  INC L                   ; that the routine at WALK will not relinquish
  LD (HL),A               ; control before the teacher has reached the middle
                          ; of the blackboard)
  LD BC,WALK              ; Hand over control to the routine at WALK, making
  CALL CALLSUBCMD         ; the teacher walk to the middle of the blackboard
; Control returns here when the teacher has reached the middle of the
; blackboard.
  CALL GETRANDOM          ; A=random number
  CP 160                  ; Should the teacher write on the blackboard?
  JR C,CLSNOERIC_1        ; Jump if not
  LD BC,WRITEBRD          ; Hand over control to the routine at WRITEBRD,
  CALL CALLSUBCMD         ; making the teacher write on the blackboard
; Control returns here when the teacher has finished writing on the blackboard.
  CALL GETRANDOM          ; A=random number
  CP 160                  ; Should the teacher tell the kids to write an essay?
  JR C,CLSNOERIC_1        ; Jump if not
  LD BC,M_CLASSDO3        ; M_CLASSDO3: 'WRITE AN ESSAY WITH THIS TITLE'
  CALL GOTOPAGE_3         ; Make the teacher tell the kids to write an essay
  JR CLSNOERIC_2
CLSNOERIC_1:
  CALL GOTOPAGE           ; Make the teacher tell the kids to go to a page in
                          ; their books
; Now we enter a loop that makes the teacher walk up and down next to the
; blackboard (or the map).
CLSNOERIC_2:
  LD L,98                 ; A=teacher's x-coordinate
  LD A,(HL)               ;
  XOR 3                   ; Store the x-coordinate of the location 1 or 3
  LD L,107                ; spaces behind the teacher in byte 107 of his buffer
  LD (HL),A               ;
  LD BC,WALK              ; Hand over control to the routine at WALK, making
  CALL CALLSUBCMD         ; the teacher turn round and walk 1 or 3 paces
  JR CLSNOERIC_2          ; Make the teacher turn round and walk 1 or 3 paces
                          ; again

; Unused
  DEFB 0

; Make a teacher tell the kids to sit down
;
; Used by the teachers' command lists 128, 130, 132 and 134. Makes the teacher
; tell the kids to sit down; also makes MR CREAK reveal his safe combination
; letter if he can see his year of birth written on the board.
;
; H Teacher's character number (163-166)
TELLSIT:
  CALL CREAKLTR           ; Make MR CREAK reveal his safe combination letter if
                          ; appropriate; also set BC=M_WACKERSIT (SILENCE! OR
                          ; I'LL CANE THE LOT OF YOU)
  LD A,H                  ; A=teacher's character number (163-166)
  SUB 163                 ; Is it MR WACKER?
  JR Z,TELLSIT_0          ; Jump if so
  LD C,89                 ; BC=M_ROCKITTSIT (RIGHT! SIT DOWN MY LITTLE CHERUBS)
  DEC A                   ; Is it MR ROCKITT?
  JR Z,TELLSIT_0          ; Jump if so
  LD C,123                ; BC=M_WITHITSIT (COME ON CHAPS - SETTLE DOWN)
  DEC A                   ; Is it MR WITHIT?
  JR Z,TELLSIT_0          ; Jump if so
  LD C,151                ; It must be MR CREAK; set BC=M_CREAKSIT (BE QUIET
                          ; AND SEATED YOU NASTY LITTLE BOYS)
TELLSIT_0:
  CALL GOTOPAGE_3         ; Make the teacher speak
  JP NEXTCMD              ; Move to the next command in the command list

; Make BOY WANDER write on a blackboard
;
; Used by command list 192, and also by the routine at BWWRITE.
;
; H 167 (BOY WANDER)
BWWRITECLN:
  LD BC,WRITEBRD          ; Hand over control to the routine at WRITEBRD,
  CALL CALLSUBCMD         ; making BOY WANDER write on the board (if it's
                          ; clean)
  JP NEXTCMD              ; When he's finished, move to the next command in the
                          ; command list

; Make BOY WANDER write on a blackboard if the teacher hasn't arrived yet
;
; Used by BOY WANDER's command lists 148, 156 and 164.
;
; H 167 (BOY WANDER)
BWWRITE:
  CALL GETPARAM           ; Get the event ID (9, 10 or 11) from the command
                          ; list
  CALL CHECKSIG           ; Has the teacher arrived yet?
  JR Z,BWWRITECLN         ; Jump if not
  JP NEXTCMD              ; Otherwise move to the next command in the command
                          ; list

; Make a teacher conduct a class
;
; Used by the teachers' command lists 128, 130, 132 and 134. Determines whether
; the teacher is conducting ERIC's class, and hands over to TEACHERIC if so or
; CLSNOERIC if not.
;
; H Teacher's character number (163-166)
DOCLASS:
  LD A,(LESSONDESC)       ; Pick up the lesson descriptor from LESSONDESC
  AND 240                 ; Keep only the 'teacher' bits (4-7)
  RLCA                    ; A=character number of the teacher (163-166) who is
  RLCA                    ; teaching ERIC and EINSTEIN this period
  RLCA                    ;
  RLCA                    ;
  ADD A,163               ;
  CP H                    ; Set the zero flag if this teacher is teaching ERIC
  LD L,99
  JR Z,DOCLASS_0          ; Jump if we're dealing with ERIC's teacher
  LD (HL),48              ; Replace the address of this routine in bytes 99 and
  JP CLSNOERIC            ; 100 of the teacher's buffer with CLSNOERIC, and
                          ; jump there now to start the lesson
; This teacher has the pleasure of conducting ERIC's class this period.
DOCLASS_0:
  LD (HL),0               ; Replace the address of this routine in bytes 99 and
  INC L                   ; 100 of the teacher's buffer with that of TEACHERIC
  LD (HL),243             ;
  LD A,0                  ; Clear all the lesson status flags at LFLAGS
  LD (LFLAGS),A           ;
  JP TEACHERIC            ; Start the lesson

; Unused
  DEFB 120,120,120,120

; Insert a pixel column into the top or bottom half of the speech bubble text
; window
;
; Used by the routine at SLIDECHR2. Slides the current contents of the top or
; bottom half of the speech bubble text window one pixel to the left, and
; places the new pixel column (4 pixels high) into the slot freed up on the
; right.
;
; A Pixel column byte
; HL Display file address
SLIDETXT:
  LD B,4                  ; There are four pixel rows to slide left
SLIDETXT_0:
  PUSH BC                 ; Save the pixel row counter
  LD C,L                  ; Save L in C briefly
  LD B,6                  ; The text window is six bytes wide
  RLCA                    ; Push a pixel into the carry flag
SLIDETXT_1:
  RL (HL)                 ; Slide this pixel into a row of the text window
  DEC HL                  ;
  DJNZ SLIDETXT_1         ;
  LD L,C                  ; Restore L
  POP BC                  ; Restore the pixel row counter to B
  INC H                   ; Move to the next row of pixels
  DJNZ SLIDETXT_0         ; Jump back until all four rows of pixels have been
                          ; done
  RET

; Make a character walk fast
;
; The address of this continual subcommand routine is placed into bytes 124 and
; 125 of a character's buffer by command lists 214 and 218. This routine is
; also used directly by the routines at SEEKERIC, FINDERIC and CHKTOUCH.
;
; H Character number (152-169)
RUN:
  LD L,123                ; Set the speed change delay counter in byte 123 of
  LD (HL),4               ; the character's buffer to 4, preventing any speed
                          ; change by the routine at DECIDEMV
  DEC L                   ; Reset bit 7 of byte 122 of the character's buffer,
  RES 7,(HL)              ; indicating that he is walking fast
  RET

; Make a character speak at the correct speed
;
; Used by the routine at SPEAK2. Prevents a character from speaking too fast.
;
; H Character number (152-169)
SLOW:
  LD L,123                ; Set the speed change delay counter in byte 123 of
  LD (HL),4               ; the character's buffer to 4, preventing any speed
                          ; change by the routine at DECIDEMV
  DEC L                   ; Set bit 7 of byte 122 of the character's buffer,
  SET 7,(HL)              ; indicating that he is moving slowly
  RET

; Exit demo mode
;
; Used by the routine at GETINPUT. Removes the speech bubble and exits demo
; mode.
EXITDEMO:
  CALL RMSPEECH           ; Remove the speech bubble (if present)
  JP START_2              ; Exit demo mode and offer to change the characters'
                          ; names

; Unused
;
; Probably remnants of a routine similar to RMSPEECH.
  AND A                   ; Is anyone speaking at the moment?
  CALL NZ,RMBUBBLE        ; Remove the speech bubble if so
  JP START_2              ; Exit demo mode

; Note duration and pitch data for tunes
;
; Used by the routine at PLAYTUNE.
PITCHDATA:
  DEFB 47,196             ; 47*196=9212
  DEFB 53,174             ; 53*174=9222
  DEFB 60,154             ; 60*154=9240
  DEFB 63,145             ; 63*145=9135
  DEFB 71,129             ; 71*129=9159
  DEFB 80,114             ; 80*114=9120
  DEFB 90,101             ; 90*101=9090 (unused)
  DEFB 95,96              ; 95*96=9120 (unused)

; Play a tune
;
; Used by the routines at PLAYTHEME0, PLAYTHEME, WINTUNE and WINTUNE0.
;
; HL Base address of the tune data table
PLAYTUNE:
  DI                      ; Disable interrupts
PLAYTUNE_0:
  LD A,(HL)               ; Pick up a tune datum in A
  INC HL                  ; Point HL at the next tune datum
  AND A                   ; Is the tune finished?
  JR NZ,PLAYTUNE_1        ; Jump if not
  INC A                   ; Make the border blue
  OUT (254),A             ;
  EI                      ; Re-enable interrupts
  RET
; A non-zero tune datum has been found, meaning the tune is not finished yet.
PLAYTUNE_1:
  PUSH HL
  LD B,A                  ; Copy the tune datum to B
  RRCA                    ; Should we pause briefly (bit 0 set) here?
  JR NC,PLAYTUNE_3        ; Jump if not
  LD DE,3840              ; Pause briefly (this happens for all but one note in
PLAYTUNE_2:
  DEC E                   ; the theme tune, and for all notes in the 'all
  JR NZ,PLAYTUNE_2        ; shields/safe' tune)
  DEC D                   ;
  JR NZ,PLAYTUNE_2        ;
PLAYTUNE_3:
  AND 7                   ; 0<=A<=7
  LD C,A                  ; C=border colour for this note
  ADD A,A                 ; Point HL at the appropriate entry in the note
  ADD A,7                 ; pitch/duration data table at PITCHDATA
  LD L,A                  ;
  LD H,126                ;
  LD A,B                  ; A=original tune datum
  RLCA                    ; Obtain the note frequency parameter in E and the
  RLCA                    ; note duration parameter in HL
  RLCA                    ;
  RLCA                    ;
  AND 15                  ;
  LD B,A                  ;
  LD E,(HL)               ;
  INC L                   ;
  LD A,(HL)               ;
  LD HL,0                 ;
PLAYTUNE_4:
  ADD HL,DE               ;
  DJNZ PLAYTUNE_4         ;
  RR H                    ;
  RR L                    ;
  LD E,A                  ;
PLAYTUNE_5:
  LD A,C                  ; Produce a note
  OUT (254),A             ;
  XOR 16                  ;
  LD C,A                  ;
  LD B,E                  ;
PLAYTUNE_6:
  DJNZ PLAYTUNE_6         ;
  DEC HL                  ;
  LD A,H                  ;
  OR L                    ;
  JR NZ,PLAYTUNE_5        ;
  POP HL                  ; Restore the pointer to the tune data table
  JP PLAYTUNE_0           ; Pick up the next tune datum

; Theme tune data
;
; Used by the routines at PLAYTHEME0 and PLAYTHEME.
THEMETUNE:
  DEFB 121,53,119,51,121,53,113
  DEFB 49,115,53,70,53,51,121,53,177
  DEFB 121,53,55,55,51,121,53,177
  DEFB 51,51,53,55,53,51,121,53,177
  DEFB 0                  ; End marker

; Unused
  DEFS 3

; Play the theme tune for the first time
;
; Used by the routine at START just after the game has loaded.
PLAYTHEME0:
  LD HL,THEMETUNE         ; The theme tune data is at THEMETUNE
  CALL PLAYTUNE           ; Play the theme tune
  LD HL,0                 ; The high score is 0 when the game has just loaded
  RET

; Play the theme tune
;
; Used by the routine at NEXTPHASE.
PLAYTHEME:
  LD HL,THEMETUNE         ; The theme tune data is at THEMETUNE
  JP PLAYTUNE             ; Play the theme tune

; Play the 'Got all shields/opened safe' tune
;
; Used by the routine at NEXTPHASE.
WINTUNE:
  CALL WINTUNE0           ; Play the first part of the tune
  CALL WINTUNE0           ; Play the first part of the tune again
  LD HL,WINTUNEDATA2      ; The data for the second part of the tune is at
                          ; WINTUNEDATA2
  JP PLAYTUNE             ; Play the second part of the tune

; Play the first part of the 'Got all shields/opened safe' tune
;
; Used by the routine at WINTUNE.
WINTUNE0:
  LD HL,WINTUNEDATA1      ; The data for the first part of the tune is at
                          ; WINTUNEDATA1
  CALL PLAYTUNE           ; Play the first part of the tune
  LD DE,15360             ; Pause briefly before returning
WINTUNE0_0:
  DEC E                   ;
  JR NZ,WINTUNE0_0        ;
  DEC D                   ;
  JR NZ,WINTUNE0_0        ;
  RET

; Data for the first part of the 'Got all shields/opened safe' tune
;
; Used by the routine at WINTUNE0. The data for the second part of the tune can
; be found at WINTUNEDATA2.
WINTUNEDATA1:
  DEFB 55,55,55,57,123,121
  DEFB 55,59,57,57,183
  DEFB 0                  ; End marker

; Data for the second part of the 'Got all shields/opened safe' tune
;
; Used by the routine at WINTUNE. The data for the first part of the tune can
; be found at WINTUNEDATA1.
WINTUNEDATA2:
  DEFB 57,57,57,57,115,115
  DEFB 57,55,53,51,241
  DEFB 55,55,55,57,123,121
  DEFB 55,59,57,57,247
  DEFB 0                  ; End marker

; Unused
  DEFB 0

; Remove the speech bubble if present
;
; Used by the routines at START and EXITDEMO. Returns with the high score in
; HL.
RMSPEECH:
  LD A,(32613)            ; A=x-coordinate of the speech bubble lip (if
                          ; present)
  AND A                   ; Is anyone speaking at the moment?
  CALL NZ,RMBUBBLE        ; Remove the speech bubble if so
  CALL UPDATESCR          ; Update the display
  LD HL,(HISCORE)         ; Collect the high score from HISCORE
  RET

; Save Skool Daze to tape
;
; This routine was used to save the high-speed block to tape.
SD2TAPE:
  LD HL,24288             ; 24288 is where the game starts after loading
  LD (23833),HL           ; Store this address where it will be popped off the
                          ; stack by the load routine
  LD BC,23                ; This parameter is used by the save routine
  EXX
  JP 33024                ; Jump into the save routine

; Unused
  DEFB 120,120,120,120,120,120,120,120,120,120,120,120,120,120,120

  ORG 32512

; x-coordinate of the leftmost column of the skool on screen
;
; Modified by the routines at RSCROLL and LSCROLL.
LEFTCOL:
  DEFB 0

; UDG back buffer
;
; Used by the routine at PRINTTILE.
BACKBUF:
  DEFS 8

; Number of on-screen character squares that need refreshing (unused)
;
; Used by the (unused) routine at XUPDATE.
XTILES:
  DEFB 0

; Unused
XTILELOCS:
  DEFS 2

; Screen refresh buffer (SRB)
;
; Used by the routines at SRBCOL, UPDATESCR, SRBXY, SPEECHSRB and BUBBLESRB.
; Each byte of the SRB corresponds to a segment of 8 character squares on the
; screen, the leftmost of which will be at x=0, x=8, x=16 or x=24. Each bit set
; in an SRB byte corresponds to a character square in the segment that needs
; refreshing. Bit 0 corresponds to the leftmost character square in the
; segment, bit 7 to the rightmost character square.
SRB:
  DEFS 84

; Unused
UGSB1:
  DEFS 4

; Speech bubble lip coordinates
;
; Used by the routines at SPKWAIT and RMBUBBLE.
LIPCOORDS:
  DEFS 2

; UDG references and attribute bytes of the part of the skool overwritten by
; the speech bubble and lip
;
; Used by the routines at SPKWAIT and RMBUBBLE.
ULIPUDG:
  DEFB 0                  ; The UDG reference of the skool tile under the
                          ; speech bubble lip is stored here
ULIPATTR:
  DEFB 0                  ; The attribute byte of the skool tile under the
                          ; speech bubble lip is stored here
UTOPUDGS:
  DEFS 8                  ; The UDG references of the skool tiles under the top
                          ; row of the speech bubble are stored here
UBOTUDGS:
  DEFS 8                  ; The UDG references of the skool tiles under the
                          ; bottom row of the speech bubble are stored here
UTOPATTR:
  DEFS 8                  ; The attribute bytes of the skool tiles under the
                          ; top row of the speech bubble are stored here
UBOTATTR:
  DEFS 8                  ; The attribute bytes of the skool tiles under the
                          ; bottom row of the speech bubble are stored here

; Unused
UGSB2:
  DEFS 4

; Workspace used to find the main kid closest to a teacher
;
; Used by the routine at CHKKIDS.
KIDDIST:
  DEFS 2                  ; ERIC's normalised coordinates are calculated and
                          ; stored here
KIDDISTBW:
  DEFS 2                  ; BOY WANDER's coordinates are stored here
KIDDISTA:
  DEFS 2                  ; ANGELFACE's coordinates are stored here
KIDDISTE:
  DEFS 2                  ; EINSTEIN's coordinates are stored here

; Unused
UGSB3:
  DEFS 2

; Safe combination letter message
;
; Used by the routine at LINESLTR.
LTRREVEAL:
  DEFB 0                  ; The ASCII code of the safe combination letter to be
                          ; revealed is placed here
  DEFB 0                  ; End marker

; Unused
UGSB4:
  DEFS 2

; Identifier for CREAK's birth year battle
;
; Used by the routines at GENCOMBO and BIRTHQ.
BATTLEID:
  DEFB 0

; CREAK's year of birth
;
; Used by the routines at CREAKLTR and GENCOMBO. Holds a copy of the
; battle-year message corresponding to CREAK's birth.
CREAKYEAR:
  DEFS 4

; Safe combination code
;
; Used by the routines at CHECKJP2 and GENCOMBO. Holds the ASCII codes of the
; letters in the safe combination (in the right order). The teachers' safe
; combination letters are stored at TEACHLTRS.
SAFECOMBO:
  DEFS 4

; Teachers' safe combination letters
;
; Used by the routines at GENCOMBO and LINESLTR. Holds the ASCII codes of the
; teachers' safe combination letters. The safe combination itself is stored at
; SAFECOMBO.
TEACHLTRS:
  DEFB 0                  ; MR WACKER's safe combination letter is stored here
  DEFB 0                  ; MR ROCKITT's safe combination letter is stored here
  DEFB 0                  ; MR WITHIT's safe combination letter is stored here
  DEFB 0                  ; MR CREAK's safe combination letter is stored here

; Unused
UGSB5:
  DEFB 0

; Reading Room blackboard contents buffer
;
; Used by the routine at CHECKJP2.
RRBRDBUF:
  DEFS 4                  ; Characters written by ERIC (see WRITING)
RRBRDPOS:
  DEFB 0                  ; Pixel coordinates for ERIC to write at (set to 1 by
                          ; WRITE)

; Unused
UGSB6:
  DEFB 0

; White Room blackboard contents buffer
;
; Used by the routine at CHECKJP2.
WRBRDBUF:
  DEFS 4                  ; Characters written by ERIC (see WRITING)
WRBRDPOS:
  DEFB 0                  ; Pixel coordinates for ERIC to write at (set to 1 by
                          ; WRITE)

; Unused
UGSB7:
  DEFB 0

; Exam Room blackboard contents buffer
;
; Used by the routine at CHECKJP2.
ERBRDBUF:
  DEFS 4                  ; Characters written by ERIC (see WRITING)
ERBRDPOS:
  DEFB 0                  ; Pixel coordinates for ERIC to write at (set to 1 by
                          ; WRITE)

; Unused
UGSB8:
  DEFS 8

; ERIC's stand-up delay counter
;
; Used by the routine at GETINPUT while in demo mode.
UPDELAY:
  DEFB 0

; Hi-score
;
; Used by the routines at PRINTB3L and RMSPEECH.
HISCORE:
  DEFW 0

; Score
;
; Used by the routine at ADD10.
SCORE:
  DEFW 0

; Lines total
;
; Used by the routines at GIVELINES and CHKLINES.
LINES:
  DEFW 0

; Lesson status flags
;
; Various status flags for the current lesson.
;
; +--------+-----------------------------------------------------------------+
; | Bit(s) | Meaning                                                         |
; +--------+-----------------------------------------------------------------+
; | 0      | End of lesson/playtime (always reset)                           |
; | 1-3    | Unused                                                          |
; | 4      | Next lines reprimand for the teacher fetching the truant ERIC   |
; |        | (toggled by CHKERIC)                                            |
; | 5      | Set after a teacher has found the truant ERIC (see CHKERIC)     |
; | 6      | Set after a classroom lesson has started with ERIC present (see |
; |        | TEACHERIC) (checked by CHKERIC; reset by NEWLESSON)             |
; | 7      | Set after EINSTEIN has had a chance to grass on the absent ERIC |
; |        | (see TEACHERIC), or after dinner has started (see DINDUTY)      |
; |        | (checked by CHKERIC; reset by NEWLESSON)                        |
; +--------+-----------------------------------------------------------------+
LFLAGS:
  DEFB 0

; Lesson signal flags
;
; Bits 0-3 are used by the classroom-based command lists to indicate that or
; check whether a teacher has arrived at the classroom door to start the
; lesson.
;
; +--------+---------------------------------------------------------------+
; | Bit(s) | Meaning if set                                                |
; +--------+---------------------------------------------------------------+
; | 0      | Teacher has arrived at the Map Room                           |
; | 1      | Teacher has arrived at the Reading Room                       |
; | 2      | Teacher has arrived at the Exam Room                          |
; | 3      | Teacher has arrived at the White Room                         |
; | 4-6    | Unused                                                        |
; | 7      | EINSTEIN is speaking (set by SWOTSPK; checked by GRASSETC and |
; |        | SWOTSPK2; reset by GRASS2)                                    |
; +--------+---------------------------------------------------------------+
LSIGS:
  DEFB 0

; Stampede signal flags
;
; These flags are set and reset by command list 204, and checked by command
; list 206.
;
; +--------+---------------------------------------------+
; | Bit(s) | Meaning if set                              |
; +--------+---------------------------------------------+
; | 0      | Little boy no. 1 is ready to stampede       |
; | 1      | Little boy no. 1 is ready to stampede again |
; | 2-7    | Unused                                      |
; +--------+---------------------------------------------+
STAMPEDESIGS:
  DEFB 0

; Special playtime signal flags
;
; These flags are used during the special playtimes (when EINSTEIN is going to
; tell MR WACKER what ERIC's up to, BOY WANDER has hidden a pea-shooter on the
; fire escape, or ANGELFACE has mumps).
;
; +--------+------------------------------------------------------------------+
; | Bit(s) | Meaning if set                                                   |
; +--------+------------------------------------------------------------------+
; | 0      | Little boy no. 10 is waiting for ERIC to respond to the message  |
; |        | just delivered (set and reset by TELLERIC; checked by LISTEN)    |
; | 1      | Unused                                                           |
; | 2      | EINSTEIN is ready (set by command list 212; checked by command   |
; |        | list 208)                                                        |
; |        | BOY WANDER is ready (set and reset by command list 216; checked  |
; |        | by command lists 208 and 218)                                    |
; | 3      | MR WACKER is ready (set by command lists 214 and 218; checked by |
; |        | command list 208)                                                |
; | 4      | Little boy no. 10 has told ERIC about EINSTEIN (set by command   |
; |        | list 208; checked by command list 212)                           |
; |        | Little boy no. 10 has told ERIC about BOY WANDER (set by command |
; |        | list 208; checked by command list 216)                           |
; |        | Little boy no. 10 has told ERIC about ANGELFACE (set by command  |
; |        | list 210; checked by command list 220)                           |
; | 5      | EINSTEIN has grassed on ERIC (set by command list 212; checked   |
; |        | by command list 214)                                             |
; |        | ERIC has mumps (set by CHKTOUCH; checked by command list 222)    |
; | 6-7    | Unused                                                           |
; +--------+------------------------------------------------------------------+
SPECIALSIGS:
  DEFB 0

; Unused
UGSB9:
  DEFS 14

; Birth year question indicator
;
; Used by the routine at BIRTHQ, and reset at the start of each lesson by the
; routine at NEWLESSON. Holds 0 if MR CREAK has not yet asked the birth year
; question during the current lesson, or 1 otherwise.
ASKED:
  DEFB 0

; Unused
UGSB10:
  DEFS 13

; ERIC's main action timer
;
; Decremented by the main loop at MAINLOOP; when it becomes zero, the keyboard
; is checked and ERIC is moved accordingly (see also MVERIC2, MVERIC1, TURNERIC
; and START).
ERICTIMER:
  DEFB 0

; ERIC's midstride/mid-action timer
;
; Zero unless ERIC is midstride or has his arm raised while writing on a board
; (see MVERIC1); if non-zero, the value is copied to the main action timer at
; ERICTIMER after ERIC has lowered his arm or moved from the midstride position
; (see MVERIC2).
ERICTIMER2:
  DEFB 0

; Game mode indicator
;
; Used by the routine at CHKSHIELD.
;
; +-------+---------------------------------------------------------------+
; | Value | Meaning                                                       |
; +-------+---------------------------------------------------------------+
; | 0     | Demo mode                                                     |
; | 1     | Shields need to be flashed                                    |
; | 2     | All shields flashing; ERIC needs to find the safe combination |
; | 3     | Shields need to be unflashed                                  |
; +-------+---------------------------------------------------------------+
GAMEMODE:
  DEFB 0

; Shield counter
;
; Used by the routine at CHKSHIELD.
SHIELDCNT:
  DEFB 0

; Next clean pixel column on the Reading Room blackboard
;
; Used by the routines at WRITECHR, WIPE2 and CHECKJP2.
RRBRDPXL:
  DEFB 0

; Number of the character who last wrote on the Reading Room blackboard
;
; Used by the routines at WIPE2, WRITEBRD, WRITE and NRBOARD. Holds 0 if the
; board is clean.
RRBRDAUTH:
  DEFB 0

; Next clean pixel column on the White Room blackboard
;
; Used by the routines at WRITECHR, WIPE2 and CHECKJP2.
WRBRDPXL:
  DEFB 0

; Number of the character who last wrote on the White Room blackboard
;
; Used by the routines at WIPE2, WRITEBRD, WRITE and NRBOARD. Holds 0 if the
; board is clean.
WRBRDAUTH:
  DEFB 0

; Next clean pixel column on the Exam Room blackboard
;
; Used by the routines at WRITECHR, WIPE2 and CHECKJP2.
ERBRDPXL:
  DEFB 0

; Number of the character who last wrote on the Exam Room blackboard
;
; Used by the routines at WIPE2, WRITEBRD and WRITE. Holds 0 if the board is
; clean.
ERBRDAUTH:
  DEFB 0

; Lines-giving delay counter
;
; Used by the routine at CHKERIC.
LINESDELAY:
  DEFB 0

; Character number of the teacher who last gave ERIC lines
;
; Used by the routine at CHKERIC.
LASTLINES:
  DEFB 0

; Lesson descriptor
;
; Used by the routines at NEWLESSON, CHKERIC, PRTLESSON, PRESENT and DOCLASS.
; The room ID is stored in bits 0-3, and the teacher ID in bits 4-7. See the
; list of lesson descriptors at LDESCS.
LESSONDESC:
  DEFB 0

; ERIC's knockout delay counter
;
; Used by the routine at ERICHIT.
KODELAY:
  DEFB 0

; Action timer for ERIC
;
; Used by the routines at FIRING, HITTING and JUMPING to coordinate ERIC's
; movements while firing his catapult, hitting, or jumping.
ACTIONCNT:
  DEFB 0

; Lesson clock
;
; Decremented in the main loop at MAINLOOP. When it reaches 0, it is reset to
; 5376 by the routine at NEWLESSON. It is also checked by the routines at
; CHKERIC and CHKTIME, and modified by the routines at START, LINES2K,
; TELLERIC, NEXTPHASE, CHKLINES and PRECHASE.
CLOCK:
  DEFW 0

; Current lesson number
;
; Used by the routines at NEWLESSON and RESTART. Holds the index (0-63) into
; the main timetable. It is adjusted by the routine at START before starting a
; new game.
LESSONNO:
  DEFB 0

; Last value seen in the system variable FRAMES
;
; Stores the value of the LSB of the system variable FRAMES as it stood at the
; end of the last pass through the main loop (see MAINLOOP).
LFRAMES:
  DEFB 0

; ERIC's status flags
;
; If ERIC is doing anything other than walking or standing still, one of the
; bits here will be set.
;
; +-----+---------------------------------------+---------+-----------------+
; | Bit | Meaning if set                        | Handler | Checked by      |
; +-----+---------------------------------------+---------+-----------------+
; | 0   | ERIC is firing the catapult           | FIRING  | MOVEERIC        |
; | 1   | ERIC is hitting                       | HITTING | MOVEERIC        |
; | 2   | ERIC is jumping                       | JUMPING | MOVEERIC,       |
; |     |                                       |         | CHKERIC,        |
; |     |                                       |         | CHKKIDS,        |
; |     |                                       |         | ERICLOC         |
; | 3   | ERIC is being spoken to by little boy | LISTEN  | MOVEERIC        |
; |     | no. 10                                |         |                 |
; | 4   | ERIC has just been knocked down or    | ERICHIT | MOVEERIC        |
; |     | unseated                              |         |                 |
; | 5   | ERIC is writing on a blackboard       | WRITING | MAINLOOP        |
; | 6   | Unused (always reset)                 |         | DOWN, UP, LEFT  |
; | 7   | ERIC is sitting or lying down (set by |         | STANDING, FIRE, |
; |     | ERICHIT or SITSTAND)                  |         | GETINPUT, WRITE |
; +-----+---------------------------------------+---------+-----------------+
STATUS:
  DEFB 0

; Last key pressed
;
; Holds the ASCII code of the last key pressed. Set by the routines at MAINLOOP
; and WRITE; checked by the routines at MVERIC1 and LEFT.
KEYCODE:
  DEFB 0

; Number of the character just moved (152-171)
;
; Used by the routine at MVCHARS.
LASTCHAR:
  DEFB 0

; Random number seed
;
; Used by the routine at GETRANDOM.
RANDSEED:
  DEFW 512                ; This is the initial value of the seed

  ORG 32768

; Skool graphic data for columns 0-31 (tiles 0-255)
;
; Used by the routine at PRINTTILE. Tiles 220-224, 231 and 232 are unused and
; blank (all 255s), tiles 225-230 and 233-238 are used by the White Room
; blackboard, and tiles 239-255 are used for the speech bubble.
  DEFB 0,255,170,255,170,171,171,170,170,128,129,128,128,255,170,170
  DEFB 176,176,255,170,0,255,255,170,160,1,255,1,1,255,170,67
  DEFB 67,170,194,255,1,255,170,0,128,128,135,255,255,170,15,0
  DEFB 175,170,255,170,255,255,170,178,102,0,15,0,170,234,234,170
  DEFB 255,255,255,170,188,218,192,192,192,192,255,255,128,255,170,170
  DEFB 167,3,3,3,7,0,255,0,26,0,235,0,168,31,64,0
  DEFB 255,170,0,255,0,255,0,255,170,234,255,175,224,20,187,255
  DEFB 170,0,1,255,255,170,234,129,129,255,171,171,170,1,255,255
  DEFB 171,170,85,255,96,255,161,160,255,84,213,255,170,255,192,224
  DEFB 86,85,170,255,170,255,252,1,0,84,170,255,255,0,216,1
  DEFB 0,176,170,255,191,191,191,255,0,191,0,160,170,170,255,255
  DEFB 255,0,192,175,255,255,255,255,255,255,170,170,240,255,255,234
  DEFB 234,239,255,255,175,171,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,0,95,255,255,255,96,192,255,255,127,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,129
  DEFB 14,126,126,126,126,126,126,112,64,0,0,0,0,0,0,2
  DEFB 0,223,213,255,213,213,213,219,214,128,254,128,128,255,85,85
  DEFB 80,80,255,85,0,0,85,85,127,1,1,1,1,255,85,67
  DEFB 67,85,103,255,1,113,101,192,128,129,191,255,95,85,1,0
  DEFB 85,255,255,85,128,128,213,255,85,0,240,0,245,245,85,255
  DEFB 255,3,3,87,254,95,192,192,192,192,0,0,127,255,245,95
  DEFB 87,3,3,3,63,126,0,255,21,227,255,255,88,32,255,126
  DEFB 255,213,126,0,255,16,255,255,85,213,1,85,24,250,248,255
  DEFB 213,0,1,0,255,85,213,129,128,255,85,85,85,0,255,255
  DEFB 85,95,170,255,224,225,96,96,255,170,170,255,213,255,188,128
  DEFB 170,170,85,255,85,255,84,1,0,191,85,127,255,0,168,255
  DEFB 240,80,85,128,128,128,128,255,0,255,0,96,85,85,0,0
  DEFB 0,0,64,95,0,0,0,0,0,0,85,85,0,0,0,213
  DEFB 213,255,0,0,127,87,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,255,95,0,0,0,160,254,0,0,65,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,129
  DEFB 49,129,129,129,129,129,129,140,64,0,0,0,0,0,0,2
  DEFB 0,171,170,171,170,171,170,170,170,128,128,128,128,255,170,170
  DEFB 176,176,255,170,0,0,170,174,0,1,1,1,3,255,170,67
  DEFB 67,191,168,255,1,162,160,192,128,190,255,255,171,170,0,0
  DEFB 6,14,255,170,128,128,170,0,103,0,0,0,234,234,170,3
  DEFB 255,3,3,174,15,222,192,192,192,193,255,255,170,255,26,248
  DEFB 255,3,3,3,255,30,255,0,26,20,255,0,168,64,0,114
  DEFB 175,170,94,255,0,16,0,255,170,234,1,170,20,116,251,255
  DEFB 170,0,1,127,255,170,234,129,128,255,171,170,171,0,255,191
  DEFB 171,255,85,255,112,225,160,160,213,85,85,175,250,255,84,128
  DEFB 86,87,170,255,170,255,172,0,0,193,170,175,255,0,88,255
  DEFB 240,255,170,191,191,191,128,255,0,255,224,255,170,170,255,255
  DEFB 255,192,254,255,255,255,255,255,255,255,170,170,0,255,255,235
  DEFB 234,255,255,255,255,175,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,95,255,255,255,160,254,255,255,73,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,129
  DEFB 64,0,0,0,0,0,0,2,64,0,0,0,0,0,0,2
  DEFB 0,213,213,213,213,213,213,217,213,128,128,128,128,127,85,93
  DEFB 94,80,87,85,0,0,5,85,0,1,1,15,15,255,85,67
  DEFB 67,71,104,255,1,101,96,215,128,192,255,255,85,85,0,0
  DEFB 253,21,255,85,128,128,223,255,119,0,0,0,245,245,85,85
  DEFB 255,3,3,244,220,223,192,192,192,207,255,191,85,255,93,15
  DEFB 255,3,3,3,255,78,174,0,21,116,255,0,88,64,0,114
  DEFB 213,245,78,254,0,16,0,255,85,213,1,85,26,122,246,95
  DEFB 213,0,2,255,255,85,213,129,128,255,85,85,127,0,255,127
  DEFB 85,254,170,255,174,97,96,96,170,170,170,215,255,255,172,128
  DEFB 191,188,85,255,85,255,84,15,252,15,87,87,255,0,168,255
  DEFB 80,129,85,191,191,191,128,255,0,255,224,3,87,85,255,85
  DEFB 255,192,30,255,255,87,255,255,255,127,85,85,0,255,175,223
  DEFB 213,255,237,255,255,95,254,255,170,117,191,250,238,59,251,189
  DEFB 255,255,255,96,95,255,238,160,0,117,255,85,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,66
  DEFB 64,0,0,0,0,0,0,2,128,0,0,0,0,0,0,1
  DEFB 0,170,170,170,170,171,160,169,171,128,128,131,128,175,170,179
  DEFB 177,176,170,170,0,0,242,170,11,1,1,241,127,255,234,227
  DEFB 67,194,167,175,1,170,160,191,128,128,255,255,170,170,0,0
  DEFB 174,255,255,170,128,128,178,7,119,0,0,0,234,234,170,255
  DEFB 255,3,3,236,210,255,192,193,192,255,190,170,255,255,170,11
  DEFB 255,3,255,3,255,102,174,252,26,247,255,0,168,64,255,78
  DEFB 170,254,70,202,0,16,255,255,170,234,1,170,20,244,246,171
  DEFB 170,0,2,255,255,170,239,129,128,192,171,170,255,0,0,175
  DEFB 170,245,85,0,86,161,160,160,85,95,85,170,255,0,84,143
  DEFB 192,224,170,255,234,3,172,255,252,122,188,170,255,0,88,255
  DEFB 176,14,170,191,165,186,128,255,0,255,160,30,188,171,255,85
  DEFB 171,192,106,255,255,85,213,255,39,84,170,170,0,255,170,255
  DEFB 234,255,197,170,255,255,170,157,170,85,165,74,170,49,170,165
  DEFB 255,64,255,127,85,255,85,192,254,84,187,73,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,36
  DEFB 128,0,0,0,0,0,0,1,128,0,0,0,0,0,0,1
  DEFB 0,213,213,213,213,213,215,213,213,128,128,252,131,85,85,80
  DEFB 80,95,85,85,0,0,93,87,127,1,1,1,255,255,93,95
  DEFB 67,65,96,85,1,101,96,191,128,128,255,255,85,85,0,0
  DEFB 93,5,95,85,128,128,209,5,191,0,0,3,245,245,85,221
  DEFB 255,3,3,116,83,255,192,254,192,255,210,170,128,255,93,95
  DEFB 255,3,3,31,255,114,174,23,21,244,255,248,88,64,106,78
  DEFB 213,255,66,202,0,16,170,95,213,213,1,85,26,250,247,85
  DEFB 213,0,2,255,127,85,255,129,128,126,85,85,255,0,0,95
  DEFB 85,255,171,0,170,97,96,96,255,240,175,213,255,0,172,255
  DEFB 63,3,85,127,213,0,84,255,172,213,224,85,7,0,175,255
  DEFB 80,245,85,167,165,186,128,255,248,255,96,245,97,95,128,85
  DEFB 171,64,214,255,11,85,181,85,147,84,85,85,0,122,170,255
  DEFB 213,255,141,170,255,255,170,205,170,85,165,74,170,56,171,165
  DEFB 128,32,252,128,85,0,171,15,1,86,171,65,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,36
  DEFB 128,0,0,0,0,0,0,1,128,0,0,0,0,0,0,1
  DEFB 0,170,170,170,171,171,174,173,191,128,128,128,159,170,170,176
  DEFB 176,170,170,255,0,0,170,171,127,1,1,1,255,191,67,67
  DEFB 255,194,160,170,1,170,255,223,128,128,255,255,170,170,0,0
  DEFB 174,6,170,170,128,255,178,246,255,127,0,31,234,234,170,171
  DEFB 191,3,255,172,210,255,192,192,195,255,234,170,128,255,170,171
  DEFB 255,3,3,227,255,60,170,26,26,148,255,168,168,64,85,44
  DEFB 170,255,36,202,0,16,85,171,234,234,1,170,20,87,255,170
  DEFB 250,1,254,255,191,170,255,129,128,171,171,170,255,0,240,171
  DEFB 170,255,126,0,86,161,160,160,255,3,112,170,255,0,84,255
  DEFB 213,30,175,175,234,0,175,255,84,170,15,170,0,248,112,255
  DEFB 176,170,175,184,165,186,128,63,24,255,160,170,142,191,255,85
  DEFB 171,192,170,255,215,85,245,85,201,84,170,175,0,117,170,255
  DEFB 235,255,29,170,255,255,170,229,170,85,165,74,170,60,171,165
  DEFB 64,145,240,255,85,0,87,16,255,86,171,65,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20
  DEFB 128,0,0,0,0,0,0,1,129,129,129,129,129,129,129,129
  DEFB 0,213,213,213,213,213,219,213,255,128,128,128,255,85,85,80
  DEFB 80,85,85,255,255,0,89,212,255,1,1,1,255,87,67,67
  DEFB 87,67,80,85,255,101,0,255,255,128,255,255,85,117,0,240
  DEFB 87,5,85,255,255,255,209,245,255,128,0,255,245,245,85,221
  DEFB 85,255,255,116,83,255,192,192,252,255,246,170,128,127,93,95
  DEFB 255,3,3,3,255,24,170,21,21,20,255,88,88,64,106,24
  DEFB 213,255,24,202,0,255,170,85,213,213,255,85,26,252,255,85
  DEFB 255,1,0,255,87,213,255,129,128,85,85,85,255,0,95,87
  DEFB 85,255,192,224,170,97,96,96,255,126,128,213,255,0,172,255
  DEFB 170,117,240,87,213,0,224,255,172,85,117,85,0,56,128,255
  DEFB 80,85,120,167,165,186,128,1,248,255,96,85,117,255,192,85
  DEFB 171,64,86,255,111,85,117,85,236,84,253,127,0,107,170,255
  DEFB 215,255,189,170,255,255,170,245,170,85,165,74,170,62,171,165
  DEFB 128,202,160,255,85,0,175,63,255,87,171,127,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,8
  DEFB 64,0,0,0,0,0,0,2,126,126,126,126,126,126,126,126

; Skool graphic data for columns 32-63 (tiles 0-255)
;
; Used by the routine at PRINTTILE. Tiles 121-190, 225-230 and 233-238 are
; unused and blank (all 255s); tiles 191-206 are used by the Reading Room
; blackboard; tiles 207-222 are used by the Exam Room blackboard; tiles 223,
; 224, 231 and 232 are used by the White Room blackboard; and tiles 239-255 are
; used for the speech bubble.
  DEFB 0,255,128,176,96,192,213,213,255,64,95,85,85,255,0,255
  DEFB 191,191,191,0,128,255,255,255,128,128,0,252,255,1,129,1
  DEFB 0,0,160,255,255,0,0,255,234,234,255,253,255,255,255,160
  DEFB 191,0,127,84,212,0,0,255,2,65,127,0,5,0,128,6
  DEFB 224,0,255,255,0,1,0,0,3,255,8,2,254,255,0,239
  DEFB 239,255,3,255,0,255,255,128,8,239,11,3,7,0,3,253
  DEFB 255,66,2,0,191,25,240,224,0,0,1,0,8,15,247,248
  DEFB 0,0,0,63,3,31,221,254,1,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,129
  DEFB 14,126,126,126,126,126,126,112,64,0,0,0,0,0,0,2
  DEFB 0,255,192,208,160,254,170,170,128,255,95,170,170,1,0,128
  DEFB 128,128,128,0,128,0,0,0,129,0,0,252,0,1,1,0
  DEFB 0,0,175,0,0,0,255,223,213,213,251,250,3,0,255,96
  DEFB 127,0,191,172,236,0,126,255,2,255,127,0,245,255,128,133
  DEFB 250,0,135,23,0,255,126,0,135,0,12,1,252,0,0,239
  DEFB 239,251,3,0,0,255,0,255,8,239,7,3,7,0,2,253
  DEFB 0,225,1,255,191,243,192,224,126,0,1,0,4,255,247,248
  DEFB 1,128,0,63,1,255,157,254,129,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,129
  DEFB 49,129,129,129,129,129,129,140,64,0,0,0,0,0,0,2
  DEFB 0,255,160,176,160,254,213,212,128,255,95,85,84,1,0,191
  DEFB 191,191,128,0,128,0,255,255,130,0,1,255,255,3,1,0
  DEFB 255,191,175,255,255,255,255,239,234,234,253,253,1,255,255,160
  DEFB 255,251,127,212,212,239,30,254,2,255,127,253,245,255,128,133
  DEFB 250,0,139,23,0,255,94,0,139,31,4,1,248,255,0,239
  DEFB 239,243,3,248,0,255,0,64,4,239,15,3,7,0,2,253
  DEFB 64,33,0,191,191,248,128,192,114,0,1,0,132,247,247,240
  DEFB 3,64,13,63,1,255,138,254,65,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,129
  DEFB 64,0,0,0,0,0,0,2,64,0,0,0,0,0,0,2
  DEFB 0,255,208,208,160,0,170,168,128,255,96,170,168,1,0,175
  DEFB 181,170,128,0,128,0,255,174,132,0,2,255,238,5,1,0
  DEFB 0,191,239,191,255,255,255,215,213,213,251,250,0,254,255,96
  DEFB 255,251,191,236,236,239,78,254,2,255,0,253,247,255,128,133
  DEFB 6,128,139,23,0,255,78,0,139,16,4,1,240,0,0,224
  DEFB 255,227,3,4,0,0,0,64,4,15,15,9,3,0,2,254
  DEFB 96,16,0,191,255,240,136,192,114,0,1,128,132,247,255,240
  DEFB 1,32,25,158,0,255,201,252,33,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,66
  DEFB 64,0,0,0,0,0,0,2,128,0,0,0,0,0,0,1
  DEFB 0,255,176,176,192,254,213,208,128,255,127,85,80,1,0,170
  DEFB 181,170,128,0,128,0,64,174,136,0,2,255,85,9,1,0
  DEFB 0,192,239,170,157,0,255,235,234,235,253,253,0,202,255,160
  DEFB 255,0,95,212,212,0,102,254,2,255,127,1,247,254,128,133
  DEFB 254,128,11,23,0,255,70,0,11,16,4,1,240,0,0,239
  DEFB 255,195,3,4,0,255,0,32,2,239,63,0,3,0,2,255
  DEFB 80,8,0,191,255,240,128,128,78,0,1,64,68,247,255,224
  DEFB 2,16,51,24,0,255,101,124,17,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,36
  DEFB 128,0,0,0,0,0,0,1,128,0,0,0,0,0,0,1
  DEFB 0,255,208,208,15,1,170,160,128,252,128,170,160,1,0,170
  DEFB 181,170,128,0,128,0,32,174,144,0,2,255,171,17,1,0
  DEFB 0,192,239,170,205,0,255,213,213,215,250,250,0,202,255,96
  DEFB 255,0,191,236,236,0,114,254,2,252,128,1,247,240,128,125
  DEFB 1,128,11,23,0,252,66,0,11,8,2,1,224,0,0,239
  DEFB 255,131,3,2,0,255,0,32,2,239,63,0,3,0,2,255
  DEFB 72,8,0,128,255,252,68,64,78,0,1,32,36,7,255,68
  DEFB 0,8,127,16,0,255,2,120,9,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,36
  DEFB 128,0,0,0,0,0,0,1,128,0,0,0,0,0,0,1
  DEFB 0,255,176,176,16,255,213,192,128,240,255,85,64,1,0,170
  DEFB 181,170,128,0,128,0,145,170,160,0,4,255,87,33,1,0
  DEFB 0,128,255,170,229,0,255,234,234,239,253,253,0,202,255,160
  DEFB 255,0,95,212,212,0,60,254,2,240,255,1,255,128,128,33
  DEFB 255,128,11,23,0,240,36,0,11,8,2,1,192,0,0,239
  DEFB 255,3,3,2,0,255,0,16,1,239,63,0,6,0,2,255
  DEFB 72,4,0,191,255,248,200,0,44,0,1,32,36,247,255,12
  DEFB 0,4,127,8,0,255,0,56,5,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,20
  DEFB 128,0,0,0,0,0,0,1,129,129,129,129,129,129,129,129
  DEFB 0,255,208,208,63,255,170,128,255,160,255,170,128,255,224,170
  DEFB 181,170,128,63,128,0,202,170,192,0,4,255,175,65,1,0
  DEFB 0,239,255,170,245,255,255,213,213,223,250,254,255,202,127,96
  DEFB 255,255,175,236,236,255,24,254,2,128,255,253,255,128,127,31
  DEFB 255,128,127,251,254,128,24,7,127,8,2,1,128,0,239,239
  DEFB 255,3,3,1,255,255,255,16,238,239,7,3,4,3,2,255
  DEFB 68,2,0,191,255,240,224,0,24,127,1,16,20,247,255,8
  DEFB 0,2,63,7,0,255,15,16,3,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,8
  DEFB 64,0,0,0,0,0,0,2,126,126,126,126,126,126,126,126

; Skool graphic data for columns 64-95 (tiles 0-255)
;
; Used by the routine at PRINTTILE. Tiles 192-238 are unused and identical to
; tiles 73-119, and tiles 239-255 are used for the speech bubble.
  DEFB 0,255,255,0,128,0,0,127,0,255,0,0,134,128,0,255
  DEFB 0,0,0,255,128,128,128,0,255,0,1,1,0,1,0,255
  DEFB 255,1,1,1,15,0,3,245,255,128,192,255,0,21,253,0
  DEFB 5,85,85,255,0,255,255,0,27,255,128,0,13,85,85,0
  DEFB 3,255,63,128,0,42,85,255,1,255,255,3,128,7,106,170
  DEFB 255,6,255,132,4,255,170,171,255,255,0,170,255,128,0,255
  DEFB 255,0,129,129,255,255,1,0,0,255,240,255,224,255,0,5
  DEFB 5,255,0,255,127,255,1,0,255,136,255,224,0,255,255,1
  DEFB 6,2,2,254,0,240,128,132,240,0,255,145,0,16,32,255
  DEFB 17,0,8,4,252,28,20,20,255,6,6,7,255,248,64,64
  DEFB 160,80,40,2,0,255,82,82,41,21,64,0,255,148,148,138
  DEFB 74,74,212,63,31,255,0,192,128,240,0,255,0,32,255,1
  DEFB 255,255,3,128,7,106,170,255,6,255,132,4,255,170,171,255
  DEFB 255,0,170,255,128,0,255,255,0,129,129,255,255,1,0,0
  DEFB 255,240,255,224,255,0,5,5,255,0,255,127,255,1,0,129
  DEFB 14,126,126,126,126,126,126,112,64,0,0,0,0,0,0,2
  DEFB 0,255,1,255,128,0,255,127,129,5,126,0,133,250,126,128
  DEFB 255,126,255,255,128,128,192,255,255,0,1,1,0,1,129,9
  DEFB 255,1,1,1,0,0,2,250,255,255,192,255,0,26,255,0
  DEFB 6,170,170,252,131,17,255,0,21,128,255,15,10,170,170,0
  DEFB 3,255,42,128,0,253,170,253,3,8,255,61,0,1,85,85
  DEFB 255,7,135,4,4,255,85,85,255,255,248,85,255,128,0,255
  DEFB 255,0,129,1,255,255,1,255,255,255,128,255,224,0,0,253
  DEFB 253,255,0,255,0,253,1,127,136,136,0,224,126,255,1,1
  DEFB 254,254,254,254,0,240,128,132,224,0,145,145,0,16,32,17
  DEFB 17,0,8,4,4,28,20,20,255,6,7,7,255,128,64,64
  DEFB 160,80,20,1,0,255,82,42,41,21,48,0,255,148,146,138
  DEFB 74,74,211,63,31,255,0,128,128,255,224,255,0,16,193,1
  DEFB 8,255,61,0,1,85,85,255,7,135,4,4,255,85,85,255
  DEFB 255,248,85,255,128,0,255,255,0,129,1,255,255,1,255,255
  DEFB 255,128,255,224,0,0,253,253,255,0,255,0,253,1,127,129
  DEFB 49,129,129,129,129,129,129,140,64,0,0,0,0,0,0,2
  DEFB 0,255,1,255,128,0,255,127,130,5,30,0,133,250,94,128
  DEFB 255,114,255,255,128,128,240,255,255,0,1,193,0,1,134,9
  DEFB 241,1,1,1,0,3,127,255,255,255,192,255,0,21,255,7
  DEFB 255,85,85,240,141,17,255,0,26,128,255,15,255,85,85,0
  DEFB 3,255,53,0,0,131,85,241,31,8,255,42,0,1,106,234
  DEFB 255,14,134,4,4,171,170,170,255,252,255,170,255,0,192,255
  DEFB 255,0,129,1,254,241,1,0,0,255,0,255,224,0,0,1
  DEFB 1,255,0,255,0,241,1,127,136,136,0,248,126,255,1,1
  DEFB 0,0,0,255,0,252,128,132,224,0,145,145,0,16,64,17
  DEFB 17,0,8,2,4,28,20,20,255,6,7,7,255,128,64,64
  DEFB 160,80,20,0,240,255,82,42,41,13,204,0,255,148,138,138
  DEFB 74,74,127,63,31,255,0,128,128,255,24,240,0,24,1,1
  DEFB 8,255,42,0,1,106,234,255,14,134,4,4,171,170,170,255
  DEFB 252,255,170,255,0,192,255,255,0,129,1,254,241,1,0,0
  DEFB 255,0,255,224,0,0,1,1,255,0,255,0,241,1,127,129
  DEFB 64,0,0,0,0,0,0,2,64,0,0,0,0,0,0,2
  DEFB 0,255,1,255,128,0,255,0,130,5,78,0,133,6,78,128
  DEFB 255,114,255,255,128,128,248,255,255,0,1,249,0,1,132,9
  DEFB 129,1,1,1,0,3,120,255,255,255,192,255,0,26,255,7
  DEFB 192,234,170,192,137,17,255,0,21,128,255,10,129,234,170,0
  DEFB 3,255,42,240,63,240,170,193,255,8,255,53,0,1,253,61
  DEFB 255,117,132,4,4,85,85,85,255,252,127,85,255,0,254,255
  DEFB 254,0,129,1,248,1,1,255,255,255,0,255,224,0,0,253
  DEFB 253,255,0,255,0,129,15,127,136,136,0,248,126,0,1,1
  DEFB 254,254,254,255,0,252,128,136,192,0,145,145,0,16,64,17
  DEFB 17,0,8,2,4,28,20,20,0,6,7,7,0,128,64,64
  DEFB 160,80,10,0,255,0,82,42,41,11,51,0,0,148,138,138
  DEFB 74,74,254,63,31,16,0,128,128,255,20,0,0,20,1,1
  DEFB 8,255,53,0,1,253,61,255,117,132,4,4,85,85,85,255
  DEFB 252,127,85,255,0,254,255,254,0,129,1,248,1,1,255,255
  DEFB 255,0,255,224,0,0,253,253,255,0,255,0,129,15,127,66
  DEFB 64,0,0,0,0,0,0,2,128,0,0,0,0,0,0,1
  DEFB 0,255,1,255,128,0,254,127,130,5,102,0,5,254,70,128
  DEFB 255,78,255,255,128,128,254,255,252,0,1,255,0,1,132,9
  DEFB 1,1,1,1,0,3,86,255,255,254,192,255,0,21,255,5
  DEFB 120,61,213,0,9,17,255,0,26,128,255,13,112,61,85,3
  DEFB 3,192,53,255,63,94,85,1,255,8,0,42,0,241,3,7
  DEFB 0,106,132,4,4,170,250,170,0,240,175,170,3,0,255,255
  DEFB 192,240,129,1,192,1,1,0,0,255,0,255,224,0,0,5
  DEFB 5,254,0,255,0,1,127,127,136,136,0,254,126,255,1,1
  DEFB 2,2,2,255,0,255,129,136,224,0,145,145,255,16,64,17
  DEFB 17,192,8,2,4,28,20,20,255,6,7,7,255,128,64,64
  DEFB 160,80,10,0,255,255,82,42,41,7,12,240,255,148,138,138
  DEFB 74,74,54,63,9,0,0,128,0,255,18,0,0,18,1,241
  DEFB 8,0,42,0,241,3,7,0,106,132,4,4,170,250,170,0
  DEFB 240,175,170,3,0,255,255,192,240,129,1,192,1,1,0,0
  DEFB 255,0,255,224,0,0,5,5,254,0,255,0,1,127,127,36
  DEFB 128,0,0,0,0,0,0,1,128,0,0,0,0,0,0,1
  DEFB 0,255,1,252,128,0,241,128,2,5,114,0,253,1,66,128
  DEFB 254,78,255,255,128,128,255,254,224,0,1,255,1,193,4,9
  DEFB 1,1,1,1,0,2,107,255,252,248,192,255,31,26,255,6
  DEFB 175,134,250,0,9,17,224,0,245,128,255,10,175,7,170,3
  DEFB 3,0,42,255,53,171,170,1,255,8,0,53,0,255,252,192
  DEFB 0,85,132,4,4,255,15,245,0,240,255,213,126,0,255,254
  DEFB 0,254,129,1,0,1,1,255,0,255,0,255,224,0,0,253
  DEFB 5,248,3,255,0,1,255,127,136,136,0,254,126,255,1,1
  DEFB 254,254,2,255,0,255,131,136,248,128,145,145,255,32,128,17
  DEFB 17,240,4,1,4,20,20,20,254,6,7,7,255,128,64,160
  DEFB 160,40,5,0,255,222,82,42,21,3,3,255,212,148,138,74
  DEFB 74,73,246,63,0,0,0,128,0,255,17,0,0,17,1,255
  DEFB 8,0,53,0,255,252,192,0,85,132,4,4,255,15,245,0
  DEFB 240,255,213,126,0,255,254,0,254,129,1,0,1,1,255,0
  DEFB 255,0,255,224,0,0,253,5,248,3,255,0,1,255,127,36
  DEFB 128,0,0,0,0,0,0,1,128,0,0,0,0,0,0,1
  DEFB 0,255,1,240,128,0,128,255,2,5,60,0,129,255,36,128
  DEFB 248,44,248,254,128,0,255,240,0,0,1,255,1,241,4,9
  DEFB 1,7,1,193,0,3,85,255,240,192,192,252,24,21,255,5
  DEFB 85,113,253,0,9,17,0,31,14,128,255,13,85,240,245,3
  DEFB 3,0,245,255,42,85,245,1,255,8,0,42,0,255,171,120
  DEFB 0,106,132,4,4,255,192,14,15,192,255,126,192,0,255,252
  DEFB 0,255,129,1,0,1,129,0,0,255,0,224,224,0,2,1
  DEFB 5,192,3,0,0,1,255,127,136,136,0,255,126,0,1,1
  DEFB 0,0,2,255,192,255,135,136,254,192,145,145,255,32,128,17
  DEFB 17,248,4,1,4,20,20,244,2,6,7,7,255,128,64,160
  DEFB 80,40,5,0,255,222,82,41,21,1,0,255,148,148,138,74
  DEFB 74,165,54,31,240,0,48,128,0,255,17,0,128,17,1,255
  DEFB 8,0,42,0,255,171,120,0,106,132,4,4,255,192,14,15
  DEFB 192,255,126,192,0,255,252,0,255,129,1,0,1,129,0,0
  DEFB 255,0,224,224,0,2,1,5,192,3,0,0,1,255,127,20
  DEFB 128,0,0,0,0,0,0,1,129,129,129,129,129,129,129,129
  DEFB 0,255,255,128,127,255,128,255,127,254,24,7,127,255,24,255
  DEFB 128,24,128,240,128,0,255,128,0,1,1,255,1,253,127,254
  DEFB 1,255,255,241,0,2,106,255,192,192,191,128,31,26,255,6
  DEFB 170,174,255,0,127,254,0,28,1,128,255,10,170,174,30,3
  DEFB 3,0,7,255,53,170,15,1,255,255,0,53,0,255,85,174
  DEFB 7,85,132,4,4,255,126,1,248,128,255,3,0,0,255,224
  DEFB 0,255,129,1,0,1,255,255,0,254,0,255,255,0,5,253
  DEFB 5,0,3,255,255,1,255,0,136,136,255,255,0,255,1,7
  DEFB 254,254,2,255,192,255,132,136,255,192,145,145,127,32,128,17
  DEFB 17,248,4,1,252,20,20,252,254,6,7,7,255,128,64,160
  DEFB 80,40,2,0,255,210,82,41,21,129,0,255,148,148,138,74
  DEFB 74,165,62,31,255,0,96,128,0,255,17,0,64,17,1,255
  DEFB 255,0,53,0,255,85,174,7,85,132,4,4,255,126,1,248
  DEFB 128,255,3,0,0,255,224,0,255,129,1,0,1,255,255,0
  DEFB 254,0,255,255,0,5,253,5,0,3,255,255,1,255,0,8
  DEFB 64,0,0,0,0,0,0,2,126,126,126,126,126,126,126,126

; Skool UDG references (LSBs) for row 0
;
; Used by the routine at PRINTTILE.
  DEFB 1,13,29,43,43,43,43,43,96,103,77,43,43,43,43,43
  DEFB 139,147,43,43,43,43,43,43,43,43,43,43,43,43,43,43
  DEFB 1,1,1,1,1,1,1,1,42,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,81,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,40,71,1,1,1,1
  DEFB 1,105,113,117,123,132,0,0,0,0,0,0,0,0,0,0

; Character buffer for little boy no. 1 (152)
;
; See the character buffer documentation for details of how the buffer is used.
BOY01CBUF:
  DEFS 32

; Skool attribute data for row 0
;
; Used by the routine at RSCROLL.
  DEFB 104,104,104,104,40,40,40,40,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,112,112,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,96,96,96,96,96,48,48,48,48
  DEFB 48,112,112,112,106,106,104,104,104,104,104,104,104,104,104,104

; Personal timetable for little boy no. 1 (152)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 146                ; Lesson 226 (MR WACKER - EXAM ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 154 (Exam
                          ; Room)
  DEFB 176                ; Lesson 228 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 136                ; Lesson 229 (REVISION LIBRARY): 136 (Map Room)
  DEFB 154                ; Lesson 230 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 146                ; Lesson 231 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 232 (MR WACKER - READING ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 233 (MR ROCKITT - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 234 (MR CREAK - READING ROOM): 146 (Reading
                          ; Room)
  DEFB 146                ; Lesson 235 (MR CREAK - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 136                ; Lesson 236 (MR WACKER - WHITE ROOM): 136 (Map Room)
  DEFB 146                ; Lesson 237 (MR WITHIT - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 136                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 136 (Map
                          ; Room)
  DEFB 154                ; Lesson 239 (MR WACKER - MAP ROOM): 154 (Exam Room)
  DEFB 146                ; Lesson 240 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 242 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 198                ; Lesson 246 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 176                ; Lesson 247 (PLAYTIME): 176 (Revision Library)
  DEFB 176                ; Lesson 248 (PLAYTIME): 176 (Revision Library)
  DEFB 202                ; Lesson 249 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 196                ; Lesson 250 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 204                ; Lesson 252 (PLAYTIME (little boys stampede)): 204
                          ; (Stampede)
  DEFB 204                ; Lesson 253 (PLAYTIME (little boys stampede)): 204
                          ; (Stampede)
  DEFB 176                ; Lesson 254 (PLAYTIME): 176 (Revision Library)
  DEFB 196                ; Lesson 255 (PLAYTIME): 196 (Walkabout)

; Skool UDG references (LSBs) for row 1
;
; Used by the routine at PRINTTILE.
  DEFB 2,14,14,44,13,29,43,43,97,104,107,111,116,43,43,43
  DEFB 140,148,155,147,43,43,43,43,43,43,43,43,43,43,43,43
  DEFB 1,1,1,1,1,1,1,1,43,50,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,76,82,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,40,51,63,72,1,1,1,95
  DEFB 100,106,114,118,1,133,137,0,0,0,0,0,0,0,0,0

; Character buffer for little boy no. 2 (153)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 1
;
; Used by the routine at RSCROLL.
  DEFB 104,104,104,104,104,104,40,40,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,112,112,112,48,32,32,32,32
  DEFB 48,48,48,48,48,48,48,96,96,96,96,96,48,48,48,112
  DEFB 112,112,114,114,114,106,106,104,104,104,104,104,104,104,104,104

; Personal timetable for little boy no. 2 (153)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 146                ; Lesson 226 (MR WACKER - EXAM ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 228 (REVISION LIBRARY): 146 (Reading Room)
  DEFB 136                ; Lesson 229 (REVISION LIBRARY): 136 (Map Room)
  DEFB 162                ; Lesson 230 (REVISION LIBRARY): 162 (White Room)
  DEFB 162                ; Lesson 231 (MR WITHIT - MAP ROOM): 162 (White Room)
  DEFB 146                ; Lesson 232 (MR WACKER - READING ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 233 (MR ROCKITT - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 234 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 136                ; Lesson 235 (MR CREAK - WHITE ROOM): 136 (Map Room)
  DEFB 146                ; Lesson 236 (MR WACKER - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 237 (MR WITHIT - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 239 (MR WACKER - MAP ROOM): 162 (White Room)
  DEFB 146                ; Lesson 240 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 242 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 198                ; Lesson 246 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 198                ; Lesson 247 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 176                ; Lesson 248 (PLAYTIME): 176 (Revision Library)
  DEFB 202                ; Lesson 249 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 196                ; Lesson 250 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 198                ; Lesson 254 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 176                ; Lesson 255 (PLAYTIME): 176 (Revision Library)

; Skool UDG references (LSBs) for row 2
;
; Used by the routine at PRINTTILE.
  DEFB 2,15,30,45,14,14,85,0,98,105,14,14,14,0,0,0
  DEFB 0,105,14,14,0,52,106,0,52,106,0,52,106,0,52,106
  DEFB 0,8,13,0,8,13,0,8,44,51,8,13,0,8,13,70
  DEFB 8,13,104,8,13,0,8,13,20,82,86,13,0,8,13,0
  DEFB 2,0,15,2,0,15,2,0,20,0,64,0,0,0,17,20
  DEFB 0,0,64,0,0,0,20,0,0,0,0,0,0,0,0,0

; Character buffer for little boy no. 3 (154)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 2
;
; Used by the routine at RSCROLL.
  DEFB 104,104,104,104,104,104,42,40,41,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,104,104,48,104,104,48,104,104,48,104,104
  DEFB 48,40,40,48,40,40,48,40,40,48,40,40,48,40,40,49
  DEFB 40,40,52,40,40,48,40,40,112,112,40,40,32,40,40,32
  DEFB 104,32,104,104,32,104,104,32,96,96,96,48,48,48,49,112
  DEFB 112,112,112,48,48,48,104,104,104,104,104,104,104,104,104,104

; Personal timetable for little boy no. 3 (154)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 146                ; Lesson 226 (MR WACKER - EXAM ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 228 (REVISION LIBRARY): 146 (Reading Room)
  DEFB 136                ; Lesson 229 (REVISION LIBRARY): 136 (Map Room)
  DEFB 154                ; Lesson 230 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 146                ; Lesson 231 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 136                ; Lesson 232 (MR WACKER - READING ROOM): 136 (Map
                          ; Room)
  DEFB 154                ; Lesson 233 (MR ROCKITT - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 234 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 235 (MR CREAK - WHITE ROOM): 154 (Exam Room)
  DEFB 136                ; Lesson 236 (MR WACKER - WHITE ROOM): 136 (Map Room)
  DEFB 154                ; Lesson 237 (MR WITHIT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 136 (Map
                          ; Room)
  DEFB 154                ; Lesson 239 (MR WACKER - MAP ROOM): 154 (Exam Room)
  DEFB 136                ; Lesson 240 (MR WITHIT - MAP ROOM): 136 (Map Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 242 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 198                ; Lesson 246 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 200                ; Lesson 247 (PLAYTIME): 200 (Walk around the gym)
  DEFB 198                ; Lesson 248 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 202                ; Lesson 249 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 196                ; Lesson 250 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 200                ; Lesson 254 (PLAYTIME): 200 (Walk around the gym)
  DEFB 196                ; Lesson 255 (PLAYTIME): 196 (Walkabout)

; Skool UDG references (LSBs) for row 3
;
; Used by the routine at PRINTTILE.
  DEFB 2,16,31,46,60,74,86,75,99,105,14,14,14,0,0,52
  DEFB 101,105,14,163,174,180,183,189,194,74,86,75,204,207,212,217
  DEFB 2,0,0,15,22,28,35,23,45,52,206,205,204,203,202,201
  DEFB 200,199,0,0,0,0,0,0,20,82,0,90,101,111,114,117
  DEFB 0,0,0,0,0,0,0,0,20,0,64,73,2,0,0,20
  DEFB 0,0,64,119,124,0,20,0,0,0,0,0,0,0,0,0

; Character buffer for little boy no. 4 (155)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 3
;
; Used by the routine at RSCROLL.
  DEFB 104,104,104,104,104,56,56,56,56,48,48,48,48,48,48,40
  DEFB 40,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,112,112,32,120,120,120,120,120
  DEFB 32,32,32,32,32,32,32,32,96,96,96,104,104,48,48,112
  DEFB 112,112,112,56,56,48,104,104,104,104,104,104,104,104,104,104

; Personal timetable for little boy no. 4 (155)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 146                ; Lesson 226 (MR WACKER - EXAM ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 162 (White
                          ; Room)
  DEFB 146                ; Lesson 228 (REVISION LIBRARY): 146 (Reading Room)
  DEFB 146                ; Lesson 229 (REVISION LIBRARY): 146 (Reading Room)
  DEFB 154                ; Lesson 230 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 162                ; Lesson 231 (MR WITHIT - MAP ROOM): 162 (White Room)
  DEFB 154                ; Lesson 232 (MR WACKER - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 233 (MR ROCKITT - READING ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 234 (MR CREAK - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 235 (MR CREAK - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 146                ; Lesson 236 (MR WACKER - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 136                ; Lesson 237 (MR WITHIT - WHITE ROOM): 136 (Map Room)
  DEFB 136                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 136 (Map
                          ; Room)
  DEFB 162                ; Lesson 239 (MR WACKER - MAP ROOM): 162 (White Room)
  DEFB 146                ; Lesson 240 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 242 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 200                ; Lesson 246 (PLAYTIME): 200 (Walk around the gym)
  DEFB 176                ; Lesson 247 (PLAYTIME): 176 (Revision Library)
  DEFB 198                ; Lesson 248 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 198                ; Lesson 249 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 200                ; Lesson 250 (PLAYTIME): 200 (Walk around the gym)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 202                ; Lesson 254 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 198                ; Lesson 255 (PLAYTIME): 198 (Walk around the fire
                          ; escape)

; Skool UDG references (LSBs) for row 4
;
; Used by the routine at PRINTTILE.
  DEFB 2,17,32,47,61,75,75,86,99,105,14,14,14,0,0,52
  DEFB 101,105,14,164,175,181,184,99,75,198,200,202,205,208,213,213
  DEFB 3,0,0,16,23,28,36,35,45,52,198,197,196,195,194,193
  DEFB 192,191,0,0,0,0,0,0,20,82,0,91,102,112,115,118
  DEFB 0,0,0,0,0,0,0,0,20,0,64,73,2,0,0,20
  DEFB 0,0,64,119,124,0,20,0,0,0,0,0,0,0,0,0

; Character buffer for little boy no. 5 (156)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 4
;
; Used by the routine at RSCROLL.
  DEFB 104,104,104,104,104,56,56,56,56,48,48,48,48,48,48,40
  DEFB 40,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,112,112,32,120,120,120,120,120
  DEFB 32,32,32,32,32,32,32,32,96,96,96,104,104,48,48,112
  DEFB 112,112,112,56,56,48,104,104,104,104,104,104,104,104,104,104

; Personal timetable for little boy no. 5 (156)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 146                ; Lesson 226 (MR WACKER - EXAM ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 162 (White
                          ; Room)
  DEFB 162                ; Lesson 228 (REVISION LIBRARY): 162 (White Room)
  DEFB 176                ; Lesson 229 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 162                ; Lesson 230 (REVISION LIBRARY): 162 (White Room)
  DEFB 146                ; Lesson 231 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 136                ; Lesson 232 (MR WACKER - READING ROOM): 136 (Map
                          ; Room)
  DEFB 146                ; Lesson 233 (MR ROCKITT - READING ROOM): 146
                          ; (Reading Room)
  DEFB 162                ; Lesson 234 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 136                ; Lesson 235 (MR CREAK - WHITE ROOM): 136 (Map Room)
  DEFB 154                ; Lesson 236 (MR WACKER - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 237 (MR WITHIT - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 239 (MR WACKER - MAP ROOM): 154 (Exam Room)
  DEFB 162                ; Lesson 240 (MR WITHIT - MAP ROOM): 162 (White Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 242 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 200                ; Lesson 246 (PLAYTIME): 200 (Walk around the gym)
  DEFB 198                ; Lesson 247 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 202                ; Lesson 248 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 200                ; Lesson 249 (PLAYTIME): 200 (Walk around the gym)
  DEFB 200                ; Lesson 250 (PLAYTIME): 200 (Walk around the gym)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 176                ; Lesson 254 (PLAYTIME): 176 (Revision Library)
  DEFB 200                ; Lesson 255 (PLAYTIME): 200 (Walk around the gym)

; Skool UDG references (LSBs) for row 5
;
; Used by the routine at PRINTTILE.
  DEFB 2,14,33,48,62,76,87,91,100,105,14,14,14,0,0,0
  DEFB 0,105,14,165,176,182,185,190,195,199,201,203,206,209,214,218
  DEFB 3,0,0,17,23,23,35,23,36,52,1,1,1,1,1,55
  DEFB 61,66,61,72,61,72,61,72,61,72,0,92,103,0,116,119
  DEFB 3,8,16,8,18,8,23,30,41,52,64,0,0,0,0,20
  DEFB 0,0,64,120,120,120,138,143,143,143,143,143,143,143,186,188

; Character buffer for little boy no. 6 (157)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 5
;
; Used by the routine at RSCROLL.
  DEFB 104,104,104,104,104,40,40,40,40,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,112,112,32,120,120,120,120,120
  DEFB 32,32,32,32,32,32,32,32,96,96,96,48,48,48,48,112
  DEFB 112,112,112,48,48,48,104,104,104,104,104,104,104,104,104,104

; Personal timetable for little boy no. 6 (157)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 154                ; Lesson 226 (MR WACKER - EXAM ROOM): 154 (Exam Room)
  DEFB 162                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 162 (White
                          ; Room)
  DEFB 162                ; Lesson 228 (REVISION LIBRARY): 162 (White Room)
  DEFB 146                ; Lesson 229 (REVISION LIBRARY): 146 (Reading Room)
  DEFB 162                ; Lesson 230 (REVISION LIBRARY): 162 (White Room)
  DEFB 162                ; Lesson 231 (MR WITHIT - MAP ROOM): 162 (White Room)
  DEFB 154                ; Lesson 232 (MR WACKER - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 233 (MR ROCKITT - READING ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 234 (MR CREAK - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 154                ; Lesson 235 (MR CREAK - WHITE ROOM): 154 (Exam Room)
  DEFB 136                ; Lesson 236 (MR WACKER - WHITE ROOM): 136 (Map Room)
  DEFB 162                ; Lesson 237 (MR WITHIT - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 146                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 239 (MR WACKER - MAP ROOM): 154 (Exam Room)
  DEFB 162                ; Lesson 240 (MR WITHIT - MAP ROOM): 162 (White Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 242 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 200                ; Lesson 246 (PLAYTIME): 200 (Walk around the gym)
  DEFB 202                ; Lesson 247 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 200                ; Lesson 248 (PLAYTIME): 200 (Walk around the gym)
  DEFB 198                ; Lesson 249 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 202                ; Lesson 250 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 198                ; Lesson 254 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 202                ; Lesson 255 (PLAYTIME): 202 (Walk around the big
                          ; window)

; Skool UDG references (LSBs) for row 6
;
; Used by the routine at PRINTTILE.
  DEFB 2,14,34,49,63,9,88,92,0,105,14,14,14,0,0,0
  DEFB 0,105,14,166,21,21,21,21,21,21,21,21,21,21,21,21
  DEFB 3,0,0,18,21,21,21,21,21,52,0,0,0,0,0,56
  DEFB 62,67,62,67,62,67,62,67,62,67,0,0,0,0,0,0
  DEFB 4,9,4,9,4,9,4,31,42,53,64,0,0,0,0,20
  DEFB 0,0,64,121,121,121,139,144,144,144,144,144,144,144,144,189

; Character buffer for little boy no. 7 (158)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 6
;
; Used by the routine at RSCROLL.
  DEFB 104,104,104,104,104,40,40,40,40,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,112,112,32,32,32,32,32,32
  DEFB 32,32,32,32,32,32,32,32,96,96,96,48,48,48,48,112
  DEFB 112,112,112,48,48,48,104,104,104,104,104,104,104,104,104,104

; Personal timetable for little boy no. 7 (158)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 154                ; Lesson 226 (MR WACKER - EXAM ROOM): 154 (Exam Room)
  DEFB 162                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 228 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 146                ; Lesson 229 (REVISION LIBRARY): 146 (Reading Room)
  DEFB 154                ; Lesson 230 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 154                ; Lesson 231 (MR WITHIT - MAP ROOM): 154 (Exam Room)
  DEFB 146                ; Lesson 232 (MR WACKER - READING ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 233 (MR ROCKITT - READING ROOM): 162 (White
                          ; Room)
  DEFB 136                ; Lesson 234 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 146                ; Lesson 235 (MR CREAK - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 236 (MR WACKER - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 237 (MR WITHIT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 136 (Map
                          ; Room)
  DEFB 146                ; Lesson 239 (MR WACKER - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 240 (MR WITHIT - MAP ROOM): 162 (White Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 242 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 202                ; Lesson 246 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 176                ; Lesson 247 (PLAYTIME): 176 (Revision Library)
  DEFB 200                ; Lesson 248 (PLAYTIME): 200 (Walk around the gym)
  DEFB 200                ; Lesson 249 (PLAYTIME): 200 (Walk around the gym)
  DEFB 202                ; Lesson 250 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 200                ; Lesson 254 (PLAYTIME): 200 (Walk around the gym)
  DEFB 176                ; Lesson 255 (PLAYTIME): 176 (Revision Library)

; Skool UDG references (LSBs) for row 7
;
; Used by the routine at PRINTTILE.
  DEFB 3,18,35,50,64,77,43,43,43,43,43,96,13,121,126,131
  DEFB 141,149,156,167,43,43,44,13,29,43,43,43,43,43,43,43
  DEFB 1,1,1,1,1,1,1,39,46,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,19,24,32,43,54,65,74,80,88,92,96
  DEFB 101,107,115,115,125,125,125,125,125,152,156,165,172,181,187,190

; Character buffer for little boy no. 8 (159)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 7
;
; Used by the routine at RSCROLL.
  DEFB 32,32,32,32,32,32,32,32,48,32,32,48,48,48,48,48
  DEFB 48,48,48,48,48,48,56,56,56,56,56,56,56,56,56,56
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,112,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,112,48,48,48,48,48,48,48,112
  DEFB 112,48,48,48,48,48,48,48,48,48,112,112,112,112,112,112

; Personal timetable for little boy no. 8 (159)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 154                ; Lesson 226 (MR WACKER - EXAM ROOM): 154 (Exam Room)
  DEFB 162                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 228 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 146                ; Lesson 229 (REVISION LIBRARY): 146 (Reading Room)
  DEFB 162                ; Lesson 230 (REVISION LIBRARY): 162 (White Room)
  DEFB 146                ; Lesson 231 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 232 (MR WACKER - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 233 (MR ROCKITT - READING ROOM): 136 (Map
                          ; Room)
  DEFB 154                ; Lesson 234 (MR CREAK - READING ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 235 (MR CREAK - WHITE ROOM): 136 (Map Room)
  DEFB 146                ; Lesson 236 (MR WACKER - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 136                ; Lesson 237 (MR WITHIT - WHITE ROOM): 136 (Map Room)
  DEFB 146                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 239 (MR WACKER - MAP ROOM): 154 (Exam Room)
  DEFB 146                ; Lesson 240 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 242 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 202                ; Lesson 246 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 198                ; Lesson 247 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 202                ; Lesson 248 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 176                ; Lesson 249 (PLAYTIME): 176 (Revision Library)
  DEFB 198                ; Lesson 250 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 202                ; Lesson 254 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 198                ; Lesson 255 (PLAYTIME): 198 (Walk around the fire
                          ; escape)

; Skool UDG references (LSBs) for row 8
;
; Used by the routine at PRINTTILE.
  DEFB 4,19,19,51,51,14,0,0,0,0,0,112,117,122,26,132
  DEFB 0,0,0,0,0,25,186,14,14,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,40,47,0,0,0,0,54,0,0
  DEFB 0,0,70,0,0,0,0,54,0,0,0,0,104,0,0,0
  DEFB 0,10,0,0,0,20,25,33,0,0,0,0,81,57,20,0
  DEFB 26,108,116,122,122,122,122,122,148,153,157,166,173,0,0,26

; Character buffer for little boy no. 9 (160)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 8
;
; Used by the routine at RSCROLL.
  DEFB 32,32,32,32,32,32,32,32,32,32,32,48,48,48,56,48
  DEFB 48,48,48,48,48,48,56,56,56,56,56,56,56,56,56,56
  DEFB 56,56,56,56,56,56,56,48,48,48,48,48,48,50,48,48
  DEFB 48,48,52,48,48,48,48,50,48,48,48,48,49,48,48,48
  DEFB 48,50,48,48,48,112,112,112,48,48,48,48,48,56,112,112
  DEFB 112,48,48,48,48,48,48,48,48,48,112,112,112,112,112,112

; Personal timetable for little boy no. 9 (160)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 154                ; Lesson 226 (MR WACKER - EXAM ROOM): 154 (Exam Room)
  DEFB 154                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 154 (Exam
                          ; Room)
  DEFB 154                ; Lesson 228 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 136                ; Lesson 229 (REVISION LIBRARY): 136 (Map Room)
  DEFB 162                ; Lesson 230 (REVISION LIBRARY): 162 (White Room)
  DEFB 154                ; Lesson 231 (MR WITHIT - MAP ROOM): 154 (Exam Room)
  DEFB 136                ; Lesson 232 (MR WACKER - READING ROOM): 136 (Map
                          ; Room)
  DEFB 136                ; Lesson 233 (MR ROCKITT - READING ROOM): 136 (Map
                          ; Room)
  DEFB 162                ; Lesson 234 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 154                ; Lesson 235 (MR CREAK - WHITE ROOM): 154 (Exam Room)
  DEFB 154                ; Lesson 236 (MR WACKER - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 237 (MR WITHIT - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 239 (MR WACKER - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 146                ; Lesson 240 (MR WITHIT - MAP ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 136                ; Lesson 242 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 202                ; Lesson 246 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 200                ; Lesson 247 (PLAYTIME): 200 (Walk around the gym)
  DEFB 196                ; Lesson 248 (PLAYTIME): 196 (Walkabout)
  DEFB 176                ; Lesson 249 (PLAYTIME): 176 (Revision Library)
  DEFB 198                ; Lesson 250 (PLAYTIME): 198 (Walk around the fire
                          ; escape)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 176                ; Lesson 254 (PLAYTIME): 176 (Revision Library)
  DEFB 200                ; Lesson 255 (PLAYTIME): 200 (Walk around the gym)

; Skool UDG references (LSBs) for row 9
;
; Used by the routine at PRINTTILE.
  DEFB 5,20,36,52,65,14,0,52,101,106,219,52,105,123,127,26
  DEFB 142,0,0,0,52,101,106,105,14,0,238,237,236,235,234,233
  DEFB 232,231,0,0,0,0,0,40,47,222,221,220,219,218,217,216
  DEFB 215,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,20,26,2,0,0,0,75,57,89,93,0
  DEFB 26,109,109,109,126,0,0,0,149,153,20,166,173,0,0,26

; Character buffer for little boy no. 10 (161)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 9
;
; Used by the routine at RSCROLL.
  DEFB 32,104,104,104,104,32,32,40,40,40,32,40,48,48,48,56
  DEFB 48,48,48,48,40,40,40,56,56,56,56,56,56,56,56,56
  DEFB 56,56,56,56,56,56,56,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,104,48,48,48,48,56,112,112,112
  DEFB 112,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for little boy no. 10 (161)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 154                ; Lesson 226 (MR WACKER - EXAM ROOM): 154 (Exam Room)
  DEFB 154                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 154 (Exam
                          ; Room)
  DEFB 154                ; Lesson 228 (REVISION LIBRARY): 154 (Exam Room)
  DEFB 136                ; Lesson 229 (REVISION LIBRARY): 136 (Map Room)
  DEFB 176                ; Lesson 230 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 154                ; Lesson 231 (MR WITHIT - MAP ROOM): 154 (Exam Room)
  DEFB 162                ; Lesson 232 (MR WACKER - READING ROOM): 162 (White
                          ; Room)
  DEFB 136                ; Lesson 233 (MR ROCKITT - READING ROOM): 136 (Map
                          ; Room)
  DEFB 136                ; Lesson 234 (MR CREAK - READING ROOM): 136 (Map
                          ; Room)
  DEFB 146                ; Lesson 235 (MR CREAK - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 236 (MR WACKER - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 154                ; Lesson 237 (MR WITHIT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 146                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 146 (Reading
                          ; Room)
  DEFB 154                ; Lesson 239 (MR WACKER - MAP ROOM): 154 (Exam Room)
  DEFB 162                ; Lesson 240 (MR WITHIT - MAP ROOM): 162 (White Room)
  DEFB 154                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 154 (Exam
                          ; Room)
  DEFB 162                ; Lesson 242 (MR CREAK - READING ROOM): 162 (White
                          ; Room)
  DEFB 210                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 210
                          ; (Tell ERIC about ANGELFACE)
  DEFB 208                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 208 (Tell ERIC about EINSTEIN)
  DEFB 208                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 208 (Tell ERIC about BOY WANDER)
  DEFB 176                ; Lesson 246 (PLAYTIME): 176 (Revision Library)
  DEFB 200                ; Lesson 247 (PLAYTIME): 200 (Walk around the gym)
  DEFB 196                ; Lesson 248 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 249 (PLAYTIME): 196 (Walkabout)
  DEFB 176                ; Lesson 250 (PLAYTIME): 176 (Revision Library)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 176                ; Lesson 254 (PLAYTIME): 176 (Revision Library)
  DEFB 202                ; Lesson 255 (PLAYTIME): 202 (Walk around the big
                          ; window)

; Skool UDG references (LSBs) for row 10
;
; Used by the routine at PRINTTILE.
  DEFB 5,21,26,53,66,14,0,52,101,106,0,52,105,14,128,133
  DEFB 26,150,157,0,52,101,106,105,14,0,230,229,228,227,226,225
  DEFB 224,223,0,0,0,0,0,40,47,214,213,212,211,210,209,208
  DEFB 207,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,20,26,34,0,55,66,57,82,76,0,0
  DEFB 26,110,0,0,127,0,0,0,150,153,20,166,173,0,0,26

; Character buffer for little boy no. 11 (162)
;
; See the character buffer documentation for details of how the buffer is used.
BOY11CBUF:
  DEFS 32

; Skool attribute data for row 10
;
; Used by the routine at RSCROLL.
  DEFB 32,104,104,104,104,32,32,40,40,40,32,40,48,48,48,48
  DEFB 56,48,48,48,40,40,40,56,56,56,56,56,56,56,56,56
  DEFB 56,56,56,56,56,56,56,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,104,48,48,48,56,48,112,112,112
  DEFB 112,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for little boy no. 11 (162)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 154                ; Lesson 226 (MR WACKER - EXAM ROOM): 154 (Exam Room)
  DEFB 154                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 154 (Exam
                          ; Room)
  DEFB 176                ; Lesson 228 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 176                ; Lesson 229 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 176                ; Lesson 230 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 136                ; Lesson 231 (MR WITHIT - MAP ROOM): 136 (Map Room)
  DEFB 146                ; Lesson 232 (MR WACKER - READING ROOM): 146 (Reading
                          ; Room)
  DEFB 146                ; Lesson 233 (MR ROCKITT - READING ROOM): 146
                          ; (Reading Room)
  DEFB 146                ; Lesson 234 (MR CREAK - READING ROOM): 146 (Reading
                          ; Room)
  DEFB 162                ; Lesson 235 (MR CREAK - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 162                ; Lesson 236 (MR WACKER - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 162                ; Lesson 237 (MR WITHIT - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 162                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 136                ; Lesson 239 (MR WACKER - MAP ROOM): 136 (Map Room)
  DEFB 136                ; Lesson 240 (MR WITHIT - MAP ROOM): 136 (Map Room)
  DEFB 162                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 162 (White
                          ; Room)
  DEFB 146                ; Lesson 242 (MR CREAK - READING ROOM): 146 (Reading
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 176                ; Lesson 246 (PLAYTIME): 176 (Revision Library)
  DEFB 202                ; Lesson 247 (PLAYTIME): 202 (Walk around the big
                          ; window)
  DEFB 196                ; Lesson 248 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 249 (PLAYTIME): 196 (Walkabout)
  DEFB 176                ; Lesson 250 (PLAYTIME): 176 (Revision Library)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 206                ; Lesson 252 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 206                ; Lesson 253 (PLAYTIME (little boys stampede)): 206
                          ; (Stampede)
  DEFB 176                ; Lesson 254 (PLAYTIME): 176 (Revision Library)
  DEFB 196                ; Lesson 255 (PLAYTIME): 196 (Walkabout)

; Skool UDG references (LSBs) for row 11
;
; Used by the routine at PRINTTILE.
  DEFB 6,22,37,54,67,78,0,93,21,21,108,113,105,14,14,134
  DEFB 26,26,158,168,0,0,0,105,14,0,43,43,43,43,43,43
  DEFB 1,1,14,19,14,19,14,40,47,1,1,1,1,1,1,1
  DEFB 1,68,71,68,71,68,0,0,0,0,0,93,105,71,105,71
  DEFB 5,11,5,11,5,21,26,0,44,56,57,57,83,0,0,0
  DEFB 26,111,103,103,128,20,0,0,150,153,20,166,173,0,0,26

; Character buffer for MR WACKER (163)
;
; See the character buffer documentation for details of how the buffer is used.
WACKERCBUF:
  DEFS 32

; Skool attribute data for row 11
;
; Used by the routine at RSCROLL.
  DEFB 32,32,32,32,32,32,32,32,32,32,32,32,48,48,48,48
  DEFB 48,56,48,48,48,48,48,56,56,56,56,56,56,56,56,56
  DEFB 56,56,56,56,56,56,56,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,48,56,48,48,112,112,112
  DEFB 112,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for MR WACKER (163)
;
; Used by the routine at NEWLESSON.
  DEFB 186                ; Lesson 224 (DINNER (MR WITHIT)): 186 (Head's study)
  DEFB 184                ; Lesson 225 (DINNER (MR WACKER)): 184 (Dinner duty)
  DEFB 132                ; Lesson 226 (MR WACKER - EXAM ROOM): 132 (Exam Room)
  DEFB 190                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 190
                          ; (Walkabout)
  DEFB 130                ; Lesson 228 (REVISION LIBRARY): 130 (Reading Room)
  DEFB 190                ; Lesson 229 (REVISION LIBRARY): 190 (Walkabout)
  DEFB 190                ; Lesson 230 (REVISION LIBRARY): 190 (Walkabout)
  DEFB 132                ; Lesson 231 (MR WITHIT - MAP ROOM): 132 (Exam Room)
  DEFB 130                ; Lesson 232 (MR WACKER - READING ROOM): 130 (Reading
                          ; Room)
  DEFB 132                ; Lesson 233 (MR ROCKITT - READING ROOM): 132 (Exam
                          ; Room)
  DEFB 134                ; Lesson 234 (MR CREAK - READING ROOM): 134 (White
                          ; Room)
  DEFB 130                ; Lesson 235 (MR CREAK - WHITE ROOM): 130 (Reading
                          ; Room)
  DEFB 134                ; Lesson 236 (MR WACKER - WHITE ROOM): 134 (White
                          ; Room)
  DEFB 128                ; Lesson 237 (MR WITHIT - WHITE ROOM): 128 (Map Room)
  DEFB 132                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 132 (Exam
                          ; Room)
  DEFB 128                ; Lesson 239 (MR WACKER - MAP ROOM): 128 (Map Room)
  DEFB 190                ; Lesson 240 (MR WITHIT - MAP ROOM): 190 (Walkabout)
  DEFB 132                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 132 (Exam
                          ; Room)
  DEFB 190                ; Lesson 242 (MR CREAK - READING ROOM): 190
                          ; (Walkabout)
  DEFB 190                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 190
                          ; (Walkabout)
  DEFB 214                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 214 (Wait for EINSTEIN to grass on ERIC)
  DEFB 218                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 218 (Look for the pea-shooter)
  DEFB 190                ; Lesson 246 (PLAYTIME): 190 (Walkabout)
  DEFB 186                ; Lesson 247 (PLAYTIME): 186 (Head's study)
  DEFB 190                ; Lesson 248 (PLAYTIME): 190 (Walkabout)
  DEFB 190                ; Lesson 249 (PLAYTIME): 190 (Walkabout)
  DEFB 190                ; Lesson 250 (PLAYTIME): 190 (Walkabout)
  DEFB 190                ; Lesson 251 (PLAYTIME): 190 (Walkabout)
  DEFB 190                ; Lesson 252 (PLAYTIME (little boys stampede)): 190
                          ; (Walkabout)
  DEFB 196                ; Lesson 253 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 188                ; Lesson 254 (PLAYTIME): 188 (Staff room)
  DEFB 190                ; Lesson 255 (PLAYTIME): 190 (Walkabout)

; Skool UDG references (LSBs) for row 12
;
; Used by the routine at PRINTTILE.
  DEFB 7,23,38,55,68,79,89,94,102,102,109,114,105,14,14,135
  DEFB 125,26,26,92,0,0,0,105,14,0,0,0,0,210,215,210
  DEFB 4,9,4,9,4,9,4,40,47,0,0,9,4,9,4,57
  DEFB 63,69,63,69,63,69,63,0,0,0,0,94,69,63,69,63
  DEFB 6,12,6,12,6,12,26,0,45,57,57,76,84,0,0,0
  DEFB 26,111,103,103,129,20,0,0,150,154,158,166,173,0,0,26

; Character buffer for MR ROCKITT (164)
;
; See the character buffer documentation for details of how the buffer is used.
ROCKITTCBUF:
  DEFS 32

; Skool attribute data for row 12
;
; Used by the routine at RSCROLL.
  DEFB 32,32,32,32,32,32,32,32,32,32,32,32,48,48,48,48
  DEFB 48,48,56,48,48,48,48,56,56,56,56,56,56,56,56,56
  DEFB 56,56,56,56,56,56,56,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,56,48,48,48,112,112,112
  DEFB 112,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for MR ROCKITT (164)
;
; Used by the routine at NEWLESSON.
  DEFB 188                ; Lesson 224 (DINNER (MR WITHIT)): 188 (Staff room)
  DEFB 188                ; Lesson 225 (DINNER (MR WACKER)): 188 (Staff room)
  DEFB 196                ; Lesson 226 (MR WACKER - EXAM ROOM): 196 (Walkabout)
  DEFB 132                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 132 (Exam
                          ; Room)
  DEFB 132                ; Lesson 228 (REVISION LIBRARY): 132 (Exam Room)
  DEFB 154                ; Lesson 229 (REVISION LIBRARY): 154 (Exam Room)
                          ; (this is a bug)
  DEFB 134                ; Lesson 230 (REVISION LIBRARY): 134 (White Room)
  DEFB 134                ; Lesson 231 (MR WITHIT - MAP ROOM): 134 (White Room)
  DEFB 132                ; Lesson 232 (MR WACKER - READING ROOM): 132 (Exam
                          ; Room)
  DEFB 130                ; Lesson 233 (MR ROCKITT - READING ROOM): 130
                          ; (Reading Room)
  DEFB 132                ; Lesson 234 (MR CREAK - READING ROOM): 132 (Exam
                          ; Room)
  DEFB 132                ; Lesson 235 (MR CREAK - WHITE ROOM): 132 (Exam Room)
  DEFB 132                ; Lesson 236 (MR WACKER - WHITE ROOM): 132 (Exam
                          ; Room)
  DEFB 132                ; Lesson 237 (MR WITHIT - WHITE ROOM): 132 (Exam
                          ; Room)
  DEFB 134                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 134 (White
                          ; Room)
  DEFB 130                ; Lesson 239 (MR WACKER - MAP ROOM): 130 (Reading
                          ; Room)
  DEFB 130                ; Lesson 240 (MR WITHIT - MAP ROOM): 130 (Reading
                          ; Room)
  DEFB 134                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 134 (White
                          ; Room)
  DEFB 134                ; Lesson 242 (MR CREAK - READING ROOM): 134 (White
                          ; Room)
  DEFB 222                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 222
                          ; (Mumps duty)
  DEFB 190                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 190 (Walkabout)
  DEFB 190                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 190 (Walkabout)
  DEFB 190                ; Lesson 246 (PLAYTIME): 190 (Walkabout)
  DEFB 188                ; Lesson 247 (PLAYTIME): 188 (Staff room)
  DEFB 196                ; Lesson 248 (PLAYTIME): 196 (Walkabout)
  DEFB 188                ; Lesson 249 (PLAYTIME): 188 (Staff room)
  DEFB 196                ; Lesson 250 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 252 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 253 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 188                ; Lesson 254 (PLAYTIME): 188 (Staff room)
  DEFB 196                ; Lesson 255 (PLAYTIME): 196 (Walkabout)

; Skool UDG references (LSBs) for row 13
;
; Used by the routine at PRINTTILE.
  DEFB 8,24,39,56,69,80,90,43,43,43,110,115,118,124,129,136
  DEFB 143,151,159,169,43,43,43,191,196,43,43,43,43,211,216,211
  DEFB 5,10,5,10,5,10,5,41,48,1,1,10,5,10,5,58
  DEFB 64,58,64,58,64,58,64,1,1,1,1,95,58,64,58,64
  DEFB 7,13,7,13,7,13,27,1,46,58,67,77,85,90,94,97
  DEFB 102,111,103,103,129,20,0,0,150,155,159,166,174,0,0,26

; Character buffer for MR WITHIT (165)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 13
;
; Used by the routine at RSCROLL.
  DEFB 32,32,32,32,32,32,32,32,32,32,32,32,48,48,48,48
  DEFB 48,48,48,48,48,48,48,56,56,56,56,56,56,56,56,56
  DEFB 56,56,56,56,56,56,56,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,48,48,48,48,48,112,112
  DEFB 112,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for MR WITHIT (165)
;
; Used by the routine at NEWLESSON.
  DEFB 184                ; Lesson 224 (DINNER (MR WITHIT)): 184 (Dinner duty)
  DEFB 198                ; Lesson 225 (DINNER (MR WACKER)): 198 (Walk around
                          ; the fire escape)
  DEFB 130                ; Lesson 226 (MR WACKER - EXAM ROOM): 130 (Reading
                          ; Room)
  DEFB 190                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 190
                          ; (Walkabout)
  DEFB 202                ; Lesson 228 (REVISION LIBRARY): 202 (Walk around the
                          ; big window)
  DEFB 128                ; Lesson 229 (REVISION LIBRARY): 128 (Map Room)
  DEFB 132                ; Lesson 230 (REVISION LIBRARY): 132 (Exam Room)
  DEFB 128                ; Lesson 231 (MR WITHIT - MAP ROOM): 128 (Map Room)
  DEFB 128                ; Lesson 232 (MR WACKER - READING ROOM): 128 (Map
                          ; Room)
  DEFB 128                ; Lesson 233 (MR ROCKITT - READING ROOM): 128 (Map
                          ; Room)
  DEFB 128                ; Lesson 234 (MR CREAK - READING ROOM): 128 (Map
                          ; Room)
  DEFB 128                ; Lesson 235 (MR CREAK - WHITE ROOM): 128 (Map Room)
  DEFB 128                ; Lesson 236 (MR WACKER - WHITE ROOM): 128 (Map Room)
  DEFB 134                ; Lesson 237 (MR WITHIT - WHITE ROOM): 134 (White
                          ; Room)
  DEFB 128                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 128 (Map
                          ; Room)
  DEFB 132                ; Lesson 239 (MR WACKER - MAP ROOM): 132 (Exam Room)
  DEFB 128                ; Lesson 240 (MR WITHIT - MAP ROOM): 128 (Map Room)
  DEFB 196                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 196
                          ; (Walkabout)
  DEFB 128                ; Lesson 242 (MR CREAK - READING ROOM): 128 (Map
                          ; Room)
  DEFB 190                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 190
                          ; (Walkabout)
  DEFB 190                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 190 (Walkabout)
  DEFB 190                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 190 (Walkabout)
  DEFB 190                ; Lesson 246 (PLAYTIME): 190 (Walkabout)
  DEFB 188                ; Lesson 247 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 248 (PLAYTIME): 188 (Staff room)
  DEFB 196                ; Lesson 249 (PLAYTIME): 196 (Walkabout)
  DEFB 188                ; Lesson 250 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 251 (PLAYTIME): 188 (Staff room)
  DEFB 196                ; Lesson 252 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 253 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 188                ; Lesson 254 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 255 (PLAYTIME): 188 (Staff room)

; Skool UDG references (LSBs) for row 14
;
; Used by the routine at PRINTTILE.
  DEFB 9,25,9,0,70,81,0,0,0,0,0,0,119,26,130,137
  DEFB 144,152,0,0,0,25,14,14,14,0,95,0,85,0,98,25
  DEFB 6,11,11,20,20,29,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,106,0,0,106
  DEFB 0,14,0,17,0,20,0,26,0,0,68,78,86,91,20,98
  DEFB 103,111,103,103,129,20,0,0,150,155,160,167,175,0,0,26

; Character buffer for MR CREAK (166)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 14
;
; Used by the routine at RSCROLL.
  DEFB 120,120,120,120,120,120,48,48,48,48,48,48,48,56,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,52,48,50,48,49,48
  DEFB 48,48,48,24,48,48,24,24,24,24,24,24,24,24,24,24
  DEFB 24,24,24,24,24,24,24,24,24,24,24,24,24,48,48,48
  DEFB 48,51,48,50,48,112,112,112,48,48,48,48,48,48,56,48
  DEFB 48,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for MR CREAK (166)
;
; Used by the routine at NEWLESSON.
  DEFB 188                ; Lesson 224 (DINNER (MR WITHIT)): 188 (Staff room)
  DEFB 188                ; Lesson 225 (DINNER (MR WACKER)): 188 (Staff room)
  DEFB 188                ; Lesson 226 (MR WACKER - EXAM ROOM): 188 (Staff
                          ; room)
  DEFB 188                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 188 (Staff
                          ; room)
  DEFB 134                ; Lesson 228 (REVISION LIBRARY): 134 (White Room)
  DEFB 130                ; Lesson 229 (REVISION LIBRARY): 130 (Reading Room)
  DEFB 130                ; Lesson 230 (REVISION LIBRARY): 130 (Reading Room)
                          ; (this is a bug)
  DEFB 130                ; Lesson 231 (MR WITHIT - MAP ROOM): 130 (Reading
                          ; Room)
  DEFB 134                ; Lesson 232 (MR WACKER - READING ROOM): 134 (White
                          ; Room)
  DEFB 134                ; Lesson 233 (MR ROCKITT - READING ROOM): 134 (White
                          ; Room)
  DEFB 130                ; Lesson 234 (MR CREAK - READING ROOM): 130 (Reading
                          ; Room)
  DEFB 134                ; Lesson 235 (MR CREAK - WHITE ROOM): 134 (White
                          ; Room)
  DEFB 130                ; Lesson 236 (MR WACKER - WHITE ROOM): 130 (Reading
                          ; Room)
  DEFB 130                ; Lesson 237 (MR WITHIT - WHITE ROOM): 130 (Reading
                          ; Room)
  DEFB 130                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 130 (Reading
                          ; Room)
  DEFB 134                ; Lesson 239 (MR WACKER - MAP ROOM): 134 (White Room)
  DEFB 134                ; Lesson 240 (MR WITHIT - MAP ROOM): 134 (White Room)
  DEFB 188                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 188 (Staff
                          ; room)
  DEFB 130                ; Lesson 242 (MR CREAK - READING ROOM): 130 (Reading
                          ; Room)
  DEFB 190                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 190
                          ; (Walkabout)
  DEFB 190                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 190 (Walkabout)
  DEFB 190                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 190 (Walkabout)
  DEFB 188                ; Lesson 246 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 247 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 248 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 249 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 250 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 251 (PLAYTIME): 188 (Staff room)
  DEFB 196                ; Lesson 252 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 253 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 188                ; Lesson 254 (PLAYTIME): 188 (Staff room)
  DEFB 188                ; Lesson 255 (PLAYTIME): 188 (Staff room)

; Skool UDG references (LSBs) for row 15
;
; Used by the routine at PRINTTILE.
  DEFB 9,25,40,57,71,82,0,0,0,0,0,0,120,26,26,138
  DEFB 145,153,160,0,0,25,187,148,14,0,0,0,0,0,0,25
  DEFB 6,11,11,20,24,30,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,106,0,0,106
  DEFB 0,0,0,0,0,20,28,35,0,59,69,79,87,57,57,99
  DEFB 103,111,103,103,129,20,0,0,150,155,161,168,176,0,0,26

; Character buffer for BOY WANDER (167)
;
; See the character buffer documentation for details of how the buffer is used.
  DEFS 32

; Skool attribute data for row 15
;
; Used by the routine at RSCROLL.
  DEFB 120,120,120,120,120,120,48,48,48,48,48,48,48,48,56,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,24,48,48,24,24,24,24,24,24,24,24,24,24
  DEFB 24,24,24,24,24,24,24,24,24,24,24,24,24,48,48,48
  DEFB 48,48,48,48,48,112,112,112,48,48,48,48,48,56,48,48
  DEFB 48,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for BOY WANDER (167)
;
; Used by the routine at NEWLESSON.
  DEFB 172                ; Lesson 224 (DINNER (MR WITHIT)): 172 (Dinner)
  DEFB 172                ; Lesson 225 (DINNER (MR WACKER)): 172 (Dinner)
  DEFB 156                ; Lesson 226 (MR WACKER - EXAM ROOM): 156 (Exam Room)
  DEFB 156                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 156 (Exam
                          ; Room)
  DEFB 178                ; Lesson 228 (REVISION LIBRARY): 178 (Revision
                          ; Library)
  DEFB 140                ; Lesson 229 (REVISION LIBRARY): 140 (Map Room)
  DEFB 178                ; Lesson 230 (REVISION LIBRARY): 178 (Revision
                          ; Library)
  DEFB 156                ; Lesson 231 (MR WITHIT - MAP ROOM): 156 (Exam Room)
  DEFB 148                ; Lesson 232 (MR WACKER - READING ROOM): 148 (Reading
                          ; Room)
  DEFB 148                ; Lesson 233 (MR ROCKITT - READING ROOM): 148
                          ; (Reading Room)
  DEFB 148                ; Lesson 234 (MR CREAK - READING ROOM): 148 (Reading
                          ; Room)
  DEFB 164                ; Lesson 235 (MR CREAK - WHITE ROOM): 164 (White
                          ; Room)
  DEFB 178                ; Lesson 236 (MR WACKER - WHITE ROOM): 178 (Revision
                          ; Library)
  DEFB 178                ; Lesson 237 (MR WITHIT - WHITE ROOM): 178 (Revision
                          ; Library)
  DEFB 164                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 164 (White
                          ; Room)
  DEFB 156                ; Lesson 239 (MR WACKER - MAP ROOM): 156 (Exam Room)
  DEFB 140                ; Lesson 240 (MR WITHIT - MAP ROOM): 140 (Map Room)
  DEFB 164                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 164 (White
                          ; Room)
  DEFB 148                ; Lesson 242 (MR CREAK - READING ROOM): 148 (Reading
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 216                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 216 (Collect the pea-shooter)
  DEFB 192                ; Lesson 246 (PLAYTIME): 192 (Write on the boards)
  DEFB 194                ; Lesson 247 (PLAYTIME): 194 (Walkabout)
  DEFB 192                ; Lesson 248 (PLAYTIME): 192 (Write on the boards)
  DEFB 192                ; Lesson 249 (PLAYTIME): 192 (Write on the boards)
  DEFB 192                ; Lesson 250 (PLAYTIME): 192 (Write on the boards)
  DEFB 194                ; Lesson 251 (PLAYTIME): 194 (Walkabout)
  DEFB 194                ; Lesson 252 (PLAYTIME (little boys stampede)): 194
                          ; (Walkabout)
  DEFB 194                ; Lesson 253 (PLAYTIME (little boys stampede)): 194
                          ; (Walkabout)
  DEFB 192                ; Lesson 254 (PLAYTIME): 192 (Write on the boards)
  DEFB 194                ; Lesson 255 (PLAYTIME): 194 (Walkabout)

; Skool UDG references (LSBs) for row 16
;
; Used by the routine at PRINTTILE.
  DEFB 10,26,9,0,70,81,0,0,0,0,0,0,9,125,26,26
  DEFB 146,154,161,170,0,125,188,105,14,0,0,0,0,0,0,25
  DEFB 6,11,12,21,25,31,21,21,21,21,21,21,21,21,21,21
  DEFB 21,21,21,21,21,21,21,73,77,83,21,96,21,113,0,106
  DEFB 0,0,0,0,0,20,26,36,47,60,61,70,57,57,76,26
  DEFB 103,111,103,103,129,134,140,145,150,155,162,169,177,182,0,26

; Character buffer for ANGELFACE (168)
;
; See the character buffer documentation for details of how the buffer is used.
BULLYCBUF:
  DEFS 32

; Skool attribute data for row 16
;
; Used by the routine at RSCROLL.
  DEFB 120,120,120,120,120,120,48,48,48,48,48,48,112,112,112,120
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,48,48,48,120,112,112,112
  DEFB 48,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for ANGELFACE (168)
;
; Used by the routine at NEWLESSON.
  DEFB 174                ; Lesson 224 (DINNER (MR WITHIT)): 174 (Dinner)
  DEFB 174                ; Lesson 225 (DINNER (MR WACKER)): 174 (Dinner)
  DEFB 158                ; Lesson 226 (MR WACKER - EXAM ROOM): 158 (Exam Room)
  DEFB 158                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 158 (Exam
                          ; Room)
  DEFB 166                ; Lesson 228 (REVISION LIBRARY): 166 (White Room)
  DEFB 180                ; Lesson 229 (REVISION LIBRARY): 180 (Revision
                          ; Library)
  DEFB 180                ; Lesson 230 (REVISION LIBRARY): 180 (Revision
                          ; Library)
  DEFB 142                ; Lesson 231 (MR WITHIT - MAP ROOM): 142 (Map Room)
  DEFB 166                ; Lesson 232 (MR WACKER - READING ROOM): 166 (White
                          ; Room)
  DEFB 150                ; Lesson 233 (MR ROCKITT - READING ROOM): 150
                          ; (Reading Room)
  DEFB 150                ; Lesson 234 (MR CREAK - READING ROOM): 150 (Reading
                          ; Room)
  DEFB 166                ; Lesson 235 (MR CREAK - WHITE ROOM): 166 (White
                          ; Room)
  DEFB 166                ; Lesson 236 (MR WACKER - WHITE ROOM): 166 (White
                          ; Room)
  DEFB 180                ; Lesson 237 (MR WITHIT - WHITE ROOM): 180 (Revision
                          ; Library)
  DEFB 180                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 180 (Revision
                          ; Library)
  DEFB 142                ; Lesson 239 (MR WACKER - MAP ROOM): 142 (Map Room)
  DEFB 142                ; Lesson 240 (MR WITHIT - MAP ROOM): 142 (Map Room)
  DEFB 166                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 166 (White
                          ; Room)
  DEFB 150                ; Lesson 242 (MR CREAK - READING ROOM): 150 (Reading
                          ; Room)
  DEFB 220                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 220
                          ; (Mumps walkabout)
  DEFB 196                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 196 (Walkabout)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 182                ; Lesson 246 (PLAYTIME): 182 (Walkabout)
  DEFB 182                ; Lesson 247 (PLAYTIME): 182 (Walkabout)
  DEFB 182                ; Lesson 248 (PLAYTIME): 182 (Walkabout)
  DEFB 182                ; Lesson 249 (PLAYTIME): 182 (Walkabout)
  DEFB 182                ; Lesson 250 (PLAYTIME): 182 (Walkabout)
  DEFB 182                ; Lesson 251 (PLAYTIME): 182 (Walkabout)
  DEFB 182                ; Lesson 252 (PLAYTIME (little boys stampede)): 182
                          ; (Walkabout)
  DEFB 182                ; Lesson 253 (PLAYTIME (little boys stampede)): 182
                          ; (Walkabout)
  DEFB 182                ; Lesson 254 (PLAYTIME): 182 (Walkabout)
  DEFB 182                ; Lesson 255 (PLAYTIME): 182 (Walkabout)

; Skool UDG references (LSBs) for row 17
;
; Used by the routine at PRINTTILE.
  DEFB 9,25,9,0,72,83,0,0,0,0,0,0,9,0,125,26
  DEFB 26,146,154,171,177,0,0,105,14,0,0,0,0,0,0,25
  DEFB 6,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,74,0,0,87,97,107,31,113,106
  DEFB 0,0,0,0,0,20,26,37,48,61,70,57,57,76,0,26
  DEFB 104,112,104,104,130,135,141,146,150,155,163,170,178,183,0,26

; Character buffer for EINSTEIN (169)
;
; See the character buffer documentation for details of how the buffer is used.
SWOTCBUF:
  DEFS 32

; Skool attribute data for row 17
;
; Used by the routine at RSCROLL.
  DEFB 120,120,120,120,120,120,48,48,48,48,48,48,112,112,112,112
  DEFB 120,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,48,48,120,112,112,112,112
  DEFB 48,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for EINSTEIN (169)
;
; Used by the routine at NEWLESSON.
  DEFB 170                ; Lesson 224 (DINNER (MR WITHIT)): 170 (Dinner)
  DEFB 170                ; Lesson 225 (DINNER (MR WACKER)): 170 (Dinner)
  DEFB 160                ; Lesson 226 (MR WACKER - EXAM ROOM): 160 (Exam Room)
  DEFB 160                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 160 (Exam
                          ; Room)
  DEFB 176                ; Lesson 228 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 176                ; Lesson 229 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 176                ; Lesson 230 (REVISION LIBRARY): 176 (Revision
                          ; Library)
  DEFB 144                ; Lesson 231 (MR WITHIT - MAP ROOM): 144 (Map Room)
  DEFB 152                ; Lesson 232 (MR WACKER - READING ROOM): 152 (Reading
                          ; Room)
  DEFB 152                ; Lesson 233 (MR ROCKITT - READING ROOM): 152
                          ; (Reading Room)
  DEFB 152                ; Lesson 234 (MR CREAK - READING ROOM): 152 (Reading
                          ; Room)
  DEFB 168                ; Lesson 235 (MR CREAK - WHITE ROOM): 168 (White
                          ; Room)
  DEFB 168                ; Lesson 236 (MR WACKER - WHITE ROOM): 168 (White
                          ; Room)
  DEFB 168                ; Lesson 237 (MR WITHIT - WHITE ROOM): 168 (White
                          ; Room)
  DEFB 168                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 168 (White
                          ; Room)
  DEFB 144                ; Lesson 239 (MR WACKER - MAP ROOM): 144 (Map Room)
  DEFB 144                ; Lesson 240 (MR WITHIT - MAP ROOM): 144 (Map Room)
  DEFB 168                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 168 (White
                          ; Room)
  DEFB 152                ; Lesson 242 (MR CREAK - READING ROOM): 152 (Reading
                          ; Room)
  DEFB 196                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 196
                          ; (Walkabout)
  DEFB 212                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 212 (Grass on ERIC)
  DEFB 196                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 196 (Walkabout)
  DEFB 196                ; Lesson 246 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 247 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 248 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 249 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 250 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 251 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 252 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 253 (PLAYTIME (little boys stampede)): 196
                          ; (Walkabout)
  DEFB 196                ; Lesson 254 (PLAYTIME): 196 (Walkabout)
  DEFB 196                ; Lesson 255 (PLAYTIME): 196 (Walkabout)

; Skool UDG references (LSBs) for row 18
;
; Used by the routine at PRINTTILE.
  DEFB 11,27,41,58,70,81,0,0,0,0,0,0,9,0,0,125
  DEFB 26,26,162,172,178,0,0,105,14,0,0,0,0,0,0,25
  DEFB 7,0,0,0,26,32,32,32,32,32,32,32,32,32,32,32
  DEFB 65,0,0,0,0,0,0,75,78,84,88,98,108,0,31,120
  DEFB 0,0,0,0,0,20,26,38,49,62,57,57,76,0,0,26
  DEFB 1,46,1,1,131,136,142,147,151,155,0,0,179,20,0,26

; Character buffer for BOY WANDER's pellet (170)
;
; See the character buffer documentation for details of how the buffer is used.
BWPELCBUF:
  DEFS 32

; Skool attribute data for row 18
;
; Used by the routine at RSCROLL.
  DEFB 120,120,120,120,120,120,48,48,48,48,48,48,112,112,112,112
  DEFB 112,120,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,48,120,112,112,112,112,112
  DEFB 48,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for BOY WANDER's pellet (170)
;
; Used by the routine at NEWLESSON.
  DEFB 138                ; Lesson 224 (DINNER (MR WITHIT)): 138 (Do nothing)
  DEFB 138                ; Lesson 225 (DINNER (MR WACKER)): 138 (Do nothing)
  DEFB 138                ; Lesson 226 (MR WACKER - EXAM ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 228 (REVISION LIBRARY): 138 (Do nothing)
  DEFB 138                ; Lesson 229 (REVISION LIBRARY): 138 (Do nothing)
  DEFB 138                ; Lesson 230 (REVISION LIBRARY): 138 (Do nothing)
  DEFB 138                ; Lesson 231 (MR WITHIT - MAP ROOM): 138 (Do nothing)
  DEFB 138                ; Lesson 232 (MR WACKER - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 233 (MR ROCKITT - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 234 (MR CREAK - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 235 (MR CREAK - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 236 (MR WACKER - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 237 (MR WITHIT - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 239 (MR WACKER - MAP ROOM): 138 (Do nothing)
  DEFB 138                ; Lesson 240 (MR WITHIT - MAP ROOM): 138 (Do nothing)
  DEFB 138                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 242 (MR CREAK - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 138
                          ; (Do nothing)
  DEFB 138                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 138 (Do nothing)
  DEFB 138                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 138 (Do nothing)
  DEFB 138                ; Lesson 246 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 247 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 248 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 249 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 250 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 251 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 252 (PLAYTIME (little boys stampede)): 138
                          ; (Do nothing)
  DEFB 138                ; Lesson 253 (PLAYTIME (little boys stampede)): 138
                          ; (Do nothing)
  DEFB 138                ; Lesson 254 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 255 (PLAYTIME): 138 (Do nothing)

; Skool UDG references (LSBs) for row 19
;
; Used by the routine at PRINTTILE.
  DEFB 9,25,9,59,73,84,43,43,43,43,43,43,9,0,0,0
  DEFB 125,26,26,173,179,43,43,192,197,43,43,43,43,43,43,43
  DEFB 1,1,1,1,27,33,37,37,49,37,37,37,53,37,37,59
  DEFB 1,1,1,1,1,1,1,1,79,85,89,99,109,1,1,1
  DEFB 1,1,1,1,1,22,29,39,50,57,57,76,0,0,0,26
  DEFB 1,1,1,1,1,1,1,1,1,1,164,171,180,184,0,26

; Character buffer for ERIC's pellet (171)
;
; See the character buffer documentation for details of how the buffer is used.
ERICPELCBUF:
  DEFS 32

; Skool attribute data for row 19
;
; Used by the routine at RSCROLL.
  DEFB 56,56,56,56,56,56,48,48,48,48,48,48,112,112,112,112
  DEFB 112,112,120,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,120,112,112,112,112,112,112
  DEFB 48,48,48,48,48,112,112,112,112,48,112,112,112,112,112,112

; Personal timetable for ERIC's pellet (171)
;
; Used by the routine at NEWLESSON.
  DEFB 138                ; Lesson 224 (DINNER (MR WITHIT)): 138 (Do nothing)
  DEFB 138                ; Lesson 225 (DINNER (MR WACKER)): 138 (Do nothing)
  DEFB 138                ; Lesson 226 (MR WACKER - EXAM ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 227 (MR ROCKITT - EXAM ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 228 (REVISION LIBRARY): 138 (Do nothing)
  DEFB 138                ; Lesson 229 (REVISION LIBRARY): 138 (Do nothing)
  DEFB 138                ; Lesson 230 (REVISION LIBRARY): 138 (Do nothing)
  DEFB 138                ; Lesson 231 (MR WITHIT - MAP ROOM): 138 (Do nothing)
  DEFB 138                ; Lesson 232 (MR WACKER - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 233 (MR ROCKITT - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 234 (MR CREAK - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 235 (MR CREAK - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 236 (MR WACKER - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 237 (MR WITHIT - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 238 (MR ROCKITT - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 239 (MR WACKER - MAP ROOM): 138 (Do nothing)
  DEFB 138                ; Lesson 240 (MR WITHIT - MAP ROOM): 138 (Do nothing)
  DEFB 138                ; Lesson 241 (MR ROCKITT - WHITE ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 242 (MR CREAK - READING ROOM): 138 (Do
                          ; nothing)
  DEFB 138                ; Lesson 243 (PLAYTIME (ANGELFACE has mumps)): 138
                          ; (Do nothing)
  DEFB 138                ; Lesson 244 (PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)): 138 (Do nothing)
  DEFB 138                ; Lesson 245 (PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)): 138 (Do nothing)
  DEFB 138                ; Lesson 246 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 247 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 248 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 249 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 250 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 251 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 252 (PLAYTIME (little boys stampede)): 138
                          ; (Do nothing)
  DEFB 138                ; Lesson 253 (PLAYTIME (little boys stampede)): 138
                          ; (Do nothing)
  DEFB 138                ; Lesson 254 (PLAYTIME): 138 (Do nothing)
  DEFB 138                ; Lesson 255 (PLAYTIME): 138 (Do nothing)

; Skool UDG references (LSBs) for row 20
;
; Used by the routine at PRINTTILE.
  DEFB 12,28,42,43,43,43,43,43,43,43,43,43,43,43,43,43
  DEFB 43,43,43,43,43,43,43,193,43,43,43,43,43,43,43,43
  DEFB 1,1,1,1,1,34,38,38,38,38,38,38,38,38,38,60
  DEFB 1,1,1,1,1,1,1,1,80,1,80,100,110,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
  DEFB 1,1,1,1,1,1,1,1,1,1,1,1,1,185,164,191

; Character buffer for ERIC (172)
;
; See the character buffer documentation for details of how the buffer is used.
ERICCBUF:
  DEFS 32

; Skool attribute data for row 20
;
; Used by the routine at RSCROLL.
  DEFB 56,56,56,56,56,56,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48
  DEFB 48,48,48,48,48,112,112,48,48,48,48,48,48,112,48,48
  DEFB 48,48,48,48,48,112,112,112,112,112,112,112,112,112,112,112

; Lesson descriptors
;
; When a lesson starts, the routine at NEWLESSON picks up the lesson descriptor
; from this table and copies it into LESSONDESC. Each entry in this table
; defines the teacher for the period (if any), and the room. The teacher is
; identified by bits 4-7:
;
; +------+------------+
; | Bits | Teacher    |
; +------+------------+
; | 0000 | MR WACKER  |
; | 0001 | MR ROCKITT |
; | 0010 | MR WITHIT  |
; | 0011 | MR CREAK   |
; | 0100 | None       |
; +------+------------+
;
; The room is identified by bits 0-3:
;
; +------+------------------+
; | Bits | Room             |
; +------+------------------+
; | 0001 | READING ROOM     |
; | 0010 | MAP ROOM         |
; | 0011 | WHITE ROOM       |
; | 0100 | EXAM ROOM        |
; | 0101 | REVISION LIBRARY |
; | 0110 | DINNER           |
; | 0111 | PLAYTIME         |
; +------+------------------+
LDESCS:
  DEFB 38                 ; Lesson 224: DINNER (MR WITHIT)
  DEFB 6                  ; Lesson 225: DINNER (MR WACKER)
  DEFB 4                  ; Lesson 226: MR WACKER - EXAM ROOM
  DEFB 20                 ; Lesson 227: MR ROCKITT - EXAM ROOM
  DEFB 69                 ; Lesson 228: REVISION LIBRARY
  DEFB 69                 ; Lesson 229: REVISION LIBRARY
  DEFB 69                 ; Lesson 230: REVISION LIBRARY
  DEFB 34                 ; Lesson 231: MR WITHIT - MAP ROOM
  DEFB 1                  ; Lesson 232: MR WACKER - READING ROOM
  DEFB 17                 ; Lesson 233: MR ROCKITT - READING ROOM
  DEFB 49                 ; Lesson 234: MR CREAK - READING ROOM
  DEFB 51                 ; Lesson 235: MR CREAK - WHITE ROOM
  DEFB 3                  ; Lesson 236: MR WACKER - WHITE ROOM
  DEFB 35                 ; Lesson 237: MR WITHIT - WHITE ROOM
  DEFB 19                 ; Lesson 238: MR ROCKITT - WHITE ROOM
  DEFB 2                  ; Lesson 239: MR WACKER - MAP ROOM
  DEFB 34                 ; Lesson 240: MR WITHIT - MAP ROOM
  DEFB 19                 ; Lesson 241: MR ROCKITT - WHITE ROOM
  DEFB 49                 ; Lesson 242: MR CREAK - READING ROOM
  DEFB 71                 ; Lesson 243: PLAYTIME (ANGELFACE has mumps)
  DEFB 71                 ; Lesson 244: PLAYTIME (EINSTEIN is going to grass on
                          ; ERIC)
  DEFB 71                 ; Lesson 245: PLAYTIME (BOY WANDER has hidden a
                          ; pea-shooter)
  DEFB 71                 ; Lesson 246: PLAYTIME
  DEFB 71                 ; Lesson 247: PLAYTIME
  DEFB 71                 ; Lesson 248: PLAYTIME
  DEFB 71                 ; Lesson 249: PLAYTIME
  DEFB 71                 ; Lesson 250: PLAYTIME
  DEFB 71                 ; Lesson 251: PLAYTIME
  DEFB 71                 ; Lesson 252: PLAYTIME (little boys stampede)
  DEFB 71                 ; Lesson 253: PLAYTIME (little boys stampede)
  DEFB 71                 ; Lesson 254: PLAYTIME
  DEFB 71                 ; Lesson 255: PLAYTIME

; UDG references for animatory states 0-103 at row 0, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 44292       |
; | 14-15    | 44302-44303 |
; | 20       | 44308       |
; | 24-31    | 44312-44319 |
; | 36       | 44324       |
; | 40-41    | 44328-44329 |
; | 44-47    | 44332-44335 |
; | 52       | 44340       |
; | 58-59    | 44346-44347 |
; | 62       | 44350       |
; | 68       | 44356       |
; | 76       | 44364       |
; | 78       | 44366       |
; | 84       | 44372       |
; | 86       | 44374       |
; | 92       | 44380       |
; | 94       | 44382       |
; | 100      | 44388       |
; | 102      | 44390       |
; +----------+-------------+
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,71,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,71,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,71,0,0
  DEFB 0,0,0,0,0,0,0,0,0,102,0,102,116,116,0,0
  DEFB 0,90,0,90,76,76,0,0,0,9,0,9,0,0,0,0
  DEFB 0,39,0,39,0,0,0,0

; 'CAST OF CHARACTERS'
;
; Used by the routine at GETNAMES.
M_CAST:
  DEFM "CAST OF CHARACTERS"
  DEFB 0                  ; End marker

; Unused
  DEFS 4

; UDG references for animatory states 127-231 at row 0, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 44420       |
; | 142-143  | 44430-44431 |
; | 148      | 44436       |
; | 152-159  | 44440-44447 |
; | 164      | 44452       |
; | 168-169  | 44456-44457 |
; | 172-175  | 44460-44463 |
; | 180      | 44468       |
; | 186-187  | 44474-44475 |
; | 190      | 44478       |
; | 196      | 44484       |
; | 204      | 44492       |
; | 206      | 44494       |
; | 212      | 44500       |
; | 214      | 44502       |
; | 220      | 44508       |
; | 222      | 44510       |
; | 228      | 44516       |
; | 230      | 44518       |
; +----------+-------------+
  DEFB 0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,187,232,187,232,187,187,0,0
  DEFB 228,0,228,0,228,228,0,0,130,0,130,0,130,130,0,0
  DEFB 158,228,158,228,158,158,0,0

; Insert a pixel column into the message graphic buffer
;
; Used by the routines at TXT2LBUF and PREPMSG2. Slides the current contents of
; the message graphic buffer (at 23296) one pixel to the left, and places the
; new pixel column into the slot freed up on the right.
;
; A Pixel column byte
SLIDECOL:
  EXX
  LD HL,23551             ; Point HL' at the end of the graphic buffer at 23296
  LD C,8                  ; There are 8 pixel rows in the graphic buffer
SLIDECOL_0:
  LD B,32                 ; Append a pixel to the row and slide all the other
  RRCA                    ; pixels one space to the left
SLIDECOL_1:
  RL (HL)                 ;
  DEC HL                  ;
  DJNZ SLIDECOL_1         ;
  DEC C                   ; Next pixel row
  JR NZ,SLIDECOL_0        ; Jump back until all 8 pixels rows are done
  EXX
  RET

; Unused
  DEFS 4

; UDG reference for animatory state 255 at row 0, column 0
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 1, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 44548       |
; | 14-15    | 44558-44559 |
; | 20       | 44564       |
; | 24-31    | 44568-44575 |
; | 36       | 44580       |
; | 40-41    | 44584-44585 |
; | 44-47    | 44588-44591 |
; | 52       | 44596       |
; | 58-59    | 44602-44603 |
; | 62       | 44606       |
; | 68       | 44612       |
; | 76       | 44620       |
; | 78       | 44622       |
; | 84       | 44628       |
; | 86       | 44630       |
; | 92       | 44636       |
; | 94       | 44638       |
; | 100      | 44644       |
; | 102      | 44646       |
; +----------+-------------+
  DEFB 38,7,38,7,38,38,0,0,38,57,38,66,69,72,0,0
  DEFB 0,33,0,33,0,0,0,0,0,23,0,17,0,23,0,0
  DEFB 38,41,38,41,38,38,0,0,0,0,38,66,69,72,0,0
  DEFB 38,47,38,47,38,38,0,0,38,57,0,0,69,72,0,0
  DEFB 0,117,0,117,0,0,0,0,0,105,0,105,117,117,120,120
  DEFB 99,88,99,88,75,75,0,0,0,11,0,11,25,25,0,0
  DEFB 31,42,31,42,55,55,0,0

; 'PRESS'C'TO CHANGE NAME'
;
; Used by the routine at PRTCPROMPT.
M_CTOCHANGE:
  DEFM "PRESS'C'TO CHANGE NAME"
  DEFB 0                  ; End marker

; UDG references for animatory states 127-231 at row 1, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 44676       |
; | 142-143  | 44686-44687 |
; | 148      | 44692       |
; | 152-159  | 44696-44703 |
; | 164      | 44708       |
; | 168-169  | 44712-44713 |
; | 172-175  | 44716-44719 |
; | 180      | 44724       |
; | 186-187  | 44730-44731 |
; | 190      | 44734       |
; | 196      | 44740       |
; | 204      | 44748       |
; | 206      | 44750       |
; | 212      | 44756       |
; | 214      | 44758       |
; | 220      | 44764       |
; | 222      | 44766       |
; | 228      | 44772       |
; | 230      | 44774       |
; +----------+-------------+
  DEFB 0
  DEFB 130,0,130,0,130,130,0,0,130,130,130,130,130,130,0,0
  DEFB 167,0,167,0,167,167,0,0,0,0,0,0,0,0,0,0
  DEFB 130,0,130,0,130,130,0,0,0,0,130,130,130,130,0,0
  DEFB 130,0,130,0,130,130,0,0,130,130,0,0,130,130,0,0
  DEFB 0,0,0,0,0,0,0,0,189,235,189,235,189,189,250,250
  DEFB 225,228,225,228,225,225,200,200,132,141,132,141,132,132,255,255
  DEFB 161,172,161,172,161,161,169,169

; Insert message text into the message graphic buffer
;
; Used by the routine at PREPMSG2. Generates the graphic data for the text of
; the message and inserts it into the buffer at 23296. Returns with the message
; right-aligned in the buffer, and with C holding the number of empty pixel
; columns remaining on the left.
;
; C 254
; DE Message address
TXT2LBUF:
  LD A,(DE)               ; A=ASCII code of a character in the message
  INC DE                  ; Point DE at the next character in the message
  AND A                   ; Have we reached the end of the message?
  RET Z                   ; Return if so
  LD L,A                  ; B=width (in pixels) of the bitmap for this font
  LD H,217                ; character
  LD B,(HL)               ;
TXT2LBUF_0:
  INC H                   ; Pick up a pixel column for the font character and
  LD A,(HL)               ; slide it into the graphic buffer
  CALL SLIDECOL           ;
  DEC C                   ; Decrease the pixel column count
  DJNZ TXT2LBUF_0         ; Jump back until the entire font character has been
                          ; done
  XOR A                   ; Insert a blank pixel column between characters
  DEC C                   ;
  CALL SLIDECOL           ;
  JR TXT2LBUF             ; Continue until the message is finished

; UDG reference for animatory state 255 at row 1, column 0
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 2, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 44804       |
; | 14-15    | 44814-44815 |
; | 20       | 44820       |
; | 24-31    | 44824-44831 |
; | 36       | 44836       |
; | 40-41    | 44840-44841 |
; | 44-47    | 44844-44847 |
; | 52       | 44852       |
; | 58-59    | 44858-44859 |
; | 62       | 44862       |
; | 68       | 44868       |
; | 76       | 44876       |
; | 78       | 44878       |
; | 84       | 44884       |
; | 86       | 44886       |
; | 92       | 44892       |
; | 94       | 44894       |
; | 100      | 44900       |
; | 102      | 44902       |
; +----------+-------------+
  DEFB 3,8,3,8,17,17,0,0,52,55,60,64,70,70,0,0
  DEFB 3,8,3,8,17,17,0,0,0,24,0,28,0,24,0,0
  DEFB 3,8,3,8,17,17,0,0,0,0,60,64,70,70,0,0
  DEFB 3,8,3,8,17,17,0,0,52,55,0,0,70,70,0,0
  DEFB 120,115,120,115,112,112,108,0,0,108,0,108,0,0,123,123
  DEFB 96,86,96,86,96,96,0,0,0,14,0,14,27,27,0,0
  DEFB 205,45,205,45,0,0,0,0

; 'ENTER NEW NAME'
;
; Used by the routine at CHANGENAME.
M_ENTERNAME:
  DEFM "ENTER NEW NAME"
  DEFB 0                  ; End marker

; Prepare a character to be walked onto the screen (1)
;
; Used by the routine at WALKCHAR4.
;
; A Character's animatory state
; HL 39008 (96, 152)
SETASY:
  LD DE,46572
  CP 200                  ; Reset the carry flag if the animatory state is >=
                          ; 200 (i.e. if we're dealing with a teacher)
  JP SETASY2

; UDG references for animatory states 127-231 at row 2, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 44932       |
; | 142-143  | 44942-44943 |
; | 148      | 44948       |
; | 152-159  | 44952-44959 |
; | 164      | 44964       |
; | 168-169  | 44968-44969 |
; | 172-175  | 44972-44975 |
; | 180      | 44980       |
; | 186-187  | 44986-44987 |
; | 190      | 44990       |
; | 196      | 44996       |
; | 204      | 45004       |
; | 206      | 45006       |
; | 212      | 45012       |
; | 214      | 45014       |
; | 220      | 45020       |
; | 222      | 45022       |
; | 228      | 45028       |
; | 230      | 45030       |
; +----------+-------------+
  DEFB 0
  DEFB 133,0,133,0,146,146,151,0,182,184,133,133,133,133,0,0
  DEFB 133,0,133,0,146,146,164,0,0,0,0,0,0,0,0,0
  DEFB 133,0,133,0,146,146,172,0,0,0,133,133,133,133,0,0
  DEFB 133,0,133,0,146,146,178,0,182,184,0,0,133,133,0,0
  DEFB 246,0,246,0,0,0,0,0,191,238,191,238,191,191,253,253
  DEFB 222,212,222,212,222,222,198,198,134,144,134,144,134,134,202,202
  DEFB 164,175,164,175,164,164,180,180

; Print a message centred across the entire screen
;
; Used by the routines at INPUTDEV, WALKCHAR, WALKCHAR2, PRTCPROMPT, GETKEY,
; GETNAMES and ASKNAMES.
;
; DE Display file address
; HL Message address
PRINTMSG:
  EX DE,HL                ; Now DE=message address, HL=display file address
  PUSH HL                 ; Save the display file address
  CALL PREPMSG            ; Prepare the message in the buffer at 23296
  POP DE                  ; Restore the display file address to DE
  LD HL,23296             ; Transfer the message from the buffer to the screen
PRINTMSG_0:
  LD C,32                 ;
  PUSH DE                 ;
  LDIR                    ;
  POP DE                  ;
  INC D                   ;
  BIT 1,H                 ;
  JR NZ,PRINTMSG_0        ;
  RET

; Unused
  DEFS 2

; UDG reference for animatory state 255 at row 2, column 0
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 3, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 45060       |
; | 14-15    | 45070-45071 |
; | 20       | 45076       |
; | 24-31    | 45080-45087 |
; | 36       | 45092       |
; | 40-41    | 45096-45097 |
; | 44-47    | 45100-45103 |
; | 52       | 45108       |
; | 58-59    | 45114-45115 |
; | 62       | 45118       |
; | 68       | 45124       |
; | 76       | 45132       |
; | 78       | 45134       |
; | 84       | 45140       |
; | 86       | 45142       |
; | 92       | 45148       |
; | 94       | 45150       |
; | 100      | 45156       |
; | 102      | 45158       |
; +----------+-------------+
  DEFB 0,11,0,14,19,19,24,27,0,0,62,62,0,0,0,0
  DEFB 0,11,0,14,19,19,24,27,0,25,0,29,0,25,0,0
  DEFB 0,11,0,14,19,19,24,27,0,0,62,62,0,0,0,0
  DEFB 0,11,0,14,19,19,24,27,0,0,0,0,0,0,0,27
  DEFB 0,126,0,123,110,110,106,104,0,111,0,111,0,0,69,69
  DEFB 93,83,93,79,93,93,69,69,0,17,0,22,0,0,69,69
  DEFB 0,48,0,53,0,0,69,69

; 'OUR HERO'
;
; Used by the routine at WALKCHAR.
M_OURHERO:
  DEFM "OUR HERO"
  DEFB 0                  ; End marker

; Unused
  DEFB 0

; Get the LSB of the message address for a main character's name
;
; Used by the routine at WALKCHAR. Returns with the LSB in L.
;
; B 1-8 (corresponding to one of the eight main characters)
NAMELSB:
  LD A,8                  ; C=8-B
  SUB B                   ;
  LD C,A                  ;
  ADD A,A                 ; A=8*(8-B)
  ADD A,A                 ;
  ADD A,A                 ;
  SUB C                   ; A=7*(8-B)
  ADD A,A                 ; A=14*(8-B)
  ADD A,128               ; A=128+14*(8-B)
  LD L,A                  ; Place this LSB in L
  RET

; UDG references for animatory states 127-231 at row 3, column 0
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 45188       |
; | 142-143  | 45198-45199 |
; | 148      | 45204       |
; | 152-159  | 45208-45215 |
; | 164      | 45220       |
; | 168-169  | 45224-45225 |
; | 172-175  | 45228-45231 |
; | 180      | 45236       |
; | 186-187  | 45242-45243 |
; | 190      | 45246       |
; | 196      | 45252       |
; | 204      | 45260       |
; | 206      | 45262       |
; | 212      | 45268       |
; | 214      | 45270       |
; | 220      | 45276       |
; | 222      | 45278       |
; | 228      | 45284       |
; | 230      | 45286       |
; +----------+-------------+
  DEFB 0
  DEFB 0,0,0,0,0,0,154,157,0,0,133,133,0,0,0,0
  DEFB 0,0,0,0,0,0,154,165,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,154,173,0,0,133,133,0,0,0,0
  DEFB 0,0,0,0,0,0,154,179,0,0,0,0,0,0,0,179
  DEFB 0,0,0,0,0,0,0,230,193,241,243,241,193,193,195,195
  DEFB 219,141,219,205,219,219,195,195,136,147,149,152,136,136,195,195
  DEFB 166,178,166,141,166,166,195,195

; Print a character's title and name, and walk him onto the screen (1)
;
; Used by the routine at GETNAMES2.
;
; B 1-8 (corresponding to one of the eight main characters)
WALKCHAR:
  LD A,184                ; Set HL to the address of the appropriate character
  SUB B                   ; title message, e.g. M_OURHERO: 'OUR HERO'
  LD H,A                  ;
  LD L,104                ;
  LD DE,16608             ; This is the display file address at which to print
                          ; the character's title
  PUSH BC
  CALL PRINTMSG           ; Print the character's title
  POP BC
  CALL NAMELSB            ; Set HL to the message address for the character's
  LD H,217                ; name
  PUSH HL                 ; Save this message address
  JP WALKCHAR2

; UDG reference for animatory state 255 at row 3, column 0
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 0, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 45316       |
; | 14-15    | 45326-45327 |
; | 20       | 45332       |
; | 24-31    | 45336-45343 |
; | 36       | 45348       |
; | 40-41    | 45352-45353 |
; | 44-47    | 45356-45359 |
; | 52       | 45364       |
; | 58-59    | 45370-45371 |
; | 62       | 45374       |
; | 68       | 45380       |
; | 76       | 45388       |
; | 78       | 45390       |
; | 84       | 45396       |
; | 86       | 45398       |
; | 92       | 45404       |
; | 94       | 45406       |
; | 100      | 45412       |
; | 102      | 45414       |
; +----------+-------------+
  DEFB 0,0,0,0,21,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,21,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,21,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,21,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,58,103,58,103,58,58,0,0
  DEFB 101,89,101,89,101,101,0,0,1,10,1,10,1,1,0,0
  DEFB 29,40,29,40,29,29,0,0

; 'THE TEARAWAY'
;
; Used by the routine at WALKCHAR.
M_TEARAWAY:
  DEFM "THE TEARAWAY"
  DEFB 0                  ; End marker

; Prepare a character to be walked onto the screen (2)
;
; Continues from SETASY. On entry here, the carry flag is set if the character
; to be walked onto the screen is a boy.
;
; DE 46572
; HL 39008 (96, 152)
SETASY2:
  JR C,SETASY2_0          ; Jump if we are dealing with a boy
  LD H,164                ; 164=MR ROCKITT
SETASY2_0:
  EX DE,HL                ; Change the instruction at CHARBUF in the routine at
  LD (HL),D               ; WALKONOFF to LD H,152 (boy) or LD H,164 (teacher)
  EX DE,HL                ; as appropriate
  JP SETASY3

; UDG references for animatory states 127-231 at row 0, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 45444       |
; | 142-143  | 45454-45455 |
; | 148      | 45460       |
; | 152-159  | 45464-45471 |
; | 164      | 45476       |
; | 168-169  | 45480-45481 |
; | 172-175  | 45484-45487 |
; | 180      | 45492       |
; | 186-187  | 45498-45499 |
; | 190      | 45502       |
; | 196      | 45508       |
; | 204      | 45516       |
; | 206      | 45518       |
; | 212      | 45524       |
; | 214      | 45526       |
; | 220      | 45532       |
; | 222      | 45534       |
; | 228      | 45540       |
; | 230      | 45542       |
; +----------+-------------+
  DEFB 0
  DEFB 0,0,0,0,149,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,149,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,149,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,149,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,186,231,186,231,186,186,0,0
  DEFB 229,217,229,217,229,229,0,0,129,138,129,138,129,129,0,0
  DEFB 157,168,157,168,157,157,0,0

; Print a character's title and name, and walk him onto the screen (2)
;
; Continues from WALKCHAR. Prints the character's name followed by a space.
;
; B 1-8 (corresponding to one of the eight main characters)
; HL Message address for the character's name
WALKCHAR2:
  PUSH BC
  LD DE,20512             ; This is the display file address at which to print
                          ; the character's name
  CALL PRINTMSG           ; Print the character's name
  LD HL,M_SPACE           ; M_SPACE: ' '
  CALL PRTNAME            ; Print a space
  LD HL,BOY01CBUF         ; Point HL at byte 96 of little boy no. 1's buffer
  POP BC
  LD A,B                  ; A=1-8 (corresponding to one of the eight main
                          ; characters)
  NOP
  NOP
  JP WALKCHAR3

; UDG reference for animatory state 255 at row 0, column 1
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 1, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 45572       |
; | 14-15    | 45582-45583 |
; | 20       | 45588       |
; | 24-31    | 45592-45599 |
; | 36       | 45604       |
; | 40-41    | 45608-45609 |
; | 44-47    | 45612-45615 |
; | 52       | 45620       |
; | 58-59    | 45626-45627 |
; | 62       | 45630       |
; | 68       | 45636       |
; | 76       | 45644       |
; | 78       | 45646       |
; | 84       | 45652       |
; | 86       | 45654       |
; | 92       | 45660       |
; | 94       | 45662       |
; | 100      | 45668       |
; | 102      | 45670       |
; +----------+-------------+
  DEFB 1,9,1,9,1,1,0,0,1,58,1,68,1,1,0,0
  DEFB 32,34,32,34,32,32,0,0,0,0,0,20,0,0,0,0
  DEFB 40,42,40,42,40,40,0,0,0,0,40,67,40,40,0,0
  DEFB 46,48,46,48,46,46,0,0,46,59,0,0,46,46,0,0
  DEFB 121,116,121,116,113,113,0,0,60,106,60,106,118,118,121,121
  DEFB 98,87,98,87,98,98,73,73,3,12,3,12,26,26,66,66
  DEFB 32,43,32,43,56,56,81,81

; 'THE BULLY'
;
; Used by the routine at WALKCHAR.
M_BULLY:
  DEFM "THE BULLY"
  DEFB 0                  ; End marker

; Prepare a message in the message graphic buffer (1)
;
; Used by the routine at PRINTMSG. Clears the buffer at 23296 in preparation
; for the message.
;
; DE Message address
PREPMSG:
  XOR A                   ; Clear the buffer at 23296 in preparation for the
  LD HL,23296             ; message
PREPMSG_0:
  LD (HL),A               ;
  INC L                   ;
  JR NZ,PREPMSG_0         ;
  LD C,254                ; Initialise the pixel column counter
  JP PREPMSG2

; UDG references for animatory states 127-231 at row 1, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 45700       |
; | 142-143  | 45710-45711 |
; | 148      | 45716       |
; | 152-159  | 45720-45727 |
; | 164      | 45732       |
; | 168-169  | 45736-45737 |
; | 172-175  | 45740-45743 |
; | 180      | 45748       |
; | 186-187  | 45754-45755 |
; | 190      | 45758       |
; | 196      | 45764       |
; | 204      | 45772       |
; | 206      | 45774       |
; | 212      | 45780       |
; | 214      | 45782       |
; | 220      | 45788       |
; | 222      | 45790       |
; | 228      | 45796       |
; | 230      | 45798       |
; +----------+-------------+
  DEFB 73
  DEFB 129,137,129,137,129,129,0,0,129,186,129,196,129,129,0,0
  DEFB 160,162,160,162,160,160,0,0,0,0,0,148,0,0,0,0
  DEFB 168,170,168,170,168,168,0,0,0,0,168,195,168,168,0,0
  DEFB 174,176,174,176,174,174,0,0,174,187,0,0,174,174,0,0
  DEFB 249,244,249,244,241,241,0,0,188,234,188,234,246,246,249,249
  DEFB 226,215,226,215,226,226,201,201,131,140,131,140,154,154,194,194
  DEFB 160,171,160,171,184,184,209,209

; Print a character's title and name, and walk him onto the screen (3)
;
; Continues from WALKCHAR2. Determines the animatory state of the character to
; be walked onto the screen.
;
; A 1-8 (corresponding to one of the eight main characters)
; HL 39008 (96, 152)
WALKCHAR3:
  SUB 5                   ; Are we dealing with one of the teachers?
  JR C,WALKCHAR3_0        ; Jump if so
  ADD A,9                 ; A=9, 10, 11, 12
  AND 11                  ; A=9, 10, 11, 8
  ADD A,A                 ; A=18, 20, 22, 16
  JR WALKCHAR3_1
WALKCHAR3_0:
  CPL                     ; A=0, 1, 2, 3
  INC A                   ; A=1, 2, 3, 4
  AND 3                   ; A=1, 2, 3, 0
  ADD A,25                ; A=26, 27, 28, 25
WALKCHAR3_1:
  RLCA                    ; A=208 (MR WACKER), 216 (MR ROCKITT), 224 (MR
  RLCA                    ; WITHIT), 200 (MR CREAK), 144 (EINSTEIN), 160
  RLCA                    ; (ANGELFACE), 176 (BOY WANDER), or 128 (ERIC)
  JP WALKCHAR4

; UDG reference for animatory state 255 at row 1, column 1
;
; Used by the routine at PRINTTILE.
  DEFB 201

; UDG references for animatory states 0-103 at row 2, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 45828       |
; | 14-15    | 45838-45839 |
; | 20       | 45844       |
; | 24-31    | 45848-45855 |
; | 36       | 45860       |
; | 40-41    | 45864-45865 |
; | 44-47    | 45868-45871 |
; | 52       | 45876       |
; | 58-59    | 45882-45883 |
; | 62       | 45886       |
; | 68       | 45892       |
; | 76       | 45900       |
; | 78       | 45902       |
; | 84       | 45908       |
; | 86       | 45910       |
; | 92       | 45916       |
; | 94       | 45918       |
; | 100      | 45924       |
; | 102      | 45926       |
; +----------+-------------+
  DEFB 4,10,4,10,16,16,22,0,53,53,61,65,4,4,0,0
  DEFB 30,31,30,31,16,16,35,0,0,26,0,30,0,26,0,0
  DEFB 4,10,4,10,16,16,43,0,0,0,61,65,4,4,0,0
  DEFB 4,10,4,10,16,16,49,0,53,53,0,0,4,4,0,0
  DEFB 119,114,119,114,111,111,107,0,62,109,62,109,119,119,124,124
  DEFB 95,85,95,85,95,95,71,71,5,15,5,15,28,28,126,126
  DEFB 35,46,35,46,57,57,34,34

; 'THE SWOT'
;
; Used by the routine at WALKCHAR.
M_SWOT:
  DEFM "THE SWOT"
  DEFB 0                  ; End marker

; Prepare a message in the message graphic buffer (2)
;
; Continues from PREPMSG. Generates the graphic data for the text of the
; message and centres it in the buffer at 23296.
;
; C 254
; DE Message address
PREPMSG2:
  CALL TXT2LBUF           ; Slide the message text into the graphic buffer
  SRL C                   ; Halve the number of remaining pixel columns and add
  INC C                   ; one
PREPMSG2_0:
  XOR A                   ; Insert this number of blank pixel columns, thus
  CALL SLIDECOL           ; centring the message
  DEC C                   ;
  JR NZ,PREPMSG2_0        ;
  RET

; UDG references for animatory states 127-231 at row 2, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 45956       |
; | 142-143  | 45966-45967 |
; | 148      | 45972       |
; | 152-159  | 45976-45983 |
; | 164      | 45988       |
; | 168-169  | 45992-45993 |
; | 172-175  | 45996-45999 |
; | 180      | 46004       |
; | 186-187  | 46010-46011 |
; | 190      | 46014       |
; | 196      | 46020       |
; | 204      | 46028       |
; | 206      | 46030       |
; | 212      | 46036       |
; | 214      | 46038       |
; | 220      | 46044       |
; | 222      | 46046       |
; | 228      | 46052       |
; | 230      | 46054       |
; +----------+-------------+
  DEFB 0
  DEFB 132,138,132,138,144,144,150,0,181,181,189,193,132,132,0,0
  DEFB 158,159,158,159,144,144,163,0,0,154,0,158,0,154,0,0
  DEFB 132,138,132,138,144,144,171,0,0,0,189,193,132,132,0,0
  DEFB 132,138,132,138,144,144,177,0,181,181,0,0,132,132,0,0
  DEFB 247,242,247,242,239,239,235,0,190,237,190,237,247,247,252,252
  DEFB 223,213,223,213,223,223,199,199,133,143,133,143,156,156,254,254
  DEFB 163,174,163,174,185,185,162,162

; Print a character's title and name, and walk him onto the screen (4)
;
; Continues from WALKCHAR3. Sets the animatory state and location of the
; character and walks him to the middle of the screen.
;
; A Animatory state
; B 1-8 (corresponding to one of the eight main characters)
; HL 39008 (96, 152)
WALKCHAR4:
  PUSH BC
  CALL SETASY             ; Set the animatory state and y-coordinate of the
                          ; character to be walked on screen
  NOP
  INC L                   ; L=98
  LD (HL),3               ; Set the character's x-coordinate to 3
  CALL WALKONOFF          ; Walk the character to the middle of the screen
  POP BC
  POP HL                  ; Restore the message address for the character's
                          ; name to HL
  RET

; Print 'PRESS'C'TO CHANGE NAME'
;
; Used by the routine at CPROMPT.
PRTCPROMPT:
  LD HL,M_CTOCHANGE       ; M_CTOCHANGE: 'PRESS'C'TO CHANGE NAME'
; This entry point is used by the routines at WALKCHAR2 (with HL=M_SPACE: ' ')
; and CHANGENAME (with HL=M_ENTERNAME: 'ENTER NEW NAME').
PRTNAME:
  LD DE,20640             ; Set DE to the display file address at which to
                          ; print the message
  JP PRINTMSG             ; Print the message

; UDG reference for animatory state 255 at row 2, column 1
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 3, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 46084       |
; | 14-15    | 46094-46095 |
; | 20       | 46100       |
; | 24-31    | 46104-46111 |
; | 36       | 46116       |
; | 40-41    | 46120-46121 |
; | 44-47    | 46124-46127 |
; | 52       | 46132       |
; | 58-59    | 46138-46139 |
; | 62       | 46142       |
; | 68       | 46148       |
; | 76       | 46156       |
; | 78       | 46158       |
; | 84       | 46164       |
; | 86       | 46166       |
; | 92       | 46172       |
; | 94       | 46174       |
; | 100      | 46180       |
; | 102      | 46182       |
; +----------+-------------+
  DEFB 6,12,13,15,20,20,25,28,6,6,63,63,6,6,0,0
  DEFB 6,12,13,15,20,20,25,28,0,27,0,31,0,27,0,0
  DEFB 6,12,13,15,20,20,25,28,0,0,63,63,6,6,0,0
  DEFB 6,12,13,15,20,20,25,28,6,6,0,0,6,6,0,28
  DEFB 127,125,124,122,109,109,105,103,64,112,114,112,64,64,68,68
  DEFB 92,82,80,78,92,92,68,68,7,18,20,23,7,7,68,68
  DEFB 37,49,51,54,37,37,68,68

; 'THE HEADMASTER'
;
; Used by the routine at WALKCHAR.
M_HEAD:
  DEFM "THE HEADMASTER"
  DEFB 0                  ; End marker

; ' ' (single space)
;
; Used by the routine at WALKCHAR2.
M_SPACE:
  DEFM " "
  DEFB 0                  ; End marker

; Unused
  DEFB 0

; Prepare a character to be walked onto the screen (3)
;
; Continues from SETASY2. Sets the animatory state and y-coordinate of the
; character to be walked onto the screen.
;
; A Animatory state
; H 152 (boy) or 164 (teacher)
; L 96
SETASY3:
  LD (HL),A               ; Set the character's animatory state
  INC L                   ; L=97
  LD (HL),164             ; Set the character's y-coordinate to 164
  RET

; UDG references for animatory states 127-231 at row 3, column 1
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 46212       |
; | 142-143  | 46222-46223 |
; | 148      | 46228       |
; | 152-159  | 46232-46239 |
; | 164      | 46244       |
; | 168-169  | 46248-46249 |
; | 172-175  | 46252-46255 |
; | 180      | 46260       |
; | 186-187  | 46266-46267 |
; | 190      | 46270       |
; | 196      | 46276       |
; | 204      | 46284       |
; | 206      | 46286       |
; | 212      | 46292       |
; | 214      | 46294       |
; | 220      | 46300       |
; | 222      | 46302       |
; | 228      | 46308       |
; | 230      | 46310       |
; +----------+-------------+
  DEFB 0
  DEFB 134,140,141,143,148,148,153,156,134,134,191,191,134,134,0,0
  DEFB 134,140,141,143,148,148,153,156,0,155,0,159,0,155,0,0
  DEFB 134,140,141,143,148,148,153,156,0,0,191,191,134,134,0,0
  DEFB 134,140,141,143,148,148,153,156,134,134,0,0,134,134,0,156
  DEFB 255,253,252,250,237,237,233,231,192,240,242,240,192,192,196,196
  DEFB 220,210,208,206,220,220,196,196,135,146,148,151,135,135,196,196
  DEFB 165,177,179,182,165,165,196,196

; Move a character one step along the catwalk
;
; Used by the routine at WALKONOFF.
;
; H 152 (boy) or 164 (teacher)
CATWALK:
  CALL UPDATESRB          ; Update the SRB for the character's current location
  INC A                   ; A=character's next animatory state
  LD C,A                  ; Store this in C
  RRCA                    ; Set the carry flag if this animatory state is
                          ; midstride
  LD A,C                  ; Restore the next animatory state to A
  JR C,CATWALK_0          ; Jump if the character will be midstride
; The character will be moving from the midstride position, so move him one
; space forward.
  INC E                   ; Increment the character's x-coordinate
  AND 3                   ; Keep only the walking phase identifier bits of the
                          ; animatory state (bits 0-1)
  LD A,C                  ; Restore the next animatory state to A again
  JR NZ,CATWALK_0         ; Jump if the next animatory state is
                          ; standing/walking phase 3
  SUB 4                   ; Reset the animatory state to standing/walking phase
                          ; 1
; Now A holds the character's new animatory state, and E his x-coordinate.
CATWALK_0:
  CALL UPDATEAS           ; Update the character's animatory state and location
                          ; and update the SRB
  JP UPDATESCR            ; Update the display

; UDG reference for animatory state 255 at row 3, column 1
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 0, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 46340       |
; | 14-15    | 46350-46351 |
; | 20       | 46356       |
; | 24-31    | 46360-46367 |
; | 36       | 46372       |
; | 40-41    | 46376-46377 |
; | 44-47    | 46380-46383 |
; | 52       | 46388       |
; | 58-59    | 46394-46395 |
; | 62       | 46398       |
; | 68       | 46404       |
; | 76       | 46412       |
; | 78       | 46414       |
; | 84       | 46420       |
; | 86       | 46422       |
; | 92       | 46428       |
; | 94       | 46430       |
; | 100      | 46436       |
; | 102      | 46438       |
; +----------+-------------+
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,59,104,59,104,59,59,0,0
  DEFB 100,0,100,0,100,100,0,0,2,0,2,0,2,2,0,0
  DEFB 30,100,30,100,30,30,0,0

; 'THE SCIENCE TEACHER'
;
; Used by the routine at WALKCHAR.
M_SCITEACH:
  DEFM "THE SCIENCE TEACHER"
  DEFB 0                  ; End marker

; Unused
  DEFS 3

; UDG references for animatory states 127-231 at row 0, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 46468       |
; | 142-143  | 46478-46479 |
; | 148      | 46484       |
; | 152-159  | 46488-46495 |
; | 164      | 46500       |
; | 168-169  | 46504-46505 |
; | 172-175  | 46508-46511 |
; | 180      | 46516       |
; | 186-187  | 46522-46523 |
; | 190      | 46526       |
; | 196      | 46532       |
; | 204      | 46540       |
; | 206      | 46542       |
; | 212      | 46548       |
; | 214      | 46550       |
; | 220      | 46556       |
; | 222      | 46558       |
; | 228      | 46564       |
; | 230      | 46566       |
; +----------+-------------+
  DEFB 0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,199,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,199,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,199,0,0
  DEFB 0,0,0,0,0,0,0,0,0,230,0,230,244,244,0,0
  DEFB 0,218,0,218,204,204,0,0,0,137,0,137,0,0,0,0
  DEFB 0,167,0,167,0,0,0,0

; Guide a character onto the catwalk or off it
;
; Used by the routines at WALKCHAR4 and GETNAMES2. Makes a character walk in
; from the left to the middle of the screen (at which point his name may be
; changed), or from the middle of the screen and off to the right.
WALKONOFF:
  LD B,38                 ; The character will take 38 steps along the catwalk
WALKONOFF_0:
  PUSH BC
CHARBUF:
  LD H,152                ; This instruction is set by the routine at SETASY2
                          ; to either 'LD H,152' (boy) or 'LD H,164' (teacher)
                          ; as appropriate
  CALL CATWALK            ; Move the character one step along the catwalk
  CALL WALKSOUND          ; Make a walking sound effect
  LD C,100                ; Pause briefly between steps
WALKONOFF_1:
  DJNZ WALKONOFF_1        ;
  DEC C                   ;
  JR NZ,WALKONOFF_1       ;
  POP BC
  DJNZ WALKONOFF_0        ; Jump back until 38 steps have been taken
  RET

; Unused
  DEFB 0

; UDG reference for animatory state 255 at row 0, column 2
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 1, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 46596       |
; | 14-15    | 46606-46607 |
; | 20       | 46612       |
; | 24-31    | 46616-46623 |
; | 36       | 46628       |
; | 40-41    | 46632-46633 |
; | 44-47    | 46636-46639 |
; | 52       | 46644       |
; | 58-59    | 46650-46651 |
; | 62       | 46654       |
; | 68       | 46660       |
; | 76       | 46668       |
; | 78       | 46670       |
; | 84       | 46676       |
; | 86       | 46678       |
; | 92       | 46684       |
; | 94       | 46686       |
; | 100      | 46692       |
; | 102      | 46694       |
; +----------+-------------+
  DEFB 2,0,2,0,2,2,0,0,2,2,2,2,2,2,0,0
  DEFB 39,0,39,0,39,39,0,0,0,0,0,0,0,0,0,0
  DEFB 2,0,2,0,2,2,0,0,0,0,2,2,2,2,0,0
  DEFB 2,0,2,0,2,2,0,0,2,2,0,0,2,2,0,0
  DEFB 0,0,0,0,0,0,0,0,61,107,61,107,61,61,122,122
  DEFB 97,100,97,100,97,97,72,72,4,13,4,13,4,4,127,127
  DEFB 33,44,33,44,33,33,41,41

; 'THE GEOGRAPHY TEACHER'
;
; Used by the routine at WALKCHAR.
M_GEOGTEACH:
  DEFM "THE GEOGRAPHY TEACHER"
  DEFB 0                  ; End marker

; Unused
  DEFB 0

; UDG references for animatory states 127-231 at row 1, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 46724       |
; | 142-143  | 46734-46735 |
; | 148      | 46740       |
; | 152-159  | 46744-46751 |
; | 164      | 46756       |
; | 168-169  | 46760-46761 |
; | 172-175  | 46764-46767 |
; | 180      | 46772       |
; | 186-187  | 46778-46779 |
; | 190      | 46782       |
; | 196      | 46788       |
; | 204      | 46796       |
; | 206      | 46798       |
; | 212      | 46804       |
; | 214      | 46806       |
; | 220      | 46812       |
; | 222      | 46814       |
; | 228      | 46820       |
; | 230      | 46822       |
; +----------+-------------+
  DEFB 0
  DEFB 166,135,166,135,166,166,0,0,166,185,166,194,197,200,0,0
  DEFB 0,161,0,161,0,0,0,0,0,151,0,145,0,151,0,0
  DEFB 166,169,166,169,166,166,0,0,0,0,166,194,197,200,0,0
  DEFB 166,175,166,175,166,166,0,0,166,185,0,0,197,200,0,0
  DEFB 0,245,0,245,0,0,0,0,0,233,0,233,245,245,248,248
  DEFB 227,216,227,216,203,203,0,0,0,139,0,139,153,153,0,0
  DEFB 159,170,159,170,183,183,0,0

; Print 'PRESS'C'TO CHANGE NAME' and wait for a keypress
;
; Used by the routines at CHANGENAME2 and GETNAMES2. Returns with the zero flag
; set if 'c' was pressed.
CPROMPT:
  LD HL,23611             ; This is the FLAGS system variable
  RES 5,(HL)              ; Signal: no key pressed
  CALL PRTCPROMPT         ; Print 'PRESS'C'TO CHANGE NAME'
CPROMPT_0:
  CALL READKEY            ; Wait for a keypress and collect its ASCII code in A
  JR Z,CPROMPT_0          ;
  OR 32                   ; Convert the ASCII code to lower case
  CP 99                   ; Set the zero flag if 'c' (or 'C') was pressed
  RET

; Unused
  DEFS 5

; UDG reference for animatory state 255 at row 1, column 2
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 2, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 46852       |
; | 14-15    | 46862-46863 |
; | 20       | 46868       |
; | 24-31    | 46872-46879 |
; | 36       | 46884       |
; | 40-41    | 46888-46889 |
; | 44-47    | 46892-46895 |
; | 52       | 46900       |
; | 58-59    | 46906-46907 |
; | 62       | 46910       |
; | 68       | 46916       |
; | 76       | 46924       |
; | 78       | 46926       |
; | 84       | 46932       |
; | 86       | 46934       |
; | 92       | 46940       |
; | 94       | 46942       |
; | 100      | 46948       |
; | 102      | 46950       |
; +----------+-------------+
  DEFB 5,0,5,0,18,18,23,0,54,56,5,5,5,5,0,0
  DEFB 5,0,5,0,18,18,36,0,0,0,0,0,0,0,0,0
  DEFB 5,0,5,0,18,18,44,0,0,0,5,5,5,5,0,0
  DEFB 5,0,5,0,18,18,50,0,54,56,0,0,5,5,0,0
  DEFB 118,0,118,0,0,0,0,0,63,110,63,110,63,63,125,125
  DEFB 94,84,94,84,94,94,70,70,6,16,6,16,6,6,74,74
  DEFB 36,47,36,47,36,36,52,52

; 'THE HISTORY MASTER'
;
; Used by the routine at WALKCHAR.
M_HISTTEACH:
  DEFM "THE HISTORY MASTER"
  DEFB 0                  ; End marker

; Unused
  DEFS 4

; UDG references for animatory states 127-231 at row 2, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 46980       |
; | 142-143  | 46990-46991 |
; | 148      | 46996       |
; | 152-159  | 47000-47007 |
; | 164      | 47012       |
; | 168-169  | 47016-47017 |
; | 172-175  | 47020-47023 |
; | 180      | 47028       |
; | 186-187  | 47034-47035 |
; | 190      | 47038       |
; | 196      | 47044       |
; | 204      | 47052       |
; | 206      | 47054       |
; | 212      | 47060       |
; | 214      | 47062       |
; | 220      | 47068       |
; | 222      | 47070       |
; | 228      | 47076       |
; | 230      | 47078       |
; +----------+-------------+
  DEFB 0
  DEFB 131,136,131,136,145,145,0,0,180,183,188,192,198,198,0,0
  DEFB 131,136,131,136,145,145,0,0,0,152,0,156,0,152,0,0
  DEFB 131,136,131,136,145,145,0,0,0,0,188,192,198,198,0,0
  DEFB 131,136,131,136,145,145,0,0,180,183,0,0,198,198,0,0
  DEFB 248,243,248,243,240,240,236,0,0,236,0,236,0,0,251,251
  DEFB 224,214,224,214,224,224,0,0,0,142,0,142,155,155,0,0
  DEFB 77,173,77,173,0,0,0,0

; Collect a keypress while a character's name is being entered
;
; Used by the routine at CHANGENAME2. Waits for a keypress and returns with the
; zero flag set if it was ENTER; otherwise inserts the ASCII code of the
; keypress into the next slot in the character's name message, and returns with
; A holding the number of characters typed so far.
;
; DE Address of the next slot in the character's name message
; HL Base address of the character's name message
GETKEY:
  PUSH HL                 ; Save the message address for the character's name
  PUSH DE
  LD DE,20512             ; Set DE to the display file address at which to
                          ; print the character's name
  CALL PRINTMSG           ; Print the character's name (which is being edited)
GETKEY_0:
  CALL READKEY            ; Wait for a keypress and collect its ASCII code in A
  JR Z,GETKEY_0           ;
  POP DE
  POP HL                  ; Restore the character name message address to HL
  CP 13                   ; Was ENTER pressed?
  RET Z                   ; Return if so
  LD (DE),A               ; Store the ASCII code of the key just pressed
  INC E                   ; Move DE along to the next slot in the name message
  LD A,E                  ; Set A equal to the number of characters typed so
  SUB L                   ; far
  RET

; UDG reference for animatory state 255 at row 2, column 2
;
; Used by the routine at PRINTTILE.
  DEFB 0

; UDG references for animatory states 0-103 at row 3, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 4        | 47108       |
; | 14-15    | 47118-47119 |
; | 20       | 47124       |
; | 24-31    | 47128-47135 |
; | 36       | 47140       |
; | 40-41    | 47144-47145 |
; | 44-47    | 47148-47151 |
; | 52       | 47156       |
; | 58-59    | 47162-47163 |
; | 62       | 47166       |
; | 68       | 47172       |
; | 76       | 47180       |
; | 78       | 47182       |
; | 84       | 47188       |
; | 86       | 47190       |
; | 92       | 47196       |
; | 94       | 47198       |
; | 100      | 47204       |
; | 102      | 47206       |
; +----------+-------------+
  DEFB 0,0,0,0,0,0,26,29,0,0,5,5,0,0,0,0
  DEFB 0,0,0,0,0,0,26,37,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,26,45,0,0,5,5,0,0,0,0
  DEFB 0,0,0,0,0,0,26,51,0,0,0,0,0,0,0,51
  DEFB 0,0,0,0,0,0,0,102,65,113,115,113,65,65,67,67
  DEFB 91,13,91,77,91,91,67,67,8,19,21,24,8,8,67,67
  DEFB 38,50,38,13,38,38,67,67

; Change a character's name (1)
;
; Used by the routine at GETNAMES2 after 'c' was pressed to change a
; character's name.
;
; HL Address of the character's name message
CHANGENAME:
  PUSH HL                 ; Save the message address
  LD (HL),32              ; Set the character's name to ' ' (single space)
  LD B,13                 ;
CHANGENAME_0:
  INC L                   ;
  LD (HL),0               ;
  DJNZ CHANGENAME_0       ;
  LD HL,M_ENTERNAME       ; M_ENTERNAME: 'ENTER NEW NAME'
  CALL PRTNAME            ; Print this message
  LD HL,23611             ; Point HL at the FLAGS system variable
  POP DE                  ; Restore the message address for the character's
                          ; name to DE
  JP CHANGENAME2

; UDG references for animatory states 127-231 at row 3, column 2
;
; Used by the routine at PRINTTILE. The following animatory states and
; corresponding addresses are unused:
;
; +----------+-------------+
; | State(s) | Address(es) |
; +----------+-------------+
; | 132      | 47236       |
; | 142-143  | 47246-47247 |
; | 148      | 47252       |
; | 152-159  | 47256-47263 |
; | 164      | 47268       |
; | 168-169  | 47272-47273 |
; | 172-175  | 47276-47279 |
; | 180      | 47284       |
; | 186-187  | 47290-47291 |
; | 190      | 47294       |
; | 196      | 47300       |
; | 204      | 47308       |
; | 206      | 47310       |
; | 212      | 47316       |
; | 214      | 47318       |
; | 220      | 47324       |
; | 222      | 47326       |
; | 228      | 47332       |
; | 230      | 47334       |
; +----------+-------------+
  DEFB 0
  DEFB 0,139,0,142,147,147,152,155,0,0,190,190,0,0,0,0
  DEFB 0,139,0,142,147,147,152,155,0,153,0,157,0,153,0,0
  DEFB 0,139,0,142,147,147,152,155,0,0,190,190,0,0,0,0
  DEFB 0,139,0,142,147,147,152,155,0,0,0,0,0,0,0,155
  DEFB 0,254,0,251,238,238,234,232,0,239,0,239,0,0,197,197
  DEFB 221,211,221,207,221,221,197,197,0,145,0,150,0,0,197,197
  DEFB 0,176,0,181,0,0,197,197

; Change a character's name (2)
;
; Continues from CHANGENAME. Returns with the zero flag set if 'c' was pressed
; after entering the character's name.
;
; DE Address of the character's name message
; HL 23611 (FLAGS)
CHANGENAME2:
  RES 5,(HL)              ; Signal: no key pressed
  LD H,D                  ; Set HL to the message address for the character's
  LD L,E                  ; name
CHANGENAME2_0:
  CALL GETKEY             ; Collect a keypress
  JR Z,CHANGENAME2_1      ; Jump forward if ENTER was pressed
  CP 13                   ; Have 13 characters been typed?
  JR NZ,CHANGENAME2_0     ; Jump back to collect another keypress if not
  DEC E                   ; Set the character name message address offset back
  JR CHANGENAME2_0        ; to 12 to avoid overflow (any subsequent keypresses
                          ; will be stored at that offset)
CHANGENAME2_1:
  PUSH HL                 ; Save the message address
  CALL CPROMPT            ; Print 'PRESS'C'TO CHANGE NAME' and wait for a
                          ; keypress
  POP HL                  ; Restore the character name message address to HL
  RET

; Unused
  DEFB 184

; UDG reference for animatory state 255 at row 3, column 2
;
; Used by the routine at PRINTTILE.
  DEFB 0

; Sprite graphic data for the boys and the catapult pellet
;
; Used by the routine at PRINTTILE. Tiles 0 and 128 are unused by design (and
; would appear completely blank if they were used); tiles 74 and 202 are also
; unused; tiles 21 and 149 belong to the unused sprites of the main kids with
; their hands raised; tiles 75-80, 82-101, 203-208 and 210-229 belong to unused
; copies of MR WACKER's sprites.
  DEFB 16,0,0,0,255,128,127,0,15,0,248,7,240,127,7,240
  DEFB 127,0,128,2,128,0,0,0,0,96,248,0,0,0,255,248
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,1,255,128,127,192,0,0,0,20,127,0,79
  DEFB 127,255,0,0,0,16,7,0,10,0,4,20,0,0,143,14
  DEFB 228,0,231,12,128,228,62,156,11,0,0,240,228,0,72,238
  DEFB 3,192,185,0,0,0,0,0,0,254,3,0,0,64,1,62
  DEFB 0,0,240,2,0,0,0,47,0,0,240,7,127,240,7,127
  DEFB 8,0,0,0,255,1,254,0,240,0,31,224,15,254,224,15
  DEFB 254,0,1,64,1,0,0,0,0,6,31,0,0,0,255,31
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,128,255,1,254,3,0,0,0,40,254,0,242
  DEFB 254,255,0,0,0,8,224,0,80,0,32,40,0,0,241,112
  DEFB 39,0,231,48,1,39,124,57,208,0,0,15,39,0,18,119
  DEFB 192,3,157,0,0,0,0,0,0,127,192,0,0,2,128,124
  DEFB 0,0,15,64,0,0,0,244,0,0,15,224,254,15,224,254
  DEFB 0,7,255,254,255,191,127,240,239,127,251,199,247,127,231,247
  DEFB 127,255,191,250,191,255,255,255,255,110,251,255,255,253,255,251
  DEFB 195,253,63,255,255,0,255,255,135,248,127,255,255,255,131,248
  DEFB 63,0,255,253,249,255,191,127,223,255,7,131,212,127,255,79
  DEFB 127,255,255,135,7,215,247,255,234,239,244,212,255,127,143,238
  DEFB 228,127,231,236,191,228,190,157,235,63,252,247,228,254,75,238
  DEFB 251,223,185,254,255,195,255,255,255,254,251,135,255,95,253,190
  DEFB 255,255,247,250,255,255,127,175,255,255,247,231,127,247,199,127
  DEFB 0,224,255,127,255,253,254,15,247,254,223,227,239,254,231,239
  DEFB 254,255,253,95,253,255,255,255,255,118,223,255,255,191,255,223
  DEFB 195,191,252,255,255,0,255,255,225,31,254,255,255,255,193,31
  DEFB 252,0,255,191,159,255,253,254,251,255,224,193,43,254,255,242
  DEFB 254,255,255,225,224,235,239,255,87,247,47,43,255,254,241,119
  DEFB 39,254,231,55,253,39,125,185,215,252,63,239,39,127,210,119
  DEFB 223,251,157,127,255,195,255,255,255,127,223,225,255,250,191,125
  DEFB 255,255,239,95,255,255,254,245,255,255,239,231,254,239,227,254
  DEFB 0,248,0,0,255,128,126,15,15,128,248,3,32,126,11,208
  DEFB 63,0,128,3,128,0,0,0,0,144,248,0,0,2,251,184
  DEFB 60,3,192,0,0,0,0,0,120,7,128,0,0,0,124,7
  DEFB 192,0,0,2,7,255,192,32,192,0,248,124,31,255,0,147
  DEFB 2,127,0,120,248,40,3,0,14,16,2,18,0,0,143,15
  DEFB 244,0,243,12,128,232,38,254,15,192,3,112,228,0,136,110
  DEFB 2,224,255,0,0,60,0,0,0,126,2,120,0,192,1,31
  DEFB 0,0,248,7,0,0,128,127,0,0,208,11,126,32,3,126
  DEFB 0,31,0,0,255,1,126,240,240,1,31,192,4,126,208,11
  DEFB 252,0,1,192,1,0,0,0,0,9,31,0,0,64,223,29
  DEFB 60,192,3,0,0,0,0,0,30,224,1,0,0,0,62,224
  DEFB 3,0,0,64,224,255,3,4,3,0,31,62,248,255,0,201
  DEFB 64,254,0,30,31,20,192,0,112,8,64,72,0,0,241,240
  DEFB 47,0,207,48,1,23,100,127,240,3,192,14,39,0,17,118
  DEFB 64,7,255,0,0,60,0,0,0,126,64,30,0,3,128,248
  DEFB 0,0,31,224,0,0,1,254,0,0,11,208,126,4,192,126
  DEFB 0,251,255,254,255,191,126,239,239,191,251,251,47,126,235,215
  DEFB 191,255,191,251,191,255,248,63,254,148,251,255,255,194,251,187
  DEFB 188,251,207,254,31,243,254,255,123,247,191,252,63,131,125,247
  DEFB 223,252,31,2,247,255,223,160,223,254,251,125,223,255,254,147
  DEFB 130,127,254,123,251,170,251,255,238,215,250,210,255,127,143,239
  DEFB 244,127,243,236,191,232,166,254,239,223,251,119,228,254,139,110
  DEFB 250,239,255,254,255,189,225,255,255,126,250,123,255,223,253,223
  DEFB 255,255,251,247,255,255,191,127,255,255,215,235,126,47,251,126
  DEFB 0,223,255,127,255,253,126,247,247,253,223,223,244,126,215,235
  DEFB 253,255,253,223,253,255,31,252,127,41,223,255,255,67,223,221
  DEFB 61,223,243,127,248,207,127,255,222,239,253,63,252,193,190,239
  DEFB 251,63,248,64,239,255,251,5,251,127,223,190,251,255,127,201
  DEFB 65,254,127,222,223,85,223,255,119,235,95,75,255,254,241,247
  DEFB 47,254,207,55,253,23,101,127,247,251,223,238,39,127,209,118
  DEFB 95,247,255,127,255,189,135,255,255,126,95,222,255,251,191,251
  DEFB 255,255,223,239,255,255,253,254,255,255,235,215,126,244,223,126
  DEFB 0,124,0,0,255,128,42,7,15,192,248,2,64,42,11,80
  DEFB 127,0,128,3,128,0,7,192,1,11,252,0,0,46,255,248
  DEFB 79,4,240,1,224,12,0,0,252,15,192,3,192,108,130,8
  DEFB 32,3,224,90,11,255,192,0,0,0,124,130,7,255,0,162
  DEFB 3,255,0,252,124,56,1,0,12,40,1,23,0,0,159,15
  DEFB 244,0,241,9,64,239,30,237,20,32,4,112,228,0,244,238
  DEFB 1,208,110,1,0,66,22,0,0,62,4,92,0,192,1,63
  DEFB 0,0,184,7,0,0,128,123,0,0,80,11,42,64,2,42
  DEFB 0,62,0,0,255,1,84,224,240,3,31,64,2,84,208,10
  DEFB 254,0,1,192,1,0,224,3,128,208,63,0,0,116,255,31
  DEFB 242,32,15,128,7,48,0,0,63,240,3,192,3,54,65,16
  DEFB 4,192,7,90,208,255,3,0,0,0,62,65,224,255,0,69
  DEFB 192,255,0,63,62,28,128,0,48,20,128,232,0,0,249,240
  DEFB 47,0,143,144,2,247,120,183,40,4,32,14,39,0,47,119
  DEFB 128,11,118,128,0,66,104,0,0,124,32,58,0,3,128,252
  DEFB 0,0,29,224,0,0,1,222,0,0,10,208,84,2,64,84
  DEFB 0,125,255,254,255,191,170,247,239,223,251,250,95,170,235,87
  DEFB 127,255,191,251,191,159,247,223,253,11,253,255,240,46,255,251
  DEFB 79,244,247,253,231,141,254,127,253,239,223,251,223,109,130,232
  DEFB 47,251,239,90,235,255,223,222,63,254,125,130,231,255,254,170
  DEFB 251,255,254,253,125,186,253,255,236,171,253,215,255,127,159,239
  DEFB 244,127,241,233,95,239,222,237,212,47,244,119,228,254,245,238
  DEFB 253,215,110,253,255,66,214,255,255,62,244,93,255,223,253,191
  DEFB 255,255,187,247,255,255,191,123,255,255,87,235,170,95,250,170
  DEFB 0,190,255,127,255,253,85,239,247,251,223,95,250,85,215,234
  DEFB 254,255,253,223,253,249,239,251,191,208,191,255,15,116,255,223
  DEFB 242,47,239,191,231,177,127,254,191,247,251,223,251,182,65,23
  DEFB 244,223,247,90,215,255,251,123,252,127,190,65,231,255,127,85
  DEFB 223,255,127,191,190,93,191,255,55,213,191,235,255,254,249,247
  DEFB 47,254,143,151,250,247,123,183,43,244,47,238,39,127,175,119
  DEFB 191,235,118,191,255,66,107,255,255,124,47,186,255,251,191,253
  DEFB 255,255,221,239,255,255,253,222,255,255,234,215,85,250,95,85
  DEFB 0,92,0,0,255,128,42,5,15,192,248,4,160,42,20,72
  DEFB 95,0,128,7,128,96,3,224,2,71,252,0,15,230,255,248
  DEFB 75,4,176,2,120,82,0,0,156,9,192,7,224,198,98,6
  DEFB 32,4,16,202,15,255,0,0,0,0,92,98,2,127,0,162
  DEFB 0,127,0,156,92,48,0,0,12,16,0,13,0,0,23,7
  DEFB 116,128,120,9,64,31,15,237,22,112,4,112,110,0,244,241
  DEFB 0,208,110,1,0,71,50,0,0,62,5,4,0,192,3,95
  DEFB 0,0,248,7,0,0,128,127,0,0,72,20,42,160,4,42
  DEFB 0,58,0,0,255,1,84,160,240,3,31,32,5,84,40,18
  DEFB 250,0,1,224,1,6,192,7,64,226,63,0,240,103,255,31
  DEFB 210,32,13,64,30,74,0,0,57,144,3,224,7,99,70,96
  DEFB 4,32,8,83,240,255,0,0,0,0,58,70,64,254,0,69
  DEFB 0,254,0,57,58,12,0,0,48,8,0,176,0,0,232,224
  DEFB 46,1,30,144,2,248,240,183,104,14,32,14,118,0,47,143
  DEFB 0,11,118,128,0,226,76,0,0,124,160,32,0,3,192,250
  DEFB 0,0,31,224,0,0,1,254,0,0,18,40,84,5,32,84
  DEFB 0,93,255,254,255,191,170,245,239,223,251,244,175,170,213,75
  DEFB 95,255,191,247,191,127,251,239,234,71,253,190,15,230,255,251
  DEFB 75,244,183,250,123,82,254,127,157,233,223,247,239,198,98,246
  DEFB 47,244,23,202,239,255,63,254,127,192,93,98,250,127,254,170
  DEFB 252,127,254,157,93,182,254,255,236,215,254,237,255,127,23,247
  DEFB 116,191,120,233,95,31,239,237,214,119,244,119,110,255,245,241
  DEFB 254,215,110,253,127,71,50,128,239,62,245,5,255,223,251,95
  DEFB 255,135,251,247,127,248,191,127,255,135,75,213,170,175,244,170
  DEFB 0,186,255,127,255,253,85,175,247,251,223,47,245,85,171,210
  DEFB 250,255,253,239,253,254,223,247,87,226,191,125,240,103,255,223
  DEFB 210,47,237,95,222,74,127,254,185,151,251,239,247,99,70,111
  DEFB 244,47,232,83,247,255,252,127,254,3,186,70,95,254,127,85
  DEFB 63,254,127,185,186,109,127,255,55,235,127,183,255,254,232,239
  DEFB 46,253,30,151,250,248,247,183,107,238,47,238,118,255,175,143
  DEFB 127,235,118,191,254,226,76,1,247,124,175,160,255,251,223,250
  DEFB 255,225,223,239,254,31,253,254,255,225,210,171,85,245,47,85
  DEFB 0,4,0,0,255,128,42,0,15,64,248,5,208,26,20,36
  DEFB 255,0,128,0,0,160,2,224,23,163,252,65,255,174,255,248
  DEFB 99,6,48,2,88,66,0,0,20,1,64,4,224,198,90,5
  DEFB 160,3,16,218,19,255,0,0,0,63,244,230,1,255,0,162
  DEFB 0,63,0,20,4,48,0,0,14,0,0,9,0,0,31,1
  DEFB 12,192,4,9,0,15,14,109,22,240,6,64,36,0,240,224
  DEFB 0,208,110,1,0,111,214,127,16,62,13,124,0,0,0,127
  DEFB 0,120,248,7,128,7,128,127,0,120,36,20,26,208,5,42
  DEFB 0,32,0,0,255,1,84,0,240,2,31,160,11,88,40,36
  DEFB 255,0,1,0,0,5,64,7,232,197,63,130,255,117,255,31
  DEFB 198,96,12,64,26,66,0,0,40,128,2,32,7,99,90,160
  DEFB 5,192,8,91,200,255,0,0,0,252,47,103,128,255,0,69
  DEFB 0,252,0,40,32,12,0,0,112,0,0,144,0,0,248,128
  DEFB 48,3,32,144,0,240,112,182,104,15,96,2,36,0,15,7
  DEFB 0,11,118,128,0,246,107,254,8,124,176,62,0,0,0,254
  DEFB 0,30,31,224,1,224,1,254,0,30,36,40,88,11,160,84
  DEFB 0,5,255,254,255,191,170,240,239,95,251,245,215,218,213,165
  DEFB 255,254,191,248,127,175,250,239,215,163,253,65,255,174,255,251
  DEFB 99,246,55,250,91,66,254,127,21,225,95,244,239,198,90,245
  DEFB 175,251,23,218,211,255,127,254,127,191,245,230,253,255,254,170
  DEFB 255,191,254,21,5,182,254,255,238,239,254,233,255,127,31,249
  DEFB 140,223,4,233,31,15,238,109,214,247,246,79,164,255,243,224
  DEFB 254,215,110,253,127,111,214,127,208,62,205,125,255,63,252,127
  DEFB 255,123,251,247,191,247,191,127,255,123,165,213,218,215,245,170
  DEFB 0,160,255,127,255,253,85,15,247,250,223,175,235,91,171,165
  DEFB 255,127,253,31,254,245,95,247,235,197,191,130,255,117,255,223
  DEFB 198,111,236,95,218,66,127,254,168,135,250,47,247,99,90,175
  DEFB 245,223,232,91,203,255,254,127,254,253,175,103,191,255,127,85
  DEFB 255,253,127,168,160,109,127,255,119,247,127,151,255,254,248,159
  DEFB 49,251,32,151,248,240,119,182,107,239,111,242,37,255,207,7
  DEFB 127,235,118,191,254,246,107,254,11,124,179,190,255,252,63,254
  DEFB 255,222,223,239,253,239,253,254,255,222,165,171,91,235,175,85
  DEFB 0,126,0,1,255,128,62,7,31,224,248,14,80,30,28,28
  DEFB 31,1,128,0,0,144,0,48,31,251,236,127,255,238,255,248
  DEFB 14,0,224,3,24,206,0,0,134,8,96,0,160,222,4,0
  DEFB 64,2,208,210,12,255,0,0,0,32,70,70,0,63,0,227
  DEFB 0,63,0,134,126,56,0,0,14,0,0,9,0,0,231,3
  DEFB 29,192,2,30,0,79,14,173,22,112,4,192,61,0,240,228
  DEFB 0,208,110,1,0,71,246,255,31,126,63,44,0,0,0,159
  DEFB 0,92,248,15,192,5,128,255,0,92,28,28,30,80,14,62
  DEFB 0,126,0,128,255,1,124,224,248,7,31,112,10,120,56,56
  DEFB 248,128,1,0,0,9,0,12,248,223,55,254,255,119,255,31
  DEFB 112,0,7,192,24,115,0,0,97,16,6,0,5,123,32,0
  DEFB 2,64,11,75,48,255,0,0,0,4,98,98,0,252,0,199
  DEFB 0,252,0,97,126,28,0,0,112,0,0,144,0,0,231,192
  DEFB 184,3,64,120,0,242,112,181,104,14,32,3,188,0,15,39
  DEFB 0,11,118,128,0,226,111,255,248,126,252,52,0,0,0,249
  DEFB 0,58,31,240,3,160,1,255,0,58,56,56,120,10,112,124
  DEFB 0,126,255,253,255,191,190,247,223,239,251,238,87,222,221,221
  DEFB 31,253,191,255,255,151,248,55,223,251,237,127,255,238,255,251
  DEFB 14,240,239,251,27,206,254,255,134,232,111,240,175,222,5,240
  DEFB 95,250,215,210,236,255,127,254,127,160,70,70,254,191,254,235
  DEFB 255,191,128,134,126,186,254,255,238,255,254,233,255,127,231,251
  DEFB 221,223,250,222,127,79,238,173,214,119,244,223,189,255,247,238
  DEFB 254,215,110,253,127,71,246,255,223,126,191,45,252,255,255,159
  DEFB 254,93,251,239,223,245,191,255,254,93,221,221,222,87,238,190
  DEFB 0,126,255,191,255,253,125,239,251,247,223,119,234,123,187,187
  DEFB 248,191,253,255,255,233,31,236,251,223,183,254,255,119,255,223
  DEFB 112,15,247,223,216,115,127,255,97,23,246,15,245,123,160,15
  DEFB 250,95,235,75,55,255,254,127,254,5,98,98,127,253,127,215
  DEFB 255,253,1,97,126,93,127,255,119,255,127,151,255,254,231,223
  DEFB 187,251,95,123,254,242,119,181,107,238,47,251,189,255,239,119
  DEFB 127,235,118,191,254,226,111,255,251,126,253,180,63,255,255,249
  DEFB 127,186,223,247,251,175,253,255,127,186,187,187,123,234,119,125
  DEFB 0,47,0,2,255,128,62,2,47,240,248,30,56,14,56,12
  DEFB 127,2,0,0,0,224,3,240,31,159,234,113,191,252,255,248
  DEFB 116,7,64,0,112,166,0,0,255,15,240,4,48,206,126,7
  DEFB 224,0,48,252,0,127,0,0,0,63,231,229,0,63,0,227
  DEFB 0,63,127,255,239,28,0,0,14,0,0,4,0,0,198,7
  DEFB 63,192,3,62,0,79,14,205,22,240,6,192,127,0,240,228
  DEFB 0,208,6,1,0,111,254,111,28,252,31,62,3,0,0,62
  DEFB 1,4,248,23,64,0,128,127,1,4,12,56,14,56,30,62
  DEFB 0,244,0,64,255,1,124,64,244,15,31,120,28,112,28,48
  DEFB 254,64,0,0,0,7,192,15,248,249,87,142,253,63,255,31
  DEFB 46,224,2,0,14,101,0,0,255,240,15,32,12,115,126,224
  DEFB 7,0,12,63,0,254,0,0,0,252,231,167,0,252,0,199
  DEFB 0,252,254,255,247,56,0,0,112,0,0,32,0,0,99,224
  DEFB 252,3,192,124,0,242,112,179,104,15,96,3,254,0,15,39
  DEFB 0,11,96,128,0,246,127,246,56,63,248,124,192,0,0,124
  DEFB 128,32,31,232,2,0,1,254,128,32,48,28,112,28,120,124
  DEFB 0,175,127,250,255,191,190,250,175,247,251,162,187,238,187,237
  DEFB 127,250,127,255,255,239,251,247,223,159,234,113,191,252,255,251
  DEFB 116,247,79,248,115,166,254,255,255,239,247,244,55,206,126,247
  DEFB 231,248,55,253,243,127,127,254,127,191,231,229,255,191,254,235
  DEFB 255,191,127,255,239,220,254,255,238,255,254,244,247,255,198,247
  DEFB 191,223,251,190,127,79,238,205,214,247,246,223,127,255,247,228
  DEFB 254,215,6,253,127,111,254,111,220,252,159,62,251,255,255,62
  DEFB 253,5,251,215,95,240,191,127,253,5,237,187,238,187,162,190
  DEFB 0,245,254,95,255,253,125,95,245,239,223,69,221,119,221,183
  DEFB 254,95,254,255,255,247,223,239,251,249,87,142,253,63,255,223
  DEFB 46,239,242,31,206,101,127,255,255,247,239,47,236,115,126,239
  DEFB 231,31,236,191,207,254,254,127,254,253,231,167,255,253,127,215
  DEFB 255,253,254,255,247,59,127,255,119,255,127,47,239,255,99,239
  DEFB 253,251,223,125,254,242,119,179,107,239,111,251,254,255,239,39
  DEFB 127,235,96,191,254,246,127,246,59,63,249,124,223,255,255,124
  DEFB 191,160,223,235,250,15,253,254,191,160,183,221,119,221,69,125
  DEFB 0,127,128,1,126,128,126,7,23,248,232,60,120,30,120,28
  DEFB 128,2,0,0,0,112,1,120,31,7,206,127,31,224,126,232
  DEFB 23,1,112,3,176,124,0,0,127,7,248,7,248,252,127,7
  DEFB 248,3,248,192,0,127,0,0,0,79,255,255,0,63,1,231
  DEFB 0,63,95,255,255,15,0,4,15,0,0,4,8,0,204,7
  DEFB 127,128,7,126,0,79,14,239,30,224,7,192,255,0,240,228
  DEFB 0,240,232,1,0,126,240,199,31,120,14,254,4,0,0,64
  DEFB 1,124,232,15,224,7,128,254,0,126,28,120,30,120,60,126
  DEFB 0,254,1,128,126,1,126,224,232,31,23,60,30,120,30,56
  DEFB 1,64,0,0,0,14,128,30,248,224,115,254,248,7,126,23
  DEFB 232,128,14,192,13,62,0,0,254,224,31,224,31,63,254,224
  DEFB 31,192,31,3,0,254,0,0,0,242,255,255,0,252,128,231
  DEFB 0,252,250,255,255,240,0,32,240,0,0,32,16,0,51,224
  DEFB 254,1,224,126,0,242,112,247,120,7,224,3,255,0,15,39
  DEFB 0,15,23,128,0,126,15,227,248,30,112,127,32,0,0,2
  DEFB 128,62,23,240,7,224,1,127,0,126,56,30,120,30,60,126
  DEFB 0,127,191,253,126,191,126,247,215,251,235,189,123,222,123,221
  DEFB 128,250,255,255,255,119,157,123,223,7,206,127,31,225,126,235
  DEFB 23,241,119,155,183,125,255,127,127,247,251,151,251,253,127,247
  DEFB 251,155,251,193,255,127,127,254,127,79,255,255,255,191,253,231
  DEFB 255,191,95,255,255,239,255,244,239,255,254,244,203,255,205,247
  DEFB 127,191,247,126,127,79,238,239,222,227,247,223,255,254,247,228
  DEFB 254,247,232,253,63,126,241,199,223,121,238,254,244,255,255,65
  DEFB 253,125,235,239,239,247,191,254,254,126,221,123,222,123,189,126
  DEFB 0,254,253,191,126,253,126,239,235,223,215,189,222,123,222,187
  DEFB 1,95,255,255,255,238,185,222,251,224,115,254,248,135,126,215
  DEFB 232,143,238,217,237,190,255,254,254,239,223,233,223,191,254,239
  DEFB 223,217,223,131,255,254,254,127,254,242,255,255,255,253,191,231
  DEFB 255,253,250,255,255,247,255,47,247,255,127,47,211,255,179,239
  DEFB 254,253,239,126,254,242,119,247,123,199,239,251,255,127,239,39
  DEFB 127,239,23,191,252,126,143,227,251,158,119,127,47,255,255,130
  DEFB 191,190,215,247,247,239,253,127,127,126,187,222,123,222,189,126

; Sprite graphic data for the teachers
;
; Used by the routine at PRINTTILE. Tiles 0 and 128 are unused by design (and
; would appear completely blank if they were used).
  DEFB 16,0,0,27,192,68,152,36,64,0,0,1,188,0,4,73
  DEFB 128,1,68,0,18,32,2,82,0,4,27,1,100,0,0,0
  DEFB 56,252,80,176,50,31,192,0,0,0,3,143,192,11,3,32
  DEFB 3,222,0,63,80,1,252,8,56,224,0,0,60,252,94,198
  DEFB 29,94,0,164,6,0,80,92,0,0,208,20,0,0,143,14
  DEFB 228,0,231,12,128,228,62,156,11,0,0,240,228,0,72,238
  DEFB 2,192,185,0,0,0,0,0,0,3,207,192,5,236,96,1
  DEFB 181,224,29,94,0,5,60,126,0,0,0,7,26,160,74,0
  DEFB 8,0,0,216,3,34,25,36,2,0,0,128,61,0,32,146
  DEFB 1,128,34,0,72,4,64,74,0,32,216,128,38,0,0,0
  DEFB 28,63,10,13,76,248,3,0,0,0,192,241,3,208,192,4
  DEFB 192,123,0,252,10,128,63,16,28,7,0,0,60,63,122,99
  DEFB 184,122,0,37,96,0,10,58,0,0,11,40,0,0,241,112
  DEFB 39,0,231,48,1,39,124,57,208,0,0,15,39,0,18,119
  DEFB 64,3,157,0,0,0,0,0,0,192,243,3,160,55,6,128
  DEFB 173,7,184,122,0,160,60,126,0,0,0,224,88,5,82,0
  DEFB 0,227,255,219,223,68,155,164,95,254,63,253,189,255,244,73
  DEFB 191,253,69,255,210,47,250,82,255,229,155,253,100,224,127,255
  DEFB 184,253,80,176,50,223,223,254,7,255,251,143,223,235,3,47
  DEFB 251,222,255,191,87,253,253,139,184,224,225,255,188,253,94,198
  DEFB 221,94,255,165,246,255,87,92,255,255,215,212,255,127,143,238
  DEFB 228,255,231,236,191,228,190,157,235,63,252,247,228,254,75,238
  DEFB 250,223,185,254,255,195,254,31,255,251,207,223,245,236,111,253
  DEFB 181,239,221,94,255,245,60,126,255,255,255,247,90,175,74,255
  DEFB 0,199,255,219,251,34,217,37,250,127,252,191,189,255,47,146
  DEFB 253,191,162,255,75,244,95,74,255,167,217,191,38,7,254,255
  DEFB 29,191,10,13,76,251,251,127,224,255,223,241,251,215,192,244
  DEFB 223,123,255,253,234,191,191,209,29,7,135,255,61,191,122,99
  DEFB 187,122,255,165,111,255,234,58,255,255,235,43,255,254,241,119
  DEFB 39,255,231,55,253,39,125,185,215,252,63,239,39,127,210,119
  DEFB 95,251,157,127,255,195,127,248,255,223,243,251,175,55,246,191
  DEFB 173,247,187,122,255,175,60,126,255,255,255,239,90,245,82,255
  DEFB 0,28,0,63,160,72,144,20,64,1,192,3,250,0,4,145
  DEFB 0,1,140,0,18,32,4,137,0,10,127,0,228,31,128,0
  DEFB 79,194,78,163,84,31,192,1,248,0,4,252,32,10,53,64
  DEFB 3,207,0,31,80,0,252,20,79,59,30,0,125,244,125,46
  DEFB 29,94,0,228,6,0,208,78,0,0,80,18,0,0,143,15
  DEFB 244,0,243,12,128,232,38,254,15,192,3,112,228,0,136,110
  DEFB 2,224,255,0,0,60,1,224,0,7,223,64,7,210,224,1
  DEFB 151,224,25,94,0,4,253,29,0,0,0,4,144,224,69,0
  DEFB 0,56,0,252,5,18,9,40,2,128,3,192,95,0,32,137
  DEFB 0,128,49,0,72,4,32,145,0,80,254,0,39,248,1,0
  DEFB 242,67,114,197,42,248,3,128,31,0,32,63,4,80,172,2
  DEFB 192,243,0,248,10,0,63,40,242,220,120,0,190,47,190,116
  DEFB 184,122,0,39,96,0,11,114,0,0,10,72,0,0,241,240
  DEFB 47,0,207,48,1,23,100,127,240,3,192,14,39,0,17,118
  DEFB 64,7,255,0,0,60,128,7,0,224,251,2,224,75,7,128
  DEFB 233,7,152,122,0,32,191,184,0,0,0,32,9,7,162,0
  DEFB 0,220,255,191,175,72,151,212,95,253,207,251,250,255,244,145
  DEFB 127,253,141,255,210,47,244,137,127,234,127,254,228,223,159,255
  DEFB 79,194,78,163,85,223,223,253,249,255,244,252,47,234,53,95
  DEFB 251,207,127,223,87,254,253,213,79,59,222,255,125,245,125,46
  DEFB 221,94,255,229,246,191,215,78,255,252,87,210,255,127,143,239
  DEFB 244,248,243,236,191,232,166,254,239,223,251,119,228,254,139,110
  DEFB 250,239,255,254,255,189,253,239,255,247,223,95,247,210,239,253
  DEFB 151,239,217,94,255,244,253,157,255,255,255,244,144,239,69,255
  DEFB 0,59,255,253,245,18,233,43,250,191,243,223,95,255,47,137
  DEFB 254,191,177,255,75,244,47,145,254,87,254,127,39,251,249,255
  DEFB 242,67,114,197,170,251,251,191,159,255,47,63,244,87,172,250
  DEFB 223,243,254,251,234,127,191,171,242,220,123,255,190,175,190,116
  DEFB 187,122,255,167,111,253,235,114,255,63,234,75,255,254,241,247
  DEFB 47,31,207,55,253,23,101,127,247,251,223,238,39,127,209,118
  DEFB 95,247,255,127,255,189,191,247,255,239,251,250,239,75,247,191
  DEFB 233,247,155,122,255,47,191,185,255,255,255,47,9,247,162,255
  DEFB 0,3,0,110,16,104,112,20,64,0,48,6,225,0,6,135
  DEFB 0,0,142,0,18,32,4,136,128,9,142,0,40,15,224,0
  DEFB 133,194,72,199,140,31,192,0,254,0,8,92,32,12,120,192
  DEFB 7,207,128,31,32,0,126,18,133,35,33,0,126,244,126,30
  DEFB 31,92,0,180,14,64,160,71,0,3,32,23,0,0,159,15
  DEFB 244,7,241,9,64,239,30,237,20,32,4,112,228,0,244,238
  DEFB 1,208,110,1,0,66,2,16,0,7,239,64,7,225,224,1
  DEFB 149,224,25,92,0,5,254,28,0,0,0,4,141,96,68,0
  DEFB 0,192,0,118,8,22,14,40,2,0,12,96,135,0,96,225
  DEFB 0,0,113,0,72,4,32,17,1,144,113,0,20,240,7,0
  DEFB 161,67,18,227,49,248,3,0,127,0,16,58,4,48,30,3
  DEFB 224,243,1,248,4,0,126,72,161,196,132,0,126,47,126,120
  DEFB 248,58,0,45,112,2,5,226,0,192,4,232,0,0,249,240
  DEFB 47,224,143,144,2,247,120,183,40,4,32,14,39,0,47,119
  DEFB 128,11,118,128,0,66,64,8,0,224,247,2,224,135,7,128
  DEFB 169,7,152,58,0,160,127,56,0,0,0,32,177,6,34,0
  DEFB 0,195,127,110,23,104,119,212,95,252,55,246,225,127,246,135
  DEFB 127,254,142,255,210,47,244,168,191,233,142,255,40,239,239,254
  DEFB 133,194,72,199,141,223,223,254,254,127,232,92,47,236,120,223
  DEFB 247,207,191,223,47,255,126,210,133,163,161,127,126,245,126,30
  DEFB 223,93,252,181,206,95,167,71,127,251,47,215,255,127,159,239
  DEFB 244,247,241,233,95,239,222,237,212,47,244,119,228,254,245,238
  DEFB 253,215,110,253,255,66,250,23,255,247,239,95,247,225,239,253
  DEFB 149,239,217,93,255,245,254,220,127,255,255,244,141,111,68,127
  DEFB 0,195,254,118,232,22,238,43,250,63,236,111,135,254,111,225
  DEFB 254,127,113,255,75,244,47,21,253,151,113,255,20,247,247,127
  DEFB 161,67,18,227,177,251,251,127,127,254,23,58,244,55,30,251
  DEFB 239,243,253,251,244,255,126,75,161,197,133,254,126,175,126,120
  DEFB 251,186,63,173,115,250,229,226,254,223,244,235,255,254,249,247
  DEFB 47,239,143,151,250,247,123,183,43,244,47,238,39,127,175,119
  DEFB 191,235,118,191,255,66,95,232,255,239,247,250,239,135,247,191
  DEFB 169,247,155,186,255,175,127,59,254,255,255,47,177,246,34,254
  DEFB 0,43,128,68,136,36,80,20,64,2,184,4,72,128,2,69
  DEFB 0,0,141,0,18,32,4,132,64,11,132,0,40,16,112,0
  DEFB 133,130,40,143,196,31,192,1,7,128,8,88,32,8,252,64
  DEFB 7,135,128,31,240,0,255,22,133,35,67,128,114,228,29,254
  DEFB 31,92,3,178,61,96,240,39,128,4,112,13,0,0,23,7
  DEFB 116,8,120,9,64,31,15,237,22,112,4,112,110,0,244,241
  DEFB 0,208,110,1,0,71,4,56,0,7,46,64,1,223,224,1
  DEFB 149,144,25,92,0,3,114,29,128,0,0,3,72,112,39,128
  DEFB 0,212,1,34,17,36,10,40,2,64,29,32,18,1,64,162
  DEFB 0,0,177,0,72,4,32,33,2,208,33,0,20,8,14,0
  DEFB 161,65,20,241,35,248,3,128,224,1,16,26,4,16,63,2
  DEFB 224,225,1,248,15,0,255,104,161,196,194,1,78,39,184,127
  DEFB 248,58,192,77,188,6,15,228,1,32,14,176,0,0,232,224
  DEFB 46,16,30,144,2,248,240,183,104,14,32,14,118,0,47,143
  DEFB 0,11,118,128,0,226,32,28,0,224,116,2,128,251,7,128
  DEFB 169,9,152,58,0,192,78,184,1,0,0,192,18,14,228,1
  DEFB 0,171,191,68,139,164,87,212,95,250,187,244,72,191,250,69
  DEFB 127,254,141,127,210,47,244,180,95,235,132,255,168,208,119,254
  DEFB 133,130,168,143,197,223,223,253,7,191,232,88,47,232,252,95
  DEFB 247,183,191,223,247,254,255,214,133,163,67,191,114,229,157,254
  DEFB 223,93,251,178,61,96,247,167,191,228,119,237,255,127,23,247
  DEFB 116,232,120,233,95,31,239,237,214,119,244,119,110,255,245,241
  DEFB 254,215,110,253,127,71,244,59,255,247,46,95,249,223,239,253
  DEFB 149,151,217,93,255,251,114,221,191,255,255,251,72,119,167,191
  DEFB 0,213,253,34,209,37,234,43,250,95,221,47,18,253,95,162
  DEFB 254,127,177,254,75,244,47,45,250,215,33,255,21,11,238,127
  DEFB 161,65,21,241,163,251,251,191,224,253,23,26,244,23,63,250
  DEFB 239,237,253,251,239,127,255,107,161,197,194,253,78,167,185,127
  DEFB 251,186,223,77,188,6,239,229,253,39,238,183,255,254,232,239
  DEFB 46,23,30,151,250,248,247,183,107,238,47,238,118,255,175,143
  DEFB 127,235,118,191,254,226,47,220,255,239,116,250,159,251,247,191
  DEFB 169,233,155,186,255,223,78,187,253,255,255,223,18,238,229,253
  DEFB 0,86,128,69,200,44,16,12,64,5,104,4,92,128,2,193
  DEFB 0,0,159,128,18,32,5,2,96,6,165,0,36,26,240,0
  DEFB 165,146,39,159,232,31,192,1,175,128,10,89,32,9,254,128
  DEFB 7,131,64,15,144,0,255,29,133,35,106,128,120,230,29,94
  DEFB 3,72,6,170,195,127,144,35,192,31,144,9,0,0,31,1
  DEFB 12,31,4,9,0,15,14,109,22,240,6,64,36,0,240,224
  DEFB 0,208,110,1,0,111,6,168,0,7,142,96,1,213,224,0
  DEFB 148,144,9,72,0,1,56,29,64,0,0,3,95,144,34,128
  DEFB 0,106,1,162,19,52,8,48,2,160,22,32,58,1,64,131
  DEFB 0,0,249,1,72,4,160,64,6,96,165,0,36,88,15,0
  DEFB 165,73,228,249,23,248,3,128,245,1,80,154,4,144,127,1
  DEFB 224,193,2,240,9,0,255,184,161,196,86,1,30,103,184,122
  DEFB 192,18,96,85,195,254,9,196,3,248,9,144,0,0,248,128
  DEFB 48,248,32,144,0,240,112,182,104,15,96,2,36,0,15,7
  DEFB 0,11,118,128,0,246,96,21,0,224,113,6,128,171,7,0
  DEFB 41,9,144,18,0,128,28,184,2,0,0,192,250,9,68,1
  DEFB 0,86,191,69,203,172,23,236,95,245,107,244,92,191,250,193
  DEFB 127,254,159,191,210,47,245,122,111,246,165,255,164,218,247,254
  DEFB 165,146,167,159,235,223,223,253,175,159,234,89,47,233,254,191
  DEFB 231,187,79,239,151,254,255,221,133,163,106,191,120,230,221,94
  DEFB 227,75,230,170,195,127,151,163,223,223,151,233,255,127,31,249
  DEFB 140,223,4,233,31,15,238,109,214,247,246,79,164,255,243,224
  DEFB 254,215,110,253,127,111,246,187,255,247,142,111,253,213,239,254
  DEFB 148,151,233,75,255,253,56,221,95,248,127,251,95,151,162,191
  DEFB 0,106,253,162,211,53,232,55,250,175,214,47,58,253,95,131
  DEFB 254,127,249,253,75,244,175,94,246,111,165,255,37,91,239,127
  DEFB 165,73,229,249,215,251,251,191,245,249,87,154,244,151,127,253
  DEFB 231,221,242,247,233,127,255,187,161,197,86,253,30,103,187,122
  DEFB 199,210,103,85,195,254,233,197,251,251,233,151,255,254,248,159
  DEFB 49,251,32,151,248,240,119,182,107,239,111,242,37,255,207,7
  DEFB 127,235,118,191,254,246,111,221,255,239,113,246,191,171,247,127
  DEFB 41,233,151,210,255,191,28,187,250,31,254,223,250,233,69,253
  DEFB 0,40,128,68,72,46,16,31,64,2,144,4,68,128,2,225
  DEFB 0,1,212,128,59,160,15,1,224,6,164,0,44,16,80,0
  DEFB 165,146,23,127,248,63,192,1,5,224,10,89,32,7,255,128
  DEFB 31,129,64,31,8,1,246,4,133,47,67,128,120,230,29,94
  DEFB 1,72,31,238,55,112,136,23,32,36,8,9,0,0,231,3
  DEFB 29,47,2,30,0,79,14,173,22,112,4,192,61,0,240,228
  DEFB 0,208,110,1,0,71,4,56,0,7,142,96,1,213,224,0
  DEFB 148,112,9,72,0,0,152,29,32,7,128,5,47,136,19,128
  DEFB 0,20,1,34,18,116,8,248,2,64,9,32,34,1,64,135
  DEFB 0,128,43,1,220,5,240,128,7,96,37,0,52,8,10,0
  DEFB 165,73,232,254,31,252,3,128,160,7,80,154,4,224,255,1
  DEFB 248,129,2,248,16,128,111,32,161,244,194,1,30,103,184,122
  DEFB 128,18,248,119,236,14,17,232,4,36,16,144,0,0,231,192
  DEFB 184,244,64,120,0,242,112,181,104,14,32,3,188,0,15,39
  DEFB 0,11,118,128,0,226,32,28,0,224,113,6,128,171,7,0
  DEFB 41,14,144,18,0,0,25,184,4,224,1,160,244,17,200,1
  DEFB 0,168,191,68,75,174,23,223,95,250,147,244,68,191,250,225
  DEFB 127,253,212,191,187,175,239,125,239,246,164,255,172,208,87,254
  DEFB 165,146,215,127,251,191,223,253,5,239,234,89,47,247,255,191
  DEFB 223,189,79,223,11,253,246,228,133,175,67,143,120,230,221,94
  DEFB 253,75,159,238,55,112,139,215,47,164,11,233,255,127,231,251
  DEFB 221,175,250,222,127,79,238,173,214,119,244,223,189,255,247,238
  DEFB 254,215,110,253,127,71,244,56,255,247,142,111,253,213,239,254
  DEFB 148,119,233,75,255,254,152,221,175,247,191,245,47,139,211,159
  DEFB 0,21,253,34,210,117,232,251,250,95,201,47,34,253,95,135
  DEFB 254,191,43,253,221,245,247,190,247,111,37,255,53,11,234,127
  DEFB 165,73,235,254,223,253,251,191,160,247,87,154,244,239,255,253
  DEFB 251,189,242,251,208,191,111,39,161,245,194,241,30,103,187,122
  DEFB 191,210,249,119,236,14,209,235,244,37,208,151,255,254,231,223
  DEFB 187,245,95,123,254,242,119,181,107,238,47,251,189,255,239,119
  DEFB 127,235,118,191,254,226,47,28,255,239,113,246,191,171,247,127
  DEFB 41,238,151,210,255,127,25,187,245,239,253,175,244,209,203,249
  DEFB 0,33,0,76,104,54,16,53,192,2,16,4,198,128,3,193
  DEFB 0,1,117,0,110,224,25,1,96,2,44,0,46,30,80,0
  DEFB 165,146,15,63,224,66,32,1,229,112,10,89,32,3,254,0
  DEFB 48,130,192,36,72,2,20,2,5,63,53,240,124,230,29,94
  DEFB 1,120,100,233,255,112,200,14,112,68,72,4,0,0,198,7
  DEFB 63,80,3,62,0,79,14,205,22,240,6,192,127,0,240,228
  DEFB 0,208,6,1,0,111,3,95,0,7,206,96,1,213,224,1
  DEFB 244,224,31,112,0,0,156,29,16,8,64,0,142,200,15,96
  DEFB 0,132,0,50,22,108,8,172,3,64,8,32,99,1,192,131
  DEFB 0,128,174,0,118,7,152,128,6,64,52,0,116,120,10,0
  DEFB 165,73,240,252,7,66,4,128,167,14,80,154,4,192,127,0
  DEFB 12,65,3,36,18,64,40,64,160,252,172,15,62,103,184,122
  DEFB 128,30,38,151,255,14,19,112,14,34,18,32,0,0,99,224
  DEFB 252,10,192,124,0,242,112,179,104,15,96,3,254,0,15,39
  DEFB 0,11,96,128,0,246,192,250,0,224,115,6,128,171,7,128
  DEFB 47,7,248,14,0,0,57,184,8,16,2,0,113,19,240,6
  DEFB 0,161,127,76,107,182,23,181,223,250,23,244,198,191,251,193
  DEFB 127,253,117,127,110,239,217,125,111,250,44,255,174,222,87,254
  DEFB 165,146,239,191,231,66,47,253,229,119,234,89,47,251,254,127
  DEFB 176,186,207,164,75,226,20,250,5,191,181,247,124,230,221,94
  DEFB 253,123,100,233,255,112,203,238,119,68,75,244,247,255,198,247
  DEFB 191,80,251,190,127,79,238,205,214,247,246,223,127,255,247,228
  DEFB 254,215,6,253,127,111,251,95,127,247,206,111,253,213,239,253
  DEFB 244,239,223,119,253,254,156,221,215,232,95,250,142,203,239,111
  DEFB 0,133,254,50,214,109,232,173,251,95,232,47,99,253,223,131
  DEFB 254,191,174,254,118,247,155,190,246,95,52,255,117,123,234,127
  DEFB 165,73,247,253,231,66,244,191,167,238,87,154,244,223,127,254
  DEFB 13,93,243,37,210,71,40,95,160,253,173,239,62,103,187,122
  DEFB 191,222,38,151,255,14,211,119,238,34,210,47,239,255,99,239
  DEFB 253,10,223,125,254,242,119,179,107,239,111,251,254,255,239,39
  DEFB 127,235,96,191,254,246,223,250,254,239,115,246,191,171,247,191
  DEFB 47,247,251,238,191,127,57,187,235,23,250,95,113,211,247,246
  DEFB 0,61,128,101,72,53,224,72,64,3,216,6,84,128,3,94
  DEFB 0,2,30,0,68,32,17,2,32,2,37,0,54,8,32,0
  DEFB 163,122,15,63,224,127,224,0,130,240,10,55,160,3,254,0
  DEFB 15,135,128,127,104,7,247,1,35,63,33,248,92,230,29,94
  DEFB 3,120,69,222,255,127,232,14,240,78,200,4,8,0,204,7
  DEFB 127,90,7,126,0,79,14,239,30,224,7,192,255,0,240,228
  DEFB 0,240,232,1,0,126,2,31,128,5,206,96,1,213,224,3
  DEFB 245,192,63,112,2,0,92,29,8,16,224,0,126,232,15,112
  DEFB 0,188,1,166,18,172,7,18,2,192,27,96,42,1,192,122
  DEFB 0,64,120,0,34,4,136,64,4,64,164,0,108,16,4,0
  DEFB 197,94,240,252,7,254,7,0,65,15,80,236,5,192,127,0
  DEFB 240,225,1,254,22,224,239,128,196,252,132,31,58,103,184,122
  DEFB 192,30,162,123,255,254,23,112,15,114,19,32,16,0,51,224
  DEFB 254,90,224,126,0,242,112,247,120,7,224,3,255,0,15,39
  DEFB 0,15,23,128,0,126,64,248,1,160,115,6,128,171,7,192
  DEFB 175,3,252,14,64,0,58,184,16,8,7,0,126,23,240,14
  DEFB 0,189,191,101,75,181,239,72,95,251,219,246,84,191,251,94
  DEFB 255,250,30,255,68,47,209,122,47,250,37,255,182,200,35,254
  DEFB 163,122,239,191,239,127,239,252,130,247,234,55,175,251,254,255
  DEFB 143,183,191,127,107,247,247,253,35,191,161,251,92,230,221,94
  DEFB 251,123,69,222,255,127,235,238,247,78,203,244,203,255,205,247
  DEFB 127,90,247,126,127,79,238,239,222,227,247,223,255,254,247,228
  DEFB 254,247,232,253,63,126,250,31,191,245,206,111,253,213,239,251
  DEFB 245,223,191,119,242,255,92,221,232,208,239,255,126,235,239,119
  DEFB 0,189,253,166,210,173,247,18,250,223,219,111,42,253,223,122
  DEFB 255,95,120,255,34,244,139,94,244,95,164,255,109,19,196,127
  DEFB 197,94,247,253,247,254,247,63,65,239,87,236,245,223,127,255
  DEFB 241,237,253,254,214,239,239,191,196,253,133,223,58,103,187,122
  DEFB 223,222,162,123,255,254,215,119,239,114,211,47,211,255,179,239
  DEFB 254,90,239,126,254,242,119,247,123,199,239,251,255,127,239,39
  DEFB 127,239,23,191,252,126,95,248,253,175,115,246,191,171,247,223
  DEFB 175,251,253,238,79,255,58,187,23,11,247,255,126,215,247,238

; Number string and graphic buffer
;
; Used by the routine at NUMGRAPH.
NBUF:
  DEFS 11                 ; The ASCII codes of the digits of the number are
                          ; stored here
NBUFG:
  DEFS 21                 ; The corresponding graphic data is generated and
                          ; stored here

; Font character bitmap widths for CHR$(32-127)
;
; Used by the routines at WRITECHR and TXT2BUF.
  DEFB 3,1,3,5,5,3,5,2,2,2,5,5,2,4,2,3
  DEFB 4,3,4,4,4,4,4,4,4,4,2,2,4,4,4,4
  DEFB 5,4,4,4,4,4,4,4,4,3,4,4,4,5,4,4
  DEFB 4,5,4,4,5,4,5,5,5,5,5,2,3,2,5,5
  DEFB 4,4,4,4,4,4,4,4,4,1,3,4,1,5,4,4
  DEFB 4,4,4,4,3,4,3,5,5,4,4,3,1,3,4,5

; 'ERIC'
;
; Used by the routines at NEXTCHR, NAME2BUF and WALKCHAR2.
M_ERIC:
  DEFM "ERIC"
  DEFB 0                  ; End marker
  DEFS 9

; 'BOY WANDER'
;
; Used by the routines at NEXTCHR, NAME2BUF and WALKCHAR2.
M_BOYWANDER:
  DEFM "BOY WANDER"
  DEFB 0                  ; End marker
  DEFS 3

; 'ANGELFACE'
;
; Used by the routines at NEXTCHR, NAME2BUF and WALKCHAR2.
M_ANGELFACE:
  DEFM "ANGELFACE"
  DEFB 0                  ; End marker
  DEFS 4

; 'EINSTEIN'
;
; Used by the routines at NEXTCHR, NAME2BUF and WALKCHAR2.
M_EINSTEIN:
  DEFM "EINSTEIN"
  DEFB 0                  ; End marker
  DEFS 5

; 'MR WACKER'
;
; Used by the routines at NEXTCHR, PRTLESSON and WALKCHAR2.
M_WACKER:
  DEFM "MR WACKER"
  DEFB 0                  ; End marker
  DEFS 4

; 'MR ROCKITT'
;
; Used by the routines at NEXTCHR, PRTLESSON and WALKCHAR2.
M_ROCKITT:
  DEFM "MR ROCKITT"
  DEFB 0                  ; End marker
  DEFS 3

; 'MR WITHIT'
;
; Used by the routines at NEXTCHR, PRTLESSON and WALKCHAR2.
M_WITHIT:
  DEFM "MR WITHIT"
  DEFB 0                  ; End marker
  DEFS 4

; 'MR CREAK'
;
; Used by the routines at NEXTCHR, PRTLESSON and WALKCHAR2.
M_CREAK:
  DEFM "MR CREAK"
  DEFB 0                  ; End marker
  DEFS 5

; 'Please Sir - I cannot tell a lie . . '
;
; Used by the routine at NEXTCHR.
M_PLEASESIR:
  DEFM "Please Sir - I cannot tell a lie . . "
  DEFB 0                  ; End marker

; '        ' (8 spaces)
;
; Used by the routine at SLIDECHR1, and as a suffix to several other messages.
M_SPACE8:
  DEFM "        "
  DEFB 0                  ; End marker

; Unused
  DEFB 0

; Font graphic data for CHR$(32-127) (pixel column 1)
;
; Used by the routines at WRITECHR and TXT2BUF.
  DEFB 0,250,192,40,18,78,108,64,124,130,16,8,1,8,3,7
  DEFB 124,66,70,130,56,242,124,128,108,98,54,49,8,20,34,64
  DEFB 76,126,254,124,254,254,254,126,254,130,4,254,254,254,254,124
  DEFB 254,124,254,98,128,252,224,252,198,192,134,255,192,129,32,1
  DEFB 2,28,254,28,28,28,16,24,254,94,1,254,254,62,62,28
  DEFB 63,24,62,18,16,60,56,56,34,56,38,24,255,129,8,124

; 'REVISION'
;
; Used in conjunction with 'LIBRARY' (at M_LIBRARY) by the routine at
; PRTLESSON.
M_REVISION:
  DEFM "REVISION"
  DEFB 0                  ; End marker

; ' ' (single space)
;
; Used by the routine at PRTLESSON.
M_SPACE_2:
  DEFM " "
  DEFB 0                  ; End marker

; Unused
  DEFS 5

; 'READING ROOM'
;
; Used by the routine at PRTLESSON.
M_READINGRM:
  DEFM "READING ROOM"
  DEFB 0                  ; End marker
  DEFS 3

; 'MAP ROOM'
;
; Used by the routine at PRTLESSON.
M_MAPROOM:
  DEFM "MAP ROOM"
  DEFB 0                  ; End marker
  DEFS 7

; 'WHITE ROOM'
;
; Used by the routine at PRTLESSON.
M_WHITEROOM:
  DEFM "WHITE ROOM"
  DEFB 0                  ; End marker
  DEFS 5

; 'EXAM ROOM'
;
; Used by the routine at PRTLESSON.
M_EXAMROOM:
  DEFM "EXAM ROOM"
  DEFB 0                  ; End marker
  DEFS 6

; 'LIBRARY'
;
; Used in conjunction with 'REVISION' (at M_REVISION) by the routine at
; PRTLESSON.
M_LIBRARY:
  DEFM "LIBRARY"
  DEFB 0                  ; End marker
  DEFS 8

; 'DINNER'
;
; Used by the routine at PRTLESSON.
M_DINNER:
  DEFM "DINNER"
  DEFB 0                  ; End marker
  DEFS 9

; 'PLAYTIME'
;
; Used by the routine at PRTLESSON.
M_PLAYTIME:
  DEFM "PLAYTIME"
  DEFB 0                  ; End marker
  DEFS 7

; Initial animatory states of the characters
;
; Used by the routine at START.
INITIALAS:
  DEFB 64                 ; 64=little boy (facing left)
  DEFB 192                ; 192=little boy (facing right)
  DEFB 64                 ; 64=little boy (facing left)
  DEFB 192                ; 192=little boy (facing right)
  DEFB 64                 ; 64=little boy (facing left)
  DEFB 192                ; 192=little boy (facing right)
  DEFB 64                 ; 64=little boy (facing left)
  DEFB 192                ; 192=little boy (facing right)
  DEFB 64                 ; 64=little boy (facing left)
  DEFB 192                ; 192=little boy (facing right)
  DEFB 64                 ; 64=little boy (facing left)
  DEFB 80                 ; 80=MR WACKER
  DEFB 88                 ; 88=MR ROCKITT
  DEFB 96                 ; 96=MR WITHIT
  DEFB 72                 ; 72=MR CREAK
  DEFB 48                 ; 48=BOY WANDER
  DEFB 32                 ; 32=ANGELFACE
  DEFB 16                 ; 16=EINSTEIN
  DEFB 127                ; 127=catapult pellet (BOY WANDER's)
  DEFB 127                ; 127=catapult pellet (ERIC's)
  DEFB 0                  ; 0=ERIC

; Unused
  DEFS 11

; Font graphic data for CHR$(32-127) (pixel column 2)
;
; Used by the routines at WRITECHR and TXT2BUF. The following addresses are
; unused, because the corresponding font character bitmaps are less than 2
; pixels wide:
;
; +------------+---------+
; | ASCII code | Address |
; +------------+---------+
; | 33 [!]     | 56097   |
; | 105 [i]    | 56169   |
; | 108 [l]    | 56172   |
; | 124 [|]    | 56188   |
; +------------+---------+
  DEFB 0,0,0,254,42,16,146,128,130,124,124,8,2,8,3,56
  DEFB 138,254,138,146,72,146,146,134,146,146,54,50,20,20,20,138
  DEFB 82,144,146,130,130,146,144,130,16,254,2,16,2,64,96,130
  DEFB 144,130,144,146,128,2,28,2,40,32,138,129,56,255,64,1
  DEFB 126,34,34,34,34,42,126,37,32,0,1,16,0,32,16,34
  DEFB 36,36,16,42,126,2,6,6,20,5,42,102,0,102,16,146

; 'DON'T SIT ON THE STAIRS'
;
; Used by the routine at LINESMSG.
M_LINES0:
  DEFM "DON'T SIT ON"
  DEFB 0                  ; End marker
  DEFS 3
  DEFM "THE STAIRS"
  DEFB 0                  ; End marker
  DEFS 5

; 'THE ROOM IS PRIVATE'
;
; Used by the routine at LINESMSG.
M_LINES1:
  DEFM "THE ROOM IS"
  DEFB 0                  ; End marker
  DEFS 4
  DEFM "PRIVATE"
  DEFB 0                  ; End marker
  DEFS 8

; 'GET TO WHERE YOU SHOULD BE'
;
; Used by the routine at LINESMSG.
M_LINES2:
  DEFM "GET TO WHERE"
  DEFB 0                  ; End marker
  DEFS 3
  DEFM "YOU SHOULD BE"
  DEFB 0                  ; End marker
  DEFS 2

; 'NOW FIND A SEAT'
;
; Used by the routine at LINESMSG.
M_LINES3:
  DEFM "NOW FIND"
  DEFB 0                  ; End marker
  DEFS 7
  DEFM "A SEAT"
  DEFB 0                  ; End marker
  DEFS 9

; Initial x-coordinates of the characters
;
; Used by the routine at START.
INITIALX:
  DEFB 43                 ; The 11 little boys stand one next to another at
  DEFB 44                 ; x-coordinates 43-53
  DEFB 45                 ;
  DEFB 46                 ;
  DEFB 47                 ;
  DEFB 48                 ;
  DEFB 49                 ;
  DEFB 50                 ;
  DEFB 51                 ;
  DEFB 52                 ;
  DEFB 53                 ;
  DEFB 10                 ; MR WACKER, MR ROCKITT, MR WITHIT and MR CREAK all
  DEFB 10                 ; stand at x-coordinate 10
  DEFB 10                 ;
  DEFB 10                 ;
  DEFB 25                 ; BOY WANDER is on his own at x-coordinate 25
  DEFB 70                 ; ANGELFACE and EINSTEIN stand one behind the other
  DEFB 70                 ; at x-coordinate 70
  DEFB 160                ; BOY WANDER's and ERIC's catapult pellets begin life
  DEFB 160                ; off-screen at x-coordinate 160
  DEFB 48                 ; ERIC stands in the middle of the crowd of little
                          ; boys at x-coordinate 48

; Unused
  DEFS 11

; Font graphic data for CHR$(32-127) (pixel column 3)
;
; Used by the routines at WRITECHR and TXT2BUF. The following addresses are
; unused, because the corresponding font character bitmaps are less than 3
; pixels wide:
;
; +---------------+-------------+
; | ASCII code(s) | Address(es) |
; +---------------+-------------+
; | 33 [!]        | 56353       |
; | 39-41 ['()]   | 56359-56361 |
; | 44 [,]        | 56364       |
; | 46 [.]        | 56366       |
; | 58-59 [:;]    | 56378-56379 |
; | 91 [[]        | 56411       |
; | 93 []]        | 56413       |
; | 105 [i]       | 56425       |
; | 108 [l]       | 56428       |
; | 124 [|]       | 56444       |
; +---------------+-------------+
  DEFB 0,0,192,40,127,228,146,0,0,0,56,62,0,8,0,192
  DEFB 146,2,146,178,254,146,146,152,146,146,0,0,20,20,20,144
  DEFB 94,144,146,130,130,146,144,138,16,130,2,40,2,48,24,130
  DEFB 144,134,152,146,254,2,2,28,16,30,146,0,3,0,191,1
  DEFB 146,34,34,34,34,42,144,37,32,0,94,40,0,30,32,34
  DEFB 36,36,32,42,16,2,56,56,8,5,50,129,0,24,8,170

; 'GET OFF THE FLOOR'
;
; Used by the routine at LINESMSG.
M_LINES4:
  DEFM "GET OFF"
  DEFB 0                  ; End marker
  DEFS 8
  DEFM "THE FLOOR"
  DEFB 0                  ; End marker
  DEFS 6

; 'COME ALONG WITH ME BOY'
;
; Used by the routine at LINESMSG.
M_LINES5:
  DEFM "COME ALONG"
  DEFB 0                  ; End marker
  DEFS 5
  DEFM "WITH ME BOY"
  DEFB 0                  ; End marker
  DEFS 4

; 'HURRY UP YOU HORROR'
;
; Used by the routine at LINESMSG.
M_LINES6:
  DEFM "HURRY UP YOU"
  DEFB 0                  ; End marker
  DEFS 3
  DEFM "HORROR"
  DEFB 0                  ; End marker
  DEFS 9

; 'DON'T TRY MY PATIENCE BOY'
;
; Used by the routine at LINESMSG.
M_LINES7:
  DEFM "DON'T TRY MY"
  DEFB 0                  ; End marker
  DEFS 3
  DEFM "PATIENCE BOY"
  DEFB 0                  ; End marker
  DEFS 3

; Initial byte 122 of the character buffers
;
; Used by the routine at START. See the character buffer documentation for
; descriptions of the individual bits in byte 122.
INITIAL122:
  DEFB 0                  ; Initial value of byte 122 for the little boys
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 0                  ;
  DEFB 16                 ; MR WACKER, MR ROCKITT, MR WITHIT and MR CREAK have
  DEFB 16                 ; bit 4 set to indicate that they are teachers; note
  DEFB 16                 ; that bit 7 is reset, making them run at the start
  DEFB 16                 ; of the game (which is a bug)
  DEFB 0                  ; Initial value of byte 122 for BOY WANDER, ANGELFACE
  DEFB 0                  ; and EINSTEIN
  DEFB 0                  ;
  DEFB 32                 ; BOY WANDER's and ERIC's catapult pellets have bit 5
  DEFB 32                 ; set, making them fly at top speed always
  DEFB 255                ; End marker

; Unused
  DEFS 11

; Font graphic data for CHR$(32-127) (pixel column 4)
;
; Used by the routines at WRITECHR and TXT2BUF. The following addresses are
; unused, because the corresponding font character bitmaps are less than 4
; pixels wide:
;
; +---------------+-------------+
; | ASCII code(s) | Address(es) |
; +---------------+-------------+
; | 32-34 [ !"]   | 56608-56610 |
; | 37 [%]        | 56613       |
; | 39-41 ['()]   | 56615-56617 |
; | 44 [,]        | 56620       |
; | 46-47 [./]    | 56622-56623 |
; | 49 [1]        | 56625       |
; | 58-59 [:;]    | 56634-56635 |
; | 73 [I]        | 56649       |
; | 91-93 [[\]]   | 56667-56669 |
; | 105-106 [ij]  | 56681-56682 |
; | 108 [l]       | 56684       |
; | 116 [t]       | 56692       |
; | 118 [v]       | 56694       |
; | 123-125 [{|}] | 56699-56701 |
; +---------------+-------------+
  DEFB 0,0,0,254,42,0,109,0,0,0,124,8,0,8,0,0
  DEFB 124,0,98,204,8,140,140,224,108,124,0,0,34,20,8,96
  DEFB 66,126,108,130,124,130,128,78,254,0,252,198,2,64,254,124
  DEFB 96,124,102,140,128,252,28,2,40,32,162,0,0,0,64,1
  DEFB 130,62,28,34,254,24,64,62,30,0,0,6,0,32,30,28
  DEFB 24,63,16,36,0,62,0,6,20,62,34,0,0,0,16,130

; 'NOW DON'T DO IT AGAIN'
;
; Used by the routine at LINESMSG.
M_LINES8:
  DEFM "NOW DON'T"
  DEFB 0                  ; End marker
  DEFS 6
  DEFM "DO IT AGAIN"
  DEFB 0                  ; End marker
  DEFS 4

; 'DON'T TELL TALES'
;
; Used by the routine at LINESMSG.
M_LINES9:
  DEFM "DON'T TELL"
  DEFB 0                  ; End marker
  DEFS 5
  DEFM "TALES"
  DEFB 0                  ; End marker
  DEFS 10

; 'NEVER BE LATE AGAIN'
;
; Used by the routine at LINESMSG.
M_LINES10:
  DEFM "NEVER BE"
  DEFB 0                  ; End marker
  DEFS 7
  DEFM "LATE AGAIN"
  DEFB 0                  ; End marker
  DEFS 5

; 'AND STAY THIS TIME'
;
; Used by the routine at LINESMSG.
M_LINES11:
  DEFM "AND STAY"
  DEFB 0                  ; End marker
  DEFS 7
  DEFM "THIS TIME"
  DEFB 0                  ; End marker
  DEFS 6

; 'TAKE 2000 LINES YOU NASTY BOY{8 spaces}'
;
; Used by the routine at LINES2K.
M_LINES2K:
  DEFM "TAKE 2000 LINES YOU NASTY BOY"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; Unused
  DEFS 2

; Font graphic data for CHR$(32-127) (pixel column 5)
;
; Used by the routines at WRITECHR and TXT2BUF. The following addresses are
; unused, because the corresponding font character bitmaps are less than 5
; pixels wide:
;
; +------------------------------+-------------+
; | ASCII code(s)                | Address(es) |
; +------------------------------+-------------+
; | 32-34 [ !"]                  | 56864-56866 |
; | 37 [%]                       | 56869       |
; | 39-41 ['()]                  | 56871-56873 |
; | 44-63 [,-./0123456789:;<=>?] | 56876-56895 |
; | 65-76 [ABCDEFGHIJKL]         | 56897-56908 |
; | 78-80 [NOP]                  | 56910-56912 |
; | 82-83 [RS]                   | 56914-56915 |
; | 85 [U]                       | 56917       |
; | 91-93 [[\]]                  | 56923-56925 |
; | 96-108 [`abcdefghijkl]       | 56928-56940 |
; | 110-118 [nopqrstuv]          | 56942-56950 |
; | 121-126 [yz{|}~]             | 56953-56958 |
; +------------------------------+-------------+
  DEFB 0,0,0,40,36,0,2,0,0,0,16,8,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 60,0,0,0,0,0,0,0,0,0,0,0,0,254,0,0
  DEFB 0,2,0,0,128,0,224,252,198,192,194,0,0,0,32,1
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0
  DEFB 0,0,0,0,0,0,0,56,34,0,0,0,0,0,0,124

; 'DON'T TOUCH BLACKBOARDS'
;
; Used by the routine at LINESMSG.
M_LINES12:
  DEFM "DON'T TOUCH"
  DEFB 0                  ; End marker
  DEFS 4
  DEFM "BLACKBOARDS"
  DEFB 0                  ; End marker
  DEFS 4

; 'CATAPULTS ARE FORBIDDEN'
;
; Used by the routine at LINESMSG.
M_LINES13:
  DEFM "CATAPULTS"
  DEFB 0                  ; End marker
  DEFS 6
  DEFM "ARE FORBIDDEN"
  DEFB 0                  ; End marker
  DEFS 2

; 'DON'T HIT YOUR MATES'
;
; Used by the routine at LINESMSG.
M_LINES14:
  DEFM "DON'T HIT"
  DEFB 0                  ; End marker
  DEFS 6
  DEFM "YOUR MATES"
  DEFB 0                  ; End marker
  DEFS 5

; 'YOU ARE NOT A KANGAROO'
;
; Used by the routine at LINESMSG.
M_LINES15:
  DEFM "YOU ARE NOT"
  DEFB 0                  ; End marker
  DEFS 4
  DEFM "A KANGAROO"
  DEFB 0                  ; End marker
  DEFS 5

; 'Sn'
;
; Question/answer word no. 128 for MR ROCKITT. The corresponding
; question/answer word (no. 129) is located at M_ELEMENT00. Used by the routine
; at PREPQA.
M_SYMBOL00:
  DEFM "Sn"
  DEFB 0                  ; End marker
  DEFM "Ug Ug"
  DEFS 4

; 'Tin'
;
; Question/answer word no. 129 for MR ROCKITT. The corresponding
; question/answer word (no. 128) is located at M_SYMBOL00. Used by the routine
; at PREPQA.
M_ELEMENT00:
  DEFM "Tin"
  DEFB 0                  ; End marker
  DEFS 8

; 'Hg'
;
; Question/answer word no. 130 for MR ROCKITT. The corresponding
; question/answer word (no. 131) is located at M_ELEMENT01. Used by the routine
; at PREPQA.
M_SYMBOL01:
  DEFM "Hg"
  DEFB 0                  ; End marker
  DEFS 9

; 'Mercury'
;
; Question/answer word no. 131 for MR ROCKITT. The corresponding
; question/answer word (no. 130) is located at M_SYMBOL01. Used by the routine
; at PREPQA.
M_ELEMENT01:
  DEFM "Mercury"
  DEFB 0                  ; End marker
  DEFS 4

; 'Au'
;
; Question/answer word no. 132 for MR ROCKITT. The corresponding
; question/answer word (no. 133) is located at M_ELEMENT02. Used by the routine
; at PREPQA.
M_SYMBOL02:
  DEFM "Au"
  DEFB 0                  ; End marker
  DEFS 9

; 'Gold'
;
; Question/answer word no. 133 for MR ROCKITT. The corresponding
; question/answer word (no. 132) is located at M_SYMBOL02. Used by the routine
; at PREPQA.
M_ELEMENT02:
  DEFM "Gold"
  DEFB 0                  ; End marker
  DEFS 7

; 'Ag'
;
; Question/answer word no. 134 for MR ROCKITT. The corresponding
; question/answer word (no. 135) is located at M_ELEMENT03. Used by the routine
; at PREPQA.
M_SYMBOL03:
  DEFM "Ag"
  DEFB 0                  ; End marker
  DEFS 9

; 'Silver'
;
; Question/answer word no. 135 for MR ROCKITT. The corresponding
; question/answer word (no. 134) is located at M_SYMBOL03. Used by the routine
; at PREPQA.
M_ELEMENT03:
  DEFM "Silver"
  DEFB 0                  ; End marker
  DEFS 5

; 'Pt'
;
; Question/answer word no. 136 for MR ROCKITT. The corresponding
; question/answer word (no. 137) is located at M_ELEMENT04. Used by the routine
; at PREPQA.
M_SYMBOL04:
  DEFM "Pt"
  DEFB 0                  ; End marker
  DEFS 9

; 'Platinum'
;
; Question/answer word no. 137 for MR ROCKITT. The corresponding
; question/answer word (no. 136) is located at M_SYMBOL04. Used by the routine
; at PREPQA.
M_ELEMENT04:
  DEFM "Platinum"
  DEFB 0                  ; End marker
  DEFS 3

; 'Cu'
;
; Question/answer word no. 138 for MR ROCKITT. The corresponding
; question/answer word (no. 139) is located at M_ELEMENT05. Used by the routine
; at PREPQA.
M_SYMBOL05:
  DEFM "Cu"
  DEFB 0                  ; End marker
  DEFS 9

; 'Copper'
;
; Question/answer word no. 139 for MR ROCKITT. The corresponding
; question/answer word (no. 138) is located at M_SYMBOL05. Used by the routine
; at PREPQA.
M_ELEMENT05:
  DEFM "Copper"
  DEFB 0                  ; End marker
  DEFS 5

; 'Mg'
;
; Question/answer word no. 140 for MR ROCKITT. The corresponding
; question/answer word (no. 141) is located at M_ELEMENT06. Used by the routine
; at PREPQA.
M_SYMBOL06:
  DEFM "Mg"
  DEFB 0                  ; End marker
  DEFS 9

; 'Magnesium'
;
; Question/answer word no. 141 for MR ROCKITT. The corresponding
; question/answer word (no. 140) is located at M_SYMBOL06. Used by the routine
; at PREPQA.
M_ELEMENT06:
  DEFM "Magnesium"
  DEFB 0                  ; End marker
  DEFS 2

; 'Pb'
;
; Question/answer word no. 142 for MR ROCKITT. The corresponding
; question/answer word (no. 143) is located at M_ELEMENT07. Used by the routine
; at PREPQA.
M_SYMBOL07:
  DEFM "Pb"
  DEFB 0                  ; End marker
  DEFS 9

; 'Lead'
;
; Question/answer word no. 143 for MR ROCKITT. The corresponding
; question/answer word (no. 142) is located at M_SYMBOL07. Used by the routine
; at PREPQA.
M_ELEMENT07:
  DEFM "Lead"
  DEFB 0                  ; End marker
  DEFS 7

; 'Mn'
;
; Question/answer word no. 144 for MR ROCKITT. The corresponding
; question/answer word (no. 145) is located at M_ELEMENT08. Used by the routine
; at PREPQA.
M_SYMBOL08:
  DEFM "Mn"
  DEFB 0                  ; End marker
  DEFS 9

; 'Manganese'
;
; Question/answer word no. 145 for MR ROCKITT. The corresponding
; question/answer word (no. 144) is located at M_SYMBOL08. Used by the routine
; at PREPQA.
M_ELEMENT08:
  DEFM "Manganese"
  DEFB 0                  ; End marker
  DEFS 2

; 'Sb'
;
; Question/answer word no. 146 for MR ROCKITT. The corresponding
; question/answer word (no. 147) is located at M_ELEMENT09. Used by the routine
; at PREPQA.
M_SYMBOL09:
  DEFM "Sb"
  DEFB 0                  ; End marker
  DEFS 9

; 'Antimony'
;
; Question/answer word no. 147 for MR ROCKITT. The corresponding
; question/answer word (no. 146) is located at M_SYMBOL09. Used by the routine
; at PREPQA.
M_ELEMENT09:
  DEFM "Antimony"
  DEFB 0                  ; End marker
  DEFS 3

; 'As'
;
; Question/answer word no. 148 for MR ROCKITT. The corresponding
; question/answer word (no. 149) is located at M_ELEMENT10. Used by the routine
; at PREPQA.
M_SYMBOL10:
  DEFM "As"
  DEFB 0                  ; End marker
  DEFS 9

; 'Arsenic'
;
; Question/answer word no. 149 for MR ROCKITT. The corresponding
; question/answer word (no. 148) is located at M_SYMBOL10. Used by the routine
; at PREPQA.
M_ELEMENT10:
  DEFM "Arsenic"
  DEFB 0                  ; End marker
  DEFS 4

; 'K'
;
; Question/answer word no. 150 for MR ROCKITT. The corresponding
; question/answer word (no. 151) is located at M_ELEMENT11. Used by the routine
; at PREPQA.
M_SYMBOL11:
  DEFM "K"
  DEFB 0                  ; End marker
  DEFS 10

; 'Potassium'
;
; Question/answer word no. 151 for MR ROCKITT. The corresponding
; question/answer word (no. 150) is located at M_SYMBOL11. Used by the routine
; at PREPQA.
M_ELEMENT11:
  DEFM "Potassium"
  DEFB 0                  ; End marker
  DEFS 2

; 'Na'
;
; Question/answer word no. 152 for MR ROCKITT. The corresponding
; question/answer word (no. 153) is located at M_ELEMENT12. Used by the routine
; at PREPQA.
M_SYMBOL12:
  DEFM "Na"
  DEFB 0                  ; End marker
  DEFS 9

; 'Sodium'
;
; Question/answer word no. 153 for MR ROCKITT. The corresponding
; question/answer word (no. 152) is located at M_SYMBOL12. Used by the routine
; at PREPQA.
M_ELEMENT12:
  DEFM "Sodium"
  DEFB 0                  ; End marker
  DEFS 5

; 'Cl'
;
; Question/answer word no. 154 for MR ROCKITT. The corresponding
; question/answer word (no. 155) is located at M_ELEMENT13. Used by the routine
; at PREPQA.
M_SYMBOL13:
  DEFM "Cl"
  DEFB 0                  ; End marker
  DEFS 9

; 'Chlorine'
;
; Question/answer word no. 155 for MR ROCKITT. The corresponding
; question/answer word (no. 154) is located at M_SYMBOL13. Used by the routine
; at PREPQA.
M_ELEMENT13:
  DEFM "Chlorine"
  DEFB 0                  ; End marker
  DEFS 3

; 'Zn'
;
; Question/answer word no. 156 for MR ROCKITT. The corresponding
; question/answer word (no. 157) is located at M_ELEMENT14. Used by the routine
; at PREPQA.
M_SYMBOL14:
  DEFM "Zn"
  DEFB 0                  ; End marker
  DEFS 9

; 'Zinc'
;
; Question/answer word no. 157 for MR ROCKITT. The corresponding
; question/answer word (no. 156) is located at M_SYMBOL14. Used by the routine
; at PREPQA.
M_ELEMENT14:
  DEFM "Zinc"
  DEFB 0                  ; End marker
  DEFS 7

; 'W'
;
; Question/answer word no. 158 for MR ROCKITT. The corresponding
; question/answer word (no. 159) is located at M_ELEMENT15. Used by the routine
; at PREPQA.
M_SYMBOL15:
  DEFM "W"
  DEFB 0                  ; End marker
  DEFS 10

; 'Tungsten'
;
; Question/answer word no. 159 for MR ROCKITT. The corresponding
; question/answer word (no. 158) is located at M_SYMBOL15. Used by the routine
; at PREPQA.
M_ELEMENT15:
  DEFM "Tungsten"
  DEFB 0                  ; End marker
  DEFS 3

; 'Cs'
;
; Question/answer word no. 160 for MR ROCKITT. The corresponding
; question/answer word (no. 161) is located at M_ELEMENT16. Used by the routine
; at PREPQA.
M_SYMBOL16:
  DEFM "Cs"
  DEFB 0                  ; End marker
  DEFS 9

; 'Caesium'
;
; Question/answer word no. 161 for MR ROCKITT. The corresponding
; question/answer word (no. 160) is located at M_SYMBOL16. Used by the routine
; at PREPQA.
M_ELEMENT16:
  DEFM "Caesium"
  DEFB 0                  ; End marker
  DEFS 4

; 'Si'
;
; Question/answer word no. 162 for MR ROCKITT. The corresponding
; question/answer word (no. 163) is located at M_ELEMENT17. Used by the routine
; at PREPQA.
M_SYMBOL17:
  DEFM "Si"
  DEFB 0                  ; End marker
  DEFS 9

; 'Silicon'
;
; Question/answer word no. 163 for MR ROCKITT. The corresponding
; question/answer word (no. 162) is located at M_SYMBOL17. Used by the routine
; at PREPQA.
M_ELEMENT17:
  DEFM "Silicon"
  DEFB 0                  ; End marker
  DEFS 4

; 'P'
;
; Question/answer word no. 164 for MR ROCKITT. The corresponding
; question/answer word (no. 165) is located at M_ELEMENT18. Used by the routine
; at PREPQA.
M_SYMBOL18:
  DEFM "P"
  DEFB 0                  ; End marker
  DEFS 10

; 'Phosphorous'
;
; Question/answer word no. 165 for MR ROCKITT. The corresponding
; question/answer word (no. 164) is located at M_SYMBOL18. Used by the routine
; at PREPQA.
M_ELEMENT18:
  DEFM "Phosphorous"
  DEFB 0                  ; End marker

; 'Br'
;
; Question/answer word no. 166 for MR ROCKITT. The corresponding
; question/answer word (no. 167) is located at M_ELEMENT19. Used by the routine
; at PREPQA.
M_SYMBOL19:
  DEFM "Br"
  DEFB 0                  ; End marker
  DEFS 9

; 'Bromine'
;
; Question/answer word no. 167 for MR ROCKITT. The corresponding
; question/answer word (no. 166) is located at M_SYMBOL19. Used by the routine
; at PREPQA.
M_ELEMENT19:
  DEFM "Bromine"
  DEFB 0                  ; End marker
  DEFS 4

; 'H'
;
; Question/answer word no. 168 for MR ROCKITT. The corresponding
; question/answer word (no. 169) is located at M_ELEMENT20. Used by the routine
; at PREPQA.
M_SYMBOL20:
  DEFM "H"
  DEFB 0                  ; End marker
  DEFS 10

; 'Hydrogen'
;
; Question/answer word no. 169 for MR ROCKITT. The corresponding
; question/answer word (no. 168) is located at M_SYMBOL20. Used by the routine
; at PREPQA.
M_ELEMENT20:
  DEFM "Hydrogen"
  DEFB 0                  ; End marker
  DEFS 3

; 'Berne'
;
; Question/answer word no. 170 for MR WITHIT. The corresponding question/answer
; word (no. 171) is located at M_COUNTRY00. Used by the routine at PREPQA.
M_CAPITAL00:
  DEFM "Berne"
  DEFB 0                  ; End marker
  DEFS 6

; 'Switzerland'
;
; Question/answer word no. 171 for MR WITHIT. The corresponding question/answer
; word (no. 170) is located at M_CAPITAL00. Used by the routine at PREPQA.
M_COUNTRY00:
  DEFM "Switzerland"
  DEFB 0                  ; End marker

; 'Helsinki'
;
; Question/answer word no. 172 for MR WITHIT. The corresponding question/answer
; word (no. 173) is located at M_COUNTRY01. Used by the routine at PREPQA.
M_CAPITAL01:
  DEFM "Helsinki"
  DEFB 0                  ; End marker
  DEFS 3

; 'Finland'
;
; Question/answer word no. 173 for MR WITHIT. The corresponding question/answer
; word (no. 172) is located at M_CAPITAL01. Used by the routine at PREPQA.
M_COUNTRY01:
  DEFM "Finland"
  DEFB 0                  ; End marker
  DEFS 4

; 'Reykjavik'
;
; Question/answer word no. 174 for MR WITHIT. The corresponding question/answer
; word (no. 175) is located at M_COUNTRY02. Used by the routine at PREPQA.
M_CAPITAL02:
  DEFM "Reykjavik"
  DEFB 0                  ; End marker
  DEFS 2

; 'Iceland'
;
; Question/answer word no. 175 for MR WITHIT. The corresponding question/answer
; word (no. 174) is located at M_CAPITAL02. Used by the routine at PREPQA.
M_COUNTRY02:
  DEFM "Iceland"
  DEFB 0                  ; End marker
  DEFS 4

; 'Budapest'
;
; Question/answer word no. 176 for MR WITHIT. The corresponding question/answer
; word (no. 177) is located at M_COUNTRY03. Used by the routine at PREPQA.
M_CAPITAL03:
  DEFM "Budapest"
  DEFB 0                  ; End marker
  DEFS 3

; 'Hungary'
;
; Question/answer word no. 177 for MR WITHIT. The corresponding question/answer
; word (no. 176) is located at M_CAPITAL03. Used by the routine at PREPQA.
M_COUNTRY03:
  DEFM "Hungary"
  DEFB 0                  ; End marker
  DEFS 4

; 'Bucharest'
;
; Question/answer word no. 178 for MR WITHIT. The corresponding question/answer
; word (no. 179) is located at M_COUNTRY04. Used by the routine at PREPQA.
M_CAPITAL04:
  DEFM "Bucharest"
  DEFB 0                  ; End marker
  DEFS 2

; 'Romania'
;
; Question/answer word no. 179 for MR WITHIT. The corresponding question/answer
; word (no. 178) is located at M_CAPITAL04. Used by the routine at PREPQA.
M_COUNTRY04:
  DEFM "Romania"
  DEFB 0                  ; End marker
  DEFS 4

; 'Tirana'
;
; Question/answer word no. 180 for MR WITHIT. The corresponding question/answer
; word (no. 181) is located at M_COUNTRY05. Used by the routine at PREPQA.
M_CAPITAL05:
  DEFM "Tirana"
  DEFB 0                  ; End marker
  DEFS 5

; 'Albania'
;
; Question/answer word no. 181 for MR WITHIT. The corresponding question/answer
; word (no. 180) is located at M_CAPITAL05. Used by the routine at PREPQA.
M_COUNTRY05:
  DEFM "Albania"
  DEFB 0                  ; End marker
  DEFS 4

; 'Jakarta'
;
; Question/answer word no. 182 for MR WITHIT. The corresponding question/answer
; word (no. 183) is located at M_COUNTRY06. Used by the routine at PREPQA.
M_CAPITAL06:
  DEFM "Jakarta"
  DEFB 0                  ; End marker
  DEFS 4

; 'Indonesia'
;
; Question/answer word no. 183 for MR WITHIT. The corresponding question/answer
; word (no. 182) is located at M_CAPITAL06. Used by the routine at PREPQA.
M_COUNTRY06:
  DEFM "Indonesia"
  DEFB 0                  ; End marker
  DEFS 2

; 'Pyongyang'
;
; Question/answer word no. 184 for MR WITHIT. The corresponding question/answer
; word (no. 185) is located at M_COUNTRY07. Used by the routine at PREPQA.
M_CAPITAL07:
  DEFM "Pyongyang"
  DEFB 0                  ; End marker
  DEFS 2

; 'North Korea'
;
; Question/answer word no. 185 for MR WITHIT. The corresponding question/answer
; word (no. 184) is located at M_CAPITAL07. Used by the routine at PREPQA.
M_COUNTRY07:
  DEFM "North Korea"
  DEFB 0                  ; End marker

; 'Vientiane'
;
; Question/answer word no. 186 for MR WITHIT. The corresponding question/answer
; word (no. 187) is located at M_COUNTRY08. Used by the routine at PREPQA.
M_CAPITAL08:
  DEFM "Vientiane"
  DEFB 0                  ; End marker
  DEFS 2

; 'Laos'
;
; Question/answer word no. 187 for MR WITHIT. The corresponding question/answer
; word (no. 186) is located at M_CAPITAL08. Used by the routine at PREPQA.
M_COUNTRY08:
  DEFM "Laos"
  DEFB 0                  ; End marker
  DEFS 7

; 'Islamabad'
;
; Question/answer word no. 188 for MR WITHIT. The corresponding question/answer
; word (no. 189) is located at M_COUNTRY09. Used by the routine at PREPQA.
M_CAPITAL09:
  DEFM "Islamabad"
  DEFB 0                  ; End marker
  DEFS 2

; 'Pakistan'
;
; Question/answer word no. 189 for MR WITHIT. The corresponding question/answer
; word (no. 188) is located at M_CAPITAL09. Used by the routine at PREPQA.
M_COUNTRY09:
  DEFM "Pakistan"
  DEFB 0                  ; End marker
  DEFS 3

; 'Rangoon'
;
; Question/answer word no. 190 for MR WITHIT. The corresponding question/answer
; word (no. 191) is located at M_COUNTRY10. Used by the routine at PREPQA.
M_CAPITAL10:
  DEFM "Rangoon"
  DEFB 0                  ; End marker
  DEFS 4

; 'Burma'
;
; Question/answer word no. 191 for MR WITHIT. The corresponding question/answer
; word (no. 190) is located at M_CAPITAL10. Used by the routine at PREPQA.
M_COUNTRY10:
  DEFM "Burma"
  DEFB 0                  ; End marker
  DEFS 6

; 'Ankara'
;
; Question/answer word no. 192 for MR WITHIT. The corresponding question/answer
; word (no. 193) is located at M_COUNTRY11. Used by the routine at PREPQA.
M_CAPITAL11:
  DEFM "Ankara"
  DEFB 0                  ; End marker
  DEFS 5

; 'Turkey'
;
; Question/answer word no. 193 for MR WITHIT. The corresponding question/answer
; word (no. 192) is located at M_CAPITAL11. Used by the routine at PREPQA.
M_COUNTRY11:
  DEFM "Turkey"
  DEFB 0                  ; End marker
  DEFS 5

; 'Amman'
;
; Question/answer word no. 194 for MR WITHIT. The corresponding question/answer
; word (no. 195) is located at M_COUNTRY12. Used by the routine at PREPQA.
M_CAPITAL12:
  DEFM "Amman"
  DEFB 0                  ; End marker
  DEFS 6

; 'Jordan'
;
; Question/answer word no. 195 for MR WITHIT. The corresponding question/answer
; word (no. 194) is located at M_CAPITAL12. Used by the routine at PREPQA.
M_COUNTRY12:
  DEFM "Jordan"
  DEFB 0                  ; End marker
  DEFS 5

; 'Gabarone'
;
; Question/answer word no. 196 for MR WITHIT. The corresponding question/answer
; word (no. 197) is located at M_COUNTRY13. Used by the routine at PREPQA.
M_CAPITAL13:
  DEFM "Gabarone"
  DEFB 0                  ; End marker
  DEFS 3

; 'Botswana'
;
; Question/answer word no. 197 for MR WITHIT. The corresponding question/answer
; word (no. 196) is located at M_CAPITAL13. Used by the routine at PREPQA.
M_COUNTRY13:
  DEFM "Botswana"
  DEFB 0                  ; End marker
  DEFS 3

; 'Lusaka'
;
; Question/answer word no. 198 for MR WITHIT. The corresponding question/answer
; word (no. 199) is located at M_COUNTRY14. Used by the routine at PREPQA.
M_CAPITAL14:
  DEFM "Lusaka"
  DEFB 0                  ; End marker
  DEFS 5

; 'Zambia'
;
; Question/answer word no. 199 for MR WITHIT. The corresponding question/answer
; word (no. 198) is located at M_CAPITAL14. Used by the routine at PREPQA.
M_COUNTRY14:
  DEFM "Zambia"
  DEFB 0                  ; End marker
  DEFS 5

; 'Monrovia'
;
; Question/answer word no. 200 for MR WITHIT. The corresponding question/answer
; word (no. 201) is located at M_COUNTRY15. Used by the routine at PREPQA.
M_CAPITAL15:
  DEFM "Monrovia"
  DEFB 0                  ; End marker
  DEFS 3

; 'Liberia'
;
; Question/answer word no. 201 for MR WITHIT. The corresponding question/answer
; word (no. 200) is located at M_CAPITAL15. Used by the routine at PREPQA.
M_COUNTRY15:
  DEFM "Liberia"
  DEFB 0                  ; End marker
  DEFS 4

; 'La Paz'
;
; Question/answer word no. 202 for MR WITHIT. The corresponding question/answer
; word (no. 203) is located at M_COUNTRY16. Used by the routine at PREPQA.
M_CAPITAL16:
  DEFM "La Paz"
  DEFB 0                  ; End marker
  DEFS 5

; 'Bolivia'
;
; Question/answer word no. 203 for MR WITHIT. The corresponding question/answer
; word (no. 202) is located at M_CAPITAL16. Used by the routine at PREPQA.
M_COUNTRY16:
  DEFM "Bolivia"
  DEFB 0                  ; End marker
  DEFS 4

; 'Caracas'
;
; Question/answer word no. 204 for MR WITHIT. The corresponding question/answer
; word (no. 205) is located at M_COUNTRY17. Used by the routine at PREPQA.
M_CAPITAL17:
  DEFM "Caracas"
  DEFB 0                  ; End marker
  DEFS 4

; 'Venezuela'
;
; Question/answer word no. 205 for MR WITHIT. The corresponding question/answer
; word (no. 204) is located at M_CAPITAL17. Used by the routine at PREPQA.
M_COUNTRY17:
  DEFM "Venezuela"
  DEFB 0                  ; End marker
  DEFS 2

; 'Quito'
;
; Question/answer word no. 206 for MR WITHIT. The corresponding question/answer
; word (no. 207) is located at M_COUNTRY18. Used by the routine at PREPQA.
M_CAPITAL18:
  DEFM "Quito"
  DEFB 0                  ; End marker
  DEFS 6

; 'Ecuador'
;
; Question/answer word no. 207 for MR WITHIT. The corresponding question/answer
; word (no. 206) is located at M_CAPITAL18. Used by the routine at PREPQA.
M_COUNTRY18:
  DEFM "Ecuador"
  DEFB 0                  ; End marker
  DEFS 4

; 'Paramaribo'
;
; Question/answer word no. 208 for MR WITHIT. The corresponding question/answer
; word (no. 209) is located at M_COUNTRY19. Used by the routine at PREPQA.
M_CAPITAL19:
  DEFM "Paramaribo"
  DEFB 0                  ; End marker
  DEFB 0

; 'Surinam'
;
; Question/answer word no. 209 for MR WITHIT. The corresponding question/answer
; word (no. 208) is located at M_CAPITAL19. Used by the routine at PREPQA.
M_COUNTRY19:
  DEFM "Surinam"
  DEFB 0                  ; End marker
  DEFS 4

; 'Santiago'
;
; Question/answer word no. 210 for MR WITHIT. The corresponding question/answer
; word (no. 211) is located at M_COUNTRY20. Used by the routine at PREPQA.
M_CAPITAL20:
  DEFM "Santiago"
  DEFB 0                  ; End marker
  DEFS 3

; 'Chile'
;
; Question/answer word no. 211 for MR WITHIT. The corresponding question/answer
; word (no. 210) is located at M_CAPITAL20. Used by the routine at PREPQA.
M_COUNTRY20:
  DEFM "Chile"
  DEFB 0                  ; End marker
  DEFS 6

; '1066'
;
; Question/answer word no. 212 for MR CREAK. The corresponding question/answer
; word (no. 213) is located at M_BATTLE00. Used by the routine at PREPQA.
M_YEAR00:
  DEFM "1066"
  DEFB 0                  ; End marker
  DEFS 7

; 'Hastings'
;
; Question/answer word no. 213 for MR CREAK. The corresponding question/answer
; word (no. 212) is located at M_YEAR00. Used by the routine at PREPQA.
M_BATTLE00:
  DEFM "Hastings"
  DEFB 0                  ; End marker
  DEFS 3

; '1265'
;
; Question/answer word no. 214 for MR CREAK. The corresponding question/answer
; word (no. 215) is located at M_BATTLE01. Used by the routine at PREPQA.
M_YEAR01:
  DEFM "1265"
  DEFB 0                  ; End marker
  DEFS 7

; 'Evesham'
;
; Question/answer word no. 215 for MR CREAK. The corresponding question/answer
; word (no. 214) is located at M_YEAR01. Used by the routine at PREPQA.
M_BATTLE01:
  DEFM "Evesham"
  DEFB 0                  ; End marker
  DEFS 4

; '1314'
;
; Question/answer word no. 216 for MR CREAK. The corresponding question/answer
; word (no. 217) is located at M_BATTLE02. Used by the routine at PREPQA.
M_YEAR02:
  DEFM "1314"
  DEFB 0                  ; End marker
  DEFS 7

; 'Bannockburn'
;
; Question/answer word no. 217 for MR CREAK. The corresponding question/answer
; word (no. 216) is located at M_YEAR02. Used by the routine at PREPQA.
M_BATTLE02:
  DEFM "Bannockburn"
  DEFB 0                  ; End marker

; '1346'
;
; Question/answer word no. 218 for MR CREAK. The corresponding question/answer
; word (no. 219) is located at M_BATTLE03. Used by the routine at PREPQA.
M_YEAR03:
  DEFM "1346"
  DEFB 0                  ; End marker
  DEFS 7

; 'Crecy'
;
; Question/answer word no. 219 for MR CREAK. The corresponding question/answer
; word (no. 218) is located at M_YEAR03. Used by the routine at PREPQA.
M_BATTLE03:
  DEFM "Crecy"
  DEFB 0                  ; End marker
  DEFS 6

; '1356'
;
; Question/answer word no. 220 for MR CREAK. The corresponding question/answer
; word (no. 221) is located at M_BATTLE04. Used by the routine at PREPQA.
M_YEAR04:
  DEFM "1356"
  DEFB 0                  ; End marker
  DEFS 7

; 'Poitiers'
;
; Question/answer word no. 221 for MR CREAK. The corresponding question/answer
; word (no. 220) is located at M_YEAR04. Used by the routine at PREPQA.
M_BATTLE04:
  DEFM "Poitiers"
  DEFB 0                  ; End marker
  DEFS 3

; '1403'
;
; Question/answer word no. 222 for MR CREAK. The corresponding question/answer
; word (no. 223) is located at M_BATTLE05. Used by the routine at PREPQA.
M_YEAR05:
  DEFM "1403"
  DEFB 0                  ; End marker
  DEFS 7

; 'Shrewsbury'
;
; Question/answer word no. 223 for MR CREAK. The corresponding question/answer
; word (no. 222) is located at M_YEAR05. Used by the routine at PREPQA.
M_BATTLE05:
  DEFM "Shrewsbury"
  DEFB 0                  ; End marker
  DEFB 0

; '1415'
;
; Question/answer word no. 224 for MR CREAK. The corresponding question/answer
; word (no. 225) is located at M_BATTLE06. Used by the routine at PREPQA.
M_YEAR06:
  DEFM "1415"
  DEFB 0                  ; End marker
  DEFS 7

; 'Agincourt'
;
; Question/answer word no. 225 for MR CREAK. The corresponding question/answer
; word (no. 224) is located at M_YEAR06. Used by the routine at PREPQA.
M_BATTLE06:
  DEFM "Agincourt"
  DEFB 0                  ; End marker
  DEFS 2

; '1485'
;
; Question/answer word no. 226 for MR CREAK. The corresponding question/answer
; word (no. 227) is located at M_BATTLE07. Used by the routine at PREPQA.
M_YEAR07:
  DEFM "1485"
  DEFB 0                  ; End marker
  DEFS 7

; 'Bosworth'
;
; Question/answer word no. 227 for MR CREAK. The corresponding question/answer
; word (no. 226) is located at M_YEAR07. Used by the routine at PREPQA.
M_BATTLE07:
  DEFM "Bosworth"
  DEFB 0                  ; End marker
  DEFS 3

; '1513'
;
; Question/answer word no. 228 for MR CREAK. The corresponding question/answer
; word (no. 229) is located at M_BATTLE08. Used by the routine at PREPQA.
M_YEAR08:
  DEFM "1513"
  DEFB 0                  ; End marker
  DEFS 7

; 'Flodden'
;
; Question/answer word no. 229 for MR CREAK. The corresponding question/answer
; word (no. 228) is located at M_YEAR08. Used by the routine at PREPQA.
M_BATTLE08:
  DEFM "Flodden"
  DEFB 0                  ; End marker
  DEFS 4

; '1571'
;
; Question/answer word no. 230 for MR CREAK. The corresponding question/answer
; word (no. 231) is located at M_BATTLE09. Used by the routine at PREPQA.
M_YEAR09:
  DEFM "1571"
  DEFB 0                  ; End marker
  DEFS 7

; 'Lepanto'
;
; Question/answer word no. 231 for MR CREAK. The corresponding question/answer
; word (no. 230) is located at M_YEAR09. Used by the routine at PREPQA.
M_BATTLE09:
  DEFM "Lepanto"
  DEFB 0                  ; End marker
  DEFS 4

; '1014'
;
; Question/answer word no. 232 for MR CREAK. The corresponding question/answer
; word (no. 233) is located at M_BATTLE10. Used by the routine at PREPQA.
M_YEAR10:
  DEFM "1014"
  DEFB 0                  ; End marker
  DEFS 7

; 'Clontarf'
;
; Question/answer word no. 233 for MR CREAK. The corresponding question/answer
; word (no. 232) is located at M_YEAR10. Used by the routine at PREPQA.
M_BATTLE10:
  DEFM "Clontarf"
  DEFB 0                  ; End marker
  DEFM "oor"

; '1685'
;
; Question/answer word no. 234 for MR CREAK. The corresponding question/answer
; word (no. 235) is located at M_BATTLE11. Used by the routine at PREPQA.
M_YEAR11:
  DEFM "1685"
  DEFB 0                  ; End marker
  DEFS 7

; 'Sedgemoor'
;
; Question/answer word no. 235 for MR CREAK. The corresponding question/answer
; word (no. 234) is located at M_YEAR11. Used by the routine at PREPQA.
M_BATTLE11:
  DEFM "Sedgemoor"
  DEFB 0                  ; End marker
  DEFS 2

; '1746'
;
; Question/answer word no. 236 for MR CREAK. The corresponding question/answer
; word (no. 237) is located at M_BATTLE12. Used by the routine at PREPQA.
M_YEAR12:
  DEFM "1746"
  DEFB 0                  ; End marker
  DEFS 7

; 'Culloden'
;
; Question/answer word no. 237 for MR CREAK. The corresponding question/answer
; word (no. 236) is located at M_YEAR12. Used by the routine at PREPQA.
M_BATTLE12:
  DEFM "Culloden"
  DEFB 0                  ; End marker
  DEFS 3

; '1775'
;
; Question/answer word no. 238 for MR CREAK. The corresponding question/answer
; word (no. 239) is located at M_BATTLE13. Used by the routine at PREPQA.
M_YEAR13:
  DEFM "1775"
  DEFB 0                  ; End marker
  DEFS 7

; 'Lexington'
;
; Question/answer word no. 239 for MR CREAK. The corresponding question/answer
; word (no. 238) is located at M_YEAR13. Used by the routine at PREPQA.
M_BATTLE13:
  DEFM "Lexington"
  DEFB 0                  ; End marker
  DEFS 2

; '1781'
;
; Question/answer word no. 240 for MR CREAK. The corresponding question/answer
; word (no. 241) is located at M_BATTLE14. Used by the routine at PREPQA.
M_YEAR14:
  DEFM "1781"
  DEFB 0                  ; End marker
  DEFS 7

; 'Yorktown'
;
; Question/answer word no. 241 for MR CREAK. The corresponding question/answer
; word (no. 240) is located at M_YEAR14. Used by the routine at PREPQA.
M_BATTLE14:
  DEFM "Yorktown"
  DEFB 0                  ; End marker
  DEFS 3

; '1805'
;
; Question/answer word no. 242 for MR CREAK. The corresponding question/answer
; word (no. 243) is located at M_BATTLE15. Used by the routine at PREPQA.
M_YEAR15:
  DEFM "1805"
  DEFB 0                  ; End marker
  DEFS 7

; 'Trafalgar'
;
; Question/answer word no. 243 for MR CREAK. The corresponding question/answer
; word (no. 242) is located at M_YEAR15. Used by the routine at PREPQA.
M_BATTLE15:
  DEFM "Trafalgar"
  DEFB 0                  ; End marker
  DEFS 2

; '1815'
;
; Question/answer word no. 244 for MR CREAK. The corresponding question/answer
; word (no. 245) is located at M_BATTLE16. Used by the routine at PREPQA.
M_YEAR16:
  DEFM "1815"
  DEFB 0                  ; End marker
  DEFS 7

; 'Waterloo'
;
; Question/answer word no. 245 for MR CREAK. The corresponding question/answer
; word (no. 244) is located at M_YEAR16. Used by the routine at PREPQA.
M_BATTLE16:
  DEFM "Waterloo"
  DEFB 0                  ; End marker
  DEFS 3

; '1812'
;
; Question/answer word no. 246 for MR CREAK. The corresponding question/answer
; word (no. 247) is located at M_BATTLE17. Used by the routine at PREPQA.
M_YEAR17:
  DEFM "1812"
  DEFB 0                  ; End marker
  DEFS 7

; 'Borodino'
;
; Question/answer word no. 247 for MR CREAK. The corresponding question/answer
; word (no. 246) is located at M_YEAR17. Used by the routine at PREPQA.
M_BATTLE17:
  DEFM "Borodino"
  DEFB 0                  ; End marker
  DEFS 3

; '1836'
;
; Question/answer word no. 248 for MR CREAK. The corresponding question/answer
; word (no. 249) is located at M_BATTLE18. Used by the routine at PREPQA.
M_YEAR18:
  DEFM "1836"
  DEFB 0                  ; End marker
  DEFS 7

; 'San Jacinto'
;
; Question/answer word no. 249 for MR CREAK. The corresponding question/answer
; word (no. 248) is located at M_YEAR18. Used by the routine at PREPQA.
M_BATTLE18:
  DEFM "San Jacinto"
  DEFB 0                  ; End marker

; '1863'
;
; Question/answer word no. 250 for MR CREAK. The corresponding question/answer
; word (no. 251) is located at M_BATTLE19. Used by the routine at PREPQA.
M_YEAR19:
  DEFM "1863"
  DEFB 0                  ; End marker
  DEFS 7

; 'Gettysburg'
;
; Question/answer word no. 251 for MR CREAK. The corresponding question/answer
; word (no. 250) is located at M_YEAR19. Used by the routine at PREPQA.
M_BATTLE19:
  DEFM "Gettysburg"
  DEFB 0                  ; End marker
  DEFB 0

; '1854'
;
; Question/answer word no. 252 for MR CREAK. The corresponding question/answer
; word (no. 253) is located at M_BATTLE20. Used by the routine at PREPQA.
M_YEAR20:
  DEFM "1854"
  DEFB 0                  ; End marker
  DEFS 7

; 'Balaclava'
;
; Question/answer word no. 253 for MR CREAK. The corresponding question/answer
; word (no. 252) is located at M_YEAR20. Used by the routine at PREPQA.
M_BATTLE20:
  DEFM "Balaclava"
  DEFB 0                  ; End marker
  DEFS 2

; Unused
;
; Perhaps this unused date was once coupled with 'Marengo'.
  DEFM "1800"

; Play the 'hit a shield' sound effect
;
; Used by the routine at CHKSHIELD.
SHIELDSND:
  DI                      ; Disable interrupts
  LD C,16                 ; There are 16 mini-sound effects in a row
SHIELDSND_0:
  LD HL,65047             ; A=1 (initial border colour: blue), H=254 (pitch
  LD A,1                  ; adjustment), L=23 (border alternates blue/yellow),
  LD DE,16384             ; D=64 (duration), E=0 (initial pitch)
  CALL SNDEFFECT_0        ; Play one mini-sound effect with these parameters
  DEC C                   ; Next mini-sound effect
  JR NZ,SHIELDSND_0       ; Jump back until all 16 have been done
  EI                      ; Re-enable interrupts
  RET

; Unused
  DEFB 0

; 'i hate^fizziks'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD0:
  DEFM "i hate"
  DEFB 2                  ; Newline
  DEFM "fizziks"
  DEFB 0                  ; End marker
  DEFS 17

; 'i hate sums'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD1:
  DEFM "i hate sums"
  DEFB 0                  ; End marker
  DEFS 20

; 'skool rools^o k'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD2:
  DEFM "skool rools"
  DEFB 2                  ; Newline
  DEFM "o k"
  DEFB 0                  ; End marker
  DEFS 16

; 'i hate skool'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD3:
  DEFM "i hate skool"
  DEFB 0                  ; End marker
  DEFS 19

; 'speling iz^boaring'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD4:
  DEFM "speling iz"
  DEFB 2                  ; Newline
  DEFM "boaring"
  DEFB 0                  ; End marker
  DEFS 13

; 'i love^WHEELIE'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD5:
  DEFM "i love"
  DEFB 2                  ; Newline
  DEFM "WHEELIE"
  DEFB 0                  ; End marker
  DEFS 17

; 'SKYRANGER^is grate'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD6:
  DEFM "SKYRANGER"
  DEFB 2                  ; Newline
  DEFM "is grate"
  DEFB 0                  ; End marker
  DEFS 13

; 'skool dinners^are orrible'
;
; Used by the routine at WRITEBRD.
M_WANDERBRD7:
  DEFM "skool dinners"
  DEFB 2                  ; Newline
  DEFM "are orrible"
  DEFB 0                  ; End marker
  DEFS 6

; 'THE 47 TIMES^TABLE'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD0:
  DEFM "THE 47 TIMES"
  DEFB 2                  ; Newline
  DEFM "TABLE"
  DEFB 0                  ; End marker
  DEFS 13

; 'QUADRATIC^EQUATIONS'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD1:
  DEFM "QUADRATIC"
  DEFB 2                  ; Newline
  DEFM "EQUATIONS"
  DEFB 0                  ; End marker
  DEFS 12

; 'WHY SUMS ARE^FUN'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD2:
  DEFM "WHY SUMS ARE"
  DEFB 2                  ; Newline
  DEFM "FUN"
  DEFB 0                  ; End marker
  DEFS 15

; 'VECTORS AND^MATRICES'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD3:
  DEFM "VECTORS AND"
  DEFB 2                  ; Newline
  DEFM "MATRICES"
  DEFB 0                  ; End marker
  DEFS 11

; 'ISOSCELES^TRIANGLES'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD4:
  DEFM "ISOSCELES"
  DEFB 2                  ; Newline
  DEFM "TRIANGLES"
  DEFB 0                  ; End marker
  DEFS 12

; 'PYTHAGORAS^THEOREM'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD5:
  DEFM "PYTHAGORAS"
  DEFB 2                  ; Newline
  DEFM "THEOREM"
  DEFB 0                  ; End marker
  DEFS 13

; 'FACTORS'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD6:
  DEFM "FACTORS"
  DEFB 0                  ; End marker
  DEFS 24

; 'THE AREA OF^A CIRCLE'
;
; Used by the routine at WRITEBRD.
M_WACKERBRD7:
  DEFM "THE AREA OF"
  DEFB 2                  ; Newline
  DEFM "A CIRCLE"
  DEFB 0                  ; End marker
  DEFS 11

; 'The Periodic^Table'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD0:
  DEFM "The Periodic"
  DEFB 2                  ; Newline
  DEFM "Table"
  DEFB 0                  ; End marker
  DEFS 13

; 'Splitting^The Atom'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD1:
  DEFM "Splitting"
  DEFB 2                  ; Newline
  DEFM "The Atom"
  DEFB 0                  ; End marker
  DEFS 13

; 'Explosions I^have known'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD2:
  DEFM "Explosions I"
  DEFB 2                  ; Newline
  DEFM "have known"
  DEFB 0                  ; End marker
  DEFS 8

; 'How to blow^yourself up'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD3:
  DEFM "How to blow"
  DEFB 2                  ; Newline
  DEFM "yourself up"
  DEFB 0                  ; End marker
  DEFS 8

; 'Things to do^with TNT'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD4:
  DEFM "Things to do"
  DEFB 2                  ; Newline
  DEFM "with TNT"
  DEFB 0                  ; End marker
  DEFS 10

; 'Chemistry^of dynamite'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD5:
  DEFM "Chemistry"
  DEFB 2                  ; Newline
  DEFM "of dynamite"
  DEFB 0                  ; End marker
  DEFS 10

; 'First aid^for chemists'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD6:
  DEFM "First aid"
  DEFB 2                  ; Newline
  DEFM "for chemists"
  DEFB 0                  ; End marker
  DEFS 9

; 'Fast ways to^open doors'
;
; Used by the routine at WRITEBRD.
M_ROCKITTBRD7:
  DEFM "Fast ways to"
  DEFB 2                  ; Newline
  DEFM "open doors"
  DEFB 0                  ; End marker
  DEFS 8

; 'MANCHESTER^MONSOONS'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD0:
  DEFM "MANCHESTER"
  DEFB 2                  ; Newline
  DEFM "MONSOONS"
  DEFB 0                  ; End marker
  DEFS 2
  DEFM "STER"             ; This blackboard message probably used to be 'WHY IT
                          ; RAINS IN MANCHESTER' (as it is in the re-release of
                          ; Skool Daze by Alternative Software Ltd)
  DEFS 6

; 'THE CLIMATE^OF CLAPHAM'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD1:
  DEFM "THE CLIMATE"
  DEFB 2                  ; Newline
  DEFM "OF CLAPHAM"
  DEFB 0                  ; End marker
  DEFS 9

; 'THE PEAKS^OF PERU'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD2:
  DEFM "THE PEAKS"
  DEFB 2                  ; Newline
  DEFM "OF PERU"
  DEFB 0                  ; End marker
  DEFM "ERU"
  DEFS 11

; 'THE GLASGOW^RAIN-FOREST'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD3:
  DEFM "THE GLASGOW"
  DEFB 2                  ; Newline
  DEFM "RAIN-FOREST"
  DEFB 0                  ; End marker
  DEFM "T"
  DEFS 7

; 'THE EXPORTS^OF FIJI'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD4:
  DEFM "THE EXPORTS"
  DEFB 2                  ; Newline
  DEFM "OF FIJI"
  DEFB 0                  ; End marker
  DEFS 12

; 'ACTIVE^VOLCANOES'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD5:
  DEFM "ACTIVE"
  DEFB 2                  ; Newline
  DEFM "VOLCANOES"
  DEFB 0                  ; End marker
  DEFS 15

; 'POP MUSIC IN^ANTARCTICA'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD6:
  DEFM "POP MUSIC IN"
  DEFB 2                  ; Newline
  DEFM "ANTARCTICA"
  DEFB 0                  ; End marker
  DEFS 8

; 'THE UPLANDS^OF RUTLAND'
;
; Used by the routine at WRITEBRD.
M_WITHITBRD7:
  DEFM "THE UPLANDS"
  DEFB 2                  ; Newline
  DEFM "OF RUTLAND"
  DEFB 0                  ; End marker
  DEFS 9

; 'Industrial^Revolution'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD0:
  DEFM "Industrial"
  DEFB 2                  ; Newline
  DEFM "Revolution"
  DEFB 0                  ; End marker
  DEFS 10

; 'The Norman^Conquest'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD1:
  DEFM "The Norman"
  DEFB 2                  ; Newline
  DEFM "Conquest"
  DEFB 0                  ; End marker
  DEFS 12

; 'The Wars of^the Roses'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD2:
  DEFM "The Wars of"
  DEFB 2                  ; Newline
  DEFM "the Roses"
  DEFB 0                  ; End marker
  DEFS 10

; 'The Spanish^Armada'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD3:
  DEFM "The Spanish"
  DEFB 2                  ; Newline
  DEFM "Armada"
  DEFB 0                  ; End marker
  DEFS 13

; 'The First^Crusade'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD4:
  DEFM "The First"
  DEFB 2                  ; Newline
  DEFM "Crusade"
  DEFB 0                  ; End marker
  DEFS 14

; 'Magna Carta'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD5:
  DEFM "Magna Carta"
  DEFB 0                  ; End marker
  DEFS 20

; 'The Boston^Tea Party'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD6:
  DEFM "The Boston"
  DEFB 2                  ; Newline
  DEFM "Tea Party"
  DEFB 0                  ; End marker
  DEFS 11

; 'The Black^Death'
;
; Used by the routine at WRITEBRD.
M_CREAKBRD7:
  DEFM "The Black"
  DEFB 2                  ; Newline
  DEFM "Death"
  DEFB 0                  ; End marker
  DEFS 16

; Temporary store for the part of the screen overwritten by a lines bubble
;
; Used by the routine at SCR2BUF.
OVERBUF:
  DEFS 24                 ; The attribute bytes are stored here
OVERBUFG:
  DEFS 192                ; The display file bytes are stored here

; Unused
  DEFS 8

; Make a hitting sound effect
;
; Used by the routine at HITTING.
HITSOUND:
  CALL WALKSOUND          ; A hitting sound effect is two walking sound effects
  CALL WALKSOUND          ; close together
  LD A,1                  ; Restore the border colour (blue)
  OUT (254),A             ;
  RET

; Make a jumping sound effect
;
; Used by the routine at JUMPING.
JUMPSOUND:
  LD HL,784               ; These parameters were meant for the routine at
  LD DE,12896             ; SNDEFFECT_0; however, the CALL below is to 62818
  LD A,1                  ; (98,245) instead of SNDEFFECT_0 (98,254), which is
                          ; a bug
  CALL 62818              ; 62818 holds the operand of a JR NZ instruction, so
                          ; in effect this is CALL JUMPING_2; anyway, this call
                          ; does nothing significant
  JP WALKSOUND2           ; Make a walking sound effect

; Unused
  DEFS 7

; Lines bubble graphic template 1
;
; Used by the routines at NAME2BUF and NUM2BUF. Lines bubble graphic template 2
; can be found at BUBBLE2.
;
; The attribute bytes will be one of the following depending on who's getting
; lines:
;
; +---------------------+-----------------------+
; | Attribute           | Lines recipient       |
; +---------------------+-----------------------+
; | 23 (INK 7: PAPER 2) | ERIC                  |
; | 31 (INK 7: PAPER 3) | BOY WANDER            |
; | 39 (INK 7: PAPER 4) | EINSTEIN or ANGELFACE |
; +---------------------+-----------------------+
BUBBLE1:
  DEFB 0,0,0,0,0,0,0,0    ; The attribute bytes are filled in here
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 63,252,63,252,63,252,63,252 ; Graphic data
  DEFB 7,224,7,224,7,224,7,224     ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 128,0,0,48,64,0,0,1         ;
  DEFB 192,0,0,72,80,0,0,3         ;
  DEFB 224,0,0,72,69,24,224,7      ;
  DEFB 240,0,0,88,86,165,0,15      ;
  DEFB 240,0,0,104,84,189,192,15   ;
  DEFB 224,0,0,72,84,160,32,7      ;
  DEFB 192,0,0,48,84,153,192,3     ;
  DEFB 128,0,0,0,0,0,0,1           ;
BUBBLE1L2:
  DEFB 128,0,0,0,0,0,0,1           ;
  DEFB 192,0,0,0,0,0,0,3           ;
  DEFB 224,0,0,0,0,0,0,7           ;
  DEFB 240,0,0,0,0,0,0,15          ;
  DEFB 240,0,0,0,0,0,0,15          ;
  DEFB 224,0,0,0,0,0,0,7           ;
  DEFB 192,0,0,0,0,0,0,3           ;
  DEFB 128,0,0,0,0,0,0,1           ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 7,224,7,224,7,224,7,224     ;
  DEFB 63,252,63,252,63,252,63,252 ;

; Unused
  DEFS 8

; 'DEMO. - PRESS A KEY TO PLAY'
;
; Used by the routine at PRINTBOX.
M_DEMOMODE:
  DEFM "DEMO. - PRESS"
  DEFB 0                  ; End marker
  DEFS 2
M_DEMOMODEL2:
  DEFM "A KEY TO PLAY"
  DEFB 0                  ; End marker
  DEFS 2

; Lines bubble graphic template 2
;
; Used by the routines at LINESMSG and LINESLTR. The initial contents of this
; buffer are almost the same as lines bubble graphic template 1 at BUBBLE1,
; except that the '0' does not have a line through it. The '0 lines' text in
; this buffer is overwritten by the lines reprimand message (or the safe
; combination letter) before the bubble is displayed for the first time.
;
; The attribute bytes will be 7 (INK 7: PAPER 0) for the safe combination
; letter bubble, or one of the following depending on who's getting lines:
;
; +---------------------+-----------------------+
; | Attribute           | Lines recipient       |
; +---------------------+-----------------------+
; | 23 (INK 7: PAPER 2) | ERIC                  |
; | 31 (INK 7: PAPER 3) | BOY WANDER            |
; | 39 (INK 7: PAPER 4) | EINSTEIN or ANGELFACE |
; +---------------------+-----------------------+
BUBBLE2:
  DEFB 0,0,0,0,0,0,0,0    ; The attribute bytes are filled in here
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
BUBBLE2GR:
  DEFB 63,252,63,252,63,252,63,252 ; Graphic data
  DEFB 7,224,7,224,7,224,7,224     ;
  DEFB 0,0,0,0,0,0,0,0             ;
BUBBLE2L1:
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 128,0,0,48,64,0,0,1         ;
  DEFB 192,0,0,72,80,0,0,3         ;
  DEFB 224,0,0,72,69,24,224,7      ;
  DEFB 240,0,0,72,86,165,0,15      ;
  DEFB 240,0,0,72,84,189,192,15    ;
  DEFB 224,0,0,72,84,160,32,7      ;
  DEFB 192,0,0,48,84,153,192,3     ;
BUBBLE2L2:
  DEFB 128,0,0,0,0,0,0,1           ;
  DEFB 128,0,0,0,0,0,0,1           ;
  DEFB 192,0,0,0,0,0,0,3           ;
  DEFB 224,0,0,0,0,0,0,7           ;
  DEFB 240,0,0,0,0,0,0,15          ;
  DEFB 240,0,0,0,0,0,0,15          ;
  DEFB 224,0,0,0,0,0,0,7           ;
  DEFB 192,0,0,0,0,0,0,3           ;
  DEFB 128,0,0,0,0,0,0,1           ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 7,224,7,224,7,224,7,224     ;
  DEFB 63,252,63,252,63,252,63,252 ;

; Unused
  DEFS 8

; 'Press 'U' if you understood{8 spaces}'
;
; Used by the routine at TELLERIC.
M_PRESSU:
  DEFM "Press 'U' if you understood"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; Unused
  DEFS 4

; Lesson box graphic buffer
;
; Used by the routine at PRTLESSON.
LESSONBOX:
  DEFB 5,5,5,5,5,5,5,5    ; Attributes (PAPER 0: INK 5)
  DEFB 5,5,5,5,5,5,5,5    ;
  DEFB 5,5,5,5,5,5,5,5    ;
  DEFS 192                ; The graphic data is filled in here

; Unused
  DEFS 8

; Prepare for a new game
;
; Used by the routine at START. Clears the screen and asks whether the
; characters' names should be changed; returns to the caller if the answer is
; no, or proceeds into the routine at GETNAMES if the answer is yes.
PREPGAME:
  LD A,6                  ; BORDER 6
  OUT (254),A             ;
  LD HL,16384             ; Blank the entire display
PREPGAME_0:
  LD (HL),0               ;
  INC HL                  ;
  LD A,H                  ;
  CP 88                   ;
  JR NZ,PREPGAME_0        ;
PREPGAME_1:
  LD (HL),48              ; PAPER 6: INK 0
  INC HL                  ;
  LD A,H                  ;
  CP 91                   ;
  JR NZ,PREPGAME_1        ;
  CALL ASKNAMES           ; Ask whether the characters' names should be
                          ; changed, and return here only if the answer is yes
CHGNAMES:
  LD B,32                 ; Prepare B for drawing the catwalk
  JP GETNAMES

; Unused
  DEFB 0

; 'Score - 0 Lines - 0 Hi-Sc - 0' box graphic
;
; Used by the routine at PRINTB3L.
SCOREBOX:
  DEFB 6,6,6,6,6,6,6,6    ; Attributes (PAPER 0: INK 6)
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 0,0,0,0,0,0,0,0         ; Graphic data
  DEFB 56,0,0,0,0,0,0,48       ;
  DEFB 64,0,0,0,0,0,0,72       ;
  DEFB 65,204,163,0,0,0,0,72   ;
  DEFB 50,18,212,128,0,0,0,88  ;
  DEFB 10,18,135,158,0,0,0,104 ;
  DEFB 10,18,132,0,0,0,0,72    ;
  DEFB 113,216,131,0,0,0,0,48  ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 64,0,0,0,0,0,0,48       ;
  DEFB 66,0,0,0,0,0,0,72       ;
  DEFB 64,163,28,0,0,0,0,72    ;
  DEFB 66,212,160,0,0,0,0,88   ;
  DEFB 66,151,152,30,0,0,0,104 ;
  DEFB 66,148,4,0,0,0,0,72     ;
  DEFB 122,147,56,0,0,0,0,48   ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 72,7,0,0,0,0,0,48       ;
  DEFB 74,8,0,0,0,0,0,72       ;
  DEFB 72,8,56,0,0,0,0,72      ;
  DEFB 122,6,64,0,0,0,0,88     ;
  DEFB 74,225,64,30,0,0,0,104  ;
  DEFB 74,1,64,0,0,0,0,72      ;
  DEFB 74,14,56,0,0,0,0,48     ;

; Unused
  DEFS 8

; Prepare to change the names of the characters
;
; Continues from PREPGAME. Sets the scene for changing the main characters'
; names.
;
; B 32
; HL 20480
GETNAMES:
  LD (HL),255             ; Draw a solid line across the screen (the catwalk)
  INC HL                  ;
  DJNZ GETNAMES           ;
  LD HL,M_CAST            ; M_CAST: 'CAST OF CHARACTERS'
  LD DE,16448             ; Set DE to the display file address at which to
                          ; print this message
  CALL PRINTMSG           ; Print this message
  LD HL,SKOOLUDG          ; Change the instruction at SKOOLUDG in the routine
  LD (HL),175             ; at PRINTTILE to XOR A, so that the skool is not
                          ; drawn in the background
  LD HL,39010             ; Set the x-coordinate of every character to 150
  LD B,21                 ; (entirely off-screen)
GETNAMES_0:
  LD (HL),150             ;
  INC H                   ;
  DJNZ GETNAMES_0         ;
  JP GETNAMES2

; Skool Daze logo
;
; Used by the routine at PRINTB3L.
LOGO:
  DEFB 6,6,6,6,6,6,6,6    ; Attributes (PAPER 0: INK 6)
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 0,0,0,12,0,0,0,0             ; Graphic data
  DEFB 0,0,0,12,0,0,0,0             ;
  DEFB 0,0,0,12,0,0,0,0             ;
  DEFB 0,0,0,12,0,0,0,0             ;
  DEFB 0,0,0,6,0,0,0,0              ;
  DEFB 0,0,0,6,6,0,0,0              ;
  DEFB 0,0,0,6,6,0,0,56             ;
  DEFB 3,0,0,3,6,0,0,108            ;
  DEFB 3,0,0,227,3,0,28,204         ;
  DEFB 3,0,1,179,3,1,30,216         ;
  DEFB 3,0,227,25,131,15,162,240    ;
  DEFB 1,129,179,73,129,153,130,96  ;
  DEFB 1,139,31,25,129,153,132,102  ;
  DEFB 13,155,73,145,143,152,198,60 ;
  DEFB 28,243,24,240,25,144,195,152 ;
  DEFB 48,225,144,96,25,221,225,192 ;
  DEFB 32,248,240,0,24,198,1,128    ;
  DEFB 56,206,96,0,16,192,1,128     ;
  DEFB 30,99,0,0,29,224,3,0         ;
  DEFB 3,96,0,0,6,0,6,0             ;
  DEFB 6,0,0,0,0,0,12,0             ;
  DEFB 28,0,0,0,0,0,8,0             ;
  DEFB 112,0,0,0,0,0,0,0            ;
  DEFB 32,0,0,0,0,0,0,0             ;

; Unused
  DEFS 8

; Display the cast of characters and change their names
;
; Continues from GETNAMES. Walks each character onto the screen, changes his
; name (if desired), and walks him off again.
GETNAMES2:
  LD HL,LEFTCOL           ; LEFTCOL holds the leftmost column of the skool on
                          ; screen (0-64)
  LD B,8                  ; There are 8 main characters to consider
  LD (HL),B               ; Set the leftmost column to 8, so that the character
                          ; can start off-screen (at x=3) and walk on from the
                          ; left
GETNAMES2_0:
  PUSH BC                 ; Save the character counter
  CALL WALKCHAR           ; Print the character's title and name, and walk him
                          ; onto the screen
  PUSH HL                 ; Save the message address for the character's name
  CALL CPROMPT            ; Print 'PRESS'C'TO CHANGE NAME' and wait for a
                          ; keypress
  POP HL                  ; Restore the message address for the character's
                          ; name
GETNAMES2_1:
  CALL Z,CHANGENAME       ; Change the character's name if 'c' was pressed
  JR Z,GETNAMES2_1        ; Change the character's name if 'c' was pressed
                          ; again
  CALL WALKONOFF          ; Walk the character off the screen
  POP BC                  ; Restore the character counter to B
  DJNZ GETNAMES2_0        ; Jump back until all 8 main characters have been
                          ; done
  LD HL,SKOOLUDG          ; Set the instruction at SKOOLUDG in the routine at
  LD (HL),126             ; PRINTTILE back to LD A,(HL)
  RET

; 'WHAT IS 12 x 34?{8 spaces}'
;
; Used by the routine at PREPQA.
M_WACKERQ:
  DEFM "WHAT IS "
M_WACKERQN1:
  DEFM "12"
  DEFM " x "
M_WACKERQN2:
  DEFM "34?"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{Please Sir I cannot tell a lie . . }It's 1234{8 spaces}'
;
; Used by the routine at PREPQA.
M_WACKERA:
  DEFB 24                 ; M_PLEASESIR: 'Please Sir - I cannot tell a lie . .
                          ; '
  DEFM "It's "
M_WACKERAN:
  DEFM "1234"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WHAT ELEMENT HAS THE SYMBOL Q?{8 spaces}'
;
; Used by the routine at PREPQA.
M_ROCKITTQ1:
  DEFM "WHAT ELEMENT HAS THE SYMBOL "
M_ROCKITTQ1Q:
  DEFM "Q?"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{Please Sir I cannot tell a lie . . }It is Q{8 spaces}'
;
; Used by the routine at PREPQA.
M_ANSWER:
  DEFB 24                 ; M_PLEASESIR: 'Please Sir - I cannot tell a lie . .
                          ; '
  DEFM "It is "
M_ANSWERQ:
  DEFM "Q"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WHAT IS THE CHEMICAL SYMBOL FOR Q?{8 spaces}'
;
; Used by the routine at PREPQA.
M_ROCKITTQ2:
  DEFM "WHAT IS THE CHEMICAL SYMBOL FOR "
M_ROCKITTQ2Q:
  DEFM "Q?"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WHAT'S THE CAPITAL OF Q?{8 spaces}'
;
; Used by the routine at PREPQA.
M_WITHITQ1:
  DEFM "WHAT'S THE CAPITAL OF "
M_WITHITQ1Q:
  DEFM "Q?"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WHICH COUNTRY'S CAPITAL IS Q?{8 spaces}'
;
; Used by the routine at PREPQA.
M_WITHITQ2:
  DEFM "WHICH COUNTRY'S CAPITAL IS "
M_WITHITQ2Q:
  DEFM "Q?"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WHEN WAS THE BATTLE OF Q?{8 spaces}'
;
; Used by the routine at PREPQA.
M_CREAKQ1:
  DEFM "WHEN WAS THE BATTLE OF "
M_CREAKQ1Q:
  DEFM "Q?"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WHICH BATTLE OCCURRED IN Q?{8 spaces}'
;
; Used by the routine at PREPQA.
M_CREAKQ2:
  DEFM "WHICH BATTLE OCCURRED IN "
M_CREAKQ2Q:
  DEFM "Q?"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{Please Sir I cannot tell a lie . . }It was in Q{8 spaces}'
;
; Used by the routine at PREPQA.
M_CREAKA1:
  DEFB 24                 ; M_PLEASESIR: 'Please Sir - I cannot tell a lie . .
                          ; '
  DEFM "It was in "
M_CREAKA1Q:
  DEFM "Q"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{Please Sir I cannot tell a lie . . }It was the BATTLE OF Q{8 spaces}'
;
; Used by the routine at PREPQA.
M_CREAKA2:
  DEFB 24                 ; M_PLEASESIR: 'Please Sir - I cannot tell a lie . .
                          ; '
  DEFM "It was the BATTLE OF "
M_CREAKA2Q:
  DEFM "Q"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{Please Sir I cannot tell a lie . . }{ERIC} hit me{8 spaces}'
;
; Used by the routine at TEACHERIC.
M_TALEHIT:
  DEFB 24                 ; M_PLEASESIR: 'Please Sir - I cannot tell a lie . .
                          ; '
  DEFB 16                 ; M_ERIC: 'ERIC'
  DEFM " hit me"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{Please Sir I cannot tell a lie . . }{ERIC} is not here{8 spaces}'
;
; Used by the routine at TEACHERIC.
M_TALEABSENT:
  DEFB 24                 ; M_PLEASESIR: 'Please Sir - I cannot tell a lie . .
                          ; '
  DEFB 16                 ; M_ERIC: 'ERIC'
  DEFM " is not here"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{Please Sir I cannot tell a lie . . }{ERIC} wrote on the blackboard{8
; spaces}'
;
; Used by the routine at TEACHERIC.
M_TALEWROTE:
  DEFB 24                 ; M_PLEASESIR: 'Please Sir - I cannot tell a lie . .
                          ; '
  DEFB 16                 ; M_ERIC: 'ERIC'; the routine at TEACHERIC may change
                          ; this to 17 (M_BOYWANDER: 'BOY WANDER') as the
                          ; occasion demands
  DEFM " wrote on the"
  DEFM " blackboard"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WHAT HAPPENED IN THE YEAR THAT I WAS BORN{8 spaces}'
;
; Used by the routine at BIRTHQ.
M_BIRTHQ:
  DEFM "WHAT HAPPENED IN THE "
  DEFM "YEAR THAT I WAS BORN"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; Unused
  DEFM "e"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'RIGHT! SIT DOWN MY LITTLE CHERUBS{8 spaces}'
;
; Used by the routine at TELLSIT.
M_ROCKITTSIT:
  DEFM "RIGHT! SIT DOWN MY LITTLE CHERUBS"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'COME ON CHAPS - SETTLE DOWN{8 spaces}'
;
; Used by the routine at TELLSIT.
M_WITHITSIT:
  DEFM "COME ON CHAPS - SETTLE DOWN"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'BE QUIET AND SEATED YOU NASTY LITTLE BOYS{8 spaces}'
;
; Used by the routine at TELLSIT.
M_CREAKSIT:
  DEFM "BE QUIET AND SEATED "
  DEFM "YOU NASTY LITTLE BOYS"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'SILENCE! OR I'LL CANE THE LOT OF YOU{8 spaces}'
;
; Used by the routine at TELLSIT.
M_WACKERSIT:
  DEFM "SILENCE! OR I'LL "
  DEFM "CANE THE LOT OF YOU"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'YOU HAVE OVER 10000 LINES TO WRITE {ERIC}. DON'T COME BACK TO SCHOOL TILL
; YOU HAVE DONE THEM ALL. . . .PRESS A KEY TO PLAY AGAIN{8 spaces}'
;
; Used by the routine at GOHOMEMSG.
M_LINES10K:
  DEFM "YOU HAVE OVER 10000 "
  DEFM "LINES TO WRITE "
  DEFB 16                 ; M_ERIC: 'ERIC'
  DEFM ". DON'T COME BACK TO SCHOOL TILL "
  DEFM "YOU HAVE DONE THEM ALL. . . ."
  DEFM "PRESS A KEY TO PLAY AGAIN"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'TURN TO PAGE 123 OF YOUR BOOKS,BE SILENT AND START READING{8 spaces}'
;
; Used by the routine at GOTOPAGE.
M_CLASSDO1:
  DEFM "TURN TO PAGE "
M_CLASSDO1P:
  DEFM "123 OF YOUR BOOKS,BE "
  DEFM "SILENT AND START READING"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'ANSWER THE QUESTIONS ON PAGE 123 OF YOUR LOVELY TEXTBOOK{8 spaces}'
;
; Used by the routine at GOTOPAGE.
M_CLASSDO2:
  DEFM "ANSWER THE QUESTIONS ON PAGE "
M_CLASSDO2P:
  DEFM "123 OF YOUR LOVELY TEXTBOOK"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; 'WRITE AN ESSAY WITH THIS TITLE{8 spaces}'
;
; Used by the routines at CLSNOERIC and TEACHERIC.
M_CLASSDO3:
  DEFM "WRITE AN ESSAY WITH THIS TITLE"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; Unused
  DEFM "           "      ; 11 spaces

; Make a teacher conduct a class with ERIC
;
; Continues from the routine at DOCLASS. Makes a teacher conduct a class with
; ERIC, from the point of reaching the left edge of the blackboard or map
; (where the teacher waits for EINSTEIN to show up) until the end of the
; lesson.
;
; H Teacher's character number (163-166)
TEACHERIC:
  CALL SWOTWAIT           ; Make the teacher wait for EINSTEIN to sit down
  JR Z,TEACHERIC_3        ; Jump if ERIC is in the room
; This entry point is used by the routine at QALESSON.
TEACHERIC_0:
  LD BC,M_TALEABSENT      ; M_TALEABSENT: 'Please Sir...ERIC is not here'
  CALL SWOTSPK            ; Make EINSTEIN say this
  CALL SWOTLINES          ; Make the teacher give lines to EINSTEIN for telling
                          ; tales (if he's in the mood)
  CALL GOTOPAGE           ; Make the teacher tell the kids to read their books
  CALL PRESENT            ; Set the zero flag if ERIC is in class
  LD A,(LFLAGS)           ; Collect the lesson status flags from LFLAGS
  JR Z,TEACHERIC_1        ; Jump if ERIC is in class
  SET 7,A                 ; Set bit 7 at LFLAGS, indicating that the next lines
  LD (LFLAGS),A           ; message for absence (if any) should be no. 11 (AND
                          ; STAY THIS TIME)
  JP DINDUTY_0            ; Make the teacher track down ERIC
TEACHERIC_1:
  RLCA                    ; Set the carry flag if the absence lines reprimand
                          ; should be...
  LD B,11                 ; ...11: AND STAY THIS TIME
  JR C,TEACHERIC_2        ; Jump if this is the appropriate lines reprimand
  DEC B                   ; B=10: NEVER BE LATE AGAIN
TEACHERIC_2:
  LD A,172                ; 172=ERIC
  CALL CLSLINES_0         ; Give lines to ERIC
; ERIC is in class now.
TEACHERIC_3:
  LD A,(LFLAGS)           ; Set bit 7 (indicating that the next absence lines
  OR 192                  ; message should be 'AND STAY THIS TIME') and bit 6
  LD (LFLAGS),A           ; (indicating that the lesson has started) at LFLAGS
  CALL SWOTWAIT           ; Is ERIC in class?
  JR NZ,TEACHERIC_0       ; Jump if not
; This is EINSTEIN's opportunity to tell tales about hitting and blackboard
; defacement.
  CALL GETRANDOM          ; A=random number
  CP 226                  ; Should EINSTEIN tell the teacher that ERIC hit him?
  JR C,TEACHERIC_6        ; Jump if not (226 times out of 256)
  LD BC,M_TALEHIT         ; M_TALEHIT: 'Please Sir...ERIC hit me'
  CP C                    ; The result of this comparison is ignored
  JR TEACHERIC_4
  LD BC,M_BIRTHQ          ; This instruction is never executed; anyway,
                          ; M_BIRTHQ='WHAT HAPPENED IN THE YEAR THAT I WAS
                          ; BORN'
TEACHERIC_4:
  CALL SWOTSPK            ; Make EINSTEIN tell the teacher that ERIC hit him
  CALL SWOTLINES          ; Make the teacher give lines to EINSTEIN for telling
                          ; tales (if he's in the mood)
  INC L                   ; Did the teacher give lines to EINSTEIN?
  JR NZ,TEACHERIC_5       ; Jump if so
  LD B,8                  ; Lines reprimand 8: NOW DON'T DO IT AGAIN
  LD A,172                ; 172=ERIC
  CALL CLSLINES_0         ; Give ERIC lines
TEACHERIC_5:
  CALL SWOTWAIT           ; Is ERIC in class?
  JR NZ,TEACHERIC_0       ; Jump if not
TEACHERIC_6:
  CALL BOARDID            ; A=identifier of the blackboard closest to the
                          ; teacher
  LD C,A                  ; Transfer this to C
  CP 236                  ; Are we on the top floor?
  JR NZ,TEACHERIC_7       ; Jump if not
  INC L                   ; L=98
  LD A,(HL)               ; A=teacher's x-coordinate
  CP 58                   ; Are we in the Map Room?
  JR NC,TEACHERIC_12      ; Jump if so (no blackboard in there)
TEACHERIC_7:
  INC C                   ; BC=RRBRDAUTH (Reading Room blackboard), WRBRDAUTH
  LD B,127                ; (White Room blackboard) or ERBRDAUTH (Exam Room
                          ; blackboard)
  LD A,(BC)               ; A=number of the character who last wrote on the
                          ; board
  AND A                   ; Is the board clean?
  JR Z,TEACHERIC_11       ; Jump if so
  CP 167                  ; Did a teacher write on the board?
  JR C,TEACHERIC_11       ; Jump if so
  JR NZ,TEACHERIC_8       ; Jump unless BOY WANDER wrote on the board
  CALL GETRANDOM          ; A=random number
  CP 200                  ; Set the carry flag 200 times out of 256
  LD A,17                 ; 17='BOY WANDER'
  JR C,TEACHERIC_9        ; Jump if EINSTEIN is going to blame BOY WANDER
TEACHERIC_8:
  LD A,16                 ; 16='ERIC'
TEACHERIC_9:
  LD BC,61715             ; Insert ERIC's or BOY WANDER's name into the 'X
  LD (BC),A               ; wrote on the blackboard' message at M_TALEWROTE
  DEC C                   ; Point BC at the start of the message
  CALL SWOTSPK            ; Make EINSTEIN tell a tale about ERIC or BOY WANDER
                          ; writing on the blackboard
  CALL SWOTLINES          ; Make the teacher give lines to EINSTEIN for telling
                          ; tales (if he's in the mood)
  INC L                   ; Did the teacher give lines to EINSTEIN?
  JR NZ,TEACHERIC_11      ; Jump if so
  LD A,(61715)            ; A=16 ('ERIC') or 17 ('BOY WANDER')
  RRCA                    ; Set the carry flag if EINSTEIN blamed BOY WANDER
  LD A,167                ; 167=BOY WANDER
  JR C,TEACHERIC_10       ; Jump if EINSTEIN blamed BOY WANDER for writing on
                          ; the board
  LD A,172                ; 172=ERIC
TEACHERIC_10:
  LD B,12                 ; Lines reprimand 12: DON'T TOUCH BLACKBOARDS
  CALL CLSLINES_0         ; Make the teacher give lines to ERIC or BOY WANDER
; With EINSTEIN's tales now safely told, it's time to wipe the blackboard.
TEACHERIC_11:
  LD BC,WIPE1             ; Hand over control to the routine at WIPE1, making
  CALL CALLSUBCMD         ; the teacher wipe the blackboard
; Control returns here after the teacher has wiped the blackboard.
  LD L,98                 ; A=teacher's x-coordinate (equal to that of the left
  LD A,(HL)               ; edge of the blackboard)
  ADD A,3                 ; Place the x-coordinate of the middle of the
  LD L,107                ; blackboard into byte 107 of the teacher's buffer,
  LD (HL),A               ; and the same value into byte 108 (which ensures
  INC L                   ; that the routine at WALK will not relinquish
  LD (HL),A               ; control before the teacher has reached the middle
                          ; of the blackboard)
  LD BC,WALK              ; Hand over control to the routine at WALK, making
  CALL CALLSUBCMD         ; the teacher walk to the middle of the blackboard
; Control returns here when the teacher has walked to the middle of the
; blackboard.
  CALL GETRANDOM          ; A=random number
  CP 180                  ; Should the teacher write on the blackboard?
  JR C,TEACHERIC_12       ; Jump if not (180 times out of 256)
  LD BC,WRITEBRD          ; Hand over control to the routine at WRITEBRD,
  CALL CALLSUBCMD         ; making the teacher write on the blackboard
; Control returns here when the teacher has finished writing on the blackboard.
  CALL GETRANDOM          ; A=random number
  CP 160                  ; Should the teacher tell the kids to write an essay?
  JR C,TEACHERIC_12       ; Jump if not (10 times out of 16)
  LD BC,M_CLASSDO3        ; M_CLASSDO3: 'WRITE AN ESSAY WITH THIS TITLE'
  CALL GOTOPAGE_3         ; Make the teacher tell the kids to write an essay
  JR TEACHERIC_13
TEACHERIC_12:
  CALL GETRANDOM          ; A=random number
  CP 240                  ; Is this going to be a questions-and-answers lesson?
  JR C,QALESSON           ; Jump if so (15 times out of 16)
  CALL GOTOPAGE           ; Make the teacher tell the kids to go to a page in
                          ; their books
; Now we enter a loop that makes the teacher walk up and down next to the
; blackboard (or the map), checking periodically whether ERIC has gone AWOL.
TEACHERIC_13:
  CALL SWOTWAIT           ; Is ERIC in class?
  JP NZ,TEACHERIC_0       ; Jump if not
  LD L,98                 ; Collect the teacher's x-coordinate from byte 98 of
  LD A,(HL)               ; his buffer
  XOR 3                   ; Now A holds the x-coordinate of the location 1 or 3
                          ; spaces behind the teacher
  LD L,107                ; Place this into byte 107 of his buffer, and the
  LD (HL),A               ; same value into byte 108 (which ensures that the
  INC L                   ; routine at WALK will not relinquish control before
  LD (HL),A               ; the teacher has reached that destination)
  LD BC,WALK              ; Hand over control to the routine at WALK, making
  CALL CALLSUBCMD         ; the teacher turn round and walk 1 or 3 paces
  JR TEACHERIC_13         ; Make the teacher turn round and walk 1 or 3 paces
                          ; again

; Unused
  DEFS 11

; Make a teacher conduct a question-and-answer session
;
; Used by the routine at TEACHERIC. Controls the teacher during a
; question-and-answer session, but hands control back to the routine at
; TEACHERIC if ERIC goes missing.
;
; H Teacher's character number (163-166)
QALESSON:
  LD L,100                ; Replace the primary command routine address in
  LD (HL),244             ; bytes 99 and 100 of the teacher's buffer with
                          ; QALESSON (this routine)
QALESSON_0:
  CALL SWOTWAIT           ; Is ERIC in the room?
  JR Z,QALESSON_1         ; Jump if so
; ERIC is absent.
  LD L,100                ; Replace the address of this primary command routine
  LD (HL),243             ; in bytes 99 and 100 of the teacher's buffer with
                          ; that of TEACHERIC
  JP TEACHERIC_0          ; Make EINSTEIN tell the teacher that ERIC is not in
                          ; class
; ERIC is in class, so fire off a question and answer.
QALESSON_1:
  CALL PREPQA             ; Prepare a question and the answer
  LD BC,SPEAK             ; Hand over control to the routine at SPEAK, making
  CALL CALLSUBCMD         ; the teacher ask the question
; Control returns here when the teacher has finished asking the question.
  EX DE,HL                ; Transfer the teacher's character number to D
  CALL SWOTSPK_0          ; Make the teacher wait for EINSTEIN to answer the
                          ; question
  JR QALESSON_0

; Deal with ERIC when he's firing the catapult
;
; Used by the routine at MOVEERIC when bit 0 of ERIC's status flags at STATUS
; is set (by the routine at FIRE).
FIRING:
  LD HL,ACTIONCNT         ; ACTIONCNT holds the catapult-firing action timer
                          ; (initially 24)
  DEC (HL)                ; Has ERIC finished firing the catapult?
  JR NZ,FIRING_1          ; Jump if not
; This entry point is used by the routines at HITTING and JUMPING to restore
; ERIC's animatory state to what it was before he jumped or threw a punch.
FIRING_0:
  XOR A                   ; Clear all of ERIC's status flags at STATUS
  LD (STATUS),A           ;
  LD H,172                ; 172=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
  LD L,102                ; Restore ERIC's animatory state to what it was
  LD A,(HL)               ; before he fired the catapult, jumped, or threw the
                          ; punch
  JP UPDATEAS             ; Update ERIC's animatory state and location and
                          ; update the SRB
; ERIC hasn't finished firing the catapult. Check which phase of the firing
; action we're at.
FIRING_1:
  LD A,(HL)               ; Pick up the current value of the action timer in A
  LD H,172                ; 172=ERIC
  CP 18                   ; Is it time to raise the catapult to eye level?
  JP Z,FIRE3_0            ; Jump if so
  CP 6                    ; Is it time to start lowering the catapult?
  JP Z,FIRE5_0            ; Jump if so
  CP 12                   ; Is it time to launch the pellet?
  RET NZ                  ; Return if not
; It's time to launch the catapult pellet.
  LD L,96                 ; Byte 96 of ERIC's buffer holds his animatory state
  LD A,(HL)               ; Pick this up in A
  OR 127                  ; A=127 or 255 (catapult pellet)
  INC L                   ; Pick up ERIC's coordinates in DE; they will be the
  LD D,(HL)               ; initial coordinates of the catapult pellet
  INC L                   ;
  LD E,(HL)               ;
  DEC H                   ; H=171 (ERIC's catapult pellet)
  CALL FIRE4_0            ; Launch the catapult pellet
  CALL FIRESOUND          ; Play the catapult sound effect
  LD B,13                 ; Reprimand message 13: CATAPULTS ARE FORBIDDEN
; This entry point is used by the routines at HITTING (with B=14: DON'T HIT
; YOUR MATES) and JUMPING (with B=15: YOU ARE NOT A KANGAROO).
FIRING_2:
  LD H,172                ; 172=ERIC
  PUSH BC                 ; Save the reprimand message identifier
  CALL VISIBLE            ; Check whether any teachers saw ERIC throw a punch,
                          ; fire his catapult, or jump
  POP BC                  ; Restore the reprimand message identifier to B
  RET NC                  ; Return if ERIC wasn't seen by a teacher
  EXX
  LD L,97                 ; Byte 97 of the teacher's buffer holds his
                          ; y-coordinate
  LD D,(HL)               ; Pick up the coordinates of the teacher who saw ERIC
  INC L                   ; in DE'
  LD E,(HL)               ;
  PUSH DE
  EXX
  POP DE                  ; Now DE holds the coordinates of the teacher who saw
                          ; ERIC
  LD A,H                  ; A=172 (ERIC)
  JP GIVELINES            ; Make the teacher give ERIC lines

; Unused
  DEFS 3

; 'F' pressed - fire catapult
;
; The address of this routine is found in the table of keypress handling
; routines at K_WRITE. It is called from the main loop at MAINLOOP when 'F',
; 'C' or '0' (zero) is pressed.
;
; DE ERIC's coordinates
FIRE:
  LD A,(43874)            ; A=x-coordinate of ERIC's catapult pellet
  RLCA                    ; Is ERIC's catapult pellet already airborne?
  RET NC                  ; Return if so
  LD BC,2049              ; B=8 (ERIC raising catapult), C=1 (bit 0 set)
  LD A,24                 ; This will initialise the catapult-firing action
                          ; timer at ACTIONCNT to 24
; This entry point is used by the routine at HIT with A=18, B=10 (ERIC raising
; fist), C=2 (bit 1 set); and by the routine at JUMP with A=16, B=12 (ERIC with
; arm raised), C=4 (bit 2 set).
FIRE_0:
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  BIT 7,(HL)              ; Is ERIC sitting or lying down?
  RET NZ                  ; Return if so (ERIC can't fire, jump or hit while
                          ; sitting or lying down)
  LD (HL),C               ; Otherwise set the appropriate bit in STATUS
  LD L,246                ; Initialise the action timer at ACTIONCNT
  LD (HL),A               ;
  PUSH BC                 ; Save ERIC's next animatory state temporarily
  LD H,172                ; 172=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
  POP BC                  ; Restore ERIC's next animatory state to B
  LD L,102                ; Store ERIC's current animatory state in byte 102 of
  LD (HL),A               ; his buffer
  AND 128                 ; A=ERIC's next animatory state (phase 1 of firing,
  ADD A,B                 ; hitting or jumping)
  JP UPDATEAS             ; Update ERIC's animatory state and update the SRB

; 'H' pressed - hit
;
; The address of this routine is found in the table of keypress handling
; routines at K_WRITE. It is called from the main loop at MAINLOOP when 'H' is
; pressed.
HIT:
  LD BC,2562              ; B=10 (ERIC raising fist), C=2 (bit 1 set)
  LD A,18                 ; This will initialise the hitting action timer at
                          ; ACTIONCNT to 18
  JR FIRE_0               ; Start the punch

; 'J' pressed - jump
;
; The address of this routine is found in the table of keypress handling
; routines at K_WRITE. It is called from the main loop at MAINLOOP when 'J' or
; 'L' is pressed.
JUMP:
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  RET NZ                  ; Return if so (ERIC can't jump while on a staircase)
  LD BC,3076              ; B=12 (ERIC with arm raised), C=4 (bit 2 set)
  LD A,16                 ; This will initialise the jumping action timer at
                          ; ACTIONCNT to 16
  JR FIRE_0               ; Start the jump

; Deal with ERIC when he's hitting
;
; Used by the routine at MOVEERIC when bit 1 of ERIC's status flags at STATUS
; is set (by the routine at FIRE, via HIT).
HITTING:
  LD HL,ACTIONCNT         ; ACTIONCNT holds the hitting action timer (initially
                          ; 18)
  DEC (HL)                ; Has ERIC finished the punch?
  JP Z,FIRING_0           ; Jump if so
  LD A,(HL)               ; Pick up the current value of the action timer in A
  LD H,172                ; 172=ERIC
  CP 12                   ; Is it time to raise the fist to eye level?
  JP Z,FIRE3_0            ; Jump if so
  CP 11                   ; Is it time to see if anyone was hit?
  RET NZ                  ; Return if not
; ERIC's fist is fully raised. Check whether it's in someone's face.
  CALL CHECKHIT           ; Check whether ERIC managed to deck anyone
  LD A,H                  ; A=character number of the decked boy (if any)
  CP 163                  ; Was anyone decked?
  JR Z,HITTING_0          ; Jump if not
  CP 168                  ; 168=ANGELFACE
  LD A,3                  ; Add 30 the score and print it if ANGELFACE was
  CALL Z,ADDPTS           ; decked
HITTING_0:
  CALL HITSOUND           ; Make a hitting sound effect
  LD B,14                 ; Lines reprimand 14: DON'T HIT YOUR MATES
  JP FIRING_2             ; Make any nearby teacher give ERIC lines

; Unused
  DEFS 5

; Get the attribute file address of a pellet's potential target or the safe
;
; Used by the routines at CHKSHIELD (to check whether a shield has been hit)
; and CHECKJP2 (to make the safe flash). If the pellet's potential target or
; the safe is on-screen, this routine returns with the carry flag reset and HL
; holding the attribute file address.
;
; D Target y-coordinate
; E 128 + target x-coordinate
GETATTR:
  LD HL,LEFTCOL           ; LEFTCOL holds the leftmost column of the skool on
                          ; screen (0-64)
  LD A,E                  ; Set A to the true x-coordinate of the potential
  AND 127                 ; target
  SUB (HL)                ; Return with the carry flag set if the pellet is
  RET C                   ; off-screen to the left
  CP 32                   ; The screen is 32 columns wide
  CCF                     ; Return with carry flag set if the pellet is
  RET C                   ; off-screen to the right
; The potential target is on-screen.
  LD L,A                  ; L=screen x-coordinate (0-31) of the potential
                          ; target
  LD A,D                  ; A=screen y-coordinate (2, 8 or 14) of the potential
  SUB 152                 ; target
  RRCA                    ; A=64 (top floor), 1 (middle floor), or 193 (bottom
  RRCA                    ; floor
  RRCA                    ;
  LD H,A                  ; Save this floor indicator in H for now
  AND 224                 ; A=64 (top floor), 0 (middle) or 192 (bottom)
  ADD A,L                 ; Add the screen x-coordinate to get the LSB of the
                          ; attribute file address of the potential target
  LD L,A                  ; Copy the LSB to L
  LD A,H                  ; A=0 (top floor) or 1 (middle floor or bottom floor)
  AND 3                   ;
  ADD A,88                ; Add 88 to get the MSB of the attribute file address
                          ; of the potential target
  LD H,A                  ; Copy the MSB to H
  RET                     ; Return with the carry flag reset

; Set the new score and print it
;
; Used by the routine at NEXTPHASE after ERIC has made the 15th shield flash,
; opened the safe, or gone up a year.
;
; HL New score
SETSCORE:
  LD (SCORE),HL           ; Save the new score in SCORE
  LD DE,20924             ; Set DE to the display file address at which to
                          ; print the score
  JP PRINTNUM             ; Print the new score

; Open the lip of the speech bubble
;
; Used by the routine at SPKWAIT. Removes the solid line above the lip of the
; speech bubble, thus 'opening' it.
;
; DE Coordinates of the speech bubble lip
OPENLIP:
  LD A,E                  ; UDG references 248-255 correspond to the 8 UDGs
  AND 7                   ; that make up the bottom half of the speech bubble;
  ADD A,248               ; set E to the reference of the UDG that is above the
  LD E,A                  ; lip
  LD C,3                  ; We need to adjust the UDGs for each third of the
                          ; skool
  LD H,127
OPENLIP_0:
  LD B,8                  ; 8 UDGs make up the bottom half of the speech bubble
  LD A,H                  ; Point HL at the bottom pixel row of the UDG for the
  ADD A,B                 ; bottom-left corner of the speech bubble
  LD H,A                  ;
  LD A,E                  ;
  LD L,248                ;
OPENLIP_1:
  LD (HL),126             ; Open the bottom edge (129=10000001) of this speech
  CP L                    ; bubble UDG if the lip is underneath, or close it
  JR NZ,OPENLIP_2         ; (126=01111110) otherwise
  LD (HL),129             ;
OPENLIP_2:
  INC L                   ; Move HL to the next UDG along the bottom of the
                          ; speech bubble
  DJNZ OPENLIP_1          ; Jump back until the lip is open and the others are
                          ; closed
  DEC C                   ; Next third of the skool
  JR NZ,OPENLIP_0         ; Jump back until the UDGs for each third of the
                          ; skool have been modified
  JP SPEECHSRB            ; Update the SRB for the middle 6 UDG columns of the
                          ; bubble

; Unused
  DEFS 2

; Check whether ERIC has jumped up to the safe or a shield (1)
;
; Used by the routine at JUMPING.
;
; H 172 (ERIC)
CHECKJP:
  CALL UPDATESRB          ; Update the SRB for ERIC's current location
  INC A                   ; A=ERIC's next animatory state
  DEC D                   ; Get the coordinates of the spot above ERIC in BC
  LD B,D                  ;
  LD C,E                  ;
  BIT 7,A                 ; Is ERIC facing left?
  JR Z,CHECKJP_0          ; Jump if so
  INC C                   ; Add 2 to the x-coordinate in C if ERIC is facing
  INC C                   ; right
CHECKJP_0:
  PUSH BC                 ; Store the coordinates of ERIC's hand
  CALL UPDATEAS           ; Update ERIC's animatory state and location and
                          ; update the SRB
  POP DE                  ; Restore the coordinates of ERIC's hand to DE
  JP CHECKJP2

; Deal with ERIC when he's jumping
;
; Used by the routine at MOVEERIC when bit 2 of ERIC's status flags at STATUS
; is set (by the routine at FIRE, via JUMP).
JUMPING:
  LD HL,ACTIONCNT         ; Decrement the jumping action timer at ACTIONCNT
                          ; (which starts at 16)
  DEC (HL)                ; Has ERIC completed the jump?
  JP Z,FIRING_0           ; Jump if so
  LD A,(HL)               ; Pick up the current value of the action timer in A
JUMPING_0:
  CP 12
  LD B,15                 ; Lines reprimand 15: YOU ARE NOT A KANGAROO
  JP Z,FIRING_2           ; Jump if it's time to check whether any teachers can
                          ; see ERIC jumping
  LD H,172                ; 172=ERIC
  CP 13                   ; Is it time to check whether ERIC has touched a
                          ; shield or opened the safe?
  JP Z,CHECKJP            ; Jump if so
  CP 14                   ; Is it time to make the jumping sound effect?
  JP Z,JUMPSOUND          ; Jump if so
  CP 9                    ; Is it time to move ERIC's arm back?
  JP Z,FIRE5_0            ; Jump if so
  CP 3                    ; Is it time to bring ERIC back down to the floor?
  JR NZ,JUMPING_1         ; Jump if not
  CALL UPDATESRB          ; Update the SRB for ERIC's current location
  INC D                   ; Move ERIC downwards
  JP UPDATEAS             ; Update ERIC's location and update the SRB
JUMPING_1:
  CP 6                    ; Return if A < 6 (nothing to check or do)
  RET C                   ;
; At this point A=6 (which means it's time to check whether ERIC has jumped
; onto a boy, or ERIC is already standing on a boy), or A is one of {7, 8, 10,
; 11, 15}, or A>128 (which means ERIC has jumped while standing on a boy).
  JR NZ,JUMPING_6         ; Jump unless (a) it's time to check whether ERIC has
                          ; jumped on a boy, or (b) ERIC is already standing on
                          ; a boy
; This entry point is erroneously used by the routine at JUMPSOUND with H=3
; (which means (D,E) is set to (198,32) by the code that follows, and the
; routine exits at NOTONMKID).
JUMPING_2:
  LD L,97                 ; Byte 97 of ERIC's buffer holds his y-coordinate
  LD D,(HL)               ; Pick this up in D
  INC D                   ; Down a level (where the boy that ERIC is standing
                          ; on would be, if any)
  INC L                   ; Byte 98 holds ERIC's x-coordinate
  LD E,(HL)               ; Pick this up in E
  LD B,11                 ; There are 11 little boys
  LD H,152                ; 152=little boy no. 1
  CALL BOYCHECK           ; Is ERIC standing on a little boy?
  JR C,JUMPING_3          ; Jump if so
  LD H,167                ; 167=BOY WANDER
  LD B,3                  ; There are three main kids besides ERIC
  CALL BOYCHECK_0         ; Is ERIC standing on one of the three main kids?
NOTONMKID:
  RET NC                  ; Return if not
; ERIC is standing on a kid who's been knocked out.
JUMPING_3:
  CALL READKEY            ; Check for keypresses
ONKID:
  LD HL,ACTIONCNT         ; Set the jumping action timer at ACTIONCNT to 7, so
  LD (HL),7               ; that we keep returning to this section of code
                          ; while ERIC is standing on a boy
  RET Z                   ; Return if no key of interest was pressed
  SET 5,A                 ; Convert the ASCII code of the keypress in A to
                          ; lower case
  CP 106                  ; Was 'j' (jump) pressed?
  JR NZ,JUMPING_4         ; Jump if not
  LD (HL),144             ; Set the jumping action timer at ACTIONCNT to 144 to
                          ; indicate that ERIC has jumped while standing on a
                          ; boy
  RET                     ; Return to the main loop with the carry flag reset
; A key other than 'j' was pressed while ERIC was standing on a boy.
JUMPING_4:
  CP 111                  ; 111='o'
  LD B,0                  ; Set B=0 (ERIC facing left) for later comparison
  JR Z,JUMPING_5          ; Jump if 'o' (left) was pressed
  LD B,128                ; Set B=128 (ERIC facing right) for later comparison
  CP 112                  ; Was 'p' (right) pressed?
  RET NZ                  ; Return if not
JUMPING_5:
  LD HL,44134             ; A=animatory state of ERIC before he jumped (stored
  LD A,(HL)               ; in byte 102 of his buffer)
  AND 128                 ; Keep only the 'direction' bit
  CP B                    ; Should ERIC turn round?
  RET Z                   ; Return if not
  LD A,(HL)               ; Adjust ERIC's pre-jump animatory state to reflect
  XOR 128                 ; his turning round while standing on a boy
  LD (HL),A               ;
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
  XOR 128                 ; A=animatory state of ERIC facing the other way
  JP UPDATEAS             ; Update ERIC's animatory state and update the SRB
; At this point A is one of {7, 8, 10, 11, 15} (which means there is nothing to
; do), or A=128-143 (which means ERIC has jumped while standing on a boy).
JUMPING_6:
  CP 134                  ; Return now if the SUB 128 instruction below would
  RET Z                   ; make A=6
  SUB 128                 ; Return unless ERIC jumped while standing on a boy
  RET C                   ;
; So ERIC has jumped while standing on a boy. Now either A=0, meaning it's time
; to set the jumping action timer at ACTIONCNT back to 7, so we can deal with
; ERIC while he's standing on a boy; or A=1-15 (but not 6, because that would
; lead us into the code at JUMPING_2 to perform a redundant check on whether
; ERIC is standing on a boy), meaning we need to run the usual checks while
; ERIC is in mid-air.
  JP NZ,JUMPING_0         ; Run the usual checks while ERIC is in mid-air above
                          ; a supine boy
  JR 62844                ; This should be JR ONKID

; Unused
  DEFS 3

; Check whether ERIC is standing on a boy who's been knocked out
;
; Used by the routine at JUMPING. Returns with the carry flag set if ERIC is
; standing on a boy.
;
; B 11 (there are 11 little boys)
; D 1 + ERIC's y-coordinate
; E ERIC's x-coordinate
; H 152 (little boy no. 1)
BOYCHECK:
  LD C,E                  ; Set E=x-1 and C=x+2 where x is ERIC's x-coordinate;
  DEC E                   ; any boy with an x-coordinate in this range is a
  INC C                   ; potential step for ERIC
  INC C                   ;
  LD L,97                 ; Byte 97 of the boy's buffer holds his y-coordinate
; This entry point is also used by the routine at JUMPING with H=167 (BOY
; WANDER) and B=3 (and E and C already set as above).
BOYCHECK_0:
  LD A,D                  ; A=y+1 (y=ERIC's y-coordinate)
  CP (HL)                 ; Is there a boy at this y-coordinate?
  JR NZ,BOYCHECK_1        ; Jump if not
  DEC L                   ; Collect the boy's animatory state from byte 96 of
  LD A,(HL)               ; his buffer
  INC L                   ; L=97
  AND 15                  ; Keep only bits 0-3 of the animatory state
  CP 7                    ; Is the boy lying flat on his back?
  JR NZ,BOYCHECK_1        ; Jump if not
  INC L                   ; Collect the boy's x-coordinate from byte 98 of his
  LD A,(HL)               ; buffer
  DEC L                   ; L=97
  CP E                    ; Return with the carry flag set if ERIC is standing
  JR C,BOYCHECK_1         ; on this boy; otherwise continue
  CP C                    ;
  RET C                   ;
BOYCHECK_1:
  INC H                   ; Next boy
  DJNZ BOYCHECK_0         ; Jump back until all the boys have been checked
  AND A                   ; Return with the carry flag reset to indicate that
  RET                     ; ERIC is not standing on a boy

; Unused
  DEFB 0

; Collect a keypress during the game (or simulate one in demo mode)
;
; Called from the main loop at MAINLOOP. Returns with A holding the ASCII code
; of the last (actual or simulated) keypress, or the zero flag set if there was
; no (actual or simulated) keypress.
GETINPUT:
  LD HL,GAMEMODE          ; GAMEMODE holds the game mode indicator
  LD A,(HL)               ; Are we in demo mode?
  AND A                   ;
  JP NZ,READKEY           ; Collect a keypress if not
; We're in demo mode.
  LD L,199                ; Point HL at the MSB of the lines total
  LD B,4                  ; Reset the score (stored at SCORE) and lines total
GETINPUT_0:
  LD (HL),A               ; (stored at LINES) to zero
  DEC L                   ;
  DJNZ GETINPUT_0         ;
  CALL READKEY            ; Check for input from the keyboard or joystick
  JP NZ,EXITDEMO          ; Exit demo mode if there was input
; We're in demo mode and there was no keyboard or joystick input. Time to
; simulate a keypress to move ERIC.
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  CP 5                    ; 5=ERIC sitting in a chair
  LD HL,UPDELAY           ; UPDELAY holds the stand-up delay counter
  JR NZ,GETINPUT_5        ; Jump unless ERIC is sitting in a chair
  LD A,(BOY11CBUF)        ; A=animatory state of little boy no. 11
  CP 69                   ; 69=little boy sitting in a chair
  JR Z,GETINPUT_4         ; Jump if little boy no. 11 is sitting in a chair
; ERIC is sitting in a chair, but little boy no. 11 isn't. ERIC should stand up
; now or soon.
  LD A,(HL)               ; The stand-up delay counter at UPDELAY normally
  AND A                   ; holds 0; if little boy no. 11 has just stood up,
  JR Z,GETINPUT_3         ; initialise the delay counter to 29
  DEC (HL)                ; Is it time for ERIC to stand up?
  JR NZ,GETINPUT_2        ; Jump if not
GETINPUT_1:
  LD A,115                ; Set A to 115 to simulate 's' (sit/stand) being
                          ; pressed
  AND A                   ; Return with the zero flag reset to indicate input
  RET                     ;
GETINPUT_2:
  XOR A                   ; Return with the zero flag set to indicate no input
  RET                     ;
GETINPUT_3:
  LD (HL),29              ; Initialise the stand-up delay counter at UPDELAY to
                          ; 29
  RET                     ; Return with the zero flag set to indicate no input
GETINPUT_4:
  LD (HL),0               ; Set the stand-up delay counter at UPDELAY to 0
  RET                     ; Return with the zero flag set to indicate no input
; ERIC isn't sitting in a chair. Is he sitting on the floor or lying on his
; back?
GETINPUT_5:
  LD (HL),0               ; Set the stand-up delay counter at UPDELAY to 0
  LD L,251                ; HL=STATUS (ERIC's status flags)
  BIT 7,(HL)              ; Is ERIC sitting on the floor or lying on his back?
  JR NZ,GETINPUT_1        ; Simulate 's' to make ERIC stand up if so
; ERIC is upright. Seek little boy no. 11 for guidance on where to go next.
  LD HL,41569             ; Pick up the coordinates of little boy no. 11 in DE
  LD D,(HL)               ;
  INC L                   ;
  LD E,(HL)               ;
  LD H,172                ; 172=ERIC
  CALL NEXTMOVE           ; Determine ERIC's next move in his pursuit of little
                          ; boy no. 11
  AND A                   ; Are ERIC and this boy in the same location?
  JR NZ,GETINPUT_6        ; Jump if not
; ERIC and little boy no. 11 are in the same location.
  LD A,(BOY11CBUF)        ; A=animatory state of little boy no. 11
  CP 69                   ; Is little boy no. 11 sitting in a chair?
  JR NZ,GETINPUT_2        ; Jump if not (without simulating a keypress)
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  RLCA                    ; Is ERIC facing left?
  JR NC,GETINPUT_1        ; Simulate 's' (sit down) if so
  LD A,3                  ; Otherwise set A=3: ERIC should go left (i.e. turn
                          ; round)
; Now A=1 if ERIC should go up, 2 if down, 3 if left, or 4 if right (to follow
; little boy no. 11).
GETINPUT_6:
  LD B,81                 ; 81=ASCII code for 'Q' (up)
  DEC A                   ; Should ERIC go up?
  JR Z,GETINPUT_7         ; Jump if so
  LD B,65                 ; 65=ASCII code for 'A' (down)
  DEC A                   ; Should ERIC go down?
  JR Z,GETINPUT_7         ; Jump if so
  ADD A,78                ; B=79 ('O') or 80 ('P')
  LD B,A                  ;
; Now B holds the ASCII code of the simulated keypress: Q, A, O or P. Decide
; whether to switch to lower case (slow).
GETINPUT_7:
  LD A,(23672)            ; 23672=LSB of the FRAMES system variable
  AND 128                 ; A=32 (lower case, slow) or 0 (upper case, fast)
  RRCA                    ;
  RRCA                    ;
  ADD A,B                 ; A=simulated keycode
  RET                     ; Return with the zero flag reset to indicate input

; Unused
  DEFB 0

; 'W' pressed - write on a blackboard
;
; The address of this routine is found in the table of keypress handling
; routines at K_WRITE. It is called from the main loop at MAINLOOP when 'W' is
; pressed.
WRITE:
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  BIT 7,(HL)              ; Is ERIC sitting or lying down?
  RET NZ                  ; Return if so
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  RET NZ                  ; Return if so
  CP 169                  ; Is ERIC on the bottom floor?
  RET Z                   ; Return if so
  CP 155                  ; Set the zero flag if ERIC's on the top floor
  LD BC,12332             ; C=44, B=48 (x-coordinates of the edges of the
                          ; 'writing zone' for the Reading Room blackboard)
  LD A,E                  ; A=ERIC's x-coordinate
  JR Z,WRITE_0            ; Jump if ERIC's on the top floor
  LD BC,8220              ; C=28, B=32 (x-coordinates of the edges of the
                          ; writing zone for the White Room blackboard)
  CP 40                   ; Is ERIC to the left of the White Room wall?
  JR C,WRITE_0            ; Jump if so
  LD BC,12075             ; C=43, B=47 (x-coordinates of the edges of the
                          ; writing zone for the Exam Room blackboard)
; Now C and B hold the x-coordinates of the left and right edges of the writing
; zone for the blackboard closest to ERIC.
WRITE_0:
  CP C                    ; Return if ERIC is not standing close enough to the
  RET C                   ; blackboard to write on it
  CP B                    ;
  RET NC                  ;
  SET 5,(HL)              ; Set bit 5 of ERIC's status flags at STATUS: ERIC is
                          ; writing on a blackboard
  LD H,172                ; 172=ERIC
  CALL BOARDID            ; Get the blackboard identifier in A
  LD L,A                  ; HL=RRBRDAUTH (Reading Room), WRBRDAUTH (White Room)
  LD H,127                ; or ERBRDAUTH (Exam Room)
  INC L                   ;
  SUB 180                 ; B=56 (Reading Room), 58 (White Room) or 60 (Exam
  LD B,A                  ; Room)
  LD A,(HL)               ; A=number of the character who last wrote on this
                          ; board
  LD (HL),172             ; Signal that ERIC (172) wrote on this board
  XOR 128                 ; C=128 (bit 7 set) if the board is clean, <128 (bit
  LD C,A                  ; 7 reset) if not
  LD A,B                  ; HL=RRBRDBUF (Reading Room blackboard), WRBRDBUF
  ADD A,A                 ; (White Room blackboard), or ERBRDBUF (Exam Room
  ADD A,B                 ; blackboard)
  LD L,A                  ;
  LD B,4                  ; Prepare the 4 slots in the blackboard buffer that
WRITE_1:
  LD (HL),C               ; will store the first 4 characters written on the
  INC L                   ; board by ERIC; each slot will have bit 7 set if the
  DJNZ WRITE_1            ; board was clean before ERIC started writing
  LD (HL),1               ; ERIC will start writing at pixel column 1
; This entry point is used by the routine at WRITING.
WRITE_2:
  LD HL,ERICCBUF          ; Point HL at byte 96 of ERIC's buffer
  LD B,(HL)               ; Pick up ERIC's animatory state in B, and his
  INC L                   ; coordinates in DE
  LD D,(HL)               ;
  INC L                   ;
  LD E,(HL)               ;
  LD A,87                 ; Set KEYCODE (which holds the ASCII code of the last
  LD (KEYCODE),A          ; keypress) to 'W' (upper case, so ERIC can write
                          ; fast)
  LD H,D                  ; Copy ERIC's coordinates to HL
  LD L,E                  ;
  LD A,B                  ; A=ERIC's current animatory state
  AND 128                 ; A=13 or 141: ERIC with arm up, as if writing
  ADD A,13                ;
  JP MVERIC1              ; Raise ERIC's arm

; Deal with ERIC when he's writing on a blackboard
;
; Called from the main loop at MAINLOOP when bit 5 of ERIC's status flags at
; STATUS is set (by the routine at WRITE).
;
; A ASCII code of the last keypress
; HL STATUS (ERIC's status flags)
WRITING:
  CP 13                   ; Was ENTER pressed?
  JR Z,WRITING_2          ; Jump if so
  LD B,A                  ; Store the keypress code in B
  LD H,172                ; 172=ERIC
  CALL BOARDID            ; C=171 (Reading Room blackboard), 177 (White Room
  SUB 179                 ; blackboard) or 183 (Exam Room blackboard)
  LD C,A                  ;
  ADD A,A                 ;
  ADD A,C                 ;
  LD C,A                  ;
  LD L,A                  ; Point HL at byte 3 of the relevant blackboard
  LD H,127                ; contents buffer)
  BIT 7,(HL)              ; Jump if 4 or more characters have been written so
  JR Z,WRITING_1          ; far, or the board was dirty before ERIC started
                          ; writing
  SUB 4                   ; Point HL at the byte preceding the first byte of
  LD L,A                  ; the relevant blackboard contents buffer
WRITING_0:
  INC L                   ; Find the next 'open' slot (which will have bit 7
  BIT 7,(HL)              ; set) in the blackboard contents buffer
  JR Z,WRITING_0          ;
  LD (HL),B               ; Store the keypress code in the blackboard contents
                          ; buffer
WRITING_1:
  INC C                   ; Restore the keypress code to A; set BC=RRBRDPOS
  LD A,B                  ; (Reading Room blackboard), WRBRDPOS (White Room
  LD B,127                ; blackboard), or ERBRDPOS (Exam Room blackboard)
                          ; (which holds the pixel coordinates at which ERIC
                          ; should write)
  CALL WRITECHR_2         ; Write a single character on the blackboard
  JP WRITE_2              ; Move ERIC's arm
; ERIC has finished writing on the board (ENTER was pressed).
WRITING_2:
  RES 5,(HL)              ; Reset bit 5 of ERIC's status flags at STATUS to
                          ; indicate that ERIC has finished writing on the
                          ; board
  RET                     ; Return to the main loop

; Make MR CREAK reveal his safe combination letter if appropriate
;
; Used by the routine at TELLSIT. A teacher is standing at the doorway of a
; classroom, ready to tell the kids to sit down; this routine checks whether
; the teacher is MR CREAK, and if it is, makes him reveal his safe combination
; letter if his birth year has been written on the blackboard by ERIC, and ERIC
; has not yet opened the safe.
;
; H Teacher's character number (163-166)
; L 122
CREAKLTR:
  CALL CHKCREAK           ; Return unless we're dealing with CREAK and ERIC has
  RET C                   ; not yet opened the safe
  CALL NRBOARD            ; Return unless CREAK is near a blackboard last
  RET NZ                  ; written on by ERIC
  LD A,E                  ; A=56 (Reading Room blackboard) or 58 (White Room
  SUB 181                 ; blackboard)
  PUSH HL                 ; Save CREAK's character number
  LD L,A                  ; HL=RRBRDBUF (Reading Room blackboard contents
  ADD A,A                 ; buffer) or WRBRDBUF (White Room blackboard contents
  ADD A,L                 ; buffer)
  NOP                     ;
  LD L,A                  ;
  LD H,D                  ;
; Check whether CREAK's birth year has been written on the board.
  LD E,155                ; DE=CREAKYEAR (which holds CREAK's year of birth)
  LD B,4                  ; There are 4 digits in CREAK's year of birth
CREAKLTR_0:
  LD A,(DE)               ; A=digit from CREAK's year of birth
  INC E                   ; Move DE along to the next digit
  CP (HL)                 ; Compare the digit with the character written on the
                          ; board
  JR NZ,CREAKLTR_1        ; Jump if they don't match
  INC L                   ; Move to the next character written on the board
  DJNZ CREAKLTR_0         ; Jump back until all 4 digits have been checked
; The first four characters written on the board by ERIC match the digits in MR
; CREAK's year of birth.
  POP HL                  ; Restore CREAK's character number (166) to H
  PUSH HL                 ; Save it again
  CALL LINESLTR_1         ; Reveal CREAK's safe combination letter
CREAKLTR_1:
  POP HL                  ; Restore CREAK's character number (166) to H
  LD BC,M_WACKERSIT       ; Set BC to M_WACKERSIT (SILENCE! OR I'LL CANE THE
                          ; LOT OF YOU) before returning
  RET

; Get the address of the appropriate 'Go home' message
;
; Used by the routine at SENDHOME. Returns with the address of the appropriate
; message in BC.
;
; H 163 (MR WACKER) or 164 (MR ROCKITT)
GOHOMEMSG:
  LD A,H                  ; A=character number of the teacher sending ERIC home
  CP 163                  ; 163=MR WACKER
  LD BC,M_LINES10K        ; M_LINES10K: 'YOU HAVE OVER 10000 LINES...'
  RET Z                   ; Return if MR WACKER is sending ERIC home
  LD BC,M_HAVEMUMPS       ; M_HAVEMUMPS: 'You have mumps...'
  RET

; Unflash the safe and all the shields
;
; Used by the routines at START (before starting a new game) and NEXTPHASE
; (after ERIC has gone up a year).
UNFLASH:
  LD HL,23040             ; Make anything that is currently flashing on-screen
UNFLASH_0:
  RES 7,(HL)              ; (shields or the safe) not flash any more by
  DEC HL                  ; directly modifying the attribute file
  LD A,H                  ;
  CP 87                   ;
  JR NZ,UNFLASH_0         ;
  LD H,154                ; Reset bit 7 (i.e. turn off FLASH) of the graphic
  LD B,13                 ; data attribute bytes of everything in the skool
UNFLASH_1:
  LD L,223                ; from the third row (H=154, where the top floor
UNFLASH_2:
  RES 7,(HL)              ; shields are) to the 15th row (H=166, where the
  DEC HL                  ; bottom floor shields are)
  BIT 7,L                 ;
  JR NZ,UNFLASH_2         ;
  INC H                   ;
  DJNZ UNFLASH_1          ;
  RET

; Check whether ERIC has jumped up to the safe or a shield (2)
;
; Continues from CHECKJP. Checks whether ERIC has jumped up to the safe or a
; shield, and makes it flash or unflash as appropriate.
;
; DE Coordinates of ERIC's hand
CHECKJP2:
  LD A,(GAMEMODE)         ; Pick up the game mode indicator from GAMEMODE
  CP 2                    ; Are all the shields flashing?
  JP NZ,PELLETV_1         ; Jump if not (to check whether ERIC touched a
                          ; shield)
  LD A,D                  ; A=y-coordinate of ERIC's hand
  CP 161                  ; This is the y-coordinate of the spot just under the
                          ; safe in the staff room
  RET NZ                  ; Return if ERIC is not on the middle floor
  LD A,E                  ; A=x-coordinate of ERIC's hand
  CP 10                   ; Has ERIC jumped up to the safe?
  RET NZ                  ; Return if not
; ERIC has jumped up to the safe. Can he open it?
  LD HL,RRBRDAUTH         ; RRBRDAUTH holds the number of the character who
                          ; last wrote on the Reading Room blackboard
  LD DE,RRBRDBUF          ; RRBRDBUF=Reading Room blackboard contents buffer
  LD C,3                  ; There are 3 blackboards
CHECKJP2_0:
  LD A,(HL)               ; A=number of the character who last wrote on this
                          ; board
  INC HL                  ; Move HL along to the next blackboard
  INC HL                  ;
  CP 172                  ; Did ERIC write on this blackboard?
  JR NZ,CHECKJP2_3        ; Jump if not
; ERIC wrote on this blackboard. Check whether he wrote the safe combination
; code.
  PUSH HL
  LD L,159                ; HL=SAFECOMBO (which holds the safe combination
                          ; code)
  LD B,4                  ; There are 4 characters in the safe combination code
  PUSH DE                 ; Save the pointer to the blackboard contents buffer
CHECKJP2_1:
  LD A,(DE)               ; A=character written on the blackboard by ERIC
  AND 95                  ; Convert to upper case
  INC E                   ; Move DE along to the next character on the
                          ; blackboard
  CP (HL)                 ; Does the character on the blackboard match a safe
                          ; combination letter?
  JR NZ,CHECKJP2_2        ; Jump if not
  INC L                   ; Move to the next letter of the safe combination
                          ; code
  DJNZ CHECKJP2_1         ; Jump back until all four letters have been checked
  XOR A                   ; Set the zero flag to indicate a match
CHECKJP2_2:
  POP DE                  ; Restore the blackboard contents buffer pointer to
                          ; DE
  POP HL
  JR Z,CHECKJP2_4         ; Jump if ERIC got the right combination
CHECKJP2_3:
  LD A,E                  ; Point DE at the next blackboard contents buffer
  ADD A,6                 ; (WRBRDBUF or ERBRDBUF)
  LD E,A                  ;
  DEC C                   ; Decrease the blackboard counter
  JR NZ,CHECKJP2_0        ; Jump back until all three blackboards have been
                          ; checked
  RET
; ERIC got the right combination.
CHECKJP2_4:
  CALL NEXTPHASE          ; Add 1000 to the score and play a celebratory tune
  LD HL,41354             ; Point HL at the attribute byte for the safe
  SET 7,(HL)              ; Alter the attribute byte of the safe in the skool
                          ; attribute data to make it flash
  EX DE,HL                ; Point DE at the attribute byte for the safe
  CALL GETATTR            ; Get the attribute file address of the safe in HL
  RET C                   ; Return if the safe is off-screen (unlikely!)
  SET 7,(HL)              ; Adjust the on-screen attribute byte of the safe to
                          ; make it flash
  RET

; Unused
  DEFS 6

; Deal with ERIC when he's being spoken to by a little boy
;
; Used by the routine at MOVEERIC when bit 3 of ERIC's status flags at STATUS
; is set (by the routine at TELLERIC).
LISTEN:
  LD HL,SPECIALSIGS       ; SPECIALSIGS holds the special playtime signal flags
  BIT 0,(HL)              ; Is the little boy is still delivering the main
                          ; message?
  RET Z                   ; Return if so
  CALL READKEY            ; Check the keyboard
  RET Z                   ; Return if no game keys were pressed
  OR 32                   ; Convert the keypress code to lower case
  CP 117                  ; Was 'u' pressed?
  RET NZ                  ; Return (to ERICDONE) if not
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  RES 3,(HL)              ; Signal: ERIC's conference with the little boy is
                          ; over
  RET                     ; Return to ERICDONE

; Unused
  DEFB 0

; Make a character find ERIC
;
; Used by command lists 208, 210, 214, 218 and 222 to make little boy no. 10,
; MR WACKER or MR ROCKITT track down ERIC (to deliver a message).
;
; H Character number (161, 163, 164)
FINDERIC:
  LD A,255                ; Adjust the length of the lesson to allow the
  CALL PRECHASE           ; character enough time to find ERIC
; This routine places LOOKING into bytes 111 and 112 of the character's buffer,
; but also uses byte 105 (which normally holds the LSB of a routine address)
; for coordination. When byte 105 holds 160 (the LSB of the routine at
; SEEKERIC), ERIC's coordinates are compared with those of the character
; looking for him, and the decision is made whether to keep moving. If the
; character does need to keep moving, SEEKERIC_4 is called, which either (a)
; turns the character round, or (b) puts the character midstride, and places
; 251 (the LSB of the routine at TOERIC) into byte 105. When byte 105 holds
; 251, this routine jumps to TOERIC to move the character from the midstride
; position, and set byte 105 back to 160.
  LD L,105                ; Initialise byte 105 to 160, so on this first pass
  LD (HL),160             ; we compare ERIC's location with that of his chaser
  LD L,111                ; Place LOOKING into bytes 111 and 112 of the
  LD (HL),158             ; character's buffer (thus setting the character's
  INC L                   ; uninterruptible subcommand)
  LD (HL),247             ;
; Subsequent calls to this routine will enter at this point, by virtue of its
; address being placed into bytes 111 and 112 of the character's buffer.
LOOKING:
  LD A,H                  ; A=number of the character looking for ERIC
  CP 163                  ; Is MR WACKER looking for ERIC?
  CALL Z,RUN              ; If so, make him walk fast
  LD L,105                ; Is the character midstride?
  LD A,(HL)               ;
  CP 251                  ;
  JP Z,TOERIC             ; Finish the stride if so
; The character looking for ERIC is not midstride at the moment. Decide which
; way (if any) he should go to find ERIC.
  CALL ERICLOC            ; Get ERIC's coordinates in DE
  LD L,97                 ; Byte 97 of the character's buffer holds his
                          ; y-coordinate
  LD A,(HL)               ; Pick this up in A
  CP D                    ; Set the zero flag if ERIC is on same floor
  CALL CPERICLOC          ; Compare this character's coordinates with ERIC's
  JR NZ,FINDERIC_3        ; Jump if this character is on a staircase or more
                          ; than 3 y-coordinates away from ERIC
  INC L                   ; L=98
  LD A,(HL)               ; A=character's x-coordinate
  SUB E                   ; Does it match ERIC's x-coordinate?
  JR NZ,FINDERIC_1        ; Jump if not
; The character looking for ERIC is within 3 x-coordinates and 3 y-coordinates
; of ERIC's location, and is not on a staircase. In other words, ERIC has been
; found.
FINDERIC_0:
  LD A,(STATUS)           ; Check ERIC's status flags at STATUS, and return if
  AND 127                 ; ERIC is firing, hitting, jumping, being spoken to
  RET NZ                  ; by a little boy, knocked out, or writing on a
                          ; blackboard
  LD L,112                ; Remove the address of the entry point at LOOKING
  LD (HL),A               ; from bytes 111 and 112 of the character's buffer
  LD L,106                ; Remove any interruptible subcommand routine address
  LD (HL),A               ; from bytes 105 and 106 of the character's buffer
  JP NEXTCMD              ; Move to the next command in the command list
; The character looking for ERIC is within 3 y-coordinates of ERIC's location,
; but not at the same x-coordinate. Is he close enough, though?
FINDERIC_1:
  JR NC,FINDERIC_2        ; Jump if the character is to the right of ERIC
  NEG                     ; Get the absolute x-coordinate distance between the
                          ; character and ERIC
FINDERIC_2:
  CP 4                    ; Is ERIC less than 4 x-coordinates away?
  JR C,FINDERIC_0         ; Jump if so (that's close enough)
; The character has not yet found ERIC.
FINDERIC_3:
  CALL NEXTMOVE           ; Determine this character's next move
  JP SEEKERIC_4           ; Set him off in the appropriate direction

; Unused
;
; Code remnants.
  CALL NEXTPHASE          ; Adjust the status of the shields and the safe
  JP NEXTCMD              ; Move to the next command in the command list

; Make MR WACKER give ERIC 2000 lines
;
; Used by command lists 214 (after EINSTEIN has told MR WACKER what ERIC's up
; to) and 218 (after MR WACKER has found the pea-shooter on the fire escape).
;
; H 163 (MR WACKER)
LINES2K:
  LD BC,M_LINES2K         ; M_LINES2K: 'TAKE 2000 LINES YOU NASTY BOY'
  CALL GOTOPAGE_3         ; Make MR WACKER say this
  LD HL,(LINES)           ; Add 2000 lines to ERIC's total (stored at LINES)
  LD DE,200               ;
  ADD HL,DE               ;
  LD (LINES),HL           ;
  LD DE,20956             ; Print the new lines total
  CALL PRINTNUM           ;
  LD A,1                  ; Set the MSB of the lesson clock to 1 so that the
  LD (32760),A            ; lesson will end soon
  JP NEXTCMD              ; Move to the next command in MR WACKER's command
                          ; list

; Unused
  DEFS 2

; Make little boy no. 10 give ERIC a message
;
; Used by command lists 208 and 210. Makes little boy no. 10 tell ERIC about
; BOY WANDER having hidden a pea-shooter on the fire escape, EINSTEIN's intent
; to grass him up to MR WACKER, or ANGELFACE's medical condition.
;
; H 161 (little boy no. 10)
TELLERIC:
  LD A,(STATUS)           ; Set bit 3 of ERIC's status flags at STATUS,
  SET 3,A                 ; indicating that ERIC is being spoken to
  LD (STATUS),A           ;
TELLERIC_0:
  LD A,255                ; Adjust the MSB of the lesson clock so that the
  LD (32760),A            ; lesson will not end until the drama has played out
; Figure out which message the boy should deliver, depending on what MR ROCKITT
; and MR WACKER are up to.
  LD A,(42104)            ; Collect byte 120 of MR ROCKITT's buffer, which
                          ; holds the MSB of the command list start address
  CP 250                  ; This is the MSB of CLIST222 (command list 222)
  LD BC,M_BULLYMUMPS      ; BC=M_BULLYMUMPS: 'Don't let ANGELFACE touch you...'
  JR Z,TELLERIC_1         ; Jump if MR ROCKITT is using command list 222
  LD C,53                 ; BC=M_SWOTGRASS: 'EINSTEIN is going to tell MR
                          ; WACKER...'
  LD A,(41845)            ; Collect byte 117 of MR WACKER's buffer, which holds
                          ; the LSB of the address reached in the command list
  CP 60                   ; Is MR WACKER using command list 216?
  JR C,TELLERIC_1         ; Jump if not (he must be using command list 214)
  LD C,143                ; BC=M_WANDERHIDDEN: 'BOY WANDER has hidden a
                          ; pea-shooter...'
; Now BC holds the address of the message to be delivered.
TELLERIC_1:
  CALL GOTOPAGE_3         ; Make the little boy deliver his message
  LD L,104                ; Initialise the message repetition delay counter in
  LD (HL),40              ; byte 104 of the little boy's buffer to 40
  LD BC,M_PRESSU          ; M_PRESSU: 'Press 'U' if you understood'
  LD A,(SPECIALSIGS)      ; Set bit 0 at SPECIALSIGS, indicating that the
  SET 0,A                 ; little boy is now waiting for acknowledgment from
  LD (SPECIALSIGS),A      ; ERIC
  CALL GOTOPAGE_3         ; Make the little boy say 'Press 'U' if you
                          ; understood'
  LD A,(STATUS)           ; Collect ERIC's status flags from STATUS
  BIT 3,A                 ; Has 'U' been pressed yet?
  JR NZ,TELLERIC_2        ; Jump if not
; ERIC has acknowledged delivery of the message ('U' was pressed).
  LD A,12                 ; Set the MSB of the lesson clock to 12, giving
  LD (32760),A            ; enough time for the ensuing drama to play out
  JP NEXTCMD              ; Move to the next command in little boy no. 10's
                          ; command list
; ERIC has not yet acknowledged delivery of the message ('U' has not been
; pressed).
TELLERIC_2:
  LD L,104                ; Decrement the message repetition delay counter in
  DEC (HL)                ; byte 104 of the little boy's buffer and return
  RET NZ                  ; unless it has reached 0
  LD A,(SPECIALSIGS)      ; Reset bit 0 at SPECIALSIGS, indicating that the
  RES 0,A                 ; little boy is no longer waiting for acknowledgment
  LD (SPECIALSIGS),A      ; from ERIC (because he's going to repeat the message
                          ; first)
  JP TELLERIC_0           ; Deliver the message to ERIC once again

; Generate a safe combination code and set MR CREAK's birth year
;
; Used by the routine at NEXTPHASE. Generates a new safe combination code, sets
; a new random birth year for MR CREAK, and adjusts the game mode (so that all
; the shields need to be flashed again).
;
; A 0
GENCOMBO:
  LD HL,ERBRDPOS          ; Clear the safe combination code (stored at
  LD B,25                 ; SAFECOMBO), the teachers' combination letters
GENCOMBO_0:
  LD (HL),A               ; (stored at TEACHLTRS), and the blackboard contents
  DEC L                   ; buffers (at RRBRDBUF, WRBRDBUF and ERBRDBUF)
  DJNZ GENCOMBO_0         ;
  LD D,A                  ; DE=0
  LD E,A                  ;
  LD C,4                  ; There are four letters in the combination
  CALL GETRANDOM          ; A=random number
  AND 15                  ; 0<=A<=15
  JR GENCOMBO_3           ; Store MR WACKER's safe combination letter first (at
                          ; SAFECOMBO)
GENCOMBO_1:
  LD L,159                ; HL=SAFECOMBO (first letter of the safe combination
                          ; code)
  ADD HL,BC               ; HL=SAFECOMBO+C (1<=C<=3)
GENCOMBO_2:
  CALL GETRANDOM          ; A=random number
  LD E,B                  ; E=0
  RRCA                    ; Get a random number between 0 and 3 in E
  RL E                    ;
  RRCA                    ;
  RL E                    ;
  AND 31                  ; Is A between 0 and 25?
  CP 26                   ;
  JR NC,GENCOMBO_2        ; Jump back if not
GENCOMBO_3:
  ADD A,65                ; A=ASCII code of an upper case letter
  LD (HL),A               ; Store this in one of the slots at SAFECOMBO
  LD L,163                ; Point HL at a random teacher's safe combination
  ADD HL,DE               ; letter
  BIT 6,(HL)              ; Has this letter already been decided?
  JR NZ,GENCOMBO_1        ; Jump if so
  LD (HL),A               ; Store the ASCII code in one of the four slots at
                          ; TEACHLTRS
  DEC C                   ; Next combination letter
  JR NZ,GENCOMBO_1        ; Jump back until all four combination letters are
                          ; done
; The safe combination code has been generated. Now for MR CREAK's birth year.
  LD L,154                ; HL=BATTLEID (which holds the identifier for CREAK's
                          ; birth year battle)
GENCOMBO_4:
  CALL GETRANDOM          ; Get a random number between 0 and 20 in A
  CP 21                   ;
  JR NC,GENCOMBO_4        ;
  ADD A,A                 ; A=random odd number between 213 and 253
  ADD A,213               ;
  LD (HL),A               ; Store this battle identifier at BATTLEID
  SUB 129                 ; Set HL to the address of the battle year message
  LD E,A                  ; corresponding to this battle identifier
  LD H,D                  ;
  LD L,D                  ;
  ADD HL,DE               ;
  ADD HL,DE               ;
  ADD HL,DE               ;
  ADD HL,HL               ;
  ADD HL,HL               ;
  LD A,H                  ;
  ADD A,223               ;
  LD H,A                  ;
  LD DE,CREAKYEAR         ; Store the digits of CREAK's birth year at CREAKYEAR
  LD C,4                  ;
  LDIR                    ;
; Finally, adjust the game mode.
  LD A,1                  ; Set the game mode indicator at GAMEMODE to 1,
  LD (GAMEMODE),A         ; indicating that the shields need to be flashed
  RET

; Unused
  DEFS 2

; Adjust the game mode
;
; Used by the routines at START (to prepare the shields and safe for a new
; game), CHKSHIELD (when the last shield has just been flashed or unflashed)
; and CHECKJP2 (when ERIC has opened the safe).
NEXTPHASE:
  PUSH HL
  LD HL,SHIELDCNT         ; SHIELDCNT holds the shield counter
  LD (HL),15              ; Restore the shield counter to 15
  DEC L                   ; HL=GAMEMODE (game mode indicator)
  INC (HL)                ; A=1 if the game has just started, 2 if the 15th
  LD A,(HL)               ; shield was just flashed, 3 if the safe was just
                          ; opened, or 4 if the 15th shield was just unflashed
  LD HL,(SCORE)           ; HL=score
  CP 4                    ; Was the 15th shield just unflashed?
  JR NZ,NEXTPHASE_0       ; Jump if not
; ERIC has just hit the 15th shield and made it unflash.
  LD DE,500               ; Add 5000 to the score and print it
  ADD HL,DE               ;
  CALL SETSCORE           ;
  LD HL,M_WELLDONE        ; M_WELLDONE='WELL DONE! GO UP A YEAR'
  CALL PRINTBOX_0         ; Print this in the lesson box
  LD A,12                 ; Set the MSB of the lesson clock to 12
  LD (32760),A            ;
  LD A,1                  ; Ensure that the 'DEC A' below sets the zero flag
NEXTPHASE_0:
  DEC A                   ; Was the 15th shield just flashed or the safe just
                          ; opened?
  JR NZ,NEXTPHASE_1       ; Jump if so
; Either the game has just started, or the 15th shield was just unflashed.
  CALL GENCOMBO           ; Generate a new safe combination code, and a new
                          ; random birth year for MR CREAK
  CALL UNFLASH            ; Unflash the safe and all the shields
  CALL PLAYTHEME          ; Play the theme tune
  POP HL
  RET
; ERIC has just hit the 15th shield and made it flash (A=1), or just opened the
; safe (A=2).
NEXTPHASE_1:
  LD DE,100               ; Add 1000 to the score
  ADD HL,DE               ;
  DEC A                   ; Was the safe just opened?
  JR NZ,NEXTPHASE_2       ; Jump if so
  ADD HL,DE               ; Add 1000 more to the score if the 15th shield was
                          ; hit
NEXTPHASE_2:
  CALL SETSCORE           ; Print the new score
  CALL WINTUNE            ; Play the 'Got all the shields' tune
  POP HL
  RET

; Check whether MR CREAK can reveal his safe combination letter
;
; Used by the routine at CREAKLTR. Returns with the carry flag reset if we're
; dealing with MR CREAK and ERIC has not yet opened the safe.
;
; H Teacher's character number (163-166)
CHKCREAK:
  LD BC,M_WACKERSIT       ; Prepare BC (M_WACKERSIT='SILENCE! OR I'LL CANE THE
                          ; LOT OF YOU') before returning
  LD A,H                  ; A=teacher's character number
  CP 166                  ; Return with the carry flag set if it's not MR CREAK
  RET C                   ; (166)
  LD A,(GAMEMODE)         ; Collect the game mode indicator from GAMEMODE
  DEC A                   ; Set the carry flag if it's demo mode or ERIC has
  CP 2                    ; already opened the safe
  CCF                     ;
  RET

; Collect the identifier for the next lesson
;
; Used by the routine at NEWLESSON. Returns with A holding the identifier
; (224-255) for the next lesson. If the next lesson is PLAYTIME, one of the
; 'special' PLAYTIMEs (243, 244 or 245) may be chosen in place of the main
; timetable entry.
;
; E Lesson number (0-63)
GETLESSON:
  LD D,254                ; Pick up the lesson identifier (224-255) from the
  LD A,(DE)               ; main timetable
  CP 243                  ; Is it PLAYTIME (243-255)?
  RET C                   ; Return if not
  LD A,(GAMEMODE)         ; GAMEMODE holds the game mode indicator
  AND A                   ; Set the zero flag if we're in demo mode
  LD A,(DE)               ; Restore the lesson identifier to A and return if
  RET Z                   ; we're in demo mode
  CALL GETRANDOM          ; A=random number
  JP PLAYTIME             ; Select a 'special' PLAYTIME occasionally

; Unused
  RET

; Scroll the skool into view and enter the main loop
;
; Used by the routine at START.
SCRLSKOOL:
  LD C,4                  ; Scroll the skool into view
SCRLSKOOL_0:
  CALL LSCROLL8           ;
  DEC C                   ;
  JR NZ,SCRLSKOOL_0       ;
  JP MAINLOOP             ; Enter the main loop

; Make MR CREAK ask the birth year question if appropriate
;
; Used by the routine at PREPQA. Makes MR CREAK ask the birth year question if
; (a) he hasn't asked it yet this lesson, and (b) ERIC hasn't opened the safe
; yet.
;
; A 0 (MR ROCKITT), 1 (MR WITHIT), or 2 (MR CREAK)
BIRTHQ:
  LD B,A                  ; Save the teacher identifier in B for now
  LD HL,ROCKITTQA         ; Point HL at MR ROCKITT's question/answer data table
                          ; (ready for the return to the calling routine)
  CP 2                    ; Are we dealing with MR CREAK?
  JR Z,BIRTHQ_1           ; Jump if so
BIRTHQ_0:
  LD A,B                  ; Restore the teacher identifier (0-2) to A
  AND A                   ; Set the zero flag if we're dealing with MR ROCKITT
  RET
; We're dealing with MR CREAK, so we need to determine whether he should ask
; the birth year question.
BIRTHQ_1:
  LD A,(GAMEMODE)         ; Collect the game mode indicator from GAMEMODE
  CP 3                    ; Has ERIC already opened the safe?
  JR NC,BIRTHQ_0          ; Jump if so
  AND A                   ; Are we in demo mode?
  JR Z,BIRTHQ_0           ; Jump if so
  LD A,(ASKED)            ; Has MR CREAK already asked the birth year question
  AND A                   ; this lesson?
  JR NZ,BIRTHQ_0          ; Jump if so
; MR CREAK hasn't asked the birth year question yet. Make him do so now.
  POP DE                  ; Drop the return address from the stack; we'll be
                          ; re-entering the calling routine at a different
                          ; point
  INC A                   ; Set ASKED to 1, indicating that MR CREAK has asked
  LD (ASKED),A            ; the birth year question this period
  LD A,(BATTLEID)         ; Copy the identifier of CREAK's birth year battle
  LD (M_CREAKA2Q),A       ; into the 'Q' spot in 'It was the BATTLE OF Q'
  LD HL,M_CREAKA2         ; M_CREAKA2: '{Please Sir - I cannot tell a lie . .
                          ; }It was the BATTLE OF Q'
  LD (43371),HL           ; Place this message address into bytes 107 and 108
                          ; of EINSTEIN's buffer
  LD DE,M_BIRTHQ          ; M_BIRTHQ='WHAT HAPPENED IN THE YEAR THAT I WAS
                          ; BORN'
  JP PREPQA_11            ; Re-enter the calling routine to make MR CREAK ask
                          ; the birth year question

; Make a stricken teacher give lines or reveal his safe combination letter
;
; Used by the routine at KNOCKED. If the character who has been knocked over is
; a teacher, this routine makes him reveal his safe combination letter (if all
; the shields are flashing but the safe has not been opened yet) and give lines
; to the nearest main kid (if any are close enough).
;
; A Knockout delay counter (1-18)
; H Character number (152-169)
LINESLTR:
  CP 18                   ; Is it time to reveal a safe combination letter?
  JR Z,LINESLTR_0         ; Jump if so
  CP 9                    ; Is it time to give lines to the nearest main kid?
  RET NZ                  ; Return if not
  POP BC
  JP DONT                 ; Give lines to the nearest main kid if we're dealing
                          ; with a teacher
; It's time to reveal a safe combination letter (if all shields are flashing).
LINESLTR_0:
  LD A,(GAMEMODE)         ; Collect the game mode indicator from GAMEMODE
  CP 2                    ; Set the zero flag if all the shields are flashing
                          ; but the safe has not been opened
  LD A,18                 ; Restore the knockout delay counter to A
  RET NZ                  ; Return unless all the shields are flashing
  LD A,H                  ; Return unless we are dealing with a teacher
  CP 163                  ;
  RET C                   ;
  CP 166                  ;
  RET NC                  ;
  POP BC
; This entry point is used by the routine at CREAKLTR to make MR CREAK reveal
; his safe combination letter after seeing his year of birth written on a
; blackboard.
LINESLTR_1:
  LD L,97                 ; Pick up the character's coordinates in DE
  LD D,(HL)               ;
  INC L                   ;
  LD E,(HL)               ;
  LD L,H                  ; Pick up the teacher's safe combination letter in A
  LD H,127                ;
  LD A,(HL)               ;
  LD L,150                ; Place this in LTRREVEAL
  LD (HL),A               ;
  CALL SCR2BUF            ; Save the area of the screen that will be
                          ; overwritten by the bubble
  RET C                   ; Return if the character is off-screen
  PUSH DE
  LD DE,BUBBLE2L1         ; Point DE at the graphic data portion of the safe
                          ; combination letter bubble graphic buffer at BUBBLE2
  LD HL,M_SPACE_2         ; M_SPACE_2: ' ' (single space)
  CALL NAME2BUF_1         ; Write this into the first line of the graphic
                          ; buffer
  LD DE,BUBBLE2L2         ; Write the teacher's safe combination letter into
  LD HL,LTRREVEAL         ; the second line of the graphic buffer
  CALL NAME2BUF_1         ;
  LD HL,BUBBLE2GR         ; Set the attribute bytes in the graphic buffer - INK
LINESLTR_2:
  DEC L                   ; 7: PAPER 0
  LD (HL),7               ;
  JR NZ,LINESLTR_2        ;
  POP DE                  ; Restore the attribute file address of the top left
  PUSH DE                 ; corner of the bubble to DE and save it again
  CALL BUF2SCR            ; Display the combination letter in a bubble
  XOR A                   ; A=0 (black border for the sound effect)
  JP GIVELINES_6          ; Make a sound effect and remove the bubble

; Unused
  DEFB 0

; Print the lesson
;
; Used by the routine at NEWLESSON. Prints the lesson or 'DEMO. - PRESS A KEY
; TO PLAY' in the lesson box.
PRINTBOX:
  LD A,(GAMEMODE)         ; Collect the game mode indicator from GAMEMODE
  AND A                   ; Are we in demo mode?
  JP NZ,PRTLESSON         ; Jump if not
  LD HL,M_DEMOMODE        ; M_DEMOMODE: 'DEMO. - PRESS A KEY TO PLAY'
; This entry point is used by the routine at NEXTPHASE with HL=M_WELLDONE
; ('WELL DONE! GO UP A YEAR').
PRINTBOX_0:
  LD DE,60720             ; Point DE at the lesson box graphic buffer
  CALL TXT2BUF            ; Write the first line of text into the lesson box
                          ; graphic buffer
  LD L,240                ; HL=M_DEMOMODEL2 ('A KEY TO PLAY') or M_WELLDONEL2
                          ; ('UP A YEAR')
  JP PRTLESSON_2          ; Write the second line of text into the lesson box
                          ; graphic buffer and copy the buffer to the screen

; Unused
  DEFB 0

; Check whether ERIC has 10000 lines
;
; Used by the routine at PRINTNUM2. The number of lines has just been printed.
; Has ERIC gone over the limit?
CHKLINES:
  LD A,(32711)            ; Collect the MSB of the lines total
  CP 3                    ; Is the lines total < 7680 (30*256)?
  RET C                   ; Return if so
  JR NZ,CHKLINES_0        ; Jump if the MSB > 3 (lines total > 10240)
  LD A,(LINES)            ; So the MSB is 3; collect the LSB from LINES
  CP 232                  ; Is the lines total < 10000 (=232+256*30)?
  RET C                   ; Return if so
; ERIC has 10000 lines or more. The end (of the game) is nigh.
CHKLINES_0:
  PUSH HL
  LD HL,64226             ; Point HL at the first of the last two commands in
                          ; command list 222 ('Find ERIC', and 'Send ERIC
                          ; home')
  LD (41847),HL           ; Place this address into bytes 119 and 120 of MR
                          ; WACKER's buffer, so that these commands become his
                          ; command list
  LD HL,41850             ; Set bit 0 of byte 122 of MR WACKER's buffer,
  SET 0,(HL)              ; triggering a command list restart
  LD HL,32760             ; Set the MSB of the lesson clock to 255 (so that the
  LD (HL),255             ; lesson cannot end before MR WACKER has begun
                          ; looking for ERIC)
  POP HL
  RET

; Make a teacher tell ERIC to go home, and end the game (1)
;
; Used by command list 222.
;
; H 163 (MR WACKER) or 164 (MR ROCKITT)
SENDHOME:
  CALL GOHOMEMSG          ; Get the address of the appropriate 'Go home'
                          ; message in BC (depending on who the teacher is)
  LD A,201                ; 201=RET
  JP SENDHOME2

; Unused
  DEFB 0

; Compare a character's coordinates with ERIC's
;
; Used by the routine at FINDERIC. On entry, the zero flag is set if ERIC's
; y-coordinate matches that of the character looking for him. On exit, the zero
; flag is set if the character looking for ERIC is within 3 y-coordinates of
; ERIC's location and not on a staircase (in which case the routine at FINDERIC
; checks whether the character is also within 3 x-coordinates of ERIC's
; location, i.e. has found ERIC).
;
; A Character's y-coordinate
; D ERIC's y-coordinate
; H Character number (161, 163, 164)
; L 97
CPERICLOC:
  JR Z,CPERICLOC_1        ; Jump if ERIC and his chaser are on the same floor
  SUB D                   ; A=absolute difference between the y-coordinates of
  JR NC,CPERICLOC_0       ; ERIC and his chaser
  NEG                     ;
CPERICLOC_0:
  CP 4                    ; Are ERIC and his chaser within 3 y-coordinates of
                          ; each other?
  JR C,CPERICLOC_1        ; Jump if so
  AND A                   ; Reset the zero flag (ERIC has not been found)
  RET
CPERICLOC_1:
  LD A,(HL)               ; A=y-coordinate of the character looking for ERIC
  JP ONSTAIRS_0           ; Set the zero flag set if the character is not on a
                          ; staircase

; Make a teacher tell ERIC to go home, and end the game (2)
;
; Continues from SENDHOME.
;
; A 201 (RET)
; BC 'Go home' message address (M_LINES10K or M_HAVEMUMPS)
; H 163 (MR WACKER) or 164 (MR ROCKITT)
SENDHOME2:
  LD (GIVELINES),A        ; Block the routine at GIVELINES with a RET
                          ; instruction to prevent the teachers from giving
                          ; ERIC (or anyone else) lines
  CALL GOTOPAGE_3         ; Deliver the 'Go home' message to ERIC
  LD A,197                ; 197=PUSH BC
  LD (GIVELINES),A        ; Restore the PUSH BC instruction at GIVELINES
  JP START_0              ; Restart the game

; Adjust the lesson length and prepare a character for the search for ERIC
;
; Used by the routine at FINDERIC. Before little boy no. 10, MR WACKER or MR
; ROCKITT goes off to look for ERIC (to deliver a message), the skool clock is
; adjusted to allow ERIC's chaser enough time to find him before the bell
; rings.
;
; A 255
; H Character number (161, 163, 164)
PRECHASE:
  LD (32760),A            ; Set the MSB of the lesson clock to 255 (so that the
                          ; character looking for ERIC will have time to find
                          ; him before the bell rings)
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  LD L,96                 ; Byte 96 of the character's buffer holds his
                          ; animatory state
  AND 250                 ; Adjust the character's animatory state to ensure
  LD (HL),A               ; that he is not midstride
  RET

; Check whether MR CREAK is near a blackboard last written on by ERIC
;
; Used by the routine at CREAKLTR. Returns with the zero flag set if MR CREAK
; is near a blackboard that was last written on by ERIC.
;
; H 166 (MR CREAK)
NRBOARD:
  CALL BOARDID            ; A=identifier of the blackboard nearest to CREAK
  CP 236                  ; Is CREAK on the top floor?
  JR NZ,NRBOARD_0         ; Jump if not
  INC L                   ; L=98 (which byte holds CREAK's x-coordinate)
  BIT 6,(HL)              ; Return with the zero flag reset if CREAK's
  RET NZ                  ; x-coordinate is >= 64, i.e. he's standing at the
                          ; doorway of the (blackboardless) Map Room (which
                          ; never happens, because CREAK never teaches in that
                          ; room)
NRBOARD_0:
  INC A                   ; DE=RRBRDAUTH (Reading Room blackboard) or WRBRDAUTH
  LD E,A                  ; (White Room blackboard)
  LD D,127                ;
  LD A,(DE)               ; A=number of the character who last wrote on this
                          ; board
  CP 172                  ; Set the zero flag if it was ERIC
  RET

; Restart command list 220
;
; Used by command list 220, which controls ANGELFACE when he has mumps.
;
; H 168 (ANGELFACE)
RST220:
  LD L,117                ; Place CLIST220A into bytes 117 and 118 of
  LD (HL),1               ; ANGELFACE's buffer
  JP ADDR2BUF             ; Pick up the continual subcommand routine address
                          ; (CHKTOUCH: check whether ANGELFACE is touching
                          ; ERIC) from that point in command list 220, place it
                          ; into bytes 124 and 125 of ANGELFACE's buffer, and
                          ; then move to the next command (GOTORAND: go to a
                          ; random location)

; Check whether ANGELFACE is touching ERIC
;
; The address of this continual subcommand routine is placed into bytes 124 and
; 125 of ANGELFACE's buffer by command list 220. It checks whether ERIC and
; ANGELFACE are in the same location; if they are, and ERIC has already been
; informed of ANGELFACE's medical condition, bit 5 at SPECIALSIGS is set,
; indicating that ERIC has mumps (whereupon MR ROCKITT will come looking for
; ERIC to send him home).
;
; H 168 (ANGELFACE)
CHKTOUCH:
  LD A,(SPECIALSIGS)      ; SPECIALSIGS holds the special playtime signal flags
  BIT 4,A                 ; Has ERIC been told (and understood) that ANGELFACE
                          ; has mumps?
  RET Z                   ; Return if not
  CALL RUN                ; Make ANGELFACE walk fast
  LD A,(44130)            ; A=ERIC's x-coordinate
  LD L,98                 ; Byte 98 of ANGELFACE's buffer holds his
                          ; x-coordinate
  CP (HL)                 ; Compare ERIC's and ANGELFACE's x-coordinates
  RET NZ                  ; Return if they do not match
  LD A,(44129)            ; A=ERIC's y-coordinate
  DEC L                   ; L=97 (which byte holds ANGELFACE's y-coordinate)
  CP (HL)                 ; Compare ERIC's and ANGELFACE's y-coordinates
  RET NZ                  ; Return if they do not match
; ERIC and ANGELFACE are in the same location, thus facilitating transmission
; of the latter's pestilence to the former.
  LD A,(SPECIALSIGS)      ; Set bit 5 at SPECIALSIGS, indicating that ERIC has
  SET 5,A                 ; mumps
  LD (SPECIALSIGS),A      ;
  RET

; Ask whether the characters' names should be changed
;
; Used by the routine at PREPGAME. Prints 'Do you want to put in your own names
; Y/N?', waits for 'y' or 'n' to be pressed, and responds accordingly.
ASKNAMES:
  LD DE,16448             ; Set DE to the appropriate display file address
  LD HL,M_OWNNAMES        ; M_OWNNAMES: 'Do you want to put in your own names
                          ; Y/N?'
  PUSH DE                 ; Save the display file address
  CALL PRINTMSG           ; Print this message
  POP DE                  ; Restore the display file address to DE
ASKNAMES_0:
  LD HL,23611             ; This is the system variable FLAGS
  RES 5,(HL)              ; Signal: no key pressed yet
ASKNAMES_1:
  CALL READKEY_0          ; Wait for a keypress; collect its ASCII code in A
  JR Z,ASKNAMES_1         ;
  OR 32                   ; Convert the ASCII code to lower case
  CP 121                  ; Was 'y' pressed?
  JP Z,INPUTDEV           ; Jump if so
  CP 110                  ; Was 'n' pressed?
  JR NZ,ASKNAMES_0        ; Jump back to collect another keypress if not
  POP HL                  ; Drop the return address (CHGNAMES)
  RET                     ; Return to AFTERNAMES

; Unused
  DEFB 0

  ORG 64108

; 'Do you want to put in your own names Y/N?'
;
; Used by the routine at ASKNAMES.
M_OWNNAMES:
  DEFM "Do you want to put in your own names Y/N?"
  DEFB 0                  ; End marker

; 'You have mumps {ERIC}. Go home at once. This game is over{8 spaces}'
;
; Used by the routine at GOHOMEMSG.
M_HAVEMUMPS:
  DEFM "You have mumps "
  DEFB 16                 ; M_ERIC: 'ERIC'
  DEFM ". Go home at once. "
  DEFM "This game is over"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; Unused
  DEFS 16

; Command list 222: Mumps duty
;
; Used only by MR ROCKITT in lesson 243. The last two commands in this list
; (find ERIC and send him home) are also used by MR WACKER when ERIC has
; accumulated 10000 or more lines (see CHKLINES).
CLIST222:
  DEFW GOTO               ; Go to...
  DEFB 162,9              ; ...the staff room
  DEFW MVTILL             ; Move about until...
  DEFB 29                 ; ...ERIC has mumps
  DEFW FINDERIC           ; Find ERIC
  DEFW SENDHOME           ; Tell ERIC to go home and end the game

; Command list 208: Tell ERIC about EINSTEIN or BOY WANDER
;
; Used only by little boy no. 10 in lessons 244 and 245.
CLIST208:
  DEFW GOTO               ; Go to...
  DEFB 169,94             ; ...the gym
  DEFW MVTILL             ; Move about until...
  DEFB 26                 ; ...EINSTEIN or BOY WANDER is ready
  DEFW MVTILL             ; Move about until...
  DEFB 27                 ; ...MR WACKER is ready
; This command list continues at CLIST210.

; Command list 210: Tell ERIC about ANGELFACE
;
; Used only by little boy no. 10 in lesson 243. Command list 208 also continues
; here.
CLIST210:
  DEFW FINDERIC           ; Find ERIC
  DEFW TELLERIC           ; Tell ERIC about EINSTEIN or BOY WANDER (if coming
                          ; from command list 208) or ANGELFACE
  DEFW SIGRAISE           ; Signal that...
  DEFB 28                 ; ... ERIC has been told about EINSTEIN, BOY WANDER,
                          ; or ANGELFACE
  DEFW GOTORAND           ; Go to a random location
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFS 3

; Command list 220: Mumps walkabout
;
; Used only by ANGELFACE in lesson 243.
CLIST220:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
CLIST220A:
  DEFW CHKTOUCH           ; ...check whether he's touching ERIC
  DEFW GOTORAND           ; Go to a random location
  DEFW RST220             ; Restart this command list

; Unused
  DEFS 2

; Command list 212: Grass on ERIC
;
; Used only by EINSTEIN in lesson 244.
CLIST212:
  DEFW GOTO               ; Go to...
  DEFB 155,94             ; ...the far end of the fire escape
  DEFW SIGRAISE           ; Signal that...
  DEFB 26                 ; ...EINSTEIN is ready
  DEFW MVTILL             ; Move about until...
  DEFB 28                 ; ...ERIC has been told about EINSTEIN
  DEFW GOTO               ; Go to...
  DEFB 155,6              ; ...just inside the Head's study
  DEFW SIGRAISE           ; Signal that...
  DEFB 29                 ; ...EINSTEIN has grassed on ERIC
  DEFW GOTORAND           ; Go to a random location
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...playtime is over

; Unused
  DEFB 0

; Command list 214: Wait for EINSTEIN to grass on ERIC
;
; Used only by MR WACKER in lesson 244.
CLIST214:
  DEFW GOTO               ; Go to...
  DEFB 155,8              ; ...the doorway of the Head's study
  DEFW SIGRAISE           ; Signal that...
  DEFB 27                 ; ...MR WACKER is ready
  DEFW MVTILL             ; Move about until...
  DEFB 29                 ; ...EINSTEIN has grassed on ERIC
  DEFW ADDR2CBUF          ; Put the next address in MR WACKER's buffer, making
                          ; him...
  DEFW RUN                ; ...walk fast
  DEFW FINDERIC           ; Find ERIC
  DEFW LINES2K            ; Give ERIC 2000 lines
  DEFW GOTO               ; Go to...
  DEFB 155,8              ; ...the doorway of the Head's study
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...playtime is over

; Unused
  DEFB 0

; Command list 216: Collect the pea-shooter
;
; Used only by BOY WANDER in lesson 245.
CLIST216:
  DEFW GOTO               ; Go to...
  DEFB 155,24             ; ...the left end of the Revision Library
  DEFW SIGRAISE           ; Signal that...
  DEFB 26                 ; ...BOY WANDER is ready
  DEFW MVTILL             ; Move about until...
  DEFB 28                 ; ...ERIC has been told about BOY WANDER
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 155,94             ; ...the far end of the fire escape
  DEFW SIGLOWER           ; Signal that...
  DEFB 26                 ; ...BOY WANDER has collected the pea-shooter
  DEFW GOTORAND           ; Go to a random location
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...playtime is over

; Unused
  DEFB 0

; Command list 218: Look for the pea-shooter
;
; Used only by MR WACKER in lesson 245.
CLIST218:
  DEFW GOTO               ; Go to...
  DEFB 155,42             ; ...just inside the Reading Room
  DEFW SIGRAISE           ; Signal that...
  DEFB 27                 ; ...MR WACKER is ready
  DEFW MVTILL             ; Move about until...
  DEFB 28                 ; ...ERIC has been told about BOY WANDER
  DEFW ADDR2CBUF          ; Put the next address in MR WACKER's buffer, making
                          ; him...
  DEFW RUN                ; ...walk fast
  DEFW GOTO               ; Go to...
  DEFB 155,94             ; ...the far end of the fire escape
  DEFW MVTILL             ; Move about until the bell rings unless...
  DEFB 26                 ; ...BOY WANDER has not collected the pea-shooter
  DEFW ADDR2CBUF          ; Put the next address in MR WACKER's buffer, making
                          ; him...
  DEFW RUN                ; ...walk fast
  DEFW FINDERIC           ; Find ERIC
  DEFW LINES2K            ; Give ERIC 2000 lines
  DEFW GOTO               ; Go to...
  DEFB 155,8              ; ...the doorway of the Head's study
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...playtime is over

; Unused
  DEFS 7

; Command list 128: Map Room - teacher
;
; Used by MR WACKER in lessons 237 and 239, and by MR WITHIT in lessons 229,
; 231, 232, 233, 234, 235, 236, 238, 240 and 242.
CLIST128:
  DEFW GOTO               ; Go to...
  DEFB 155,90             ; ...the fire escape
  DEFW GOTO               ; Go to...
  DEFB 155,74             ; ...the doorway of the Map Room
  DEFW CHKTIME            ; Restart the command list unless it's time to start
                          ; the lesson
  DEFW SIGRAISE           ; Signal that...
  DEFB 8                  ; ...the teacher has arrived at the Map Room
  DEFW TELLSIT            ; Tell the kids to sit down
  DEFW GOTO               ; Go to...
  DEFB 155,60             ; ...the map in the Map Room
  DEFW GOTO               ; Go to...
  DEFB 155,62             ; ...the chair nearest the map in the Map Room
  DEFW DOCLASS            ; Conduct the class

; Unused
  DEFB 0

; Command list 130: Reading Room - teacher
;
; Used by MR WACKER in lessons 228, 232 and 235, by MR ROCKITT in lessons 233,
; 239 and 240, by MR WITHIT in lesson 226, and by MR CREAK in lessons 229, 230,
; 231, 234, 236, 237, 238 and 242.
CLIST130:
  DEFW GOTO               ; Go to...
  DEFB 155,30             ; ...the middle of the Revision Library
  DEFW GOTO               ; Go to...
  DEFB 155,40             ; ...the doorway of the Reading Room
  DEFW CHKTIME            ; Restart the command list unless it's time to start
                          ; the lesson
  DEFW SIGRAISE           ; Signal that...
  DEFB 9                  ; ...the teacher has arrived at the Reading Room
  DEFW TELLSIT            ; Tell the kids to sit down
  DEFW GOTO               ; Go to...
  DEFB 155,52             ; ...the place in the Reading Room where teachers go
                          ; before turning round
  DEFW GOTO               ; Go to...
  DEFB 155,50             ; ...the place in the Reading Room where teachers
                          ; wait for EINSTEIN to grass
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Unused
  DEFB 0

; Command list 132: Exam Room - teacher
;
; Used by MR WACKER in lessons 226, 231, 233, 238 and 241, by MR ROCKITT in
; lessons 227, 228, 232, 234, 235, 236 and 237, and by MR WITHIT in lessons 230
; and 239.
CLIST132:
  DEFW GOTO               ; Go to...
  DEFB 155,90             ; ...the fire escape
  DEFW GOTO               ; Go to...
  DEFB 162,70             ; ...the doorway of the Exam Room
  DEFW CHKTIME            ; Restart the command list unless it's time to start
                          ; the lesson
  DEFW SIGRAISE           ; Signal that...
  DEFB 10                 ; ...the teacher has arrived at the Exam Room
  DEFW TELLSIT            ; Tell the kids to sit down
  DEFW GOTO               ; Go to...
  DEFB 162,49             ; ...the place in the Exam Room where teachers wait
                          ; for EINSTEIN to grass
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Unused
  DEFB 0

; Command list 134: White Room - teacher
;
; Used by MR WACKER in lessons 234 and 236, by MR ROCKITT in lessons 230, 231,
; 238, 241 and 242, by MR WITHIT in lesson 237, and by MR CREAK in lessons 228,
; 232, 233, 235, 239 and 240.
CLIST134:
  DEFW GOTO               ; Go to...
  DEFB 162,8              ; ...the staff room
  DEFW GOTO               ; Go to...
  DEFB 162,22             ; ...the doorway of the White Room
  DEFW CHKTIME            ; Restart the command list unless it's time to start
                          ; the lesson
  DEFW SIGRAISE           ; Signal that...
  DEFB 11                 ; ...the teacher has arrived at the White Room
  DEFW TELLSIT            ; Tell the kids to sit down
  DEFW GOTO               ; Go to...
  DEFB 162,35             ; ...the spot just past the right end of the board in
                          ; the White Room
  DEFW GOTO               ; Go to...
  DEFB 162,34             ; ...the right end of the board in the White Room
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Unused
  DEFB 0

; Command list 136: Map Room - little boy
;
; Used by the little boys in various lessons.
CLIST136:
  DEFW GOTO               ; Go to...
  DEFB 155,67             ; ...the Map Room
  DEFW MVTILL             ; Move about until...
  DEFB 8                  ; ...the teacher arrives at the Map Room
  DEFW FINDSEAT           ; Find a seat and sit down
; This command list continues at CLIST138.

; Command list 138: Do nothing
;
; This command list is used by BOY WANDER's and ERIC's catapult pellets; it
; ensures that the pellets do nothing when they are not airborne. Command list
; 136 also continues here.
CLIST138:
  DEFW DONOWT             ; Do nothing

; Unused
  DEFB 0

; Command list 140: Map Room - BOY WANDER
;
; Used by BOY WANDER in lessons 229 and 240.
CLIST140:
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 155,65             ; ...the Map Room
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 8                  ; ...the teacher arrives at the Map Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 142: Map Room - ANGELFACE
;
; Used by ANGELFACE in lessons 231, 239 and 240.
CLIST142:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW GOTO               ; Go to...
  DEFB 155,68             ; ...the Map Room
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 8                  ; ...the teacher arrives at the Map Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 144: Map Room - EINSTEIN
;
; Used by EINSTEIN in lessons 231, 239 and 240.
CLIST144:
  DEFW GOTO               ; Go to...
  DEFB 155,67             ; ...the Map Room
  DEFW MVTILL             ; Move about until...
  DEFB 8                  ; ...the teacher arrives at the Map Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW GRASSETC           ; Grass and answer questions

; Unused
  DEFB 0

; Command list 146: Reading Room - little boy
;
; Used by the little boys in various lessons.
CLIST146:
  DEFW GOTO               ; Go to...
  DEFB 155,54             ; ...the Reading Room
  DEFW MVTILL             ; Move about until...
  DEFB 9                  ; ...the teacher arrives at the Reading Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 148: Reading Room - BOY WANDER
;
; Used by BOY WANDER in lessons 232, 233, 234 and 242.
CLIST148:
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 155,45             ; ...the Reading Room board
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 9                  ; ...the teacher has arrived at the Reading Room
  DEFW GOTO               ; Go to...
  DEFB 155,55             ; ...the Reading Room
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 9                  ; ...the teacher arrives at the Reading Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 150: Reading Room - ANGELFACE
;
; Used by ANGELFACE in lessons 233, 234 and 242.
CLIST150:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW GOTO               ; Go to...
  DEFB 155,53             ; ...the Reading Room
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 9                  ; ...the teacher arrives at the Reading Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 152: Reading Room - EINSTEIN
;
; Used by EINSTEIN in lessons 232, 233, 234 and 242.
CLIST152:
  DEFW GOTO               ; Go to...
  DEFB 155,55             ; ...the Reading Room
  DEFW MVTILL             ; Move about until...
  DEFB 9                  ; ...the teacher arrives at the Reading Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW GRASSETC           ; Grass and answer questions

; Unused
  DEFB 0

; Command list 154: Exam Room - little boy
;
; Used by the little boys in various lessons, and also by MR ROCKITT in lesson
; 229 (which is a bug).
CLIST154:
  DEFW GOTO               ; Go to...
  DEFB 162,61             ; ...the Exam Room
  DEFW MVTILL             ; Move about until...
  DEFB 10                 ; ...the teacher arrives at the Exam Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 156: Exam Room - BOY WANDER
;
; Used by BOY WANDER in lessons 226, 227, 231 and 239.
CLIST156:
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 162,45             ; ...the Exam Room board
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 10                 ; ...the teacher has arrived at the Exam Room
  DEFW GOTO               ; Go to...
  DEFB 162,60             ; ...the Exam Room
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 10                 ; ...the teacher arrives at the Exam Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 158: Exam Room - ANGELFACE
;
; Used by ANGELFACE in lessons 226 and 227.
CLIST158:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW GOTO               ; Go to...
  DEFB 162,59             ; ...the Exam Room
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 10                 ; ...the teacher arrives at the Exam Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 160: Exam Room - EINSTEIN
;
; Used by EINSTEIN in lessons 226 and 227.
CLIST160:
  DEFW GOTO               ; Go to...
  DEFB 162,58             ; ...the Exam Room
  DEFW MVTILL             ; Move about until...
  DEFB 10                 ; ...the teacher arrives at the Exam Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW GRASSETC           ; Grass and answer questions

; Unused
  DEFB 0

; Command list 162: White Room - little boy
;
; Used by the little boys in various lessons.
CLIST162:
  DEFW GOTO               ; Go to...
  DEFB 162,36             ; ...the White Room
  DEFW MVTILL             ; Move about until...
  DEFB 11                 ; ...the teacher arrives at the White Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 164: White Room - BOY WANDER
;
; Used by BOY WANDER in lessons 235, 238 and 241.
CLIST164:
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 162,30             ; ...the White Room board
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 11                 ; ...the teacher has arrived at the White Room
  DEFW GOTO               ; Go to...
  DEFB 162,35             ; ...the spot just past the right end of the board in
                          ; the White Room
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 11                 ; ...the teacher arrives at the White Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 166: White Room - ANGELFACE
;
; Used by ANGELFACE in lessons 228, 232, 235, 236 and 241.
CLIST166:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW GOTO               ; Go to...
  DEFB 162,36             ; ...the White Room
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 11                 ; ...the teacher arrives at the White Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 168: White Room - EINSTEIN
;
; Used by EINSTEIN in lessons 235, 236, 237, 238 and 241.
CLIST168:
  DEFW GOTO               ; Go to...
  DEFB 162,34             ; ...the right end of the board in the White Room
  DEFW MVTILL             ; Move about until...
  DEFB 11                 ; ...the teacher arrives at the White Room
  DEFW FINDSEAT           ; Find a seat and sit down
  DEFW GRASSETC           ; Grass and answer questions

; Unused
  DEFB 0

; Command list 170: Dinner - EINSTEIN/little boy
;
; Used by the little boys and EINSTEIN in lessons 224 and 225.
CLIST170:
  DEFW GOTO               ; Go to...
  DEFB 169,46             ; ...the dinner hall
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 172: Dinner - BOY WANDER
;
; Used by BOY WANDER in lessons 224 and 225.
CLIST172:
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 169,45             ; ...the dinner hall
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 174: Dinner - ANGELFACE
;
; Used by ANGELFACE in lessons 224 and 225.
CLIST174:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW GOTO               ; Go to...
  DEFB 169,45             ; ...the dinner hall
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 176: Revision Library - EINSTEIN/little boy
;
; Used by the little boys (except boys 3 and 6) in various lessons, and by
; EINSTEIN in lessons 228, 229 and 230.
CLIST176:
  DEFW GOTO               ; Go to...
  DEFB 155,36             ; ...the Revision Library
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 178: Revision Library - BOY WANDER
;
; Used by BOY WANDER in lessons 228, 230, 236 and 237.
CLIST178:
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 155,35             ; ...the Revision Library
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 180: Revision Library - ANGELFACE
;
; Used by ANGELFACE in lessons 229, 230, 237 and 238.
CLIST180:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW GOTO               ; Go to...
  DEFB 155,35             ; ...the Revision Library
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 182: Walkabout - ANGELFACE
;
; Used by ANGELFACE in lessons 246, 247, 248, 249, 250, 251, 252, 253, 254 and
; 255.
CLIST182:
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW GOTORAND           ; Go to a random location
  DEFW ADDR2BUF           ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW VIOLENT            ; ...hit now and then
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 10,0               ; ...10 times
  DEFW RESTART            ; Restart the command list

; Command list 184: Dinner duty
;
; Used by MR WACKER in lesson 225, and by MR WITHIT in lesson 224.
CLIST184:
  DEFW GOTO               ; Go to...
  DEFB 169,54             ; ...the table in the dinner hall
  DEFW GOTO               ; Go to...
  DEFB 169,42             ; ...the middle of the bench in the dinner hall
  DEFW CHKTIME            ; Restart the command list unless dinner has started
  DEFW DINDUTY            ; Perform dinner duty

; Command list 186: Head's study - MR WACKER
;
; Used by MR WACKER in lessons 224 and 247.
CLIST186:
  DEFW GOTO               ; Go to...
  DEFB 155,8              ; ...the doorway of the Head's study
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Command list 188: Staff room - teacher
;
; Used by MR WACKER in lesson 254, by MR ROCKITT in lessons 224, 225, 247, 249
; and 254, by MR WITHIT in lessons 247, 248, 250, 251, 254 and 255, and by MR
; CREAK in lessons 224, 225, 226, 227, 241, 246, 247, 248, 249, 250, 251, 254
; and 255.
CLIST188:
  DEFW GOTO               ; Go to...
  DEFB 162,9              ; ...the staff room
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Command list 190: Walkabout - teacher
;
; Used by MR WACKER in lessons 227, 229, 230, 240, 242, 243, 246, 248, 249,
; 250, 251, 252 and 255, by MR ROCKITT in lessons 244, 245 and 246, by MR
; WITHIT in lessons 227, 243, 244, 245 and 246, and by MR CREAK in lessons 243,
; 244 and 245.
CLIST190:
  DEFW GOTO               ; Go to...
  DEFB 155,3              ; ...the Head's study
  DEFW GOTORAND           ; Go to a random location
  DEFW RESTART            ; Restart the command list

; Unused
  DEFB 0

; Command list 192: Write on the boards - BOY WANDER
;
; Used by BOY WANDER in lessons 246, 248, 249, 250 and 254.
CLIST192:
  DEFW GOTO               ; Go to...
  DEFB 162,45             ; ...the Exam Room board
  DEFW BWWRITECLN         ; Write on the board (if it's clean)
  DEFW GOTO               ; Go to...
  DEFB 162,30             ; ...the White Room board
  DEFW BWWRITECLN         ; Write on the board (if it's clean)
  DEFW GOTO               ; Go to...
  DEFB 155,45             ; ...the Reading Room board
  DEFW BWWRITECLN         ; Write on the board (if it's clean)
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
; This command list continues at CLIST196.

; Command list 196: Walkabout
;
; Used by every character in various lessons. Command list 192 also continues
; here.
CLIST196:
  DEFW GOTORAND           ; Go to a random location
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 10,0               ; ...10 times
  DEFW RESTART            ; Restart the command list

; Unused
  DEFB 0

; Command list 198: Walk around the fire escape
;
; Used by the little boys (except boys 7, 10 and 11) in various lessons, and by
; MR WITHIT in lesson 225.
CLIST198:
  DEFW GOTO               ; Go to...
  DEFB 155,94             ; ...the far end of the fire escape
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 200: Walk around the gym
;
; Used by the little boys (except boys 1, 2, 8 and 11) in various lessons.
CLIST200:
  DEFW GOTO               ; Go to...
  DEFB 169,94             ; ...the gym
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 202: Walk around the big window
;
; Used by the little boys in various lessons, and by MR WITHIT in lesson 228.
CLIST202:
  DEFW GOTO               ; Go to...
  DEFB 169,8              ; ...the vicinity of the big window
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Command list 204: Stampede - leader
;
; Used only by little boy no. 1 in lessons 252 and 253.
CLIST204:
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 40,0               ; ...40 times
  DEFW SIGRAISE           ; Signal that...
  DEFB 16                 ; ...little boy no. 1 is ready
  DEFW RANDTRIP           ; Go somewhere and trip people up on the way
  DEFW SIGLOWER           ; Unsignal that...
  DEFB 16                 ; ...little boy no. 1 is ready
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 40,0               ; ...40 times
  DEFW SIGRAISE           ; Signal that...
  DEFB 17                 ; ...little boy no. 1 is ready again
  DEFW RANDTRIP           ; Go somewhere and trip people up on the way
  DEFW SIGLOWER           ; Unsignal that...
  DEFB 17                 ; ...little boy no. 1 is ready again
  DEFW RESTART            ; Restart the command list

; Unused
  DEFS 2

; Command list 206: Stampede - follower
;
; Used by little boys 2-11 in lessons 252 and 253.
CLIST206:
  DEFW MVTILL             ; Move about until...
  DEFB 16                 ; ...little boy no. 1 is ready
  DEFW FOLLOW             ; Find and follow little boy no. 1 and trip people up
                          ; on the way
  DEFW MVTILL             ; Move about until...
  DEFB 17                 ; ...little boy no. 1 is ready again
  DEFW FOLLOW             ; Find and follow little boy no. 1 and trip people up
                          ; on the way
  DEFW RESTART            ; Restart the command list

; Unused
  DEFB 0

; Command list 194: Walkabout - BOY WANDER
;
; Used by BOY WANDER in lessons 247, 251, 252, 253 and 255.
CLIST194:
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTORAND           ; Go to a random location
  DEFW ADDR2BUF           ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 10,0               ; ...10 times
  DEFW RESTART            ; Restart the command list

; Unused
  DEFS 20

; Main timetable
;
; Used by the routine at GETLESSON.
TIMETABLE:
  DEFB 246                ; 0: PLAYTIME (this is an initial playtime)
  DEFB 226                ; 1: MR WACKER - EXAM ROOM
  DEFB 231                ; 2: MR WITHIT - MAP ROOM
  DEFB 247                ; 3: PLAYTIME
  DEFB 238                ; 4: MR ROCKITT - WHITE ROOM
  DEFB 234                ; 5: MR CREAK - READING ROOM
  DEFB 248                ; 6: PLAYTIME
  DEFB 224                ; 7: DINNER (MR WITHIT)
  DEFB 252                ; 8: PLAYTIME (little boys stampede)
  DEFB 254                ; 9: PLAYTIME
  DEFB 237                ; 10: MR WITHIT - WHITE ROOM
  DEFB 228                ; 11: REVISION LIBRARY
  DEFB 249                ; 12: PLAYTIME
  DEFB 241                ; 13: MR ROCKITT - WHITE ROOM
  DEFB 250                ; 14: PLAYTIME
  DEFB 236                ; 15: MR WACKER - WHITE ROOM
  DEFB 251                ; 16: PLAYTIME (this is an initial playtime)
  DEFB 232                ; 17: MR WACKER - READING ROOM
  DEFB 235                ; 18: MR CREAK - WHITE ROOM
  DEFB 248                ; 19: PLAYTIME
  DEFB 255                ; 20: PLAYTIME
  DEFB 233                ; 21: MR ROCKITT - READING ROOM
  DEFB 239                ; 22: MR WACKER - MAP ROOM
  DEFB 250                ; 23: PLAYTIME
  DEFB 225                ; 24: DINNER (MR WACKER)
  DEFB 253                ; 25: PLAYTIME (little boys stampede)
  DEFB 242                ; 26: MR CREAK - READING ROOM
  DEFB 227                ; 27: MR ROCKITT - EXAM ROOM
  DEFB 229                ; 28: REVISION LIBRARY
  DEFB 246                ; 29: PLAYTIME
  DEFB 237                ; 30: MR WITHIT - WHITE ROOM
  DEFB 233                ; 31: MR ROCKITT - READING ROOM
  DEFB 249                ; 32: PLAYTIME (this is an initial playtime)
  DEFB 240                ; 33: MR WITHIT - MAP ROOM
  DEFB 230                ; 34: REVISION LIBRARY
  DEFB 235                ; 35: MR CREAK - WHITE ROOM
  DEFB 251                ; 36: PLAYTIME
  DEFB 234                ; 37: MR CREAK - READING ROOM
  DEFB 241                ; 38: MR ROCKITT - WHITE ROOM
  DEFB 247                ; 39: PLAYTIME
  DEFB 224                ; 40: DINNER (MR WITHIT)
  DEFB 246                ; 41: PLAYTIME
  DEFB 255                ; 42: PLAYTIME
  DEFB 238                ; 43: MR ROCKITT - WHITE ROOM
  DEFB 226                ; 44: MR WACKER - EXAM ROOM
  DEFB 250                ; 45: PLAYTIME
  DEFB 229                ; 46: REVISION LIBRARY
  DEFB 231                ; 47: MR WITHIT - MAP ROOM
  DEFB 248                ; 48: PLAYTIME (this is an initial playtime)
  DEFB 231                ; 49: MR WITHIT - MAP ROOM
  DEFB 232                ; 50: MR WACKER - READING ROOM
  DEFB 250                ; 51: PLAYTIME
  DEFB 254                ; 52: PLAYTIME
  DEFB 241                ; 53: MR ROCKITT - WHITE ROOM
  DEFB 242                ; 54: MR CREAK - READING ROOM
  DEFB 247                ; 55: PLAYTIME
  DEFB 225                ; 56: DINNER (MR WACKER)
  DEFB 252                ; 57: PLAYTIME (little boys stampede)
  DEFB 254                ; 58: PLAYTIME
  DEFB 226                ; 59: MR WACKER - EXAM ROOM
  DEFB 230                ; 60: REVISION LIBRARY
  DEFB 249                ; 61: PLAYTIME
  DEFB 237                ; 62: MR WITHIT - WHITE ROOM
  DEFB 239                ; 63: MR WACKER - MAP ROOM

; Make a walking sound effect (yellow border)
;
; Used by the routines at WALKONOFF and HITSOUND.
WALKSOUND:
  PUSH BC
  LD A,6                  ; The border will be yellow
; This entry point is used by the routine at WALKSOUND2 with A=1 (blue border).
WALKSOUND_0:
  LD C,8                  ; Make the sound effect
WALKSOUND_1:
  XOR 16                  ;
  OUT (254),A             ;
  LD B,192                ;
WALKSOUND_2:
  DJNZ WALKSOUND_2        ;
  DEC C                   ;
  JR NZ,WALKSOUND_1       ;
  POP BC
  RET

; Make a walking sound effect (blue border)
;
; Used by the routines at MVERIC2, MVERIC1, TURNERIC and JUMPSOUND.
WALKSOUND2:
  PUSH BC
  LD A,1                  ; The border will be blue
  JR WALKSOUND_0

; Make a knocked-out sound effect
;
; Used by the routine at ERICHIT. Makes a sound effect just after ERIC has been
; knocked over or pushed out of his seat.
;
; A 1 + knockout delay counter value (KODELAY)
SNDEFFECT:
  CP 39                   ; Return (to ERICDONE) unless ERIC has just been
  RET NZ                  ; knocked over
  LD HL,275               ; A=2 (initial border colour: red), H=1 (pitch
  LD DE,0                 ; adjustment), L=19 (border alternates red/blue), D=0
  LD A,2                  ; (duration), E=0 (initial pitch)
; This entry point is used by the routines at SHIELDSND (to make the 'hit a
; shield' sound effect) and FIRESOUND (to make the catapult sound effect) with
; the following parameters prepared:
;
; +---+-----------------------+
; | A | Initial border colour |
; | D | Duration              |
; | E | Initial pitch         |
; | H | Pitch adjustment      |
; | L | 16 + border XOR value |
; +---+-----------------------+
SNDEFFECT_0:
  XOR L                   ; Flip the border colour and the state of the speaker
  OUT (254),A             ;
  LD B,E                  ; Pitch delay
SNDEFFECT_1:
  DJNZ SNDEFFECT_1        ;
  LD B,A                  ; Save A temporarily
  LD A,E                  ; Adjust the pitch
  ADD A,H                 ;
  LD E,A                  ;
  LD A,B                  ; Restore A
  DEC D                   ; Decrement the duration counter
  JR NZ,SNDEFFECT_0       ; Jump back unless the sound effect is finished
  LD A,1                  ; Make the border blue before returning
  OUT (254),A             ;
  RET

; Make a catapult sound effect
;
; Used by the routine at FIRING.
FIRESOUND:
  LD HL,63506             ; A=4 (initial border colour: green), H=248 (pitch
  LD DE,32768             ; adjustment), L=18 (border alternates green/yellow),
  LD A,4                  ; D=128 (duration), E=0 (initial pitch)
  JR SNDEFFECT_0          ; Make the sound effect with these parameters

; Unused
  DEFB 0

; Addresses of command lists
;
; Used by the routine at NEWLESSON.
CLISTADDR:
  DEFW CLIST128           ; 128: Map Room - teacher
  DEFW CLIST130           ; 130: Reading Room - teacher
  DEFW CLIST132           ; 132: Exam Room - teacher
  DEFW CLIST134           ; 134: White Room - teacher
  DEFW CLIST136           ; 136: Map Room - little boy
  DEFW CLIST138           ; 138: Do nothing
  DEFW CLIST140           ; 140: Map Room - BOY WANDER
  DEFW CLIST142           ; 142: Map Room - ANGELFACE
  DEFW CLIST144           ; 144: Map Room - EINSTEIN
  DEFW CLIST146           ; 146: Reading Room - little boy
  DEFW CLIST148           ; 148: Reading Room - BOY WANDER
  DEFW CLIST150           ; 150: Reading Room - ANGELFACE
  DEFW CLIST152           ; 152: Reading Room - EINSTEIN
  DEFW CLIST154           ; 154: Exam Room - little boy
  DEFW CLIST156           ; 156: Exam Room - BOY WANDER
  DEFW CLIST158           ; 158: Exam Room - ANGELFACE
  DEFW CLIST160           ; 160: Exam Room - EINSTEIN
  DEFW CLIST162           ; 162: White Room - little boy
  DEFW CLIST164           ; 164: White Room - BOY WANDER
  DEFW CLIST166           ; 166: White Room - ANGELFACE
  DEFW CLIST168           ; 168: White Room - EINSTEIN
  DEFW CLIST170           ; 170: Dinner - EINSTEIN/little boy
  DEFW CLIST172           ; 172: Dinner - BOY WANDER
  DEFW CLIST174           ; 174: Dinner - ANGELFACE
  DEFW CLIST176           ; 176: Revision Library - EINSTEIN/little boy
  DEFW CLIST178           ; 178: Revision Library - BOY WANDER
  DEFW CLIST180           ; 180: Revision Library - ANGELFACE
  DEFW CLIST182           ; 182: Walkabout - ANGELFACE
  DEFW CLIST184           ; 184: Dinner duty
  DEFW CLIST186           ; 186: Head's study - MR WACKER
  DEFW CLIST188           ; 188: Staff room - teacher
  DEFW CLIST190           ; 190: Walkabout - teacher
  DEFW CLIST192           ; 192: Write on the boards - BOY WANDER
  DEFW CLIST194           ; 194: Walkabout - BOY WANDER
  DEFW CLIST196           ; 196: Walkabout
  DEFW CLIST198           ; 198: Walk around the fire escape
  DEFW CLIST200           ; 200: Walk around the gym
  DEFW CLIST202           ; 202: Walk around the big window
  DEFW CLIST204           ; 204: Stampede - leader
  DEFW CLIST206           ; 206: Stampede - follower
  DEFW CLIST208           ; 208: Tell ERIC about EINSTEIN or BOY WANDER
  DEFW CLIST210           ; 210: Tell ERIC about ANGELFACE
  DEFW CLIST212           ; 212: Grass on ERIC
  DEFW CLIST214           ; 214: Wait for EINSTEIN to grass on ERIC
  DEFW CLIST216           ; 216: Collect the pea-shooter
  DEFW CLIST218           ; 218: Look for the pea-shooter
  DEFW CLIST220           ; 220: Mumps walkabout
  DEFW CLIST222           ; 222: Mumps duty

; 'WELL DONE! GO UP A YEAR'
;
; Used by the routine at NEXTPHASE.
M_WELLDONE:
  DEFM "WELL DONE! GO"
  DEFB 0                  ; End marker
  DEFS 2
M_WELLDONEL2:
  DEFM "UP A YEAR"
  DEFB 0                  ; End marker
  DEFS 6

; 'Don't let {ANGELFACE} touch you this playtime - He's got mumps{8 spaces}'
;
; Used by the routine at TELLERIC.
M_BULLYMUMPS:
  DEFM "Don't let "
  DEFB 18                 ; M_ANGELFACE: 'ANGELFACE'
  DEFM " touch you this playtime"
  DEFM " - He's got mumps"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{EINSTEIN} is going to tell {MR WACKER} what you're up to. You must stop him
; getting to his study this break{8 spaces}'
;
; Used by the routine at TELLERIC.
M_SWOTGRASS:
  DEFB 19                 ; M_EINSTEIN: 'EINSTEIN'
  DEFM " is going to tell "
  DEFB 20                 ; M_WACKER: 'MR WACKER'
  DEFM " what you're up to. "
  DEFM "You must stop him getting "
  DEFM "to his study this break"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; '{BOY WANDER} has hidden a pea-shooter with your name on it on the
; fire-escape. If {MR WACKER} gets there before {BOY WANDER} you'll get lines{8
; spaces}'
;
; Used by the routine at TELLERIC.
M_WANDERHIDDEN:
  DEFB 17                 ; M_BOYWANDER: 'BOY WANDER'
  DEFM " has hidden a pea-shooter "
  DEFM "with your name on it"
  DEFM " on the fire-escape."
  DEFM " If "
  DEFB 20                 ; M_WACKER: 'MR WACKER'
  DEFM " gets there before "
  DEFB 17                 ; M_BOYWANDER: 'BOY WANDER'
  DEFM " you'll get lines"
  DEFB 3                  ; M_SPACE8: '        ' (8 spaces)

; Unused
  DEFB 66,60,0

